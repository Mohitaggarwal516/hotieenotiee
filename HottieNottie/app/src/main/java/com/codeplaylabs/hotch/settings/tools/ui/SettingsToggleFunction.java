package com.codeplaylabs.hotch.settings.tools.ui;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.codeplaylabs.hotch.settings.tools.interfaces.SettingsToggleClickListener;
import com.codeplaylabs.hotch.R;


public class SettingsToggleFunction {


    public static final int PAGE_ZERO = 0;
    public static final int PAGE_ONE = 1;


    public static void toggleSwitch(final Activity context, int position,String leftText, String rightText, final SettingsToggleClickListener settingsToggleClickListener) {

        final TextView left = (TextView) context.findViewById(R.id.settings_profile_tab);
        final TextView right = (TextView) context.findViewById(R.id.settings_settings_tab);



        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsToggleClickListener.toggleClickedLeft(left,right/*PAGE_ZERO*/);
                left.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                left.setBackgroundResource(R.drawable.settings_toggle_left_selected_bg);
                right.setBackgroundResource(R.drawable.settings_toggle_right_unselected_bg);
                right.setTextColor(context.getResources().getColor(R.color.white));
            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsToggleClickListener.toggleClickedRight(left,right/*PAGE_ONE*/);
                left.setTextColor(context.getResources().getColor(R.color.white));
                left.setBackgroundResource(R.drawable.settings_toggle_left_unselected_bg);
                right.setBackgroundResource(R.drawable.settings_toggle_right_selected_bg);
                right.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            }
        });
        if (position == PAGE_ZERO) {
            left.setText(leftText);
            right.setText(rightText);
            left.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            left.setBackgroundResource(R.drawable.settings_toggle_left_selected_bg);
            right.setBackgroundResource(R.drawable.settings_toggle_right_unselected_bg);
            right.setTextColor(context.getResources().getColor(R.color.white));

        } else if (position == PAGE_ONE) {
            left.setText(leftText);
            right.setText(rightText);
            left.setTextColor(context.getResources().getColor(R.color.white));
            left.setBackgroundResource(R.drawable.settings_toggle_left_unselected_bg);
            right.setBackgroundResource(R.drawable.settings_toggle_right_selected_bg);
            right.setTextColor(context.getResources().getColor(R.color.colorPrimary));

        }


    }

}
