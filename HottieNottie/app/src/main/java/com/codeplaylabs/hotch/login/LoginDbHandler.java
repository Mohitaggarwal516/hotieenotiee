package com.codeplaylabs.hotch.login;

import com.codeplaylabs.hotch.database.databaseInitialization.DatabaseHelper;
import com.codeplaylabs.hotch.database.tables.BaseTable;
import com.codeplaylabs.hotch.database.tables.FbProfileTable;
import com.codeplaylabs.hotch.utils.Singleton;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by mohit on 05/08/17.
 */

public class LoginDbHandler {

    public enum dataBaseTables {fbProfileTable}

    public static boolean saveDataInTable(dataBaseTables tableName, BaseTable baseTable) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        switch (tableName) {
            case fbProfileTable: {

                try {
                    clearTable(dataBaseTables.fbProfileTable);
                    Dao<FbProfileTable, Integer> profileDao = databaseHelper.getFbProfileDao();
                    profileDao.createOrUpdate((FbProfileTable) baseTable);
                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            /*case filterTable: {
                try {
                    clearTable(ChatDBHandlerClass.dataBaseTables.filterTable);
                    Dao<FilterTable, Integer> profileDao = databaseHelper.getFilterDao();
                    profileDao.create((FilterTable) baseTable);
                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return false;
                }
            }*/
            default: {
                return false;
            }
        }

    }

    public static boolean clearTable(dataBaseTables tableName) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        switch (tableName) {
            /*case filterTable: {
                try {
                    Dao<FilterTable, Integer> filterDao = databaseHelper.getFilterDao();
                    filterDao.delete(filterDao.queryForAll());


                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            }*/
            case fbProfileTable: {
                try {
                    Dao<FbProfileTable, Integer> profileDao = databaseHelper.getFbProfileDao();
                    DeleteBuilder<FbProfileTable, Integer> builder = profileDao.deleteBuilder();
                    int build = builder.delete();
                    return build > 0;
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            }

        }
        return false;
    }

    public static FbProfileTable getFbProfileFromTable() {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<FbProfileTable, Integer> fbProfileDao = databaseHelper.getFbProfileDao();
            List<FbProfileTable> val = fbProfileDao.queryForAll();
            if (val != null && val.size() > 0) {
                return val.get(0);
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
