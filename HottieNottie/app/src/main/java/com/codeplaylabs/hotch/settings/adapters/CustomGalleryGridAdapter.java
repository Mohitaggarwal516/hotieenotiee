package com.codeplaylabs.hotch.settings.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.codeplaylabs.hotch.settings.interfaces.GalleryListListener;
import com.codeplaylabs.hotch.utils.CommonMethods;
import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.settings.models.albums.AlbumModel;
import com.codeplaylabs.hotch.utils.ServiceSingleton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by mohit on 30/06/17.
 */

public class CustomGalleryGridAdapter extends RecyclerView.Adapter<CustomGalleryGridAdapter.MyViewHolder> {
    private final GalleryListListener galleryListListener;
    private final ArrayList<AlbumModel> albumList;
    private final Activity activity;

    public CustomGalleryGridAdapter(Activity activity, ArrayList<AlbumModel> albumList, GalleryListListener galleryListListener) {
        this.activity = activity;
        this.albumList = albumList;
        this.galleryListListener = galleryListListener;
    }


    @Override
    public CustomGalleryGridAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.album_grid_row, parent, false);

        return new CustomGalleryGridAdapter.MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final CustomGalleryGridAdapter.MyViewHolder holder, final int position) {

        final AlbumModel albumModel = albumList.get(position);
            Picasso.with(activity).load(albumModel.getUrl()).placeholder(ServiceSingleton.getInstance().getSelfPlaceholder()).into(holder.albumImage);

        holder.albumImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryListListener.onItemClick(position,albumModel, CommonMethods.getBitmap(holder.albumImage));
            }
        });



    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private final ImageView albumImage;

        public MyViewHolder(View itemView) {
            super(itemView);

            albumImage = (ImageView)itemView.findViewById(R.id.grid_image);
        }


    }
}
