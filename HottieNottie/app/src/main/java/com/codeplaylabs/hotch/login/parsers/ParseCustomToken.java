package com.codeplaylabs.hotch.login.parsers;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.login.models.CustomToken;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONObject;

/**
 * Created by mohit on 01/08/17.
 */

public class ParseCustomToken extends BaseParser {
    BaseModel baseModel  = new BaseModel();
    @Override
    public BaseModel parseJson(JSONObject response) {
        if (validate(response) != null) {
            return moduleJson(response);
        } else {
            baseModel.error = Constants.Error.NULL_RESPONSE_ERROR;
            return baseModel;
        }
    }

    /*
      * This method is called to collect the data received from the servers in a class for the session
      * @param response
     */
    public CustomToken moduleJson(JSONObject response) {


        CustomJsonObject cjObject = new CustomJsonObject(response);
        CustomToken profile = new CustomToken();
        cjObject = cjObject.getCustomJsonObject("data");
        String s = cjObject.getString("authToken");
        if(s.length()  != 0){
            profile.setToken(s);
            return profile;
        }

        return profile;

    }
}
