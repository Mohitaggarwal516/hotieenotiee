package com.codeplaylabs.hotch.others.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseActivity;
import com.codeplaylabs.hotch.others.Adapters.TabsAdapter;
import com.codeplaylabs.hotch.others.interfaces.InformActivity;
import com.codeplaylabs.hotch.others.ui.frags.HeyHottieFrag;
import com.codeplaylabs.hotch.others.ui.frags.SuperHottieFrag;
import com.codeplaylabs.hotch.settings.tools.interfaces.SettingsToggleClickListener;
import com.codeplaylabs.hotch.settings.tools.ui.SettingsToggleFunction;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LogAnalytics;

public class OthersTabActivity extends BaseActivity {

    ViewPager viewPager;
    TabsAdapter adapter;
    TabLayout tabs;
    RelativeLayout loader;
    //TextView heyHotties,superHottie;
    private String LEFT_TAB_NAME = "SUPERHOT";
    private String RIGHT_TAB_NAME = "FAVOURITES";
    ImageView appIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_others);
        super.onCreate(savedInstanceState);
//        heyHotties= (TextView) findViewById(R.id.settings_profile_tab);
//        superHottie= (TextView) findViewById(R.id.settings_settings_tab);

        //tabs = (TabLayout) findViewById(R.id.others_tab);.
        appIcon = (ImageView) findViewById(R.id.view_profile);
        appIcon.setImageDrawable(getResources().getDrawable(R.drawable.logo));

        viewPager = (ViewPager) findViewById(R.id.others_viewpager);
        loader = (RelativeLayout) findViewById(R.id.loader_layout);
        adapter = new TabsAdapter(getSupportFragmentManager());
        adapter.addFragInfo(new HeyHottieFrag().setInformActivityListener(new InformActivity() {
            @Override
            public void showLoader(final boolean showLoader) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (showLoader) {
                            loader.setVisibility(View.VISIBLE);
                        } else {
                            loader.setVisibility(View.GONE);
                        }
                    }
                });

            }

            @Override
            public void showNetErrorDg() {
                //called by heyHottie when getSuperHotList Service fails
                showNoNetDialog();

            }
        }), "Hey hottie");
        adapter.addFragInfo(new SuperHottieFrag(), "SuperHottie");
        viewPager.setAdapter(adapter);
        callToggleSwitch(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 1) {
                   /* superHottie.setBackground(getResources().getDrawable(R.drawable.settings_toggle_right_selected_bg));
                    heyHotties.setBackground(getResources().getDrawable(R.drawable.settings_toggle_left_unselected_bg));
                    heyHotties.setTextColor(getResources().getColor(R.color.colorPrimary));
                    superHottie.setTextColor(getResources().getColor(R.color.white));*/
                    callToggleSwitch(1);
                } else {
                    callToggleSwitch(0);
                    /*superHottie.setBackground(getResources().getDrawable(R.drawable.settings_toggle_right_unselected_bg));
                    heyHotties.setBackground(getResources().getDrawable(R.drawable.settings_toggle_left_selected_bg));
                    superHottie.setTextColor(getResources().getColor(R.color.colorPrimary));
                    heyHotties.setTextColor(getResources().getColor(R.color.white));*/
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        /*tabs.setupWithViewPager(viewPager);
        tabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.white));
        tabs.setTabTextColors(getResources().getColor(R.color.white), getResources().getColor(R.color.white));
        changeTabsFont();*/
    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();
        Typeface typeface = Typeface.createFromAsset(getAssets(), "helvetica.ttf");
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(typeface);
                }
            }
        }
    }

    private void callToggleSwitch(final int position) {
        SettingsToggleFunction.toggleSwitch(this, position, LEFT_TAB_NAME, RIGHT_TAB_NAME, new SettingsToggleClickListener() {
            @Override
            public void toggleClickedLeft(TextView left, TextView right/*int position*/) {
                viewPager.setCurrentItem(SettingsToggleFunction.PAGE_ZERO, true);
                left.setText(LEFT_TAB_NAME);
                right.setText(RIGHT_TAB_NAME);
            }

            @Override
            public void toggleClickedRight(TextView left, TextView right) {
                viewPager.setCurrentItem(SettingsToggleFunction.PAGE_ONE, true);
                left.setText(LEFT_TAB_NAME);
                right.setText(RIGHT_TAB_NAME);
            }
        });
    }

    //baseActivity's method
    @Override
    protected void onTryAgainClicked() {
        //Toast.makeText(this, "tryAgain", Toast.LENGTH_SHORT).show();
        dialog.dismiss();
        dialog=null;
        startActivity(new Intent(OthersTabActivity.this, OthersTabActivity.class));
        finish();
        super.onTryAgainClicked();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogAnalytics.logScreenView(mTracker,Constants.ActivityName.OTHERS_SCREEN);
    }
}
