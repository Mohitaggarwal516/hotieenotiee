package com.codeplaylabs.hotch.settings.handler;

import com.codeplaylabs.hotch.utils.Singleton;
import com.codeplaylabs.hotch.database.databaseInitialization.DatabaseHelper;
import com.codeplaylabs.hotch.database.tables.BaseTable;
import com.codeplaylabs.hotch.database.tables.UserImageTable;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mohit on 06/07/17.
 */

public class SettingsDbHandler {

    public enum dataBaseTables {UserImageTable}

    public static boolean saveDataInTable(dataBaseTables tableName, BaseTable baseTable) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        switch (tableName) {
            case UserImageTable: {

                try {
                    Dao<UserImageTable, Integer> userImagesDao = databaseHelper.getUserImagesDao();
                    userImagesDao.createOrUpdate((UserImageTable) baseTable);
                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            default: {
                return false;
            }
        }

    }

    public static boolean clearUserImageTable() {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<UserImageTable, Integer> userImagesDao = databaseHelper.getUserImagesDao();
            userImagesDao.delete(userImagesDao.queryForAll());
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String fetchMyDpFromUserImageTable() {
        DatabaseHelper helper = Singleton.getInstance().getHelper();
        try {
            Dao<UserImageTable, Integer> imageDao = helper.getUserImagesDao();
            QueryBuilder<UserImageTable, Integer> qb = imageDao.queryBuilder();

            qb.where().eq("order", 0);
            List<UserImageTable> list = imageDao.query(qb.prepare());
            if (list != null && list.size() > 0)
                return list.get(0).getImageUrl();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean DeleteDataFromTable(dataBaseTables tableName, BaseTable baseTable) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        switch (tableName) {
            case UserImageTable: {

                try {
                    Dao<UserImageTable, Integer> userImagesDao = databaseHelper.getUserImagesDao();
                    userImagesDao.delete((UserImageTable) baseTable);
                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            default: {
                return false;
            }
        }

    }

    public static List<UserImageTable> getAllDataFromImageTable() {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<UserImageTable, Integer> userImagesDao = databaseHelper.getUserImagesDao();
            QueryBuilder<UserImageTable, Integer> queryBuilder = userImagesDao.queryBuilder();
            List<UserImageTable> userImageTables = userImagesDao.query(queryBuilder.prepare());
            return userImageTables;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<UserImageTable> getParticularDataFromImageTable(HashMap<String, Object> params) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<UserImageTable, Integer> userImagesDao = databaseHelper.getUserImagesDao();
            return userImagesDao.queryForFieldValues(params);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<UserImageTable> getParticularDataFromImageTableForOrderKey(String order) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            HashMap<String, Object> params = new HashMap<>();
            params.put("order", order);
            Dao<UserImageTable, Integer> userImagesDao = databaseHelper.getUserImagesDao();
            return userImagesDao.queryForFieldValues(params);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<UserImageTable> query(String orderByColumnName, String whereColumnName, String whereValue) {
        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();

        try {
            Dao<UserImageTable, Integer> userImagesDao = databaseHelper.getUserImagesDao();
            QueryBuilder<UserImageTable, Integer> qb = userImagesDao.queryBuilder();
            if (whereColumnName != null && whereValue != null) {
                qb.where().eq(whereColumnName, whereValue);
            }
            qb.orderBy(orderByColumnName, true);
            return userImagesDao.query(qb.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }


    }
}
