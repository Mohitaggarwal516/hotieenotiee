package com.codeplaylabs.hotch.chats;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codeplaylabs.hotch.chats.adapters.ChatAdapter;
import com.codeplaylabs.hotch.chats.models.firbaseModels.FriendlyMessage;
import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.customUi.CircleImageView;
import com.codeplaylabs.hotch.utils.CommonMethods;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.appindexing.Action;
import com.google.firebase.appindexing.FirebaseAppIndex;
import com.google.firebase.appindexing.FirebaseUserActions;
import com.google.firebase.appindexing.Indexable;
import com.google.firebase.appindexing.builders.Indexables;
import com.google.firebase.appindexing.builders.PersonBuilder;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import github.ankushsachdeva.emojicon.EmojiconTextView;

/**
 * Created by mohit on 02/08/17.
 */

public class ChatFirebaseAdapter extends FirebaseRecyclerAdapter<FriendlyMessage,
        ChatFirebaseAdapter.MessageViewHolder> {
    private final Query ref;
    private final Activity activity;
    private final int MY_LOCATION;
    private final String mUsername;
    private final String mOtheruserName;
    private static final String MESSAGE_URL = "http://friendlychat.firebase.google.com/message/";
    private final String myImage;
    private final String otherImage;
    private final ChatAdapter.ChatListener chatListener;


    /**
     * @param ref             The Firebase location to watch for data changes. Can also be a slice of a location,
     *                        using some combination of {@code limit()}, {@code startAt()}, and {@code endAt()}.
     */
    public ChatFirebaseAdapter(Activity context, int myLocation,String myUsername,String myImage,String otherImage, String otherUserName , Query ref, ChatAdapter.ChatListener chatListener) {
        super(FriendlyMessage.class, R.layout.item_message, MessageViewHolder.class, ref);

        this.ref = ref;
        this.activity = context;
        this.MY_LOCATION = myLocation;
        this.mUsername = myUsername;
        this.mOtheruserName = otherUserName;
        this.myImage = myImage;
        this.otherImage = otherImage;
        this.chatListener = chatListener;

    }

    @Override
    protected FriendlyMessage parseSnapshot(DataSnapshot snapshot) {
        FriendlyMessage friendlyMessage = super.parseSnapshot(snapshot);
        if (friendlyMessage != null) {
            friendlyMessage.setId(snapshot.getKey());
        }
        return friendlyMessage;
    }

    @Override
    protected void populateViewHolder(ChatFirebaseAdapter.MessageViewHolder viewHolder, FriendlyMessage friendlyMessage, int position) {
        if(friendlyMessage.isSelf() == MY_LOCATION){

            viewHolder.messengerImageView.setVisibility(View.GONE);
            viewHolder.selfMessengerImageView.setVisibility(View.GONE);
            if(myImage == null || myImage.trim().length() == 0) {
                Picasso.with(activity).load(R.drawable.ic_account_circle_black_36dp)
                        .into(viewHolder.selfMessengerImageView);
            }else{
                Picasso.with(activity).load(myImage)
                        .placeholder(R.drawable.ic_account_circle_black_36dp)
                        .into(viewHolder.selfMessengerImageView);
            }
//            viewHolder.selfMessageTextView.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
//            viewHolder.selfMessageTextView.setBackgroundResource(R.drawable.chat_mymsg_background);
            viewHolder.otherFrame.setVisibility(View.GONE);
            viewHolder.myFrame.setVisibility(View.VISIBLE);
            viewHolder.timeLeftSide.setText(CommonMethods.getChatDateAndTime(CommonMethods.GMTToLocal(friendlyMessage.getTimeStamp())));

        }else{

            viewHolder.messengerImageView.setVisibility(View.VISIBLE);
            viewHolder.selfMessengerImageView.setVisibility(View.GONE);
            if(otherImage == null || otherImage.trim().length() == 0) {
                Picasso.with(activity)
                        .load(R.drawable.ic_account_circle_black_36dp)
                        .into(viewHolder.messengerImageView);
            }else{
                Picasso.with(activity)
                        .load(otherImage)
                        .placeholder(R.drawable.ic_account_circle_black_36dp)
                        .into(viewHolder.messengerImageView);
            }
//            viewHolder.messageTextView.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
//            viewHolder.messageTextView.setBackgroundResource(R.drawable.chat_othermsg_background);
            viewHolder.otherFrame.setVisibility(View.VISIBLE);
            viewHolder.myFrame.setVisibility(View.GONE);
            viewHolder.timeRightSide.setText(CommonMethods.getChatDateAndTime(CommonMethods.GMTToLocal(friendlyMessage.getTimeStamp())));
        }
        if (friendlyMessage.getText() != null) {
            FirebaseAppIndex.getInstance()
                    .update(getMessageIndexable(friendlyMessage));
            FirebaseAppIndex.getInstance().update(getMessageIndexable(friendlyMessage));

// log a view action on it
            FirebaseUserActions.getInstance().end(getMessageViewAction(friendlyMessage));

            if(friendlyMessage.isSelf() == MY_LOCATION){
                viewHolder.selfMessageTextView.setText(friendlyMessage.getText());
                viewHolder.selfMessageTextView.setVisibility(View.VISIBLE);
                viewHolder.messageTextView.setVisibility(View.GONE);
            }else {
                viewHolder.messageTextView.setText(friendlyMessage.getText());
                viewHolder.messageTextView.setVisibility(TextView.VISIBLE);
                viewHolder.selfMessageTextView.setVisibility(View.GONE);
            }
            viewHolder.messageImageView.setVisibility(ImageView.GONE);
        }
        String name = "";
        if (friendlyMessage.isSelf() == MY_LOCATION) {
            name = mUsername;
        } else {
            name = mOtheruserName;
        }

        viewHolder.messengerTextView.setText(name);

    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {

        private final TextView timeLeftSide, timeRightSide;
        EmojiconTextView selfMessageTextView;
        CircleImageView selfMessengerImageView;
        LinearLayout linLay,myFrame,otherFrame;
        EmojiconTextView messageTextView;
        ImageView messageImageView;
        TextView messengerTextView;
        CircleImageView messengerImageView;

        public MessageViewHolder(View v) {
            super(v);
            messageTextView = (EmojiconTextView) itemView.findViewById(R.id.messageTextView);
            selfMessageTextView = (EmojiconTextView) itemView.findViewById(R.id.sendermessageTextView);
            messageImageView = (ImageView) itemView.findViewById(R.id.messageImageView);
            messengerTextView = (TextView) itemView.findViewById(R.id.messengerTextView);
            messengerImageView = (CircleImageView) itemView.findViewById(R.id.messengerImageView);
            linLay = (LinearLayout) itemView.findViewById(R.id.zxc);
            selfMessengerImageView = (CircleImageView) itemView.findViewById(R.id.selfMessengerImageView);
            myFrame = (LinearLayout) itemView.findViewById(R.id.frame_me);
            otherFrame = (LinearLayout) itemView.findViewById(R.id.frame);
            timeLeftSide = (TextView) itemView.findViewById(R.id.time_left_side);
            timeRightSide = (TextView) itemView.findViewById(R.id.time_right_side);


        }
    }

    private Indexable getMessageIndexable(FriendlyMessage friendlyMessage) {
        PersonBuilder sender = Indexables.personBuilder()
                .setIsSelf(true)
                .setName(mUsername)
                .setUrl(MESSAGE_URL.concat(friendlyMessage.getId() + "/sender"));

        PersonBuilder recipient = Indexables.personBuilder()
                .setName(mUsername)
                .setUrl(MESSAGE_URL.concat(friendlyMessage.getId() + "/recipient"));

        Indexable messageToIndex = Indexables.messageBuilder()
                .setName(friendlyMessage.getText())
                .setUrl(MESSAGE_URL.concat(friendlyMessage.getId()))
                .setSender(sender)
                .setRecipient(recipient)
                .build();

        return messageToIndex;
    }

    private Action getMessageViewAction(FriendlyMessage friendlyMessage) {
        String name = "";
        if(friendlyMessage.isSelf() == MY_LOCATION){
            name = mUsername;
        }else{
            name = mOtheruserName;
        }
        return new Action.Builder(Action.Builder.VIEW_ACTION)
                .setObject(name, MESSAGE_URL.concat(friendlyMessage.getId()))
                .setMetadata(new Action.Metadata.Builder().setUpload(false))
                .build();
    }



}
