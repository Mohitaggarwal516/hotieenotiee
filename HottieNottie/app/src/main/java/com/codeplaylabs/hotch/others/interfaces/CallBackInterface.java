package com.codeplaylabs.hotch.others.interfaces;

import android.widget.ImageView;

import com.codeplaylabs.hotch.database.tables.SuperLikedTable;

import java.util.List;

/**
 * Created by HP on 21-Aug-17.
 */

public interface CallBackInterface {
    public void onCellClicked(String id,boolean isMatch);
    public void onCellLongPressed(List<SuperLikedTable> spLikedList/*String id, ImageView view*/);
    public void onCrossClicked(SuperLikedTable record, ImageView view);
}
