package com.codeplaylabs.hotch.settings.parsers;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.login.models.Detail;
import com.codeplaylabs.hotch.login.models.Education;
import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.login.models.UserProfile;
import com.codeplaylabs.hotch.settings.handler.SettingsDbHandler;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.database.tables.UserImageTable;
import com.codeplaylabs.hotch.login.models.Work;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mohit on 27/06/17.
 */

public class ParseProfileById extends BaseParser {
    BaseModel baseModel = new BaseModel();

    @Override
    public BaseModel parseJson(JSONObject response) {
        if (validate(response) != null) {
            return moduleJson(response);
        } else {
            baseModel.error = Constants.Error.SettingsError.PROFILE_SETTING_ERROR;
            return baseModel;
        }
    }

    /*
      * This method is called to collect the data received from the servers in a class for the session
      * @param response
     */
    public BaseModel moduleJson(JSONObject response) {

        UserProfile userProfile = new UserProfile();
        try {

            if (response != null) {
                CustomJsonObject cjObject = new CustomJsonObject(response);
                cjObject = cjObject.getCustomJsonObject("data");
                JSONArray users = cjObject.getJsonArray("users");
                if (users.length() > 0) {
                    CustomJsonObject obj = new CustomJsonObject((JSONObject) users.get(0));
                    obj.getCustomJsonObject((JSONObject) users.get(0));
                    userProfile.setUserId(obj.getString("id"));
                    userProfile.setId(obj.getString("facebookId"));
                    userProfile.setName(obj.getString("name"));
                    userProfile.setEmail(obj.getString("email"));
                    userProfile.setGender(obj.getString("gender"));
                    userProfile.setBirthday(obj.getString("birthday"));
                    userProfile.setAbout(obj.getString("about"));

                    //setting

                    JSONArray educationArray = obj.getJsonArray("education");
                    for (int index = 0; index < educationArray.length(); index++) {
                        CustomJsonObject educationObject = new CustomJsonObject(educationArray.getJSONObject(index));
                        CustomJsonObject school = new CustomJsonObject(educationObject.getJsonObject("institute"));
                        Education education = new Education();
                        education.setFacebookId(educationObject.getString("facebookId"));
                        education.setType(educationObject.getString("type"));
                        Detail institute = new Detail();
                        institute.setId(school.getString("instituteBranchFacebookId"));
                        institute.setName(school.getString("name"));
                        education.setInstitute(institute);
                        education.setYear(educationObject.getString("year"));
                        education.setCurrentlyStudingHere(educationObject.getBoolean("isCurrentlyStudingHere", false));
                        userProfile.getEducationList().add(education);
                    }

                    JSONArray workArray = obj.getJsonArray("works");
                    for (int index = 0; index < workArray.length(); index++) {
                        CustomJsonObject workObject = new CustomJsonObject(workArray.getJSONObject(index));
                        CustomJsonObject employerJson = new CustomJsonObject(workObject.getJsonObject("employer"));

                        Work work = new Work();
                        work.setEndDate(workObject.getString("endDate"));
                        work.setFacebookId(workObject.getString("facebookId"));
                        work.setStartDate(workObject.getString("startDate"));
                       /* CustomJsonObject position = new CustomJsonObject(workObject.getJsonObject("position"));*/
                        work.setPosition(workObject.getString("position"));
                        work.setPositionFacebookId(workObject.getString("positionFacebookId"));
                        work.setIsCurrentlyWorkingHere(workObject.getBoolean("isCurrentlyWorkingHere",false));
                        Detail employer = new Detail();
                        employer.setId(employerJson.getString("facebookId"));
                        employer.setName(employerJson.getString("name"));
                        work.setEmployer(employer);

                        CustomJsonObject locationJson = new CustomJsonObject(workObject.getJsonObject("location"));
                        Detail location = new Detail();
                        location.setId(locationJson.getString("facebookId"));
                        location.setName(locationJson.getString("name"));
                        location.setCountry(locationJson.getString("country"));
                        work.setLocation(location);

                        userProfile.getWorkList().add(work);
                    }
                    CustomJsonObject locationJson = new CustomJsonObject(obj.getJsonObject("currentLocation"));
                    Detail location = new Detail();
                    location.setId(locationJson.getString("facebookId"));
                    location.setName(locationJson.getString("city"));
                    location.setCountry(locationJson.getString("country"));
                    userProfile.setLocation(location);

                    CustomJsonObject hometownJson = new CustomJsonObject(obj.getJsonObject("hometown"));
                    Detail hometown = new Detail();
                    hometown.setId(hometownJson.getString("facebookId"));
                    hometown.setName(hometownJson.getString("city"));
                    hometown.setCountry(hometownJson.getString("country"));
                    userProfile.setHometown(hometown);

                    JSONArray imagesArray = obj.getJsonArray("images");
                    if(imagesArray != null && imagesArray.length() > 0){
                        SettingsDbHandler.clearUserImageTable();
                    }
                    for (int index = 0; index < imagesArray.length(); index++) {


                        CustomJsonObject imageObject = new CustomJsonObject(imagesArray.getJSONObject(index));
                        UserImage userImage = new UserImage();
                        userImage.setImageUrl(imageObject.getString("imageUrl"));
                        userImage.setBase64Image(imageObject.getString("base64Image"));
                        userImage.setContentType(imageObject.getString("contentType"));
                        userImage.setImageBaseUrl(imageObject.getString("imageBaseUrl"));
                        if (imageObject.getString("imageSource").equals("Facebook_Profile")) {
                            userImage.setImageSource(UserImage.ImageSource.Facebook_Album.toString());
                        }else{
                            userImage.setImageSource(imageObject.getString("imageSource"));
                        }
                        userImage.setOrder(imageObject.getString("order"));

                        UserImageTable userImageTable = new UserImageTable()
                                .setSynced(true)
                                .setTimeStamp(String.valueOf(System.currentTimeMillis()))
                                .setOrder(String.valueOf(index))
                                .setImageUrl(userImage.getGenuineUrl())
                                .setImageSource(String.valueOf(userImage.getImageSource()))
                                .setImageBaseUrl(userImage.getImageBaseUrl())
                                .setContentType(userImage.getContentType());

                        SettingsDbHandler.saveDataInTable(SettingsDbHandler.dataBaseTables.UserImageTable,userImageTable);
                    }

                    JSONArray interestArray = obj.getJsonArray("interests");
                    for (int index = 0; index < interestArray.length() ; index++){
                        CustomJsonObject interestObj = new CustomJsonObject(interestArray.getJSONObject(index));
                        Detail interest = new Detail();
                        interest.setId(interestObj.getString("facebookId"));
                        interest.setName(interestObj.getString("name"));
                        userProfile.getInterestList().add(interest);
                    }

                    return userProfile;
                } else {
                    baseModel.error = Constants.Error.SettingsError.PROFILE_NOT_AVAILABLE_ERROR;
                    return baseModel;
                }

            }
            baseModel.error = Constants.Error.NULL_RESPONSE_ERROR;
            return baseModel;
        } catch (JSONException e) {
            e.printStackTrace();
            baseModel.error = Constants.Error.JSON_EXCEPTION;
            return baseModel;
        }

    }
}
