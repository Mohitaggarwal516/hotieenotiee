package com.codeplaylabs.hotch.login.ui;


import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeplaylabs.hotch.BuildConfig;
import com.codeplaylabs.hotch.base.BaseActivity;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.login.adapter.ViewPagerAdapter;
import com.codeplaylabs.hotch.login.dto.FacebookDTO;
import com.codeplaylabs.hotch.login.interfaces.TryAgainForAppConfig;
import com.codeplaylabs.hotch.login.models.Education;
import com.codeplaylabs.hotch.login.models.UserProfile;
import com.codeplaylabs.hotch.login.models.Work;
import com.codeplaylabs.hotch.login.models.dtoModels.FBUserProfileInfo;
import com.codeplaylabs.hotch.utils.CommonMethods;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.utils.LogAnalytics;
import com.codeplaylabs.hotch.utils.Singleton;
import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.home.ui.HomeActivity;
import com.codeplaylabs.hotch.login.interfaces.FacebookDTOResponse;
import com.codeplaylabs.hotch.utils.TextViewPlus;
import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.DefaultAudience;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.rd.PageIndicatorView;
import com.rd.animation.type.AnimationType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.fabric.sdk.android.Fabric;

import static java.lang.System.exit;

/**
 * Demonstrate Firebase Authentication using a Facebook access token.
 */
public class LoginActivity extends BaseActivity implements TryAgainForAppConfig {

    private static final String TAG = "FacebookLogin";
    private static final int BLOCKED_OKH = 1234;
    private static final int ERROR_OKH = 431;
    private static final int UPDATE_DIALOG = 2344;
    public static final int UPDATE_TAG = 98563;
    //private static final int MANDATORY_UPDATE_TAG = 96564;
    private static final boolean isMandatory = true;
    private static final int UPDATE_INFO_DIALOG = 6123;
    private TextView mStatusTextView;
    private TextView mDetailTextView;

    ViewPager viewPager;
    PageIndicatorView pageIndicatorView;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private CallbackManager mCallbackManager;
    private LoginManager loginManager;
    private FBUserProfileInfo fbUserProfileInfo;
    private AccessToken accessToken;
    LinearLayout fbLoginButton;
    private static int counter = 0;
    private RelativeLayout reportDgLay;
    private RelativeLayout layOutsideAlert, appUpdateDialogLayout;
    public static final String UPDATE_DG_TYPE_KEY = "updateType";
    public static final String UPDATE_MSG_KEY = "msg";
    public static final String UPDATE_APP_INTENT_ACTION = "Show App UpDate dialog";
    public static final int MANDATORY_UPDATE_TAG = 96564;

    List permissions = Arrays.asList("user_likes",
            /*"read_custom_friendlists",*/ "user_birthday", "user_friends", "email",
            "user_about_me", "user_hometown", "user_photos", "user_work_history", "user_location", "user_education_history");

    ViewPagerAdapter adapter;
    private TextView termsAndConditions;
    private boolean fromAppConfig = false;
    private RelativeLayout recallView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_facebook);
        viewPager = (ViewPager) findViewById(R.id.login_viewPager);
        LocalBroadcastManager.getInstance(LoginActivity.this).registerReceiver(appUpdateReceiver, new IntentFilter(UPDATE_APP_INTENT_ACTION));

        recallView = (RelativeLayout) findViewById(R.id.recall_view);
        reportDgLay = (RelativeLayout) findViewById(R.id.root_alert);
        appUpdateDialogLayout = (RelativeLayout) findViewById(R.id.appUpdateDialogLayout);

        ViewGroup.LayoutParams viewPagerParams = viewPager.getLayoutParams();
        viewPagerParams.height = ((getWindowManager().getDefaultDisplay().getHeight()) / 2) + 160;
        viewPager.setLayoutParams(viewPagerParams);


        adapter = new ViewPagerAdapter(LoginActivity.this);
        viewPager.setAdapter(adapter);
        pageIndicatorView = (PageIndicatorView) findViewById(R.id.pageIndicatorView);

        pageIndicatorView.setViewPager(viewPager);
        pageIndicatorView.setAnimationDuration(10000);
        pageIndicatorView.setAnimationType(AnimationType.DROP);
        pageIndicatorView.setInteractiveAnimation(true);
        pageIndicatorView.setRadius(4);
        pageIndicatorView.setSelectedColor(getResources().getColor(R.color.colorPrimary));
        pageIndicatorView.setUnselectedColor(getResources().getColor(R.color.moreWhite));

        RelativeLayout.LayoutParams pgIndicatorParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );


        pgIndicatorParams.setMargins(0, (viewPagerParams.height + 28), 0, 0);
        pgIndicatorParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        pageIndicatorView.setLayoutParams(pgIndicatorParams);

        //indicatorView= (PageIndicatorView) findViewById(R.id.pageIndicatorView);
        findViewById(R.id.loader_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        // Views

        // [START initialize_auth]
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        // [START initialize_fblogin]
        // Initialize Facebook Login button
        termsAndConditions = (TextView) findViewById(R.id.term_text);
        termsAndConditions.setMovementMethod(LinkMovementMethod.getInstance());
        ArrayList<String> array = new ArrayList<>();
        array.add("terms of service");
        array.add("private");
        array.add("policy");
        termsAndConditions.setText(addClickablePart(getString(R.string.terms_and_conditions), array), TextView.BufferType.SPANNABLE);
        //termsAndConditions.setLinkTextColor(getResources().getColor(R.color.colorPrimary));


        //AccessToken accessToken = AccessToken.getCurrentAccessToken();


        loginManager = LoginManager.getInstance();
        mCallbackManager = CallbackManager.Factory.create();
        fbLoginButton = (LinearLayout) findViewById(R.id.facebook_login_button);
        fbLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.loader_layout).setVisibility(View.VISIBLE);
                loginManager.setDefaultAudience(DefaultAudience.FRIENDS).logInWithReadPermissions(LoginActivity.this, permissions);
                loginManager.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        findViewById(R.id.loader_layout).setVisibility(View.GONE);
                        Log.d(TAG, "facebook:onSuccess:outside if" + loginResult.getAccessToken());
                        if (loginResult.getRecentlyDeniedPermissions().size() == 0) {
                            Log.d(TAG, "facebook:onSuccess:" + loginResult);
                            //saving fb id to pref for GA
                            LocalPreferenceManager.getInstance().setFbId(loginResult.getAccessToken().getUserId());
                            handleFacebookAccessToken(loginResult.getAccessToken());
                        } else {
//                            counter++;
                            showAlert();
                            // Toast.makeText(LoginActivity.this, "All permissions are required.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancel() {
                        findViewById(R.id.loader_layout).setVisibility(View.GONE);
                        Log.e(TAG, "facebook:onCancel");
                        //showAlert();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        findViewById(R.id.loader_layout).setVisibility(View.GONE);
                        Log.e(TAG, "facebook:onError", error);
                        //showAlert();
                    }
                });
            }
        });
        navigateToHomeActivity();
/*     ping test
        try {
            boolean isConnected = isConnected();
            if (!isConnected) {
                showNoNetDialog();
            } else {
                navigateToHomeActivity();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/


    }//onCreate

    BroadcastReceiver appUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            Log.w("LoginBroadCast", "Received");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    appUpdateDialogLayout.setVisibility(View.VISIBLE);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            appUpdateDialogLayout.setVisibility(View.VISIBLE);
                            appUpdateDialogLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });

                            final int tag = intent.getIntExtra(LoginActivity.UPDATE_DG_TYPE_KEY, 0);
                            String msgTxt = intent.getStringExtra(LoginActivity.UPDATE_MSG_KEY);

                            TextViewPlus msg = (TextViewPlus) findViewById(R.id.msgTxt);
                            msg.setText(Constants.ServiceConfiguration.APP_UPDATE_MESSAGE);
                            TextViewPlus positiveBtn = (TextViewPlus) findViewById(R.id.updateBtn);
                            TextViewPlus negBtn = (TextViewPlus) findViewById(R.id.notNowBtn);

                            positiveBtn.setText("Update");
                            negBtn.setText("Not Now");


                            if (tag == LoginActivity.MANDATORY_UPDATE_TAG) {
                                negBtn.setVisibility(View.GONE);
                            } //else {
                            negBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (tag == LoginActivity.UPDATE_TAG) {
                                        LocalPreferenceManager.getInstance().setPreference("myVersionCode", Constants.ServiceConfiguration.APP_CURRENT_VERSION);
                                        // mDialog.dismiss();
                                        appUpdateDialogLayout.setVisibility(View.GONE);
                                    } /*else if (tag == UPDATE_INFO_DIALOG) {
                                //negBtn=> update new(callservice)
                                callLoginService(FacebookDTO.MANDATORY_UPDATE, accessToken);
                            }*/
                                }
                            });
                            //}

                            positiveBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (tag == LoginActivity.MANDATORY_UPDATE_TAG || tag == LoginActivity.UPDATE_TAG) {
                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        } catch (android.content.ActivityNotFoundException anfe) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                        } finally {
                                            if (tag == LoginActivity.MANDATORY_UPDATE_TAG) {
                                                //mDialog.show();
                                                appUpdateDialogLayout.setVisibility(View.VISIBLE);
                                            }
                                        }
                                    }
                                }
                            });

                        }
                    });


                }
            });

        }
    };

    private void navigateToHomeActivity() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null && ((Constants.ServiceConfiguration.LOGIN_DATA * 1000) + LocalPreferenceManager.getInstance().getLongPreference(Constants.LAST_ACCESS_TOKEN_GENERATED, 0)) > System.currentTimeMillis()) {
            if (accessToken.getDeclinedPermissions().size() == 0) {
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        new FacebookDTO().checkForSignIn(LoginActivity.this);
                    }
                }, 10);

                Singleton.getInstance().setFacebookAccessToken(accessToken);
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            } else {
//                Toast.makeText(this, "Please login again!"/*"Denied permmissions size > 0"*/, Toast.LENGTH_SHORT).show();
                fbLoginButton.callOnClick();
            }
        } else if (accessToken != null) {
            recallView.setVisibility(View.VISIBLE);
            callLoginService(FacebookDTO.NON_MANDATORY_UPDATE, accessToken);
        }
    }

    protected void showCustomDialog(String title, String message, String posBtn, String negativeBtn, final int tag) {

        final AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this).create();

        View view = getLayoutInflater().inflate(R.layout.dg_mandatory_update, null, false);


        dialog.setView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.show();


        TextViewPlus msg = (TextViewPlus) view.findViewById(R.id.msgTxt);
        msg.setText(message);
        TextViewPlus positiveBtn = (TextViewPlus) view.findViewById(R.id.updateBtn);
        TextViewPlus negBtn = (TextViewPlus) view.findViewById(R.id.notNowBtn);

        positiveBtn.setText(posBtn);
        negBtn.setText(negativeBtn);


        if (tag == MANDATORY_UPDATE_TAG) {
            negBtn.setVisibility(View.GONE);
        } //else {
        negBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tag == UPDATE_TAG) {
                    LocalPreferenceManager.getInstance().setPreference("myVersionCode", Constants.ServiceConfiguration.APP_CURRENT_VERSION);
                    dialog.dismiss();
                } else if (tag == UPDATE_INFO_DIALOG) {
                    //negBtn=> update new(callservice)
                    callLoginService(FacebookDTO.MANDATORY_UPDATE, accessToken);
                }
            }
        });
        //}

        positiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tag == MANDATORY_UPDATE_TAG || tag == UPDATE_TAG) {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    } finally {
                        if (tag == MANDATORY_UPDATE_TAG)
                            dialog.show();
                    }
                } else if (tag == UPDATE_INFO_DIALOG) {
                    //posBtn=>continue with old
                    if (!fbUserProfileInfo.getFbUserProfileResponseModel().isBlocked()) {
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        showOkAlert(CommonMethods.getErrorMessage(Constants.Error.LoginError.USER_BLOCKED), "OOPS..!!", BLOCKED_OKH);
                    }
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        exit(0);
    }

    public void showAlert() {
        if (counter < 3) {
            final AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this).create();
            //dialog.setMessage("Please give the required permissions");
            //dialog.setTitle("Permissions Denied");
            View view = getLayoutInflater().inflate(R.layout.dg_permission_denied, null, false);
            dialog.setView(view);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
            TextViewPlus positiveBtn = (TextViewPlus) view.findViewById(R.id.positiveBtn);
            TextViewPlus cancel = (TextViewPlus) view.findViewById(R.id.cancel_btn);

            positiveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fbLoginButton.callOnClick();
                    LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, permissions);
                    counter++;
                    dialog.dismiss();
                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    counter++;
                    dialog.dismiss();
                }
            });
            /*dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    fbLoginButton.callOnClick();
                    LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this,permissions);
                    counter++;
                    dialog.dismiss();
                }
            });*/
            /*dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    counter++;
                    dialog.dismiss();
                    //finish();
                }
            });*/

        } else {
            finish();
        }
    }

    // [START on_start_check_user]

    // [END on_start_check_user]

    // [START on_activity_result]
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        /*if(resultCode==-1){
            showAlert();
        }*/
    }
    // [END on_activity_result]

    // [START auth_with_facebook]
    private void handleFacebookAccessToken(AccessToken token) {

        Singleton.getInstance().setFacebookAccessToken(token);
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        this.accessToken = token;
        callLoginService(FacebookDTO.NON_MANDATORY_UPDATE, token);
    }

    // [END auth_with_facebook]

    public void signOut() {
        mAuth.signOut();
        LoginManager.getInstance().logOut();

        updateUI(null);
    }

    private void updateUI(FirebaseUser user) {
        //hideProgressDialog();

    }

    /*@Override
    protected void onShowAlertOkPressed(int tag) {
        super.onShowAlertOkPressed(tag);
        switch (tag) {
            case UPDATE_DIALOG: {
                callLoginService(FacebookDTO.MANDATORY_UPDATE, accessToken);
                break;
            }
                case APP_UPDATE_TAG: {
                break;
            }
        }
    }*/

    /*@Override
    protected void onShowAlertNotOkPressed(int tag) {
        super.onShowAlertNotOkPressed(tag);
        switch (tag) {
            case UPDATE_DIALOG: {
                if (!fbUserProfileInfo.getFbUserProfileResponseModel().isBlocked()) {
                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    showOkAlert(CommonMethods.getErrorMessage(Constants.Error.LoginError.USER_BLOCKED), "OOPS..!!", BLOCKED_OKH);
                }
                break;
            }
            case APP_UPDATE_TAG: {
                exit(0);
            }
        }
    }*/

    private void callLoginService(int priority, AccessToken token) {
        findViewById(R.id.loader_layout).setVisibility(View.VISIBLE);
        new FacebookDTO().getFacebookDetails(LoginActivity.this, token, priority, new FacebookDTOResponse() {
            @Override
            public BaseModel showUpdateDialog(UserProfile userProfile, final FacebookDTO.DialogButtonPressed dialogButtonPressed) {
                recallView.setVisibility(View.GONE);
                findViewById(R.id.loader_layout).setVisibility(View.GONE);
                if (!LocalPreferenceManager.getInstance().getBooleanPreference(Constants.FIRST_LOGIN, true)) {
                    showDialogUpdate(userProfile, dialogButtonPressed);
                } else {
                    dialogButtonPressed.okClick("0");
                }
                return null;
            }

            @Override
            public void onResponse(final BaseModel baseModel, final String jsonString, final String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        findViewById(R.id.loader_layout).setVisibility(View.GONE);
                        if (baseModel != null) {
                            fbUserProfileInfo = (FBUserProfileInfo) baseModel;

                            //Singleton.getInstance().setFbUserProfileInfo(fbUserProfileInfo);
                            if (reasonOfError != null && reasonOfError.equals(Constants.Error.LoginError.DATA_UPDATED)) {
                                // showCustomDialog(reasonOfError, jsonString, UPDATE_DIALOG, getResources().getString(R.string.update_new), getResources().getString(R.string.continue_with_old));
                                if (accessToken == null)
                                    accessToken = AccessToken.getCurrentAccessToken();

                                callLoginService(FacebookDTO.MANDATORY_UPDATE, accessToken);
                                //showCustomDialog(reasonOfError, jsonString, getResources().getString(R.string.continue_with_old), getResources().getString(R.string.update_new), UPDATE_INFO_DIALOG);
                            } else {
                                if (!fbUserProfileInfo.getFbUserProfileResponseModel().isBlocked()) {
                                    LocalPreferenceManager.getInstance().setPreference(Constants.LAST_ACCESS_TOKEN_GENERATED, System.currentTimeMillis());
                                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    recallView.setVisibility(View.GONE);
                                    showOkAlert(CommonMethods.getErrorMessage(Constants.Error.LoginError.USER_BLOCKED), "OOPS..!!", BLOCKED_OKH);
                                }

                            }
                        } else {
                            showNoNetDialog();

                            //showOkAlert(CommonMethods.getErrorMessage(reasonOfError), "OOPS..!!", ERROR_OKH);
                        }
                    }
                });
            }
        });
    }

    private void showDialogUpdate(final UserProfile userProfile, final FacebookDTO.DialogButtonPressed dialogButtonPressed) {

        try {
            final String[] checkId = {""};
            boolean val = false;
            reportDgLay.setVisibility(View.VISIBLE);

            LocalPreferenceManager.getInstance().setPreference(Constants.PROFILE_SET, true);

            layOutsideAlert = (RelativeLayout) findViewById(R.id.lay_outside);
            //hideKeyBoardOnPressingOutsideView(reasonEd);
            TextViewPlus positiveBtn = (TextViewPlus) findViewById(R.id.positiveBtn);
            TextViewPlus ntvBtn = (TextViewPlus) findViewById(R.id.cancel_btn);
            RadioGroup radioGroup = (RadioGroup) findViewById(R.id.curr_loc_rad_grp);

            for (Work work : userProfile.getWorkList()) {
                if (work.getEndDate().length() == 0 || work.getEndDate().contains("0000") || Integer.parseInt(work.getEndDate().split("-")[0]) >= 2017) {
                    RadioButton radioButton = new RadioButton(LoginActivity.this);
                    radioButton.setText(work.getEmployer().getName());
                    radioButton.setTag(work.getFacebookId());
                    radioButton.setTextSize(16);
                    radioGroup.addView(radioButton);

                    if (!val) {
                        checkId[0] = work.getFacebookId();
                        val = true;
                    }

                    if (work.getIsCurrentlyWorkingHere() && radioGroup.getCheckedRadioButtonId() == -1) {
                        checkId[0] = work.getFacebookId();
                        ((RadioButton) radioGroup.getChildAt(userProfile.getWorkList().indexOf(work))).setChecked(true);
                    }
                }

            }
            for (Education education : userProfile.getEducationList()) {
                if (education.getYear().length() == 0 || Integer.parseInt(education.getYear()) >= 2017) {
                    RadioButton radioButton = new RadioButton(LoginActivity.this);
                    radioButton.setText(education.getInstitute().getName());
                    radioButton.setTag(education.getFacebookId());
                    radioButton.setTextSize(16);
                    radioGroup.addView(radioButton);

                    if (!val) {
                        checkId[0] = education.getFacebookId();
                        val = true;
                    }
                    if (education.isCurrentlyStudingHere() && radioGroup.getCheckedRadioButtonId() == -1) {
                        checkId[0] = education.getFacebookId();
                        ((RadioButton) radioGroup.getChildAt(userProfile.getWorkList().size() + userProfile.getEducationList().indexOf(education))).setChecked(true);
                    }
                }
            }


            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    checkId[0] = String.valueOf((group.findViewById(checkedId)).getTag());
                }
            });
            if (radioGroup.getCheckedRadioButtonId() == -1)

            {
                ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
            }

            positiveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkId[0].length() > 0) {
                        for (Work work : userProfile.getWorkList()) {
                            if (work.getFacebookId().equalsIgnoreCase(checkId[0])) {
                                work.setIsCurrentlyWorkingHere(true);
                                dialogButtonPressed.okClick(checkId[0]);
                                reportDgLay.setVisibility(View.VISIBLE);
                                return;
                            } else {
                                work.setIsCurrentlyWorkingHere(false);
                            }
                        }

                        for (Education education : userProfile.getEducationList()) {
                            if (education.getFacebookId().equalsIgnoreCase(checkId[0])) {
                                education.setCurrentlyStudingHere(true);
                                dialogButtonPressed.okClick(checkId[0]);
                                reportDgLay.setVisibility(View.VISIBLE);
                                return;
                            } else {
                                education.setCurrentlyStudingHere(false);
                            }
                        }
                        dialogButtonPressed.okClick(checkId[0]);
                        reportDgLay.setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(LoginActivity.this, "Please select One.", Toast.LENGTH_SHORT).show();

                    }
                }
            });
            ntvBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    findViewById(R.id.loader_layout).setVisibility(View.GONE);
                    for (Work work : userProfile.getWorkList()) {
                        if (work.getFacebookId().equalsIgnoreCase(checkId[0])) {
                            work.setIsCurrentlyWorkingHere(true);
                            dialogButtonPressed.okClick(checkId[0]);
                            reportDgLay.setVisibility(View.VISIBLE);
                            return;
                        }
                    }

                    for (Education education : userProfile.getEducationList()) {
                        if (education.getFacebookId().equalsIgnoreCase(checkId[0])) {
                            education.setCurrentlyStudingHere(true);
                            dialogButtonPressed.okClick(checkId[0]);
                            reportDgLay.setVisibility(View.VISIBLE);
                            return;
                        }
                    }
                    dialogButtonPressed.okClick(checkId[0]);
                    reportDgLay.setVisibility(View.VISIBLE);
                    //dialogButtonPressed.okClick(checkId[0]);
                    reportDgLay.setVisibility(View.GONE);
                }
            });

            layOutsideAlert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    reportDgLay.setVisibility(View.GONE);
                }
            });


        } catch (Exception e) {

        }


    }//showUpdateDialog

    private SpannableStringBuilder addClickablePart(String str, final ArrayList<String> strings) {
        SpannableStringBuilder ssb = new SpannableStringBuilder(str);
        int index = 0;
        int idx1 = str.indexOf(strings.get(index));
        int idx2 = 0;
        while (idx1 != -1 && index < strings.size()) {
            idx2 = idx1 + strings.get(index).length();
            final String clickString = str.substring(idx1, idx2);
            final int finalIndex = index;
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {
                    Intent myIntent = new Intent(LoginActivity.this, WebViewActivity.class);

                    if (strings.get(finalIndex).equals("terms of service")) {
                        myIntent.putExtra("url", "http://gohotch.com/terms/"/*"http://codeplaylabs.com/Hottieterms.html"*/);
                    } else {
                        myIntent.putExtra("url", "http://gohotch.com/privacy/"/*"http://codeplaylabs.com/HottiePrivacy.html"*/);
                    }
                    startActivity(myIntent);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                    ds.setColor(getResources().getColor(R.color.grey_600));
                    ds.bgColor = getResources().getColor(R.color.grey_100);
                }
            }, idx1, idx2, 0);
            index++;
            if (index < strings.size())
                idx1 = str.indexOf(strings.get(index));
        }

        return ssb;
    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();

    }
    // [END on_start_check_user]


    @Override
    protected void onResume() {
        super.onResume();
        LogAnalytics.logScreenView(mTracker, Constants.ActivityName.LOGIN_SCREEN);
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }


    public boolean isConnected() throws InterruptedException, IOException {
        String command = "ping -c 1 google.com";
        return (Runtime.getRuntime().exec(command).waitFor() == 0);
    }

    @Override
    protected void onTryAgainClicked() {

        if (fromAppConfig) {
            fromAppConfig = false;
            callAppConfigService();
            dialog.dismiss();
        } else {
            //when login service is called on pressing fb login
            if (accessToken != null) {
                //chances of access token to be null r remote.see the flow from the login btn tapped
                callLoginService(FacebookDTO.NON_MANDATORY_UPDATE, accessToken);
            }
        }
        super.onTryAgainClicked();
    }

    /*protected void showNoNetDialog() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dg_network_not_available, null, false);

        final AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this).create();
        dialog.setView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled()) {
                    finish();
                    return true;
                }
                return false;
            }
        });
        dialog.show();
        TextViewPlus tryAgain = (TextViewPlus) view.findViewById(R.id.tryAgain);
        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (accessToken != null) {
                    //chances of access token to be null r remote.see the flow from the login btn tapped
                    callLoginService(FacebookDTO.MANDATORY_UPDATE, accessToken);
                }
                //dialog.dismiss();
               *//* try {
                    boolean isConnected = isConnected();
                    if (isConnected) {
                        dialog.dismiss();
                        navigateToHomeActivity();
                    } else {
                        dialog.dismiss();
                        showNoNetDialog();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }*//*
            }
        });
    }*/

    @Override
    public void appConfigTryAgainClicked() {
        showNoNetDialog();
        fromAppConfig = true;
    }
}