package com.codeplaylabs.hotch.settings.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeplaylabs.hotch.base.BaseActivity;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.customUi.CircleImageView;
import com.codeplaylabs.hotch.database.databaseInitialization.DatabaseHelper;
import com.codeplaylabs.hotch.database.tables.ImageTemplate;
import com.codeplaylabs.hotch.database.tables.ProfileTable;
import com.codeplaylabs.hotch.home.handler.HomeDBHandlerClass;
import com.codeplaylabs.hotch.login.interfaces.SharedProfileResponse;
import com.codeplaylabs.hotch.login.models.Education;
import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.login.models.UserProfile;
import com.codeplaylabs.hotch.login.models.Work;
import com.codeplaylabs.hotch.login.ui.LoginActivity;
import com.codeplaylabs.hotch.profileDetail.ui.ProfileDetailActivity;
import com.codeplaylabs.hotch.settings.handler.SettingsDbHandler;
import com.codeplaylabs.hotch.settings.interfaces.DiscoveryService;
import com.codeplaylabs.hotch.settings.interfaces.InformClickToActivity;
import com.codeplaylabs.hotch.settings.interfaces.ProfileResponseListener;
import com.codeplaylabs.hotch.settings.services.SettingsService;
import com.codeplaylabs.hotch.settings.tools.interfaces.SettingsToggleClickListener;
import com.codeplaylabs.hotch.settings.tools.ui.SettingsToggleFunction;
import com.codeplaylabs.hotch.settings.ui.fragments.DiscoveryTabFragment;
import com.codeplaylabs.hotch.settings.ui.fragments.ProfileTabFragment;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.utils.LogAnalytics;
import com.codeplaylabs.hotch.utils.ServiceSingleton;
import com.codeplaylabs.hotch.utils.Singleton;
import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.database.tables.FilterTable;
import com.codeplaylabs.hotch.database.tables.UserImageTable;
import com.codeplaylabs.hotch.home.services.HomeServices;
import com.codeplaylabs.hotch.login.dto.FacebookDTO;
import com.codeplaylabs.hotch.login.interfaces.FacebookDTOResponse;
import com.codeplaylabs.hotch.utils.CustomViewPager;
import com.codeplaylabs.hotch.utils.TextViewPlus;
import com.j256.ormlite.dao.Dao;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class SettingsActivity extends BaseActivity implements DiscoveryService {

    private static final String RIGHT_TAB_NAME = "DISCOVERY";
    private static final String LEFT_TAB_NAME = "PROFILE";
    private CustomViewPager settingsViewPager;
    public PagerAdapter pagerAdapter;
    // private SwipeRefreshLayout swipeRefreshLayout;
    private int focusableChild = 1;
    public static final int PROFILE_ACTIVITY_REQ = 857;

    private ProfileTabFragment profileFragment = null;
    public TextView saveBtn;
    private DatabaseHelper dbHelper;
    private CircleImageView viewProfile;
    private RelativeLayout loaderLayout;
    public UserProfile userProfile;
    private DiscoveryTabFragment discoveryFragment = null;
    private RelativeLayout reportDgLay;
    private RelativeLayout layOutsideAlert;
    private EditText reasonEd;
    private boolean currAtChanged = false;
    public boolean fbSynced = false;
    private boolean dataSent = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_profile_view);
        super.onCreate(savedInstanceState);
        findViewById(R.id.loader_layout).setOnClickListener(null);


        //saveBtn = (TextView)findViewById(R.id.save_settings_btn);
        reportDgLay = (RelativeLayout) findViewById(R.id.root_alert);


        loaderLayout = (RelativeLayout) findViewById(R.id.loader_layout);
        viewProfile = (CircleImageView) findViewById(R.id.view_profile);

        int placeHolder = ServiceSingleton.getInstance().getPlaceHolder();
        if (placeHolder != 0)
            viewProfile.setImageResource(placeHolder);

        viewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, ProfileDetailActivity.class);
                intent.putExtra("from", ProfileDetailActivity.From.MyProfile.toString());
                intent.putExtra("profile", profileFragment.getProfile());
                startActivity(intent);
            }
        });
        settingsViewPager = (CustomViewPager) findViewById(R.id.settings_tab_view_pager);

        // swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.settings_swipe_refresh_layout);

       /* swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (settingsViewPager.getChildAt(1) == settingsViewPager.getFocusedChild())
                    focusableChild = 1;
                callProfileById();
            }
        });
*/
        callProfileById();

    }

    private void callProfileById() {
        loaderLayout.setVisibility(View.VISIBLE);
        String userId = LocalPreferenceManager.getInstance().getUserId();
        new SettingsService().getProfileById(SettingsActivity.this, userId, true, true, new ProfileResponseListener() {
            @Override
            public void onResponse(final BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                if (reasonOfError == null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            loaderLayout.setVisibility(View.GONE);
                            // swipeRefreshLayout.setRefreshing(false);
                            if (baseModel != null) {

                                userProfile = (UserProfile) baseModel;
                                // Singleton.getInstance().getFbUserProfileInfo().setUserProfile((UserProfile) baseModel);

                                Singleton.getInstance().setUserProfile((UserProfile) baseModel);
                            /*Singleton.getInstance().setImgUrl(userProfile.getProfileImageUrl());*/
                                pagerAdapter = null;
                                callFragmentAdapter(focusableChild);
                                if (!LocalPreferenceManager.getInstance().getBooleanPreference(Constants.PROFILE_SET)) {
                                    showDialogUpdate(userProfile, null);
                                }
                            } else {

                            }
                        }
                    });
                } else {
                    showNoNetDialog();
                }
            }
        });
    }

    @Override
    protected void onTryAgainClicked() {
        //Toast.makeText(this, "tryAgain", Toast.LENGTH_SHORT).show();
        dialog.dismiss();
        dialog = null;
        startActivity(new Intent(SettingsActivity.this, SettingsActivity.class));
        finish();
        super.onTryAgainClicked();
    }

    private void showDialogUpdate(final UserProfile userProfile, final FacebookDTO.DialogButtonPressed dialogButtonPressed) {
        try {
            final String[] checkId = {""};
            boolean val = false;
            reportDgLay.setVisibility(View.VISIBLE);
            LocalPreferenceManager.getInstance().setPreference(Constants.PROFILE_SET, true);

            reasonEd = (EditText) findViewById(R.id.reasonTxt);
            layOutsideAlert = (RelativeLayout) findViewById(R.id.lay_outside);
            //hideKeyBoardOnPressingOutsideView(reasonEd);
            TextViewPlus positiveBtn = (TextViewPlus) findViewById(R.id.positiveBtn);
            TextViewPlus ntvBtn = (TextViewPlus) findViewById(R.id.cancel_btn);
            RadioGroup radioGroup = (RadioGroup) findViewById(R.id.curr_loc_rad_grp);
            radioGroup.removeAllViews();
            for (Work work : userProfile.getWorkList()) {
                if (work.getEndDate().length() == 0 || work.getEndDate().contains("0000") || Integer.parseInt(work.getEndDate().split("-")[0]) >= 2017) {
                    RadioButton radioButton = new RadioButton(SettingsActivity.this);
                    radioButton.setText(work.getEmployer().getName());
                    radioButton.setTag(work.getFacebookId());
                    radioButton.setTextSize(16);
                    radioGroup.addView(radioButton);

                    if (!val) {
                        checkId[0] = work.getFacebookId();
                        val = true;
                    }

                    if (work.getIsCurrentlyWorkingHere() && radioGroup.getCheckedRadioButtonId() == -1) {
                        checkId[0] = work.getFacebookId();
                        ((RadioButton) radioGroup.getChildAt(userProfile.getWorkList().indexOf(work))).setChecked(true);
                    }
                }

            }
            for (Education education : userProfile.getEducationList()) {
                if (education.getYear().length() == 0 || Integer.parseInt(education.getYear()) >= ((new Date().getYear()) + 1900)) {
                    RadioButton radioButton = new RadioButton(SettingsActivity.this);
                    radioButton.setText(education.getInstitute().getName());
                    radioButton.setTag(education.getFacebookId());
                    radioButton.setTextSize(16);
                    radioGroup.addView(radioButton);

                    if (!val) {
                        checkId[0] = education.getFacebookId();
                        val = true;
                    }
                    if (education.isCurrentlyStudingHere() && radioGroup.getCheckedRadioButtonId() == -1) {
                        checkId[0] = education.getFacebookId();
                        ((RadioButton) radioGroup.getChildAt(userProfile.getWorkList().size() + userProfile.getEducationList().indexOf(education))).setChecked(true);
                    }
                }
            }


            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    checkId[0] = String.valueOf((group.findViewById(checkedId)).getTag());
                }
            });

            if (radioGroup.getChildCount() == 0) {
                reportDgLay.setVisibility(View.GONE);
            }

            if (radioGroup.getCheckedRadioButtonId() == -1)

            {
                ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
            }


            positiveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currAtChanged = true;

                    if (checkId[0].length() > 0) {
                        for (Work work : userProfile.getWorkList()) {
                            if (work.getFacebookId().equalsIgnoreCase(checkId[0])) {
                                work.setIsCurrentlyWorkingHere(true);

                                //  dialogButtonPressed.okClick(checkId[0]);


                            } else {
                                work.setIsCurrentlyWorkingHere(false);
                            }
                        }

                        for (Education education : userProfile.getEducationList()) {
                            if (education.getFacebookId().equalsIgnoreCase(checkId[0])) {
                                education.setCurrentlyStudingHere(true);
                                // dialogButtonPressed.okClick(checkId[0]);


                            } else {
                                education.setCurrentlyStudingHere(false);
                            }
                        }
                        new SettingsService().updateCurrentLocationService(SettingsActivity.this, userProfile, new ProfileResponseListener() {
                            @Override
                            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                                callProfileById();
                            }
                        });
                        // dialogButtonPressed.okClick(checkId[0]);
                        reportDgLay.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(SettingsActivity.this, "Please select One.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            ntvBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    loaderLayout.setVisibility(View.GONE);
                    for (Work work : userProfile.getWorkList()) {
                        if (work.getFacebookId().equalsIgnoreCase(checkId[0])) {
                            work.setIsCurrentlyWorkingHere(true);
                            //dialogButtonPressed.okClick(checkId[0]);
                           /* new SettingsService().updateCurrentLocationService(SettingsActivity.this, userProfile, new ProfileResponseListener() {
                                @Override
                                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                                }
                            });*/
                            reportDgLay.setVisibility(View.GONE);
                            return;
                        }
                    }

                    for (Education education : userProfile.getEducationList()) {
                        if (education.getFacebookId().equalsIgnoreCase(checkId[0])) {
                            education.setCurrentlyStudingHere(true);
                            // dialogButtonPressed.okClick(checkId[0]);
                           /* new SettingsService().updateCurrentLocationService(SettingsActivity.this, userProfile, new ProfileResponseListener() {
                                @Override
                                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                                }
                            });
                           */
                            reportDgLay.setVisibility(View.GONE);
                            return;
                        }
                    }
                    //dialogButtonPressed.okClick(checkId[0]);
                    reportDgLay.setVisibility(View.GONE);
                }
            });

            layOutsideAlert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    reportDgLay.setVisibility(View.GONE);
                }
            });


        } catch (Exception e) {

        }
    }

    private void callFragmentAdapter(int focusedChild) {
        if (pagerAdapter == null) {

            pagerAdapter = new PagerAdapter(getSupportFragmentManager());
            settingsViewPager.setCurrentItem(focusedChild, true);
            callToggleSwitch(focusedChild);
            settingsViewPager.setAdapter(pagerAdapter);
            settingsViewPager.setPagingEnabled(false);
            settingsViewPager.setCurrentItem(focusedChild, true);
            settingsViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    callToggleSwitch(i);
                    focusableChild = i;
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });
        } else {
            pagerAdapter.notifyDataSetChanged();
        }
    }

    private void callToggleSwitch(final int position) {
        SettingsToggleFunction.toggleSwitch(this, position, LEFT_TAB_NAME, RIGHT_TAB_NAME, new SettingsToggleClickListener() {
            @Override
            public void toggleClickedLeft(TextView left, TextView right/*int position*/) {
                settingsViewPager.setCurrentItem(SettingsToggleFunction.PAGE_ZERO, true);
                left.setText("PROFILE");
                right.setText("DISCOVERY");
            }

            @Override
            public void toggleClickedRight(TextView left, TextView right) {
                settingsViewPager.setCurrentItem(SettingsToggleFunction.PAGE_ONE, true);
                left.setText("PROFILE");
                right.setText("DISCOVERY");
            }
        });
    }

    public void changeProfileDetailImage(String imageUrl, int placeholder) {
        if (imageUrl == null) {
            Picasso.with(this).load(placeholder).into(viewProfile);
        } else {
            Picasso.with(this).load(imageUrl).placeholder(placeholder)./*centerCrop().resize(getWindowManager().getDefaultDisplay().getWidth(),getWindowManager().getDefaultDisplay().getHeight()).*/into(viewProfile);
        }
    }

    //menthod called while clicking fb sync btn on profile setting fragment
    public void informActivityOnFbSyncClicked() {

        loaderLayout.setVisibility(View.VISIBLE);
        new FacebookDTO().getFacebookDetails(SettingsActivity.this, Singleton.getInstance().getFacebookAccessToken(), FacebookDTO.MANDATORY_UPDATE, new FacebookDTOResponse() {


            @Override
            public BaseModel showUpdateDialog(UserProfile userProfile, FacebookDTO.DialogButtonPressed dialogButtonPressed) {
                loaderLayout.setVisibility(View.GONE);
                if (!LocalPreferenceManager.getInstance().getBooleanPreference(Constants.FIRST_LOGIN, true)) {
                    showDialogUpdate(userProfile, dialogButtonPressed);
                } else {
                    dialogButtonPressed.okClick("0");
                }
                return null;
            }

            @Override
            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                LogAnalytics.logButtonClickEvent(mTracker, Constants.Buttons.SYNC_TO_FB_BTN);
                //call getProfile by id service with should force... true
                String userId = LocalPreferenceManager.getInstance().getUserId();
                fbSynced = true;

                new SettingsService().getProfileById(SettingsActivity.this, userId, true, true, new ProfileResponseListener() {
                    @Override
                    public void onResponse(final BaseModel baseModel, String jsonString, final String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {


                                loaderLayout.setVisibility(View.GONE);
                                if (baseModel != null && reasonOfError == null) {
                                    //notify activity the changes
                                    userProfile = (UserProfile) baseModel;
                                    //Singleton.getInstance().getFbUserProfileInfo().setUserProfile((UserProfile) baseModel);
                                    Singleton.getInstance().setUserProfile((UserProfile) baseModel);
                                    pagerAdapter = null;
                                    callFragmentAdapter(focusableChild);
                                }
                            }
                        });
                    }
                });
            }
        });

    }

    //DiscoveryService implemented method
    @Override
    public void informActivityToCallService() {


    }

    public void onLogoutClicked() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                List<ProfileTable> profiles = HomeDBHandlerClass.syncProfilesWithServers(true);
                if (profiles != null && profiles.size() > 0 && !Singleton.getInstance().hotOrNotStatusUpdated) {
                    Singleton.getInstance().hotOrNotStatusUpdated = true;
                    new HomeServices().sendDataToServers(SettingsActivity.this, profiles, null);

                }

            }
        }, 0);
        new SettingsService().logoutService(SettingsActivity.this, new SharedProfileResponse() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

            }
        });
        String fireBaseToken = com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().getStringPreference("firebaseToken", null);
        Singleton.getInstance().getHelper().clearAllTables();
        LocalPreferenceManager.getInstance().removeAll();
        // LocalPreferenceManager.getInstance().setPreference("firebaseToken",fireBaseToken);
        Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private class PagerAdapter extends FragmentStatePagerAdapter {

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == SettingsToggleFunction.PAGE_ZERO) {
                profileFragment = new ProfileTabFragment(SettingsActivity.this);
                return profileFragment;
            } else {
                discoveryFragment = new DiscoveryTabFragment(SettingsActivity.this, new InformClickToActivity() {
                    @Override
                    public void onShareMyProfileClicked() {
                        LogAnalytics.logButtonClickEvent(mTracker, Constants.Buttons.SHARE_PERSONAL_PROFILE);
                        //String imgUrl = Singleton.getInstance().getUserProfile().getProfileImageUrl();
                        String imgUrl = SettingsDbHandler.fetchMyDpFromUserImageTable();
                        createBranchLink(SettingsActivity.this, imgUrl, LocalPreferenceManager.getInstance().getUserId(), Constants.SHARE_My_PROFILE, null);
                    }// createBranchLink(ProfileDetailActivity.this,userProfile.getProfileImageUrl(),userProfile.getUserId(),Constants.SHARE_My_PROFILE);

                    @Override
                    public void shareAppClicked() {
                        LogAnalytics.logButtonClickEvent(mTracker, Constants.Buttons.SHARE_APP);
                    }

                });
                return discoveryFragment;
            }

        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //if (requestCode == 131929) {
        if (resultCode == RESULT_OK) {

            if (data != null) {

                String cpo = data.getStringExtra("bitmap");
                dbHelper = Singleton.getInstance().getHelper();
                try {
                    Dao<ImageTemplate, Integer> dataDao = dbHelper.getTemplateDao();
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("timestamp", cpo);
                    List<ImageTemplate> dataList = dataDao.queryForFieldValues(map);

                    if (dataList != null && dataList.size() > 0) {
                        ImageTemplate imageTemplate = dataList.get(0);
                        profileFragment.selectedImage(imageTemplate.imageBase64,
                                imageTemplate.uri,
                                imageTemplate.timestamp,
                                imageTemplate.fetchedFrom);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                cpo = null;
            }
        } else if (resultCode == 8765) {
            profileFragment.imageDeleted(null);
        }
        //}

    }

    @Override
    protected void onStop() {
        super.onStop();


    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onBackPressed() {

        checkData();
        super.onBackPressed();

    }

    @Override
    protected void onHomeBtnPressed() {

        checkData();
        super.onHomeBtnPressed();
    }

    @Override
    protected void checkForSettingsUpdate() {
        super.checkForSettingsUpdate();

        checkData();
    }

    private void checkData() {
        if (profileFragment != null) {
            ArrayList<UserImage> images = profileFragment.getImages();
            saveImagesInTable(images, profileFragment.isImageDeleted());
            if (profileFragment.isValueUpdated()) {
                updateProfile();
            }
            updateDiscoveryFragment();
        }
    }

    private void updateProfile() {
        class Async extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                profileFragment.getProfile();
                String id = LocalPreferenceManager.getInstance().getUserId();
                new SettingsService().updateProfileService(SettingsActivity.this, id, userProfile, null);

                return null;
            }
        }
        new Async().execute();
    }

    private void setNewFilter(FilterTable filters) {
        new HomeServices().callDataFromService(this, filters, HomeServices.PERSIST_FILTERS, HomeServices.SHOULD_NOT_RESET_VALUES, null);
    }

    private void saveImagesInTable(ArrayList<UserImage> images, final boolean isImageDeleted) { //images will be used if arrays are introduced

        class Async extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                List<UserImageTable> imagesList = SettingsDbHandler.getAllDataFromImageTable();

                if (isImageDeleted) {

                    String id = LocalPreferenceManager.getInstance().getUserId();

                    new SettingsService().sendAndSaveImages(SettingsActivity.this, id, imagesList, null);
                    return null;
                }
                for (UserImageTable userImageTable : imagesList) {
                    if (!userImageTable.isSynced) {

                        String id = LocalPreferenceManager.getInstance().getUserId();

                        new SettingsService().sendAndSaveImages(SettingsActivity.this, id, imagesList, null);
                        break;
                    }
                }
                return null;
            }
        }
        new Async().execute();
    }


    private void updateDiscoveryFragment() {

        class Async extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {


                if (discoveryFragment.isNotificationValueUpdated()) {
                    String id = LocalPreferenceManager.getInstance().getUserId();
                    new SettingsService().setNotificationSettings(SettingsActivity.this, id, discoveryFragment.getNotificationsSettings(), null);
                }
                return null;
            }
        }
        new Async().execute();
        if (discoveryFragment.isValueUpdated() || currAtChanged || fbSynced) {
            Singleton.getInstance().filterRecieved = true;
            setNewFilter(discoveryFragment.getFilters());
        }
    }

    @Override
    protected void onResume() {
        LogAnalytics.logScreenView(mTracker, Constants.ActivityName.SETTINGS_SCREEN);
        super.onResume();
    }


}
