package com.codeplaylabs.hotch.utils;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by HP on 18-Sep-17.
 */

public class LogAnalytics {

    /*private LogAnalytics mInstance;

    public static void getInstance(){
        *//*if(mInstance)*//*
    }*/

    public static void logScreenView(Tracker tracker, String activtyName) {
        LocalPreferenceManager localPrefInstance=LocalPreferenceManager.getInstance();
        String extraDetail="_"+localPrefInstance.getUserName()+"_"+localPrefInstance.getfbId();
        tracker.setScreenName(activtyName+extraDetail);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public static void logSerViceFailEvent(Tracker tracker, String actionString, String url) {
        tracker.send(new HitBuilders.EventBuilder().setCategory("ServiceFailure")
                .setAction(actionString).setLabel(url).build());
    }

    public static void logButtonClickEvent(Tracker tracker,String btnName){
        tracker.send(new HitBuilders.EventBuilder().setCategory("ButtonClicks")
                .setAction(btnName).build());
    }
}
