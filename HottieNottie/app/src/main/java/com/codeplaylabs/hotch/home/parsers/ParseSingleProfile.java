package com.codeplaylabs.hotch.home.parsers;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.login.models.Detail;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.models.Education;
import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.login.models.Work;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mohit on 21/07/17.
 */

public class ParseSingleProfile extends BaseParser{
    BaseModel baseModel  = new BaseModel();
    @Override
    public BaseModel parseJson(JSONObject response) throws JSONException {
       if(validate(response) != null) {
           try {
               return getParsedProfile(response);
           } catch (Exception e) {
               e.printStackTrace();
               baseModel.error=Constants.Error.JSON_EXCEPTION;
               return baseModel;
           }
       }else{
           baseModel.error = Constants.Error.NULL_RESPONSE_ERROR;
           return baseModel;
       }
    }

    public ProfileModel getParsedProfile(JSONObject user) throws Exception {
        ProfileModel userProfile = new ProfileModel();
        CustomJsonObject obj = new CustomJsonObject(user);
        userProfile.setId(obj.getString("id"));
        userProfile.setName(obj.getString("name"));
        userProfile.setGender(obj.getString("gender"));
        userProfile.setDateOfBirth(obj.getString("birthday"));
        userProfile.setYearOfBirthday(obj.getString("yearOfBirthday"));
        userProfile.setAbout(obj.getString("about"));

        //setting
        {// education model
                 CustomJsonObject educationObject = new CustomJsonObject(obj.getJsonObject("educationDetail"));
                CustomJsonObject school = new CustomJsonObject(educationObject.getJsonObject("institute"));
                Education education = new Education();
                education.setFacebookId(educationObject.getString("facebookId"));
                education.setType(educationObject.getString("type"));
                Detail institute = new Detail();
                institute.setId(school.getString("instituteBranchFacebookId"));
                institute.setName(school.getString("name"));
                education.setInstitute(institute);
                education.setYear(educationObject.getString("year"));
                education.setCurrentlyStudingHere(educationObject.getBoolean("isCurrentlyStudingHere", false));
                userProfile.setEducation(education);
        }

        {// work model
            CustomJsonObject workObject = new CustomJsonObject(obj.getJsonObject("workDetail"));
            CustomJsonObject employerJson = new CustomJsonObject(workObject.getJsonObject("employer"));
            CustomJsonObject locationJson = new CustomJsonObject(workObject.getJsonObject("location"));

            Work work = new Work();
            work.setEndDate(workObject.getString("endDate"));
            work.setFacebookId(workObject.getString("facebookId"));
            work.setStartDate(workObject.getString("startDate"));
            work.setPosition(workObject.getString("position"));
            work.setPositionFacebookId(workObject.getString("positionFacebookId"));
            work.setIsCurrentlyWorkingHere(workObject.getBoolean("isCurrentlyWorkingHere", false));

            Detail employer = new Detail();
            employer.setId(employerJson.getString("id"));
            employer.setName(employerJson.getString("name"));
            work.setEmployer(employer);


            Detail location = new Detail();
            location.setId(locationJson.getString("facebookId"));
            location.setName(locationJson.getString("name"));
            location.setCountry(locationJson.getString("country"));
            location.setCountryCode(locationJson.getString("countryCode"));
            work.setLocation(location);

            userProfile.setWork(work);
        }
        CustomJsonObject locationJson = new CustomJsonObject(obj.getJsonObject("location"));
        Detail location = new Detail();
        location.setId(locationJson.getString("facebookId"));
        location.setName(locationJson.getString("city"));
        location.setCountry(locationJson.getString("country"));
        location.setCountryCode(locationJson.getString("countryCode"));
        userProfile.setLocation(location);



        JSONArray imagesArray = obj.getJsonArray("images");
        if (imagesArray != null && imagesArray.length() > 0) {
       //     SettingsDbHandler.clearUserImageTable();
        }
        for (int index = 0; index < imagesArray.length(); index++) {

            CustomJsonObject imageObject = new CustomJsonObject(imagesArray.getJSONObject(index));
            UserImage userImage = new UserImage();
            userImage.setImageUrl(imageObject.getString("imageUrl"));
            userImage.setBase64Image(imageObject.getString("base64Image"));
            userImage.setContentType(imageObject.getString("contentType"));
            userImage.setImageBaseUrl(imageObject.getString("imageBaseUrl"));
            if (imageObject.getString("imageSource").equals("Facebook_Profile")) {
                userImage.setImageSource(UserImage.ImageSource.Facebook_Album.toString());
            } else {
                userImage.setImageSource(imageObject.getString("imageSource"));
            }
            userImage.setOrder(imageObject.getString("order"));

           userProfile.getImages().add(userImage);
        }

        JSONArray interestArray = obj.getJsonArray("interests");
        for (int index = 0; index < interestArray.length(); index++) {
            CustomJsonObject interestObj = new CustomJsonObject(interestArray.getJSONObject(index));
            Detail interest = new Detail();
            interest.setId(interestObj.getString("facebookId"));
            interest.setName(interestObj.getString("name"));
            userProfile.getInterests().add(interest);
        }
        userProfile.setMatch(obj.getBoolean("match",false));
        return userProfile;

    }

    public ProfileModel getDataToSendToServers(JSONObject object)throws Exception {
        ProfileModel profileModel = new ProfileModel();
        profileModel.setMatch(object.getBoolean("match"));
        profileModel.setId(object.getString("id"));
        return profileModel;
    }

}
