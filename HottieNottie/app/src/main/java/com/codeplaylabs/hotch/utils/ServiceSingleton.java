package com.codeplaylabs.hotch.utils;

import android.app.Activity;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.home.models.ProfileModel;

/**
 * Created by mohit on 01/09/17.
 */

public class ServiceSingleton {
    private static final ServiceSingleton ourInstance = new ServiceSingleton();

    private boolean chatWithMsg = false;
    private boolean chatWithoutMsg = false;
    private int selfPlaceholder = R.drawable.female_placeholder;
    private Activity activity = null;


    private BaseModel model = null;
    private BaseModel secModel = null;

    private String placeHolder;

    public int getPlaceHolder() {
        if (placeHolder != null && placeHolder.length() > 0) {
            if (placeHolder.equalsIgnoreCase("male"))
                return R.drawable.male_placeholder;
            else
                return R.drawable.female_placeholder;
        }
        return 0;
    }

    public ProfileModel getSelfProfile() {
        return selfProfile;
    }

    public void setSelfProfile(ProfileModel selfProfile) {
        this.selfProfile = selfProfile;
        placeHolder = selfProfile.getGender();
    }

    private ProfileModel selfProfile;


    public static ServiceSingleton getInstance() {
        return ourInstance;
    }

    private ServiceSingleton() {
    }

    public boolean isChatWithMsgServiceRunning() {
        return chatWithMsg;
    }

    public void setChatWithMsgServiceRunning(boolean chatWithMsg) {
        this.chatWithMsg = chatWithMsg;
    }

    public boolean isChatWithoutMsgServiceRunning() {
        return chatWithoutMsg;
    }

    public void setChatWithoutMsgServiceRunning(boolean chatWithoutMsg) {
        this.chatWithoutMsg = chatWithoutMsg;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }


    public interface ServiceCallBacks {
        void onServiceComplete(Object object);
    }

    public BaseModel getModel() {
        return model;
    }

    public void setModel(BaseModel model) {
        this.model = model;
    }

    public BaseModel getSecModel() {
        return secModel;
    }

    public void setSecModel(BaseModel secModel) {
        this.secModel = secModel;
    }

    public int getSelfPlaceholder() {
        return selfPlaceholder;
    }

    public void setSelfPlaceholder(int selfPlaceholder) {
        this.selfPlaceholder = selfPlaceholder;
    }
}
