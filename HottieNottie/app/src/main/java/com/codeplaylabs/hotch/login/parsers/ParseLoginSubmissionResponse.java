package com.codeplaylabs.hotch.login.parsers;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.utils.CommonMethods;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.database.tables.FilterTable;
import com.codeplaylabs.hotch.home.handler.HomeDBHandlerClass;
import com.codeplaylabs.hotch.login.models.FBUserProfileResponseModel;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONObject;

/**
 * Created by mohit on 22/06/17.
 */

public class ParseLoginSubmissionResponse extends BaseParser {
    BaseModel baseModel  = new BaseModel();
    @Override
    public BaseModel parseJson(JSONObject response) {
        if (validate(response) != null) {
            return moduleJson(response);
        } else {
            baseModel.error = Constants.Error.NULL_RESPONSE_ERROR;
            return baseModel;
        }
    }

    /*
      * This method is called to collect the data received from the servers in a class for the session
      * @param response
     */
    public FBUserProfileResponseModel moduleJson(JSONObject response) {


        CustomJsonObject cjObject = new CustomJsonObject(response);
        FBUserProfileResponseModel profile = new FBUserProfileResponseModel();
        cjObject = cjObject.getCustomJsonObject("data");
        profile.setBlocked(cjObject.getBoolean("isBlocked", false));
        profile.setNewUser(cjObject.getBoolean("isNewUser", false));
        profile.setUserId(cjObject.getString("userId"));
       /* profile.setXmppUsername(cjObject.getString("xmppUsername"));
        profile.setXmppPassword(cjObject.getString("xmppPassword"));*/
        profile.setCurrentlyDetailsChanged(cjObject.getBoolean("isCurrentlyDetailsChanged",false));

        HomeDBHandlerClass.clearTable(HomeDBHandlerClass.dataBaseTables.filterTable);
        String key = CommonMethods.getStringFiltersUniqueKey(cjObject.getString("filters").toString());
        HomeDBHandlerClass.saveDataInTable(HomeDBHandlerClass.dataBaseTables.filterTable,new FilterTable()
                .setFilter(cjObject.getString("filters").toString())
                .setFilterKey(key)
                .setTimestamp(System.currentTimeMillis()));
        return profile;

    }

}
