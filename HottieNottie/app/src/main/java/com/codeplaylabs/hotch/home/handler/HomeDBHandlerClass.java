package com.codeplaylabs.hotch.home.handler;

import android.util.Log;

import com.codeplaylabs.hotch.database.databaseInitialization.DatabaseHelper;
import com.codeplaylabs.hotch.database.tables.BaseTable;
import com.codeplaylabs.hotch.database.tables.FilterTable;
import com.codeplaylabs.hotch.database.tables.ProfileTable;
import com.codeplaylabs.hotch.database.tables.SuperLikedTable;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.Singleton;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mohit on 06/07/17.
 */

public class HomeDBHandlerClass {


    private static String tag="HomeDbHandler";

    public enum dataBaseTables {profileTable, filterTable}

    public static boolean saveDataInTable(dataBaseTables tableName, BaseTable baseTable) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        switch (tableName) {
            case profileTable: {

                try {
                    Dao<ProfileTable, Integer> profileDao = databaseHelper.getProfileDao();
                    profileDao.createOrUpdate((ProfileTable) baseTable);
                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            case filterTable: {
                try {
                    clearTable(dataBaseTables.filterTable);
                    Dao<FilterTable, Integer> profileDao = databaseHelper.getFilterDao();
                    profileDao.create((FilterTable) baseTable);
                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            default: {
                return false;
            }
        }

    }

    public static boolean clearTable(dataBaseTables tableName) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        switch (tableName) {
            case filterTable: {
                try {
                    Dao<FilterTable, Integer> filterDao = databaseHelper.getFilterDao();
                    filterDao.delete(filterDao.queryForAll());
                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            }
            case profileTable: {
                try {
                    Dao<ProfileTable, Integer> profileDao = databaseHelper.getProfileDao();
                    profileDao.delete(profileDao.queryForAll());
                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            }

        }
        return false;
    }

    public static boolean deleteProfilesFromDb(long timestamp) {
        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<ProfileTable, Integer> profileDao = databaseHelper.getProfileDao();

            DeleteBuilder<ProfileTable, Integer> builder = profileDao.deleteBuilder();
            builder.where().eq("is_synced", true).and().lt("timestamp", timestamp);
            int build = builder.delete();
            if (build > 0) {
                return true;
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    public static List<ProfileTable> syncProfilesWithServers(boolean forceSync) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<ProfileTable, Integer> profileDao = databaseHelper.getProfileDao();

            QueryBuilder<ProfileTable, Integer> builder = profileDao.queryBuilder();
            builder.where().not().eq("type", ProfileTable.Type.NOT_SEEN).and().eq("is_synced", false);
            PreparedQuery query = builder.prepare();
                   /* builder.where().eq("is_synced", false).and().eq("profile_taken", false).and().not().eq("type", ProfileTable.Type.NOT_SEEN);*/
            List<ProfileTable> profileTables = profileDao.query(query);
            //testMethod(getAllProfilesFromTable(null));
            if (forceSync || profileTables.size() >= Constants.ServiceConfiguration.SYNC_NUMBER_OF_PROFILES) {
                return profileTables;
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }


    }

    public static List<ProfileTable> getAllProfilesFromTable(HashMap<String, Object> params) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<ProfileTable, Integer> profileDao = databaseHelper.getProfileDao();
            if (params != null) {
                return profileDao.queryForFieldValues(params);
            } else {
                return profileDao.queryForAll();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static FilterTable getFilterFromTable() {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<FilterTable, Integer> filterDao = databaseHelper.getFilterDao();
            List<FilterTable> val = filterDao.queryForAll();
            if (val != null && val.size() > 0) {
                return val.get(0);
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static ProfileTable getFirstProfileFromTable(HashMap<String, String> params) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<ProfileTable, Integer> profileDao = databaseHelper.getProfileDao();
            QueryBuilder<ProfileTable, Integer> builder = profileDao.queryBuilder();
            builder.where().eq("filter", params.get("filter")).and().eq("type", params.get("type")).and().eq("profile_taken", Boolean.valueOf(params.get("profile_taken")));
            ProfileTable profileTable = profileDao.queryForFirst(builder.prepare());

            if (profileTable != null) {

                updateProfile(profileTable.setProfileTaken(String.valueOf(true)));

            }

            return profileTable;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static long getCountOfProfilesFromTable(HashMap<String, String> params) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<ProfileTable, Integer> profileDao = databaseHelper.getProfileDao();
            QueryBuilder<ProfileTable, Integer> builder = profileDao.queryBuilder();
            builder.where().eq("filter", params.get("filter")).and().eq("type", params.get("type")).and().eq("profile_taken", Boolean.valueOf(params.get("profile_taken")));
            builder.setCountOf(true);
            long count = profileDao.countOf(builder.prepare());
            Log.e("profile", count + "");
            return count;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void updateProfile(ProfileTable profileTable) {
        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<ProfileTable, Integer> userProfileDao = databaseHelper.getProfileDao();
            userProfileDao.update(profileTable);
            // testMethod(getAllProfilesFromTable(null));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ProfileTable getLastSeenProfile(HashMap<String, String> params) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<ProfileTable, Integer> profileDao = databaseHelper.getProfileDao();
            QueryBuilder<ProfileTable, Integer> builder = profileDao.queryBuilder();
            builder.where().not().eq("type", params.get("type")).and().eq("profile_taken", Boolean.valueOf(params.get("profile_taken"))).and().lt("timestamp", Long.valueOf(params.get("timestamp"))).and().gt("timestamp", 0);
            builder.orderBy("timestamp", false);
            PreparedQuery query = builder.prepare();
            ProfileTable profileTable = profileDao.queryForFirst(query);

            return profileTable;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void testMethod(List<ProfileTable> profiles) {
        for (ProfileTable profileTable :
                profiles) {
            Log.e("profile", profileTable.getType() + " " + profileTable.isProfileTaken() + " " + profileTable.id + " " + profileTable.getFilter());
        }
    }

    //save sp_like
    public static void saveSpLikes(ProfileModel model, String img1url) {
        DatabaseHelper helper = Singleton.getInstance().getHelper();
        Dao<SuperLikedTable, String> spLikeDao = helper.getSpLikedDao();

        int year = 0;
        if (model.getYearOfBirthday() == null || model.getYearOfBirthday().length() == 0) {
            year = (new Date().getYear() + 1900);
        } else {
            year = (new Date().getYear() + 1900) - Integer.parseInt(model.getYearOfBirthday());
        }
        img1url=SuperLikedTable.getUrlForImgSize(UserImage.ImgType.SIZE_200x300,img1url);
        SuperLikedTable like = new SuperLikedTable(model.getId(), img1url, model.getName(), "" + year, model.getLocation().getCountry(),model.getGender(),model.isMatch(),new Date().getTime());
        try {
            spLikeDao.create(like);
            Log.e("HomeDbHandler", "saveSpLikes() executed");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static List<SuperLikedTable> fetchSpLikes() {
        DatabaseHelper helper = Singleton.getInstance().getHelper();
        Dao<SuperLikedTable, String> spLikeDao = helper.getSpLikedDao();

        try {
            QueryBuilder<SuperLikedTable,String> qb=spLikeDao.queryBuilder();
            qb.orderBy("timestamp",false);
            return spLikeDao.query(qb.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void delSpLikes(String id){
        DatabaseHelper helper = Singleton.getInstance().getHelper();
        Dao<SuperLikedTable, String> spLikeDao = helper.getSpLikedDao();

        DeleteBuilder<SuperLikedTable,String> del_b=spLikeDao.deleteBuilder();
        try {
            del_b.where().eq("id",id);
            spLikeDao.delete(del_b.prepare());
            Log.e(tag,"spLike rec deleted id : "+id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
