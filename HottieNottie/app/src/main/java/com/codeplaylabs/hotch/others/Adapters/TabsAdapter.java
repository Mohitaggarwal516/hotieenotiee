package com.codeplaylabs.hotch.others.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by HP on 17-Aug-17.
 */

public class TabsAdapter extends FragmentPagerAdapter {

    ArrayList<Fragment> fragList=new ArrayList<>();
    ArrayList<String> titleList=new ArrayList<>();

    public TabsAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragInfo(Fragment fragment,String title){
        fragList.add(fragment);
        titleList.add(title);
    }

    @Override
    public Fragment getItem(int position) {
        return fragList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);    }

    @Override
    public int getCount() {
        return fragList.size();
    }

}
