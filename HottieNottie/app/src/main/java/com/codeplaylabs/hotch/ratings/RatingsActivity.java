package com.codeplaylabs.hotch.ratings;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseActivity;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.interfaces.SharedProfileResponse;
import com.codeplaylabs.hotch.login.services.FacebookGraphService;
import com.codeplaylabs.hotch.others.models.ProfileWrapper;
import com.codeplaylabs.hotch.profileDetail.ui.ProfileDetailActivity;
import com.codeplaylabs.hotch.ratings.adapters.RatingPagerAdapter;
import com.codeplaylabs.hotch.ratings.adapters.RecyclerAdapter;
import com.codeplaylabs.hotch.ratings.interfaces.CallbackInterface;
import com.codeplaylabs.hotch.ratings.interfaces.PassProfileWrapper;
import com.codeplaylabs.hotch.ratings.interfaces.RecyclerCellClicked;
import com.codeplaylabs.hotch.ratings.interfaces.RateRecyclerCallBack;
import com.codeplaylabs.hotch.ratings.models.RateDiscoveryWrapper;
import com.codeplaylabs.hotch.settings.models.albums.discovery.DiscoverySettingModel;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LogAnalytics;
import com.codeplaylabs.hotch.utils.ServiceSingleton;
import com.codeplaylabs.hotch.utils.TextViewPlus;
import com.rd.PageIndicatorView;
import com.rd.animation.type.AnimationType;

public class RatingsActivity extends BaseActivity {

    DiscoverySettingModel discoveryModel;
    ViewPager viewPager;

    RecyclerView.LayoutManager layoutManager;
    //RecyclerAdapter rAdapter;
    RatingPagerAdapter pgAdapter;
    //ProfileWrapper profileWrapper;

    RateDiscoveryWrapper disWrapper;
    TextViewPlus male, female;
    RelativeLayout loader;
    public static final String MALE = "male";
    public static final String FEMALE = "female";
    int pagerPosition = 0;
    //private int pageNo = 0;
    PageIndicatorView pageIndicatorView;
    RateRecyclerCallBack rateRecyclerCallBack;
    //ArrayList<ProfileModel> profileModelList=new ArrayList<>();

//    public enum filterEnum {Random, CurrentCountry, CurrentLocation, CurrentlyAt}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_rate);
        super.onCreate(savedInstanceState);
        viewPager = (ViewPager) findViewById(R.id.viewpager_rate_test);
        male = (TextViewPlus) findViewById(R.id.male);
        female = (TextViewPlus) findViewById(R.id.female);
        loader = (RelativeLayout) findViewById(R.id.loader_layout);
        disWrapper = (RateDiscoveryWrapper) getIntent().getSerializableExtra("rateDisWrapper");

        final RelativeLayout oneTimeView = (RelativeLayout) findViewById(R.id.one_time_view);
        com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().initialise(this);
        if (com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().getBooleanPreference(Constants.ONE_TIME_VIEW_RATING, true)) {
            com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().setPreference(Constants.ONE_TIME_VIEW_RATING, false);
            oneTimeView.setVisibility(View.VISIBLE);
        } else {
            oneTimeView.setVisibility(View.GONE);
        }

        oneTimeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oneTimeView.setVisibility(View.GONE);
            }
        });

        /*discoveryModel = (DiscoverySettingModel) getIntent().getSerializableExtra("discoveryModel");
        profileWrapper = (ProfileWrapper) getIntent().getSerializableExtra("ratingWrapper");*/

        pageIndicatorView = (PageIndicatorView) findViewById(R.id.pageIndicatorView);

        pageIndicatorView.setAnimationDuration(10000);
        pageIndicatorView.setAnimationType(AnimationType.DROP);
        pageIndicatorView.setInteractiveAnimation(true);
        pageIndicatorView.setRadius(4);
        pageIndicatorView.setSelectedColor(getResources().getColor(R.color.colorPrimary));
        pageIndicatorView.setUnselectedColor(getResources().getColor(R.color.studioGrey));

        onMaleClicked();
        onFemaleClicked();
        String gender = ServiceSingleton.getInstance().getSelfProfile().getGender();
        if (gender.equalsIgnoreCase(MALE)) {
            toggleGender(true, false);
        } else {
            toggleGender(false, false);
        }
        setPagerAdapter(disWrapper, gender/*MALE*/);
    }

    private void onMaleClicked() {
        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogAnalytics.logButtonClickEvent(mTracker,Constants.Buttons.RATINGS_MALE_CLICKED);
                toggleGender(true, false);
                setPagerAdapter(disWrapper, MALE);
            }
        });
    }

    private void onFemaleClicked() {
        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogAnalytics.logButtonClickEvent(mTracker,Constants.Buttons.RATINGS_FEMALE_CLICKED);
                toggleGender(false, true);
                setPagerAdapter(disWrapper, FEMALE);
            }
        });
    }

    private void toggleGender(boolean mSelected, boolean fSelected) {
        if (mSelected) {
            male.setBackground(getResources().getDrawable(R.drawable.settings_toggle_left_selected_bg));
            male.setTextColor(getResources().getColor(R.color.colorPrimary));
            female.setBackground(getResources().getDrawable(R.drawable.settings_toggle_right_unselected_bg));
            female.setTextColor(getResources().getColor(R.color.white));
        } else {
            female.setBackground(getResources().getDrawable(R.drawable.settings_toggle_right_selected_bg));
            female.setTextColor(getResources().getColor(R.color.colorPrimary));
            male.setBackground(getResources().getDrawable(R.drawable.settings_toggle_left_unselected_bg));
            male.setTextColor(getResources().getColor(R.color.white));
        }
    }

    private void setPagerAdapter(RateDiscoveryWrapper disWrapper, final String gender/*DiscoverySettingModel discoveryModel*/) {

        pgAdapter = new RatingPagerAdapter(RatingsActivity.this, disWrapper/*detailList, profileWrapper, */, gender, new CallbackInterface() {
            @Override
            public void passRecyclerView(final RecyclerView recyclerView,
                                         final RateDiscoveryWrapper.filterEnum filterEnum,
                                         String id, /*ArrayList<ProfileModel> pgrProfileModelList,*/
                                         final PassProfileWrapper passProfileWrapper) {
                int pageNo = 0;
                final ProfileWrapper wrapper = new ProfileWrapper();
                final RecyclerAdapter mAdapter = null;

                callRatingService(filterEnum, gender, id, wrapper,
                        passProfileWrapper, recyclerView, mAdapter);
            }


        });
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pagerPosition = position;
                // Log.w("viewPager", " pageSelected pos : " + pagerPosition);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        //Log.w("viewPager", " after set Current Item with pg position " + pagerPosition);


        viewPager.setAdapter(pgAdapter);
        pageIndicatorView.setViewPager(viewPager);
        //viewPager.setOffscreenPageLimit(disWrapper.getDiscoveryWrapper().size());
        viewPager.setCurrentItem(pagerPosition, false);


    }

    private void callRatingService(final RateDiscoveryWrapper.filterEnum filterEnum,
                                   final String gender, final String id,
                                   final ProfileWrapper wrapper,
                                   final PassProfileWrapper passProfileWrapper,
                                   final RecyclerView recyclerView,
                                   final RecyclerAdapter mAdapter
                                   /*final ArrayList<ProfileModel> pgrProfileModelList*/) {
        //loader.setVisibility(View.VISIBLE);
        new FacebookGraphService().ratingService(RatingsActivity.this, filterEnum, wrapper.getPageNo()/*page*/,
                gender/*"male"*/, true, id, new SharedProfileResponse() {
                    @Override
                    public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                       /* loader.post(new Runnable() {
                            @Override
                            public void run() {
                                loader.setVisibility(View.GONE);
                            }
                        });*/

                        if (reasonOfError == null) {
                            final ProfileWrapper localWrapper = (ProfileWrapper) baseModel;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (wrapper.getPageNo() == 0) {
                                        //to set user's rank
                                        passProfileWrapper.onProfilWrapperPassed(localWrapper);
                               /* if (localWrapper != null) {
                                    if (localWrapper.getSelfGender().equalsIgnoreCase(MALE)) {
                                        toggleGender(true, false);
                                        gender = MALE;
                                    } else {
                                        toggleGender(false, true);
                                    }
                                }*/
                                    }
                                    if (localWrapper.getProfileWrapper().size() > 0) {

                                        wrapper.setPageNo(localWrapper.getPageNo() + 1);
                                        wrapper.getProfileWrapper().addAll(localWrapper.getProfileWrapper());
                                        //profileModelList.addAll(wrapper.getProfileWrapper());
                                        //pgrProfileModelList.addAll(localWrapper.getProfileWrapper());
                                        //pageNo++;
                                        setRecyclerAdapter(wrapper/*pgrProfileModelList*//*profileModelList*//*wrapper*/, filterEnum, id, recyclerView, gender, mAdapter);
                                    } else {
                                        if (passProfileWrapper != null)
                                            passProfileWrapper.onWrapperSizeZero();
                                        if (rateRecyclerCallBack !=null) {
                                            rateRecyclerCallBack.setBottomLoaderVisibility(false);
                                            rateRecyclerCallBack =null;
                                        }
                                    }
                                }
                            });

                        } else if (jsonString == null) {
                            showNoNetDialog();
                        }
                    }
                });
    }

    private void setRecyclerAdapter(final ProfileWrapper/*ArrayList<ProfileModel>*/ profileModelList/*ProfileWrapper wrapper*/
            , final RateDiscoveryWrapper.filterEnum filterEnum,
                                    final String id, final RecyclerView recyclerView,
                                    final String gender,
                                    RecyclerAdapter mAdapter) {

        layoutManager = new LinearLayoutManager(RatingsActivity.this);
        if (profileModelList.getPageNo() == 0 || mAdapter == null) {

            mAdapter = new RecyclerAdapter(RatingsActivity.this, profileModelList.getProfileWrapper()/*wrapper*/, gender, filterEnum, new RecyclerCellClicked() {
                @Override
                public void onCellClicked(ProfileModel profileModel/*String id*/) {
                    //calling descriptionService
                    Intent intent = new Intent(RatingsActivity.this, ProfileDetailActivity.class);
                    intent.putExtra("profile", profileModel);
                    intent.putExtra("from", "" + ProfileDetailActivity.From.RATING);
                    intent.putExtra("CallDescription", true);
                    startActivityForResult(intent, 2);
                    /*new FacebookGraphService().profileDescriptionService(RatingsActivity.this, id, "", new SharedProfileResponse() {
                        @Override
                        public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                            if (reasonOfError == null) {
                                ProfileWrapper pWrapper = (ProfileWrapper) baseModel;
                                Intent intent = new Intent(RatingsActivity.this, ProfileDetailActivity.class);
                                intent.putExtra("profile", pWrapper.getProfileWrapper().get(0));
                                intent.putExtra("from", "" + ProfileDetailActivity.From.RATING);
                                startActivity(intent);
                            }
                        }
                    });*/
                }

                @Override//for making a call to service for next page
                public void callNextPg(int page, RecyclerAdapter recyclerAdapter, RateRecyclerCallBack callBack/*RateDiscoveryWrapper.filterEnum filterEnum, String id*/) {
                    rateRecyclerCallBack =callBack;
                    callRatingService(filterEnum, gender, id, profileModelList, null, recyclerView, recyclerAdapter);
                    //callNextPgService(page, filterEnum, id, gender, recyclerView, profileModelList, setPgInterface);
                }
            });

            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(mAdapter);

        } else {
            // int firstVisiblePos = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
            mAdapter.notifyDataSetChanged();
            // recyclerView.scrollToPosition(firstVisiblePos);
        }
    }

//    private void callNextPgService(final int page, final RateDiscoveryWrapper.filterEnum filterEnum, final String id, final String gender, final RecyclerView recyclerView, final ArrayList<ProfileModel> pgrProfileModelList, final SetPageNoInterface setPgInterface) {
//        new FacebookGraphService().ratingService(RatingsActivity.this, filterEnum, page, gender, false, id, new SharedProfileResponse() {
//            @Override
//            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
//                if (reasonOfError == null) {
//                    ProfileWrapper wrapper = (ProfileWrapper) baseModel;
//                    //to set user's rank
//                    // passProfileWrapper.onProfilWrapperPassed(wrapper);
//
//                    if (wrapper.getProfileWrapper().size() > 0) {
//                        //profileModelList.addAll(wrapper.getProfileWrapper());
//                        pgrProfileModelList.addAll(wrapper.getProfileWrapper());
//                        int val = page + 1;
//                        setPgInterface.setBottomLoaderVisibility(val);
//                        //pageNo++;
//                        setRecyclerAdapter(pgrProfileModelList/*wrapper*/, filterEnum, id, recyclerView, gender);
//                    }
//                }
//            }
//        });
//    }

    @Override
    protected void onTryAgainClicked() {
        // disWrapper = (RateDiscoveryWrapper) getIntent().getSerializableExtra("rateDisWrapper");
        dialog.dismiss();
        dialog=null;
        Intent intent = new Intent(RatingsActivity.this, RatingsActivity.class);
        intent.putExtra("rateDisWrapper",disWrapper);
        startActivity(intent);
        finish();
        super.onTryAgainClicked();
    }

    @Override
    protected void onResume() {
        LogAnalytics.logScreenView(mTracker,Constants.ActivityName.RATINGS_SCREEN);
        super.onResume();
    }
}
