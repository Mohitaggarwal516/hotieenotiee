package com.codeplaylabs.hotch.login.parsers;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.login.models.Detail;
import com.codeplaylabs.hotch.login.models.Education;
import com.codeplaylabs.hotch.login.models.UserProfile;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.login.models.Work;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONArray;
import org.json.JSONObject;


/**
 * Created by mohit on 1/12/2017.
 */
public class ParseFacebookDataResponse extends BaseParser {

    BaseModel baseModel = new BaseModel();


    @Override
    public BaseModel parseJson(JSONObject response) {
        return moduleJson(response);
    }

    /*
      * This method is called to collect the data received from the servers in a class for the session
      * @param response
     */
    public BaseModel moduleJson(JSONObject response) {
        UserProfile userProfile = new UserProfile();

        if (response != null) {
            CustomJsonObject cjObject = new CustomJsonObject(response);
            userProfile.setId(cjObject.getString("id"));
            userProfile.setName(cjObject.getString("name"));
            userProfile.setEmail(cjObject.getString("email"));
            userProfile.setGender(cjObject.getString("gender"));
            userProfile.setBirthday(cjObject.getString("birthday"));
            userProfile.setAbout(cjObject.getString("about"));
            CustomJsonObject picture = new CustomJsonObject(cjObject.getJsonObject("picture"));
            CustomJsonObject data = new CustomJsonObject(picture.getJsonObject("data"));
            userProfile.setProfileImageUrl(data.getString("url"));
            JSONArray educationArray = cjObject.getJsonArray("education");
            //Education education1 = null;
            try {


                for (int index = 0; index < educationArray.length(); index++) {
                    CustomJsonObject educationObject = new CustomJsonObject(educationArray.getJSONObject(index));
                    CustomJsonObject school = new CustomJsonObject(educationObject.getJsonObject("school"));
                    Education education = new Education();
                    education.setFacebookId(educationObject.getString("id"));
                    education.setType(educationObject.getString("type"));
                    Detail institute = new Detail();
                    institute.setId(school.getString("id"));
                    institute.setName(school.getString("name"));
                    education.setInstitute(institute);
                    education.setYear(new CustomJsonObject(educationObject.getJsonObject("year")).getString("name"));
                    userProfile.getEducationList().add(education);
                }
            }catch (Exception e){

            }
            //education1.setCurrentlyStudingHere(true);
            try {
                JSONArray workArray = cjObject.getJsonArray("work");
                for (int index = 0; index < workArray.length(); index++) {
                    CustomJsonObject workObject = new CustomJsonObject(workArray.getJSONObject(index));
                    CustomJsonObject employerJson = new CustomJsonObject(workObject.getJsonObject("employer"));

                    Work work = new Work();
                    work.setEndDate(workObject.getString("end_date"));
                    work.setFacebookId(workObject.getString("id"));
                    work.setStartDate(workObject.getString("start_date"));
                    CustomJsonObject position = new CustomJsonObject(workObject.getJsonObject("position"));
                    work.setPosition(position.getString("name"));
                    work.setPositionFacebookId(position.getString("id"));

                    Detail employer = new Detail();
                    employer.setId(employerJson.getString("id"));
                    employer.setName(employerJson.getString("name"));
                    work.setEmployer(employer);

                    CustomJsonObject locationJson = new CustomJsonObject(workObject.getJsonObject("location"));
                    Detail location = new Detail();
                    location.setId(locationJson.getString("id"));
                    location.setName(locationJson.getString("name"));
                    work.setLocation(location);

                    userProfile.getWorkList().add(work);
                }
            }catch (Exception e){

            }
            CustomJsonObject locationJson = new CustomJsonObject(cjObject.getJsonObject("location"));
            Detail location = new Detail();
            try {
                location.setId(locationJson.getString("id"));
                location.setName(locationJson.getCustomJsonObject("location").getString("city"));
                location.setCountry(locationJson.getCustomJsonObject("location").getString("country"));
                location.setCountryCode(locationJson.getCustomJsonObject("location").getString("country_code"));
                userProfile.setLocation(location);
            } catch (Exception e) {
                userProfile.setLocation(location);
            }
            CustomJsonObject hometownJson = new CustomJsonObject(cjObject.getJsonObject("hometown"));
            Detail hometown = new Detail();
            try {
                hometown.setId(hometownJson.getString("id"));
                hometown.setName(hometownJson.getCustomJsonObject("location").getString("city"));
                hometown.setCountry(hometownJson.getCustomJsonObject("location").getString("country"));
                hometown.setCountryCode(hometownJson.getCustomJsonObject("location").getString("country_code"));
                userProfile.setHometown(hometown);
            } catch (Exception e) {
                userProfile.setHometown(hometown);
            }

            return userProfile;
        }
        baseModel.error = Constants.Error.NULL_RESPONSE_ERROR;
        return baseModel;
    }

}
