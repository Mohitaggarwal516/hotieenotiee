package com.codeplaylabs.hotch.base;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codeplaylabs.hotch.MyApplication;
import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.utils.TextViewPlus;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by mohit on 30/06/17.
 */

public class BaseFragment extends Fragment {

    protected LayoutInflater inflater;
    protected android.app.AlertDialog dialog;
    protected Tracker mTracker;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mTracker= MyApplication.getInstance().getTracker();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    protected void showNoNetDialogFrag(final Context context) {
        if (inflater == null)
            inflater = ((Activity)context).getLayoutInflater();
        View view = inflater.inflate(R.layout.dg_network_not_available, null, false);
        //CustomDialog dialog = new CustomDialog.CustomBuilder(this).create();
        if (dialog == null)
            dialog = new android.app.AlertDialog.Builder(context).create();
        //to listen back pressed while dg's at focus instead of an activity.
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK &&
                        event.getAction() == KeyEvent.ACTION_UP &&
                        !event.isCanceled()) {
                    //dialog.cancel();
                    ((Activity)context).finish();
                    //showDialog(DIALOG_MENU);
                    //Toast.makeText(BaseActivity.this, "back listened", Toast.LENGTH_LONG).show();
                    return true;
                }
                return false;
            }
        });
        dialog.setView(view);

        dialog.setCancelable(false);
        if (!((Activity)context).isFinishing()){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
        }

        TextViewPlus tryAgain = (TextViewPlus) view.findViewById(R.id.tryAgain);
        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTryAgainClicked();
            }
        });
    }

    //override as per requirement
    protected void onTryAgainClicked() {

    }
    /*protected void showCustomDialog(String title, String message, final int tag, String positiveBtnText, String negativeBtnText) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setPositiveButton(positiveBtnText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //onShowAlertOkPressed(tag);
            }
        });
        dialog.setNegativeButton(negativeBtnText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    protected void onShowAlertNotOkPressed(int tag) {

    }


    protected void onShowAlertOkPressed(int tag){

    }*/

}
