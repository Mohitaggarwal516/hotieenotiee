package com.codeplaylabs.hotch.settings.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AlignmentSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.rangebar.RangeBar;
import com.codeplaylabs.hotch.BuildConfig;
import com.codeplaylabs.hotch.login.ui.WebViewActivity;
import com.codeplaylabs.hotch.settings.interfaces.InformClickToActivity;
import com.codeplaylabs.hotch.utils.CommonMethods;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.database.tables.FilterTable;
import com.codeplaylabs.hotch.home.handler.HomeDBHandlerClass;
import com.codeplaylabs.hotch.home.models.FilterModel;
import com.codeplaylabs.hotch.settings.interfaces.DiscoveryResponse;
import com.codeplaylabs.hotch.settings.models.albums.discovery.DiscoverySettingModel;
import com.codeplaylabs.hotch.home.parsers.ParseFilter;
import com.codeplaylabs.hotch.settings.services.SettingsService;
import com.codeplaylabs.hotch.settings.ui.SettingsActivity;
import com.facebook.login.LoginManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by mohit on 26/06/17.
 */

public class DiscoveryTabFragment extends Fragment {

    private TextView highAge;
    private TextView lowAge;
    private SettingsActivity parent;
    private SwitchCompat interestedInMen;
    private SwitchCompat interestedInWomen;
    private SwitchCompat notificationNewMatches;
    private SwitchCompat notificationMessages;
    private SwitchCompat notificationMessageslikes;
    private SwitchCompat notificationSuperLikes;
    private TextView shareMyUrl;
    private TextView helpSupport;
    private TextView shareUs;
    private TextView restorePurchases;
    private TextView legal;
    private TextView privacyPolicy;
    private TextView termsofServices;
    private TextView logout;
    private DiscoverySettingModel discoverySettingModel;
    private RangeBar rangeBar;
    private SwitchCompat randomProfiles;
    private SwitchCompat currentCityProfiles;
    private SwitchCompat currentCountryProfiles;
    private SwitchCompat currentLocationProfiles;
    private SwitchCompat hometownProfiles;
    public boolean valueUpdated = false;
    private boolean notificationValueUpdated = false;
    private FilterModel filterModel = null;
    private InformClickToActivity clickToActivity;
    private Context context;

    @Override
    public void onAttach(Context context) {
        this.context=context;
        super.onAttach(context);
    }

    @SuppressLint("ValidFragment")
    public DiscoveryTabFragment(SettingsActivity parent, InformClickToActivity clickToActivity) {
        this.parent = parent;
        this.clickToActivity = clickToActivity;
    }

    public DiscoveryTabFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_discovery_settings_view, container, false);
        parent.informActivityToCallService();
        rangeBar = (RangeBar) view.findViewById(R.id.age_range_bar);
        lowAge = (TextView) view.findViewById(R.id.low_age);
        highAge = (TextView) view.findViewById(R.id.high_age);

        interestedInMen = (SwitchCompat) view.findViewById(R.id.interested_in_men);
        interestedInWomen = (SwitchCompat) view.findViewById(R.id.interested_in_women);

        randomProfiles = (SwitchCompat) view.findViewById(R.id.from_globe);
        currentCityProfiles = (SwitchCompat) view.findViewById(R.id.from_curr_city);
        currentCountryProfiles = (SwitchCompat) view.findViewById(R.id.from_curr_country);
        currentLocationProfiles = (SwitchCompat) view.findViewById(R.id.from_curr_location);
        hometownProfiles = (SwitchCompat) view.findViewById(R.id.from_my_hometown);

        notificationNewMatches = (SwitchCompat) view.findViewById(R.id.notification_new_matches);
        notificationMessages = (SwitchCompat) view.findViewById(R.id.notification_messages);
        notificationMessageslikes = (SwitchCompat) view.findViewById(R.id.notification_messages_likes);
        notificationSuperLikes = (SwitchCompat) view.findViewById(R.id.notification_super_likes);

        shareMyUrl = (TextView) view.findViewById(R.id.share_my_url);
        helpSupport = (TextView) view.findViewById(R.id.help_support);
        shareUs = (TextView) view.findViewById(R.id.share_us);
        restorePurchases = (TextView) view.findViewById(R.id.restore_purchases);
        legal = (TextView) view.findViewById(R.id.legal);
        privacyPolicy = (TextView) view.findViewById(R.id.privacy_policy);
        termsofServices = (TextView) view.findViewById(R.id.terms_of_services);
        logout = (TextView) view.findViewById(R.id.logout);
        TextView version = (TextView ) view.findViewById(R.id.ver);
        version.setText("v"+ BuildConfig.VERSION_NAME);


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logOut();
                parent.onLogoutClicked();
            }
        });

        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), WebViewActivity.class);

                myIntent.putExtra("url", "http://gohotch.com/terms/");
                //  myIntent.putExtra("url", "http://codeplaylabs.com/HottiePrivacy.html");

                startActivity(myIntent);
            }
        });

        termsofServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), WebViewActivity.class);

                //myIntent.putExtra("url", "http://codeplaylabs.com/Hottieterms.html");
                myIntent.putExtra("url", "http://gohotch.com/privacy/");

                startActivity(myIntent);
            }
        });

        helpSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), WebViewActivity.class);

                //myIntent.putExtra("url", "http://codeplaylabs.com/Hottieterms.html");
                myIntent.putExtra("url", "http://gohotch.com/help/");

                startActivity(myIntent);
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        boolean bool = false;
        if(parent.fbSynced){
            bool = true;
        }
        new SettingsService().getDiscoverySetting(getActivity(), bool, new DiscoveryResponse() {
            @Override
            public void onResponse(final BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                if (baseModel != null) {
                    /*getActivity()*/((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            discoverySettingModel = (DiscoverySettingModel) baseModel;

                            rangeBar.setTickStart(Integer.parseInt(discoverySettingModel.getMinAge()));
                            rangeBar.setTickEnd(Integer.parseInt(discoverySettingModel.getMaxAge()));
                            rangeBar.setTickInterval(1);

                            for (String gender : discoverySettingModel.getGender()
                                    ) {
                                if (gender.equalsIgnoreCase(interestedInMen.getTag().toString())) {
                                    interestedInMen.setVisibility(View.VISIBLE);
                                } else if (gender.equalsIgnoreCase(interestedInWomen.getTag().toString())) {
                                    interestedInWomen.setVisibility(View.VISIBLE);
                                }
                            }

                            if (discoverySettingModel.getCurrentLocation() == null) {
                                currentCityProfiles.setVisibility(View.GONE);
                                currentCityProfiles.setChecked(false);
                            } else {
                                currentCityProfiles.setText(getResources().getString(R.string.my_curr_city) + " " + discoverySettingModel.getCurrentLocation().getName());
                            }
                            if (discoverySettingModel.getCountry() == null) {
                                currentCountryProfiles.setVisibility(View.GONE);
                                currentCountryProfiles.setChecked(false);
                            } else {
                                currentCountryProfiles.setText(getResources().getString(R.string.my_curr_country) + " " + discoverySettingModel.getCountry().getName());
                            }
                            if (discoverySettingModel.getCurrentlyAt() == null) {
                                currentLocationProfiles.setVisibility(View.GONE);
                                currentLocationProfiles.setChecked(false);
                            } else {
                                currentLocationProfiles.setText(getResources().getString(R.string.from_current_location) + " " + discoverySettingModel.getCurrentlyAt().getName());
                            }
                            if (discoverySettingModel.getHometown() == null) {
                                hometownProfiles.setVisibility(View.GONE);
                                hometownProfiles.setChecked(false);
                            } else {
                                hometownProfiles.setText(getResources().getString(R.string.my_home_town) + " " + discoverySettingModel.getHometown().getName());
                            }

                            if (!discoverySettingModel.getNotificationSetting().get("superHotNotification")) {
                                notificationSuperLikes.setChecked(false);
                            } else {
                                notificationSuperLikes.setChecked(true);
                            }
                            if (!discoverySettingModel.getNotificationSetting().get("newMatchNotification")) {
                                notificationNewMatches.setChecked(false);
                            } else {
                                notificationNewMatches.setChecked(true);
                            }
                    /*if(!discoverySettingModel.getNotificationSetting().get("promotionalNotification")){
                        notificationMessages.setVisibility(View.GONE);
                    }*/
                            if (!discoverySettingModel.getNotificationSetting().get("messageNotification")) {
                                notificationMessages.setChecked(false);
                            } else {
                                notificationMessages.setChecked(true);
                            }

                            setValues();
                            valueUpdated = false;
                            interestedInMen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    interestedInWomen.setChecked(!isChecked);
                                    if (isChecked) {
                                        filterModel.setGender(interestedInMen.getTag().toString());
                                    }
                                    valueUpdated = true;
                                }
                            });

                            interestedInWomen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    interestedInMen.setChecked(!isChecked);
                                    if (isChecked) {
                                        filterModel.setGender(interestedInWomen.getTag().toString());
                                    }
                                    valueUpdated = true;
                                }
                            });
                            randomProfiles.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    filterModel.setRandom(isChecked);
                                    valueUpdated = true;
                                    checkForRandomToggle();
                                }
                            });
                            currentCityProfiles.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    valueUpdated = true;
                                    if (isChecked) {
                                        filterModel.getCurrentlyAtLocations().add(discoverySettingModel.getCurrentLocation().getId());
                                    } else {
                                        filterModel.getCurrentlyAtLocations().remove(discoverySettingModel.getCurrentLocation().getId());
                                        checkForRandomToggle();
                                    }
                                }
                            });
                            currentCountryProfiles.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    valueUpdated = true;
                                    if (isChecked) {
                                        filterModel.setCurrentlyAtCountry(discoverySettingModel.getCountry().getId());
                                    } else {
                                        filterModel.setCurrentlyAtCountry("");
                                        checkForRandomToggle();
                                    }
                                }
                            });
                            currentLocationProfiles.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    valueUpdated = true;
                                    if (isChecked) {
                                        filterModel.setCurrentlyAtFbId(discoverySettingModel.getCurrentlyAt().getId());
                                    } else {
                                        filterModel.setCurrentlyAtFbId(null);
                                        checkForRandomToggle();
                                    }
                                }
                            });
                            hometownProfiles.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    valueUpdated = true;
                                    if (isChecked) {
                                        filterModel.getCurrentlyAtLocations().add(discoverySettingModel.getHometown().getId());
                                    } else {
                                        filterModel.getCurrentlyAtLocations().remove(discoverySettingModel.getHometown().getId());
                                        checkForRandomToggle();
                                    }
                                }
                            });
                            notificationNewMatches.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    notificationValueUpdated = true;
                                    discoverySettingModel.getNotificationSetting().put("newMatchNotification", isChecked);
                                }
                            });
                            notificationMessages.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    notificationValueUpdated = true;
                                    discoverySettingModel.getNotificationSetting().put("messageNotification", isChecked);
                                }
                            });
                            notificationSuperLikes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    notificationValueUpdated = true;
                                    discoverySettingModel.getNotificationSetting().put("superHotNotification", isChecked);
                                }
                            });
                        }
                    });

                }else{
                    Spannable centeredText = new SpannableString(getResources().getString(R.string.no_internet));
                    centeredText.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
                            0, getResources().getString(R.string.no_internet).length() - 1,
                            Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                    Toast.makeText(parent,centeredText,Toast.LENGTH_SHORT).show();
                    parent.finish();

                }
            }
        });

        rangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {


                valueUpdated = true;
                lowAge.setText("" + leftPinValue);
                highAge.setText("" + rightPinValue);
                if (discoverySettingModel != null){
                    if (Integer.parseInt(leftPinValue) < Integer.parseInt(discoverySettingModel.getMinAge())) {
                        lowAge.setText(discoverySettingModel.getMinAge());
                        rangeBar.setRangePinsByValue(Integer.parseInt(discoverySettingModel.getMinAge()), Integer.parseInt(rightPinValue));
                    }
                if (Integer.parseInt(rightPinValue) > Integer.parseInt(discoverySettingModel.getMaxAge())) {
                    highAge.setText(discoverySettingModel.getMaxAge());
                    rangeBar.setRangePinsByValue(Integer.parseInt(leftPinValue), Integer.parseInt(discoverySettingModel.getMaxAge()));
                }
            }
            }
        });

        shareMyUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //inform click to Activity
                // String imgUrl=Singleton.getInstance().getFbUserProfileInfo().getUserProfile().getProfileImageUrl();

                clickToActivity.onShareMyProfileClicked();

            }
        });

        shareUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickToActivity.shareAppClicked();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                String share = getResources().getString(R.string.share_msg_detail_new_url/*share_msg_detail*/)/*"No last seen, no blue double tick and no last read\n" +
                    "Now read your Whatsapp,Wechat,Allo, Facebook and Viber messages without your contacts knowing.\r\nDownload the app for free!\r\nhttps://play.google.com/store/apps/details?id=com.spychat.android"*/;

                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", share);
                clipboard.setPrimaryClip(clip);

                Toast.makeText(getActivity(), getResources().getString(R.string.share_msg), Toast.LENGTH_SHORT).show();

                intent.putExtra(Intent.EXTRA_TEXT, share);
                intent.setType("text/plain");
                startActivity(Intent.createChooser(intent, "Share " + getResources().getString(R.string.app_name)));
            }
        });


    }

    private void setValues() {
        FilterTable values = HomeDBHandlerClass.getFilterFromTable();
        filterModel = new ParseFilter().getParsedFilters(values.getFilter());
        if (filterModel.getMinAge() < Integer.parseInt(discoverySettingModel.getMinAge())) {
            filterModel.setMinAge(Integer.parseInt(discoverySettingModel.getMinAge()));
        }
        if (filterModel.getMaxAge() > Integer.parseInt(discoverySettingModel.getMaxAge())) {
            filterModel.setMaxAge(Integer.parseInt(discoverySettingModel.getMaxAge()));
        }
        if (filterModel != null) {

            lowAge.setText("" + filterModel.getMinAge());
            highAge.setText("" + filterModel.getMaxAge());
            rangeBar.setRangePinsByValue(filterModel.getMinAge(), filterModel.getMaxAge());
            if (filterModel.getGender().equalsIgnoreCase("male")) {
                interestedInMen.setChecked(true);
                interestedInWomen.setChecked(false);
            } else {
                interestedInWomen.setChecked(true);
                interestedInMen.setChecked(false);
            }

            if (filterModel.getCurrentlyAtCountry() == null || filterModel.getCurrentlyAtCountry().length() == 0 || currentCountryProfiles.getVisibility() == View.GONE) {
                currentCountryProfiles.setChecked(false);
                filterModel.setCurrentlyAtCountry("");

            } else {
                currentCountryProfiles.setChecked(true);
            }

            ArrayList<String> contained = new ArrayList<>();
            if (filterModel.getCurrentlyAtLocations() != null && filterModel.getCurrentlyAtLocations().size() > 0) {
                for (String id :
                        filterModel.getCurrentlyAtLocations()) {
                    if (discoverySettingModel.getHometown() != null && discoverySettingModel.getHometown().getId().equalsIgnoreCase(id) && hometownProfiles.getVisibility() == View.VISIBLE) {
                        hometownProfiles.setChecked(true);
                        contained.add(id);
                    }
                    if (discoverySettingModel.getCurrentLocation() != null && discoverySettingModel.getCurrentLocation().getId().equals(id) && currentCityProfiles.getVisibility() == View.VISIBLE) {
                        currentCityProfiles.setChecked(true);
                        contained.add(id);
                    }
                }

                currentLocationProfiles.setChecked(false);
            } else {
                currentCityProfiles.setChecked(false);
                hometownProfiles.setChecked(false);
            }

            filterModel.getCurrentlyAtLocations().retainAll(contained);

            if (filterModel.getCurrentlyAtFbId() == null || filterModel.getCurrentlyAtFbId().length() == 0 || currentLocationProfiles.getVisibility() == View.GONE) {
                currentLocationProfiles.setChecked(false);
                filterModel.setCurrentlyAtFbId("");
            } else {
                if (!discoverySettingModel.getCurrentlyAt().getId().equals(filterModel.getCurrentlyAtFbId())) {
                    filterModel.setCurrentlyAtFbId(discoverySettingModel.getCurrentlyAt().getId());
                }
                currentLocationProfiles.setChecked(true);
            }
            checkForRandomToggle();
            if (filterModel.isRandom()) {
                randomProfiles.setChecked(true);
            } else {
                randomProfiles.setChecked(false);
            }
        }

    }

    private void checkForRandomToggle() {
        if (!currentCountryProfiles.isChecked() && !currentLocationProfiles.isChecked() && !hometownProfiles.isChecked() && !currentCityProfiles.isChecked() && !randomProfiles.isChecked()) {
            randomProfiles.setChecked(true);
            filterModel.setRandom(true);
        }
    }

    public FilterTable getFilters() {
        if (filterModel == null) {
            return null;
        }
        JSONObject params = new JSONObject();
        try {
            params.put("minAge", Integer.parseInt(rangeBar.getLeftPinValue()));
            params.put("maxAge", Integer.parseInt(rangeBar.getRightPinValue()));
            params.put("gender", filterModel.getGender().toLowerCase());
            params.put("currentlyAtFbId", filterModel.getCurrentlyAtFbId());
            params.put("currentlyAtCountry", filterModel.getCurrentlyAtCountry());
            JSONArray jsonArray = new JSONArray();
            if (filterModel.getCurrentlyAtLocations().size() > 0) {
                for (String s :
                        filterModel.getCurrentlyAtLocations()) {
                    jsonArray.put(s);
                }
            }
            params.put("currentlyAtLocations", jsonArray);

            params.put("isRandom", filterModel.isRandom());

            FilterTable filterTable = new FilterTable();
            filterTable.setFilter(params.toString())
                    .setFilterKey(CommonMethods.getStringFiltersUniqueKey(params.toString()))
                    .setTimestamp(System.currentTimeMillis());
            HomeDBHandlerClass.saveDataInTable(HomeDBHandlerClass.dataBaseTables.filterTable, filterTable);

            return filterTable;


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean isValueUpdated() {
        return valueUpdated;
    }

    public boolean isNotificationValueUpdated() {
        return notificationValueUpdated;
    }

    public HashMap<String, Boolean> getNotificationsSettings() {
        return discoverySettingModel.getNotificationSetting();
    }


}
