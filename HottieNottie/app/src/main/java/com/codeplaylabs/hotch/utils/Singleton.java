package com.codeplaylabs.hotch.utils;

import android.app.Activity;
import android.content.Context;

import com.codeplaylabs.hotch.login.models.dtoModels.FBUserProfileInfo;
import com.codeplaylabs.hotch.database.databaseInitialization.DatabaseHelper;
import com.codeplaylabs.hotch.login.models.UserProfile;
import com.facebook.AccessToken;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.lang.ref.WeakReference;

/**
 * Created by mohit on 22/06/17.
 */

public class Singleton {
    private static final Singleton ourInstance = new Singleton();
    private FBUserProfileInfo fbUserProfileInfo = null;
    private AccessToken facebookAccessToken;
    private DatabaseHelper databaseHelper;
    private UserProfile userProfile;
    public boolean filterRecieved = true;
    public boolean hotOrNotStatusUpdated = false;
    public WeakReference<Activity> activity ;
    private boolean networkAvailable = true;


    public static Singleton getInstance() {
        return ourInstance;
    }

    private Singleton() {
    }

   /* public void setImgUrl(String url){
        imgUrl=url;
    }*/
    private FBUserProfileInfo getFbUserProfileInfo() {
        if(fbUserProfileInfo == null){
            fbUserProfileInfo = new FBUserProfileInfo();
        }
        return fbUserProfileInfo;
    }

    private void setFbUserProfileInfo(FBUserProfileInfo fbUserProfileInfo) {
        this.fbUserProfileInfo = fbUserProfileInfo;
    }

    public void setFacebookAccessToken(AccessToken facebookAccessToken) {
        this.facebookAccessToken = facebookAccessToken;
    }

    public AccessToken getFacebookAccessToken() {
        return facebookAccessToken;
    }
    public DatabaseHelper initializeHelper(Context activity) {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(activity,DatabaseHelper.class);
        }
        return databaseHelper;
    }
    public DatabaseHelper getHelper(){
        return databaseHelper;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public boolean isNetworkAvailable() {
        return networkAvailable;
    }

    public void setNetworkAvailable(boolean networkAvailable) {
        this.networkAvailable = networkAvailable;
    }

   /* public String getImgUrl() {
        return imgUrl;
    }*/
}
