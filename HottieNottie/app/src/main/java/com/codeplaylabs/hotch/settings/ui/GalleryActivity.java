package com.codeplaylabs.hotch.settings.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.codeplaylabs.hotch.base.BaseActivity;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.database.databaseInitialization.DatabaseHelper;
import com.codeplaylabs.hotch.database.tables.ImageTemplate;
import com.codeplaylabs.hotch.database.tables.UserImageTable;
import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.settings.adapters.CustomGalleryListAdapter;
import com.codeplaylabs.hotch.settings.handler.SettingsDbHandler;
import com.codeplaylabs.hotch.settings.interfaces.AlbumResponseListener;
import com.codeplaylabs.hotch.settings.interfaces.GalleryListListener;
import com.codeplaylabs.hotch.settings.models.albums.AlbumWrapper;
import com.codeplaylabs.hotch.settings.services.SettingsService;
import com.codeplaylabs.hotch.utils.CommonMethods;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.Singleton;
import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.settings.adapters.CustomFacebookGalleryListAdapter;
import com.codeplaylabs.hotch.settings.adapters.CustomGalleryGridAdapter;
import com.codeplaylabs.hotch.settings.models.albums.AlbumModel;
import com.j256.ormlite.dao.Dao;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.sql.SQLException;
import java.util.ArrayList;

public class GalleryActivity extends BaseActivity {

    public static final int LIST_VIEW = 1;
    public static final int GRID_VIEW = 2;
    public static final int FACEBOOK_LIST_VIEW = 3;
    public static final String UPDATE_FROM_GALLERY = "Add/Update From Gallery";
    public static final String UPDATE_FROM_FACEBOOK = "Add/Update From Facebook";
    public static final String DELETE_IMAGE = "Delete Image";
    public static final String VIEW_FULL_IMAGE = "View Full Image";
    private static final int OKH = 1234;
    private static final int RESULT_LOAD_IMG = 987;
    private static final int GRID_VIEW_RESULT = 1357;
    private static final int FB_LIST_RESULT = 7531;
    private UserImage.ImageSource TAGG = UserImage.ImageSource.User_Library;

    private int viewType;
    private ArrayList<String> arrayList;
    private CustomGalleryListAdapter recyclerViewAdaper;
    private RecyclerView recyclerView;
    private ArrayList<AlbumModel> albumList;
    private CustomFacebookGalleryListAdapter fbAlbumsListRecyclerViewAdaper;
    private CustomGalleryGridAdapter albumsGridRecyclerViewAdaper;
    private DatabaseHelper databaseHelper;
    private Dao<ImageTemplate, Integer> imageTemplateDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        recyclerView = (RecyclerView) findViewById(R.id.gallery_list_view);

        databaseHelper = Singleton.getInstance().getHelper();

        viewType = getIntent().getIntExtra("viewType", 1);
        if (viewType == LIST_VIEW) {
            arrayList = getIntent().getStringArrayListExtra("array");
        } else if (viewType == FACEBOOK_LIST_VIEW || viewType == GRID_VIEW) {
            albumList = ((AlbumWrapper) getIntent().getSerializableExtra("albums")).getFacebookAlbums();
        }
        findViewById(R.id.loader_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if (viewType == LIST_VIEW) {


            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerViewAdaper = new CustomGalleryListAdapter(this, arrayList, new GalleryListListener() {
                @Override
                public void onItemClick(int position, Object item, Bitmap bitmap) {

                    switch (arrayList.get(position)) {
                        case DELETE_IMAGE: {

                            UserImageTable userImageTable = (UserImageTable) getIntent().getSerializableExtra("table");
                            SettingsDbHandler.DeleteDataFromTable(SettingsDbHandler.dataBaseTables.UserImageTable,userImageTable);
                            Intent intent = new Intent();
                            intent.putExtra("isDeleted",true);
                            //intent.putExtra("bitmap", userImageTable);
                            setResult(8765, intent);
                            finish();

                            break;
                        }
                        case UPDATE_FROM_GALLERY: {
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
                            break;
                        }
                        case UPDATE_FROM_FACEBOOK: {
                            findViewById(R.id.loader_layout).setVisibility(View.VISIBLE);
                            new SettingsService().getAlbumsList(GalleryActivity.this, Singleton.getInstance().getFacebookAccessToken(), new AlbumResponseListener() {
                                @Override
                                public void onResponse(final BaseModel baseModel, String jsonString, final String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {


                                            findViewById(R.id.loader_layout).setVisibility(View.GONE);
                                            if (baseModel != null) {
                                                if (reasonOfError == null) {
                                                    AlbumWrapper albumWrapper = (AlbumWrapper) baseModel;

                                                    Intent intent = new Intent(GalleryActivity.this, GalleryActivity.class);
                                                    intent.putExtra("viewType", GalleryActivity.FACEBOOK_LIST_VIEW);
                                                    intent.putExtra("albums", albumWrapper);
                                                    startActivityForResult(intent, FB_LIST_RESULT);

                                                } else {
                                                    showOkAlert(reasonOfError, "Error", OKH);
                                                }
                                            } else {
                                                if (reasonOfError != null) {
                                                    showOkAlert(reasonOfError, "Error", OKH);
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                            break;
                        }
                        case VIEW_FULL_IMAGE: {
                            Intent intent = new Intent(GalleryActivity.this, FullScreenActivity.class);
                            intent.putExtra("bitmap", getIntent().getStringExtra("bitmap"));
                            startActivity(intent);
                        }
                    }

                }
            });

            recyclerView.setAdapter(recyclerViewAdaper);
        } else if (viewType == FACEBOOK_LIST_VIEW) {
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            fbAlbumsListRecyclerViewAdaper = new CustomFacebookGalleryListAdapter(this, albumList, new GalleryListListener() {
                @Override
                public void onItemClick(int position, final Object item, Bitmap bitmap) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            AlbumModel albumModel = (AlbumModel) item;

                            findViewById(R.id.loader_layout).setVisibility(View.VISIBLE);
                            new SettingsService().getAlbumsImages(GalleryActivity.this, Singleton.getInstance().getFacebookAccessToken(), albumModel, new AlbumResponseListener() {
                                @Override
                                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                                    findViewById(R.id.loader_layout).setVisibility(View.GONE);
                                    if (baseModel != null) {
                                        if (reasonOfError == null) {
                                            AlbumWrapper albumWrapper = (AlbumWrapper) baseModel;

                                            Intent intent = new Intent(GalleryActivity.this, GalleryActivity.class);
                                            intent.putExtra("viewType", GalleryActivity.GRID_VIEW);
                                            intent.putExtra("albums", albumWrapper);
                                            startActivityForResult(intent, GRID_VIEW_RESULT);

                                        } else {
                                            showOkAlert(reasonOfError, "Error", OKH);
                                        }
                                    } else {
                                        if (reasonOfError != null) {
                                            showOkAlert(reasonOfError, "Error", OKH);
                                        }
                                    }
                                }
                            });
                        }
                    });
                }
            });
            recyclerView.setAdapter(fbAlbumsListRecyclerViewAdaper);


        } else if (viewType == GRID_VIEW) {
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 3);
            recyclerView.setLayoutManager(layoutManager);
            albumsGridRecyclerViewAdaper = new CustomGalleryGridAdapter(this, albumList, new GalleryListListener() {
                @Override
                public void onItemClick(int position, Object item, Bitmap bitmap) {

                    AlbumModel albumModel = (AlbumModel) item;

// start cropping activity for pre-acquired image saved on the device


                    Uri uri = null;
                    try {
                        TAGG = UserImage.ImageSource.Facebook_Album;
                       // uri = CommonMethods.getImageUri(GalleryActivity.this, bitmap);
                        String timestamp = String.valueOf(System.currentTimeMillis());

                        ImageTemplate imageTemplate = new ImageTemplate(timestamp, null, albumModel.getUrl(), TAGG.toString());
                        if (databaseHelper != null) {
                            try {
                                imageTemplateDao = databaseHelper.getTemplateDao();
                                imageTemplateDao.create(imageTemplate);

                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }

                        Intent intent = new Intent();
                        intent.putExtra("bitmap", timestamp);
                        setResult(RESULT_OK, intent);
                        finish();
                        //CropImage.activity(uri).setAspectRatio(3, 5).setFixAspectRatio(true)
                        //.setGuidelines(CropImageView.Guidelines.ON)
                        // .start(GalleryActivity.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            recyclerView.setAdapter(albumsGridRecyclerViewAdaper);
        }

    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if (reqCode == GRID_VIEW_RESULT) {
            setResult(RESULT_OK, data);
            finish();
        }
        if (reqCode == FB_LIST_RESULT) {
            setResult(RESULT_OK, data);
            finish();
        }

        if (reqCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Bitmap bitmap = result.getBitmap();
                String timestamp = String.valueOf(System.currentTimeMillis());

                ImageTemplate imageTemplate = new ImageTemplate(timestamp, CommonMethods.bitmapToBase64(bitmap, true), resultUri.toString(), TAGG.toString());
                if (databaseHelper != null) {
                    try {
                        imageTemplateDao = databaseHelper.getTemplateDao();
                        imageTemplateDao.create(imageTemplate);

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                Intent intent = new Intent();
                intent.putExtra("bitmap", timestamp);
                setResult(RESULT_OK, intent);
                finish();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }


        if (reqCode == RESULT_LOAD_IMG) {
            if (resultCode == RESULT_OK) {
                final Uri imageUri = data.getData();

                TAGG = UserImage.ImageSource.User_Library;
                CropImage.activity(imageUri).setAspectRatio(3, 5).setFixAspectRatio(true)
                        .setGuidelines(CropImageView.Guidelines.ON).setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                        .setOutputCompressQuality(100)
                        .start(this);

            } else {
                //Toast.makeText(GalleryActivity.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        }
    }
}
