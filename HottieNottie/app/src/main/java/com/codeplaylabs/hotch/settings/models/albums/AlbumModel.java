package com.codeplaylabs.hotch.settings.models.albums;

import com.codeplaylabs.hotch.base.BaseModel;

/**
 * Created by mohit on 29/06/17.
 */

public class AlbumModel extends BaseModel {

    private String photoCount = "";
    private String url = "";
    private String name = "";
    private String id = "";
    private String thumbnailImage = "";

    public String getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(String photoCount) {
        this.photoCount = photoCount;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }
}
