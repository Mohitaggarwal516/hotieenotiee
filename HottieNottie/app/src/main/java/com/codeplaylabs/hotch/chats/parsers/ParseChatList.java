package com.codeplaylabs.hotch.chats.parsers;

import com.codeplaylabs.hotch.chats.models.SubProfileModel;
import com.codeplaylabs.hotch.home.parsers.ParseSingleProfile;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.chats.models.ChatListWrapper;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by mohit on 31/07/17.
 */

public class ParseChatList extends BaseParser {
    BaseModel baseModel = new BaseModel();

    @Override
    public BaseModel parseJson(JSONObject response) {
        if (validate(response) != null) {
            try {
                return moduleJson(response);
            } catch (Exception e) {
                e.printStackTrace();
                baseModel.error = Constants.Error.JSON_EXCEPTION;
                return baseModel;

            }
        } else {
            baseModel.error = Constants.Error.SettingsError.PROFILE_SETTING_ERROR;
            return baseModel;
        }
    }

    public ChatListWrapper moduleJson(JSONObject user) throws Exception {

        ChatListWrapper chatListWrapper = new ChatListWrapper();
        JSONObject jsonObject = user.getJSONObject("data");
        JSONArray jsonArray = jsonObject.getJSONArray("messages");
        if (jsonArray != null || jsonArray.length() > 0) {
            for (int index = 0; index < jsonArray.length(); index++) {
                JSONObject userObj = jsonArray.getJSONObject(index);
                SubProfileModel subProfileModel = new SubProfileModel();
                subProfileModel.setProfileModel((ProfileModel) new ParseSingleProfile().parseJson(userObj));
                CustomJsonObject obj = new CustomJsonObject(userObj);
                subProfileModel.setChatId(obj.getString("chatId"));
                subProfileModel.setLastMessage(obj.getString("lastMessage"));
                subProfileModel.setMessageSender(obj.getString("messageSender"));
                subProfileModel.setLastMessageTimeStamp(Long.parseLong(obj.getString("lastMessageTimeStamp")));
                subProfileModel.setMatchDate(Long.parseLong(obj.getString("matchDate")));
                JSONArray jsonArrayDevices = obj.getJsonArray("devices");

                for (int i = 0; i < jsonArrayDevices.length(); i++) {
                    CustomJsonObject deviceObj = new CustomJsonObject(jsonArrayDevices.getJSONObject(i));
                    SubProfileModel.Devices device = subProfileModel.new Devices();

                    device.setDeviceTokens(deviceObj.getString("deviceToken"));
                    device.setDeviceType(deviceObj.getString("deviceType"));
                    subProfileModel.getDevices().add(device);

                }
                subProfileModel.setIsNotificationDisabledByPartner(obj.getBoolean("isNotificationDisabledByPartner", false));
                subProfileModel.setIsNotificationDisabledBySelf(obj.getBoolean("isNotificationDisabledBySelf", false));
                chatListWrapper.getProfileModels().add(subProfileModel);
            }
        }

        return chatListWrapper;

    }
}
