package com.codeplaylabs.hotch.settings.ui;

import android.os.Bundle;
import android.widget.ImageView;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseActivity;
import com.squareup.picasso.Picasso;

public class FullScreenActivity extends BaseActivity {

    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen);
        image = (ImageView) findViewById(R.id.full_image);
        String uri = getIntent().getStringExtra("bitmap");
        Picasso.with(this).load(uri).centerCrop().resize(getWindowManager().getDefaultDisplay().getWidth(),getWindowManager().getDefaultDisplay().getHeight()).into(image);
        //image.setImageBitmap((Bitmap) getIntent().getParcelableExtra("bitmap"));

    }
}
