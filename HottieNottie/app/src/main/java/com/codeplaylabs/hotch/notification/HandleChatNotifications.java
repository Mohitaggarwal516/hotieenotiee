package com.codeplaylabs.hotch.notification;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.chats.handler.ChatDBHandlerClass;
import com.codeplaylabs.hotch.chats.models.firbaseModels.UserModel;
import com.codeplaylabs.hotch.chats.ui.ChatActivity;
import com.codeplaylabs.hotch.chats.ui.ChatFragmentActivity;
import com.codeplaylabs.hotch.database.tables.ChatUsersList;
import com.codeplaylabs.hotch.home.ui.HomeActivity;
import com.codeplaylabs.hotch.others.ui.OthersTabActivity;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by mohit on 21/08/17.
 */

public class HandleChatNotifications {
    public void chatNotifications(Context context, RemoteMessage remoteMessage) {
        //  String title = remoteMessage.getData().get(Constants.Chat.TITLE);
        String message = remoteMessage.getData().get(Constants.Chat.BODY);

        UserModel userModel = new UserModel();
        userModel.setName(remoteMessage.getData().get(Constants.Chat.OTHER_USER_NAME));
        userModel.setImage(remoteMessage.getData().get(Constants.Chat.OTHER_USER_IMAGE));
        userModel.setTimestamp(0);
        userModel.setChatId(remoteMessage.getData().get(Constants.Chat.CHAT_ID));

        ChatUsersList chatUsers = ChatDBHandlerClass.getChatFromTable(userModel.getChatId());
        if (chatUsers == null) {
            chatUsers = new ChatUsersList();
        }
        ArrayList<String> arrayList1 = new ArrayList<>();
        String[] val = userModel.getChatId().split("_");
        arrayList1.add(val[0]);
        arrayList1.add(val[1]);

        if (chatUsers.getLastMessageTimestamp() < Long.parseLong(remoteMessage.getData().get(Constants.Chat.OTHER_USER_TIMESTAMP))) {
            chatUsers.setLastMessage(message).setIsNew("true")
                    .setSelf(1 - arrayList1.indexOf(LocalPreferenceManager.getInstance().getUserId()))
                    .setName(userModel.getName()).setUserId(arrayList1.get(chatUsers.isSelf())).setIsNew("true")
                    .setIsNotificationDisabledBySelf(Boolean.parseBoolean(remoteMessage.getData().get(Constants.Chat.CHAT_NOTIF_DISABLED_BY_ME))).setIsNotificationDisabledByPartner(Boolean.parseBoolean(remoteMessage.getData().get(Constants.Chat.CHAT_NOTIF_DISABLED_BY_PARTNER))).setImageUrl(userModel.getImage()).setLastMessageTimestamp(Long.parseLong(remoteMessage.getData().get(Constants.Chat.OTHER_USER_TIMESTAMP)));

            ChatDBHandlerClass.saveDataInTable(ChatDBHandlerClass.dataBaseTables.chatListTable, chatUsers);
        }

        sendChatBroadcast(context, Constants.Chat.LOCAL_BROADCAST_NEW_MESSAGE, "asd");
        Set<String> chatsTitle = LocalPreferenceManager.getInstance().getStringSet(Constants.Chat.PREFERENCE_CHATHEADS_KEY);
        Set<String> chats = LocalPreferenceManager.getInstance().getStringSet(Constants.Chat.PREFERENCE_CHATSET_KEY);
        int chatCount = LocalPreferenceManager.getInstance().getIntPreference(Constants.Chat.PREFERENCE_CHAT_COUNT, 0);
        if (chats == null || chatCount == 0) {
            chats = new HashSet<>();
            chatsTitle = new HashSet<>();
            if (!chatUsers.getIsNotificationDisabledBySelf()) {//
                chatsTitle.add(userModel.getName());//
                chats.add(chats.size() + userModel.getName() + " : " + message);//
            }//
            //chatsTitle.add(userModel.getName());
            //chats.add(chats.size() + userModel.getName() + " : " + message);
            LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHATHEADS_KEY, chatsTitle);
            LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHATSET_KEY, chats);
            LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHAT_COUNT, chatCount + 1);

            sendNotification(context, context.getResources().getString(R.string.app_name), userModel.getName() + " : " + message, userModel, chatUsers.getIsNotificationDisabledBySelf());

        } else {
            if (!chatUsers.getIsNotificationDisabledBySelf()) {//
                chats.add(chats.size() + userModel.getName() + " : " + message);
                chatsTitle.add(userModel.getName());
            }
            ArrayList<String> arrayList = new ArrayList<>();
            for (String s : chats//
                    ) {//
                arrayList.add(0, s);//
            }//
            //Collections.sort(arrayList);//
//            if (chats.size() < 9)
//                chats.add(chats.size() + userModel.getName() + " : " + message);
//            ArrayList<String> arrayList = new ArrayList<>();
//            arrayList.addAll(chats);
//            Collections.sort(arrayList);
//            chatsTitle.add(userModel.getName());
            LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHATHEADS_KEY, chatsTitle);
            LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHATSET_KEY, chats);
            LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHAT_COUNT, chatCount + 1);

            int diffChatsCount = chatsTitle.size();

            sendNotification(context, chatCount + 1, diffChatsCount, arrayList, userModel, chatUsers.getIsNotificationDisabledBySelf());


        }
        LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREF_NEW_MESSAGE, true);
    }

    private void sendNotification(Context context, int localTitle, int noOfChats, ArrayList<String> chats, UserModel userModel, boolean isNotificationDisabledBySelf) {

        Intent intent = null;
        PendingIntent pendingIntent = null;
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo);

        if (noOfChats == 1) {
            intent = new Intent(context, ChatActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("otherUser", userModel);
            pendingIntent = TaskStackBuilder.create(context)
                    .addNextIntent(new Intent(context, HomeActivity.class))
                    .addNextIntent(new Intent(context, ChatFragmentActivity.class))
                    .addNextIntent(intent)
                    .getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT);

        } else {
            intent = new Intent(context, ChatFragmentActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = TaskStackBuilder.create(context)
                    .addNextIntent(new Intent(context, HomeActivity.class))
                    .addNextIntent(intent)
                    .getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT);

        }
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        Iterator<String> val = chats.listIterator();
//        ArrayList<String> vals = new ArrayList<>();
        while (val.hasNext()) {
            inboxStyle.addLine(val.next().substring(1));
        }

        Notification summaryNotification = new NotificationCompat.Builder(context)
                .setContentTitle(context.getResources().getString(R.string.app_name)).setContentText(chats.size() + " messages from " + noOfChats + " chat(s)")
                .setSmallIcon(R.drawable.logo)
                .setLargeIcon(bitmap)
                .setStyle(inboxStyle.setBigContentTitle(context.getString(R.string.app_name)))
                .setLights(Color.GREEN,1000,5000)
                .setAutoCancel(true).setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent)
                .build();

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (!isNotificationDisabledBySelf) {
            notificationManager.cancelAll();
            notificationManager.notify(111, summaryNotification);
        }
        sendChatBroadcast(context, Constants.Chat.LOCAL_BROADCAST_NEW_MESSAGE, "asd");
        if (!isAppIsInBackground(context)) {

            sendChatBroadcast(context, Constants.Chat.LOCAL_BROADCAST, "asd");
        }
    }

    private void sendNotification(Context context, String messageBody, String msg, UserModel userModel, boolean isNotificationDisabledBySelf) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("otherUser", userModel);
        PendingIntent pendingIntent = TaskStackBuilder.create(context)
                .addNextIntent(new Intent(context, HomeActivity.class))
                .addNextIntent(new Intent(context, ChatFragmentActivity.class))
                .addNextIntent(intent)
                .getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT);
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setLargeIcon(bitmap)/*Notification icon image*/
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(messageBody).setContentText(msg)
                .setAutoCancel(true).setLights(Color.argb(255,255,255,255),1000,5000)
                .setSound(defaultSoundUri).setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (!isNotificationDisabledBySelf) {
            notificationManager.cancelAll();
            notificationManager.notify(111 /* ID of notification */, notificationBuilder.build());
            wakeScreen(context);
        }
        sendChatBroadcast(context, Constants.Chat.LOCAL_BROADCAST_NEW_MESSAGE, "asd");
        if (!isAppIsInBackground(context))
            sendChatBroadcast(context, Constants.Chat.LOCAL_BROADCAST, "asd");


/*
        .setStyle(new NotificationCompat.BigPictureStyle()
                .bigPicture(bitmap))*/
/*Notification with Image*/


    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {

                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    private void sendChatBroadcast(Context context, String intentMsg, String val) {
        Intent intent = new Intent(intentMsg);
        intent.putExtra("val", val);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private void sendChatBroadcast(Context context, String intentMsg, String val, String id) {
        Intent intent = new Intent(intentMsg);
        intent.putExtra("val", val);
        intent.putExtra("id", id);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public void HandleChatSituations(Context context, RemoteMessage remoteMessage) {
        String chatId = remoteMessage.getData().get("id");

        ChatUsersList chatUsers = ChatDBHandlerClass.getChatFromTable(chatId);
        if (chatUsers != null) {
            switch (remoteMessage.getData().get("notificationType")) {
                case "ChatMute": {
                    chatUsers.setIsNotificationDisabledByPartner(true);
                    ChatDBHandlerClass.saveDataInTable(ChatDBHandlerClass.dataBaseTables.chatListTable, chatUsers);
                    sendChatBroadcast(context, Constants.Chat.LOCAL_BROADCAST_NEW_MESSAGE, "mute");
                    break;
                }
                case "ChatUnmute": {
                    chatUsers.setIsNotificationDisabledByPartner(false);
                    ChatDBHandlerClass.saveDataInTable(ChatDBHandlerClass.dataBaseTables.chatListTable, chatUsers);
                    sendChatBroadcast(context, Constants.Chat.LOCAL_BROADCAST_NEW_MESSAGE, "unmute");
                    break;
                }
                case "Unmatch": {
                    ChatDBHandlerClass.deleteChatFromDb(chatId);
                    sendChatBroadcast(context, Constants.Chat.LOCAL_BROADCAST_NEW_MESSAGE, "unmatch", chatId);

                    break;
                }
            }
        }
    }

    public void superHotNotification(Context context, RemoteMessage remoteMessage) {
        Intent intent = null;
        PendingIntent pendingIntent = null;
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo);

        intent = new Intent(context, OthersTabActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pendingIntent = TaskStackBuilder.create(context)
                .addNextIntent(new Intent(context, HomeActivity.class))
                .addNextIntent(intent)
                .getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT);

        if (pendingIntent != null) {
            sendNotification(context, bitmap, remoteMessage.getData().get("title"), remoteMessage.getData().get("text"),
                    pendingIntent);
        }

    }

    public void sendNotification(Context context, Bitmap bitmap, String title, String msg,
                                 PendingIntent pendingIntent) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        for (int i = 0; i < msg.length(); ) {
            if (msg.length() > (i + 35))
                if (msg.charAt(i + 35) == ' ') {
                    inboxStyle.addLine(msg.substring(i, i + 35));
                    i = i + 35;
                } else {
                    int index = msg.indexOf(" ", i + 35);
                    inboxStyle.addLine(msg.substring(i, index));
                    i = index + 1;
                }
            else {
                inboxStyle.addLine(msg.substring(i));
                break;
            }
        }

        Notification notificationBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(msg)
                .setSmallIcon(R.drawable.logo)
                .setLargeIcon(bitmap)
                .setStyle(inboxStyle.setBigContentTitle(title)).setLights(Color.argb(255,255,255,255),1000,5000)
                .setAutoCancel(true).setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(pendingIntent).setSound(defaultSoundUri)
                .build();
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(123 /* ID of notification */, notificationBuilder);
        wakeScreen(context);
    }

    public void profileMatchNotification(Context context, RemoteMessage remoteMessage) {
        Intent intent = null;
        PendingIntent pendingIntent = null;
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo);
        LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREF_NEW_MESSAGE, true);
        intent = new Intent(context, ChatFragmentActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("calledFrom", ChatFragmentActivity.NEW_CHAT);
        pendingIntent = TaskStackBuilder.create(context)
                .addNextIntent(new Intent(context, HomeActivity.class))
                .addNextIntent(intent)
                .getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT);

        if (pendingIntent != null) {
            sendNotification(context, bitmap, remoteMessage.getData().get("title"), remoteMessage.getData().get("text"),
                    pendingIntent);
        }
    }

    public void wakeScreen(Context context){
        PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        Log.e("screen on.........", ""+isScreenOn);
        if(isScreenOn==false)
        {
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");

            wl_cpu.acquire(10000);
        }
    }
}


