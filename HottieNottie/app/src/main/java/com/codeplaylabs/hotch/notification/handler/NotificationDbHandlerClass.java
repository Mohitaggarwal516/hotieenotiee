package com.codeplaylabs.hotch.notification.handler;

import com.codeplaylabs.hotch.database.databaseInitialization.DatabaseHelper;
import com.codeplaylabs.hotch.database.tables.NotificationSubscriptionTable;
import com.codeplaylabs.hotch.utils.Singleton;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mohit on 22/08/17.
 */

public class NotificationDbHandlerClass {

    public  boolean saveDataInTable(NotificationSubscriptionTable table) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<NotificationSubscriptionTable, Integer> notificationDbHandlerDao = databaseHelper.getNotificationSubscriptionDao();
            notificationDbHandlerDao.createOrUpdate(table);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public  boolean clearTable(NotificationSubscriptionTable tableName) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<NotificationSubscriptionTable, Integer> notificationSubscriptionDao = databaseHelper.getNotificationSubscriptionDao();
            notificationSubscriptionDao.delete(notificationSubscriptionDao.queryForAll());
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public NotificationSubscriptionTable getSubscriptionByid(String id){

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<NotificationSubscriptionTable, Integer> notificationSubscriptionDao = databaseHelper.getNotificationSubscriptionDao();
            if (id != null) {
                HashMap<String,Object> params = new HashMap<>();
                params.put("id",id);
                List<NotificationSubscriptionTable> notificationSubscriptionTables = notificationSubscriptionDao.queryForFieldValues(params);

                if(notificationSubscriptionTables != null && notificationSubscriptionTables.size() > 0 ){
                    return notificationSubscriptionTables.get(0);
                }
                return null;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static List<NotificationSubscriptionTable> getAllSubscriptionsFromTable() {
        return getAllSubscriptionsFromTable(null);
    }
        public static List<NotificationSubscriptionTable> getAllSubscriptionsFromTable(HashMap<String, Object> params) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<NotificationSubscriptionTable, Integer> notificationSubscriptionDao = databaseHelper.getNotificationSubscriptionDao();
            if (params != null) {
                return notificationSubscriptionDao.queryForFieldValues(params);
            } else {
                return notificationSubscriptionDao.queryForAll();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public  boolean delete(NotificationSubscriptionTable table){
        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<NotificationSubscriptionTable, Integer> notificationSubscriptionDao = databaseHelper.getNotificationSubscriptionDao();

            int delete = notificationSubscriptionDao.delete(table);
            if(delete > 0){
                return true;
            }else{
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }
    public  boolean delete(String id){
        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<NotificationSubscriptionTable, Integer> notificationSubscriptionDao = databaseHelper.getNotificationSubscriptionDao();

            if(notificationSubscriptionDao.delete(getSubscriptionByid(id)) == 0){
                return true;
            }else{
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }
    public  boolean deleteBySubscriptionStatus(boolean status) {
        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<NotificationSubscriptionTable, Integer> notificationSubscriptionDao = databaseHelper.getNotificationSubscriptionDao();

            DeleteBuilder<NotificationSubscriptionTable, Integer> builder = notificationSubscriptionDao.deleteBuilder();
            builder.where().eq("subscribed", status);
            int build = builder.delete();
            if (build > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
}
