package com.codeplaylabs.hotch.login.models;

import com.codeplaylabs.hotch.base.BaseModel;

/**
 * Created by mohit on 22/06/17.
 */

public class Education extends BaseModel{

    private String type = "";
    private String typeFacebookId = "";
    private String year = "";
    private String facebookId = "";
    private Detail institute = null;
    private boolean isCurrentlyStudingHere = false;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeFacebookId() {
        return typeFacebookId;
    }

    public void setTypeFacebookId(String typeFacebookId) {
        this.typeFacebookId = typeFacebookId;
    }

    public String getYear() {
        if(year == null){
            year = "";
        }
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public Detail getInstitute() {
        return institute;
    }

    public void setInstitute(Detail institute) {
        this.institute = institute;
    }

    public boolean isCurrentlyStudingHere() {
        return isCurrentlyStudingHere;
    }

    public void setCurrentlyStudingHere(boolean currentlyStudingHere) {
        isCurrentlyStudingHere = currentlyStudingHere;
    }
}
