package com.codeplaylabs.hotch.others.ui.frags;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.interfaces.FacebookUserInfoResponse;
import com.codeplaylabs.hotch.login.interfaces.SharedProfileResponse;
import com.codeplaylabs.hotch.login.services.FacebookGraphService;
import com.codeplaylabs.hotch.others.Adapters.HottiesAdapter;
import com.codeplaylabs.hotch.others.interfaces.InformActivity;
import com.codeplaylabs.hotch.others.interfaces.InformHeyHotties;
import com.codeplaylabs.hotch.others.models.ProfileWrapper;
import com.codeplaylabs.hotch.profileDetail.ui.ProfileDetailActivity;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.TextViewPlus;

import java.util.ArrayList;

/**
 * Created by HP on 17-Aug-17.
 */

public class HeyHottieFrag extends Fragment {
    Context context;
    HottiesAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView recyclerView;
    View view;
    ArrayList<ProfileModel> profileModelList = new ArrayList<>();
    ProfileWrapper profileWrapper;
    int pageNo = 0;
    InformActivity informActivity;
    TextViewPlus emptyListTxt;
    boolean isInitiallyZero=true;

    public HeyHottieFrag setInformActivityListener(InformActivity informActivity) {
        this.informActivity = informActivity;
        return this;
    }

    @Override
    public void onAttach(Context context) {
        this.context = context;
        super.onAttach(context);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_heyhottiees, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.others_recyclerView);
        layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        emptyListTxt = (TextViewPlus) view.findViewById(R.id.if_empty_text);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        callService(pageNo);
    }

    private void callService(final int page) {
        informActivity.showLoader(true);
        new FacebookGraphService().fetchSuperHotList((Activity) context, "" + page, new SharedProfileResponse() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                informActivity.showLoader(false);
                if (reasonOfError == null) {
                    profileWrapper = (ProfileWrapper) baseModel;

                    //ProfileWrapper wrapper=(ProfileWrapper) baseModel;
                    if (profileWrapper.getProfileWrapper().size() > 0) {
                        emptyListTxt.setVisibility(View.GONE);
                        pageNo++;
                        profileModelList.addAll(profileWrapper.getProfileWrapper());
                        isInitiallyZero = false;
                        setAdapter(profileModelList/*profileWrapper.getProfileWrapper()*/);
                    } else {
                        if (isInitiallyZero)
                            emptyListTxt.setVisibility(View.VISIBLE);
                    }
                }else {
                    informActivity.showNetErrorDg();
                    //LogAnalytics.logSerViceFailEvent(mTracker,""+page,);
                }
            }
        });
    }


    private void setAdapter(final ArrayList<ProfileModel> profileModelList) {
        if (adapter == null) {
            adapter = new HottiesAdapter(context, profileModelList, HottiesAdapter.showCrossEnum.HIDE_CROSS, new InformHeyHotties() {
                @Override
                public void callNextPage(int position) {
                    callService(pageNo);
                    // recyclerView.scrollToPosition(position);
                }

                @Override
                public void onCellClicked(ProfileModel model, final int position) {
                    Intent intent = new Intent(context, ProfileDetailActivity.class);
                    intent.putExtra("from", "" + ProfileDetailActivity.From.OTHER);
                    intent.putExtra("isFromHeyHottie", true);
                    intent.putExtra("CallDescription",true);
                    intent.putExtra("isMatch", profileModelList.get(position).isMatch()/*profileWrapper.getProfileWrapper().get(position).isMatch()*/);
                    intent.putExtra("profile", model);
                    startActivityForResult(intent,909);
                    /*informActivity.showLoader(true);
                    new FacebookGraphService().profileDescriptionService(getActivity(), model.getId(), "", new SharedProfileResponse() {
                        @Override
                        public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                            informActivity.showLoader(false);
                            if (baseModel.error == null) {
                                // ProfileModel model = (ProfileModel) baseModel;
                                ProfileWrapper wrapper = (ProfileWrapper) baseModel;
                                Intent intent = new Intent(context, ProfileDetailActivity.class);
                                intent.putExtra("from", "" + ProfileDetailActivity.From.OTHER);
                                intent.putExtra("isFromHeyHottie", true);
                                // Log.e("Heyhottiefrag",profileWrapper.getProfileWrapper().get(position).getName());
                                intent.putExtra("isMatch", profileWrapper.getProfileWrapper().get(position).isMatch());
                                intent.putExtra("profile", wrapper.getProfileWrapper().get(0)*//*model*//*);
                                startActivity(intent);
                            }
                        }
                    });*/
                }

                @Override
                public void onCrossClicked(ProfileModel profileModel, ImageView view) {
                    view.setVisibility(View.GONE);
                    profileModelList.remove(profileModel);
                    adapter.notifyDataSetChanged();
                    if (profileModelList.size() == 0) {
                        emptyListTxt.setVisibility(View.VISIBLE);
                    }
                    new FacebookGraphService().delSuperHot((Activity) context, profileModel.getId(), new FacebookUserInfoResponse() {
                        @Override
                        public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                        }
                    });
                }
            });
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }
}
