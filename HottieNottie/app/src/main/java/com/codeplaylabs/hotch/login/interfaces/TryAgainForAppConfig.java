package com.codeplaylabs.hotch.login.interfaces;

/**
 * Created by HP on 23-Sep-17.
 */

public interface TryAgainForAppConfig {
    public void appConfigTryAgainClicked();
}
