package com.codeplaylabs.hotch.settings.tools.interfaces;

import android.widget.TextView;

/**
 * Created by mohit on 26/06/17.
 */

public interface SettingsToggleClickListener {

    void toggleClickedLeft(TextView left, TextView right);
    void toggleClickedRight(TextView left, TextView right);
}
