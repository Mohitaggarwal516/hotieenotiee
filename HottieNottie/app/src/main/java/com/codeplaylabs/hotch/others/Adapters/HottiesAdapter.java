package com.codeplaylabs.hotch.others.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.database.tables.SuperLikedTable;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.others.interfaces.CallBackInterface;
import com.codeplaylabs.hotch.others.interfaces.InformHeyHotties;
import com.codeplaylabs.hotch.utils.CommonMethods;
import com.codeplaylabs.hotch.utils.TextViewPlus;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by HP on 10-Aug-17.
 */

public class HottiesAdapter extends RecyclerView.Adapter<HottiesAdapter.MyViewHolder> {

    private static final int FULL_HEIGHT = 0;
    private static final int REDUCE_HEIGHT = 1;
    private final String SUPERHOTTIES_FRAG = "superHottieFrag";
    private final String HEY_HOTTIES_FRAG = "heyHottiesFrag";
    private boolean isCrossVisible = false;
    private boolean serviceCallflag = true;//to prevent service call that's happening while notifydatasetchanged()

    public enum showCrossEnum {SHOW_CROSS, HIDE_CROSS}


    Context context;
    List<SuperLikedTable> spLikedList;
    ArrayList<ProfileModel> profileModelList;
    InformHeyHotties informHeyHotties;
    String type = "";
    CallBackInterface callBackInterface;
    showCrossEnum mEnum;


    public HottiesAdapter(Context context, ArrayList<ProfileModel> profileModelList, showCrossEnum mEnum, InformHeyHotties informHeyHotties) {
        this.context = context;
        this.profileModelList = profileModelList;
        this.informHeyHotties = informHeyHotties;
        this.type = HEY_HOTTIES_FRAG;
        this.mEnum = mEnum;
    }

    public HottiesAdapter(Context context, List<SuperLikedTable> spLikedList, showCrossEnum mEnum, CallBackInterface callBackInterface) {
        this.context = context;
        this.spLikedList = spLikedList;
        this.callBackInterface = callBackInterface;
        this.type = SUPERHOTTIES_FRAG;
        this.mEnum = mEnum;

    }

    @Override
    public MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {

        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_other_recyclerview, parent, false);

        itemView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                final int type = viewType;
                final ViewGroup.LayoutParams lp = itemView.getLayoutParams();
                if (lp instanceof StaggeredGridLayoutManager.LayoutParams) {
                    StaggeredGridLayoutManager.LayoutParams sglp =
                            (StaggeredGridLayoutManager.LayoutParams) lp;
                    switch (type) {
                        case FULL_HEIGHT:
                            //sglp.setFullSpan(false);
                            sglp.width = itemView.getWidth();
                            sglp.height = CommonMethods.dpToPx(300, context);
                            break;
                        case REDUCE_HEIGHT:
                            sglp.setFullSpan(false);
                            sglp.width = itemView.getWidth();
                            sglp.height = CommonMethods.dpToPx(270, context);
                            break;
                    }
                    int margin = CommonMethods.dpToPx(6, context);
                    sglp.setMargins(margin, margin, margin, margin);
                    itemView.setLayoutParams(sglp);
                    final StaggeredGridLayoutManager lm =
                            (StaggeredGridLayoutManager) ((RecyclerView) parent).getLayoutManager();
                    lm.invalidateSpanAssignments();
                }
                itemView.getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        });

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        /*if(position==2){
            holder.dp.getLayoutParams().height=holder.othersCard.getLayoutParams().height- CommonMethods.dpToPx(100,context);
            Log.w("Othersheight @ pos",position+""+holder.dp.getLayoutParams().height);
        }else if(position%2!=0){
//            holder.othersCard.getLayoutParams().height=holder.othersCard.getLayoutParams().height-150;
            holder.dp.getLayoutParams().height=holder.othersCard.getLayoutParams().height- CommonMethods.dpToPx(100,context);
            Log.w("Othersheight @ pos",position+""+holder.dp.getLayoutParams().height);
        }*/
        if (mEnum == showCrossEnum.SHOW_CROSS) {
            holder.icCross.setVisibility(View.VISIBLE);
        } else
            holder.icCross.setVisibility(View.GONE);

        int placeHolder = 0;
        //to cancel the revious requests
        Picasso.with(context).cancelRequest(holder.dp);
        if (type.equals(SUPERHOTTIES_FRAG)) {

            if (spLikedList.get(position).gender.length() > 0) {
                if (spLikedList.get(position).gender.equals("male")) {
                    placeHolder = R.drawable.male_placeholder;
                    holder.gradientLayout.setBackgroundResource(R.drawable.gradient_male_profile);
                }else{
                placeHolder = R.drawable.female_placeholder;
            holder.gradientLayout.setBackgroundResource(R.drawable.gradient_female_profile);
                }}
            Picasso.with(context).load(spLikedList.get(position)./*getImageUrl(UserImage.ImgType.SIZE_200x300)*/imgUrl).placeholder(placeHolder).into(holder.dp);
            holder.name.setText(spLikedList.get(position).name);
            holder.age.setText(spLikedList.get(position).age);
            if (/*spLikedList.get(position).country.equals("") || */spLikedList.get(position).country.trim().length() == 0) {
                holder.locPin.setVisibility(View.GONE);
                holder.location.setVisibility(View.GONE);
            } else {
                holder.location.setText(spLikedList.get(position).country);
            }
            holder.othersCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isCrossVisible) {
                        callBackInterface.onCellClicked(spLikedList.get(position).id, spLikedList.get(position).isMatch);
                    } else {
                        mEnum = showCrossEnum.HIDE_CROSS;
                        notifyDataSetChanged();
                        isCrossVisible = false;
                    }
                }
            });
            holder.othersCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    HottiesAdapter.this.mEnum = showCrossEnum.SHOW_CROSS;
                    notifyDataSetChanged();
                    isCrossVisible = true;
                    return true;
                    //return true,prevents to onclick to invoke while long press!
                }
            });


        } else {
            if (position == (getItemCount() - 1) && position >= 6/*so the it does,nt make a call to nxt pg when there's only 1 cell*/) {
                if (serviceCallflag) {
                    informHeyHotties.callNextPage(position);
                }/*else
                    serviceCallflag=true;*/
            }
            final ProfileModel model = profileModelList.get(position);
            if (model.getGender().length() > 0) {
                if (model.getGender().equalsIgnoreCase("male")) {
                    placeHolder = R.drawable.male_placeholder;
                    holder.gradientLayout.setBackgroundResource(R.drawable.gradient_male_profile);
                } else {
                    placeHolder = R.drawable.female_placeholder;
                    holder.gradientLayout.setBackgroundResource(R.drawable.gradient_female_profile);
                }
            }
            if (model.getImages().size() > 0) {
                Picasso.with(context).load(model.getImages().get(0).getImageUrl(UserImage.ImgType.SIZE_200x300)).placeholder(placeHolder).into(holder.dp);
            }
            holder.name.setText(model.getName());

            int year = 0;
            if (model.getYearOfBirthday() == null || model.getYearOfBirthday().length() == 0) {
                year = (new Date().getYear() + 1900);
            } else {
                year = (new Date().getYear() + 1900) - Integer.parseInt(model.getYearOfBirthday());
            }
            holder.age.setText("" + year);
            if (model.getLocation() == null || model.getLocation().getCountry().trim().length() == 0) {
                holder.locPin.setVisibility(View.GONE);
                holder.location.setVisibility(View.GONE);
            } else {
                holder.location.setText(model.getLocation().getCountry());
            }

            holder.othersCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mEnum = showCrossEnum.SHOW_CROSS;
                    isCrossVisible = true;
                    serviceCallflag = false;
                    notifyDataSetChanged();
                    return true;
                }
            });

            holder.othersCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isCrossVisible) {
                        serviceCallflag = true;
                        informHeyHotties.onCellClicked(model, position);
                    } else {
                        isCrossVisible = false;
                        mEnum = showCrossEnum.HIDE_CROSS;
                        serviceCallflag = true;
                        notifyDataSetChanged();
                    }
                }
            });


        }//else
        holder.icCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == SUPERHOTTIES_FRAG)
                    callBackInterface.onCrossClicked(spLikedList.get(position), holder.icCross);
                else
                    informHeyHotties.onCrossClicked(profileModelList.get(position), holder.icCross);
            }
        });
    }//onBind

    @Override
    public int getItemCount() {
        if (type.equals(SUPERHOTTIES_FRAG)) {
            return spLikedList.size();
        } else {
            return profileModelList.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        final int modeTwo = position % 2;
        if (modeTwo % 2 == 0) {
            return REDUCE_HEIGHT;
        } else {
            return FULL_HEIGHT;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView othersCard;
        ImageView dp, locPin, icCross;
        TextViewPlus name, age, location;
        RelativeLayout gradientLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            othersCard = (CardView) itemView.findViewById(R.id.others_profile_Card);
            dp = (ImageView) itemView.findViewById(R.id.profile_dp);
            locPin = (ImageView) itemView.findViewById(R.id.loc_Pin);
            icCross = (ImageView) itemView.findViewById(R.id.ic_cross);
            name = (TextViewPlus) itemView.findViewById(R.id.name);
            age = (TextViewPlus) itemView.findViewById(R.id.age);
            location = (TextViewPlus) itemView.findViewById(R.id.location);
            gradientLayout = (RelativeLayout) itemView.findViewById(R.id.gradientLayout);
        }
    }
}
