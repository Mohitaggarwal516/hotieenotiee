package com.codeplaylabs.hotch.home.models;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.database.tables.ProfileTable;
import com.codeplaylabs.hotch.login.models.Detail;
import com.codeplaylabs.hotch.login.models.Education;
import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.login.models.Work;

import java.util.ArrayList;

/**
 * Created by mohit on 24/07/17.
 */

public class ProfileModel extends BaseModel {
    protected String id;
    protected String name;
    protected String gender;
    protected String yearOfBirthday;
    protected String about;

    protected Detail location = null;

    protected ArrayList<UserImage> images;
    protected ArrayList<Detail> interests;
    protected boolean match;
    protected Work work;
    protected Education education;
    protected Detail employer;

    ProfileTable profileTable;
    protected String dateOfBirth;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getYearOfBirthday() {
        return yearOfBirthday;
    }

    public String getAbout() {
        return about;
    }

    public Detail getLocation() {
        return location;
    }


    public ArrayList<UserImage> getImages() {
        if (images == null) {
            images = new ArrayList<>();
        }
        return images;
    }


    public ArrayList<Detail> getInterests() {
        if (interests == null) {
            interests = new ArrayList<>();
        }
        return interests;
    }
    public void setInterests(ArrayList<Detail> interestList) {
        this.interests=interestList;
    }

    public boolean isMatch() {
        return match;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setYearOfBirthday(String yearOfBirthday) {
        this.yearOfBirthday = yearOfBirthday;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public void setLocation(Detail location) {
        this.location = location;
    }


    public void setMatch(boolean match) {
        this.match = match;
    }

    public Work getWork() {
        return work;
    }

    public void setWork(Work work) {
        this.work = work;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public ProfileTable getProfileTable() {
        return profileTable;
    }

    public void setProfileTable(ProfileTable profileTable) {
        this.profileTable = profileTable;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public Detail getEmployer() {

        return employer;
    }
}
