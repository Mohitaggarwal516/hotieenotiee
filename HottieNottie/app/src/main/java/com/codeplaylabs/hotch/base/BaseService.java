package com.codeplaylabs.hotch.base;

import android.app.Activity;
import android.os.AsyncTask;

import com.codeplaylabs.hotch.MyApplication;
import com.codeplaylabs.hotch.base.interfaces.BaseInterface;
import com.codeplaylabs.hotch.database.databaseInitialization.DatabaseHelper;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LogAnalytics;
import com.codeplaylabs.hotch.utils.Singleton;
import com.codeplaylabs.hotch.webConnection.WebConnectionHandler;
import com.codeplaylabs.hotch.webConnection.WebConnectionModel;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mohit on 03/07/17.
 */

public class BaseService {

    protected DatabaseHelper dbHelper = Singleton.getInstance().getHelper();

    protected static final int NO_TIME_FACTOR_REQ = 0;



    protected void parseData(BaseInterface baseInterface, BaseParser parser, String jsonString, Constants.ReceivedFrom receivedFrom) {
        BaseModel baseModel = null;
        try {
            if (jsonString == null) {
                /*BaseActivity.*/
                baseInterface.onResponse(null,null,Constants.JSON_STRING_NULL,receivedFrom);
            }
            else if (parser == null) {
                baseInterface.onResponse(null, jsonString, null, receivedFrom);
            } else {//(if(parser != null)
                baseModel = parser.parseJson(new JSONObject(jsonString));

                if (baseModel.error == null) {
                    baseInterface.onResponse(baseModel, jsonString, null, receivedFrom);
                } else {
                    baseInterface.onResponse(null, jsonString, baseModel.error, receivedFrom);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            baseInterface.onResponse(null, jsonString, Constants.Error.JSON_EXCEPTION, receivedFrom);
        }
    }


    protected void callService(final Activity activity, final WebConnectionModel webConnectionModel, final BaseParser parser, final BaseInterface profileResponseListener) {

        class Async extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                WebConnectionHandler webConnectionHandler = new WebConnectionHandler();
                webConnectionHandler.fetchAsyncRequestForModel(activity, webConnectionModel, new WebConnectionHandler.WebConnectionResponse() {
                    @Override
                    public void Response(String jsonObject, Constants.ReceivedFrom receivedFrom, WebConnectionModel webConnectionModel) {
                        if(jsonObject == null){
                            Tracker mtracker=MyApplication.getInstance().getTracker();

                            LogAnalytics.logSerViceFailEvent(mtracker,webConnectionModel.getJsonRequest(),webConnectionModel.getUrl());

                        }
                        parseData(profileResponseListener, parser, jsonObject, receivedFrom);

                    }
                });
                return null;
            }
        }
        new Async().execute();
    }
}
