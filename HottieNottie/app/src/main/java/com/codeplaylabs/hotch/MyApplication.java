package com.codeplaylabs.hotch;

import android.support.multidex.MultiDexApplication;

import com.codeplaylabs.hotch.utils.ConnectivityReceiver;
import com.codeplaylabs.hotch.utils.LifeCycleCallbackClass;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import io.branch.referral.Branch;

/**
 * Created by mohit on 04/08/17.
 */

public class MyApplication extends MultiDexApplication {

    private static MyApplication mInstance;
    private Tracker mTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        //FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        Branch.getAutoInstance(this);
        registerActivityLifecycleCallbacks(new LifeCycleCallbackClass());
        mInstance = this;
    }
    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    synchronized public Tracker getTracker() {
        if (mTracker == null) {
            GoogleAnalytics googleAnalytics = GoogleAnalytics.getInstance(this);
            mTracker = googleAnalytics.newTracker("UA-74337894-5"/*"UA-82853281-6"*/);
        }
        return mTracker;
    }
}
