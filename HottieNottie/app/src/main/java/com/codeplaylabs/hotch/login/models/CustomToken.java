package com.codeplaylabs.hotch.login.models;

import com.codeplaylabs.hotch.base.BaseModel;

/**
 * Created by mohit on 01/08/17.
 */

public class CustomToken extends BaseModel {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
