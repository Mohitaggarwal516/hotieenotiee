package com.codeplaylabs.hotch.settings.parsers;

import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.login.models.UserProfile;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONObject;

/**
 * Created by mohit on 05/07/17.
 */

public class ParseImageUpdateResponse extends BaseParser {
    BaseModel baseModel = new BaseModel();

    @Override
    public BaseModel parseJson(JSONObject response) {
        if (validate(response) != null) {
            return moduleJson(response);
        } else {
            baseModel.error = Constants.Error.SettingsError.PROFILE_SETTING_ERROR;
            return baseModel;
        }
    }

    /*
      * This method is called to collect the data received from the servers in a class for the session
      * @param response
     */
    public BaseModel moduleJson(JSONObject response) {

        UserProfile userProfile = new UserProfile();

        if (response != null) {
            CustomJsonObject cjObject = new CustomJsonObject(response);
            cjObject.getBoolean("status",false);

            return userProfile;
        } else {
            baseModel.error = Constants.Error.SettingsError.PROFILE_NOT_AVAILABLE_ERROR;
            return baseModel;
        }

    }

}
