/**
 * Copyright Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codeplaylabs.hotch.chats.models.firbaseModels;

import com.codeplaylabs.hotch.utils.CommonMethods;

import java.io.Serializable;

public class FriendlyMessage implements Serializable {

    private String id;
    private String text;
    private long timeStamp = 0;
    private int self = 0;

    private String dateStr;
    public FriendlyMessage() {
    }
/*
    public FriendlyMessage(String text, String name, String photoUrl, String imageUrl) {
        this.text = text;
        this.name = name;
        this.photoUrl = photoUrl;
        this.imageUrl = imageUrl;
    }*/

    public String getId() {
        return id;
    }

    public FriendlyMessage setId(String id) {
        this.id = id;
        return this;
    }


    public String getText() {
        return text;
    }

    public FriendlyMessage setText(String text) {
        this.text = text;
        return this;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public FriendlyMessage setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
        dateStr = CommonMethods.getChatDateAndTime(timeStamp);
//        if(timeStamp > 0) {
//            this.timeStamp =
//            this.timeStamp = new Date().getTime();
//        }
//        this.timeStamp = CommonMethods.localToGMT(timeStamp);
        return this;
    }

    public int isSelf() {
        return self;
    }

    public String getDateStr(){
      //  if(dateStr == null){
            dateStr = CommonMethods.getChatDateAndTime(timeStamp);

        //}
        return  dateStr;
    }

    public FriendlyMessage setSelf(int self) {
        this.self = self;
        return this;
    }
}
