package com.codeplaylabs.hotch.profileDetail.parsers;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.models.Detail;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by HP on 17-Aug-17.
 */
public class InterestParser extends BaseParser {
  public enum ReturnType{PROFILEMODEL,DETAIL}
    @Override
    public BaseModel parseJson(JSONObject response) throws JSONException {
        return parseInterestJson(response);
    }

    public BaseModel parseInterestJson(JSONObject response) {

        ProfileModel interestModel=new ProfileModel();
        try {
            CustomJsonObject cReponse=new CustomJsonObject(response);
            //JSONObject data
            CustomJsonObject cDataObj=new CustomJsonObject(cReponse.getJsonObject("data"));
            JSONArray userArray=cDataObj.getJsonArray("users");

            for (int i=0;i<userArray.length();i++){
                CustomJsonObject obj=new CustomJsonObject(userArray.getJSONObject(i));
                interestModel.setId(obj.getString("id"));
                JSONArray interestArray=obj.getJsonArray("interests");
                 for (int j=0;j<interestArray.length();j++){
                     Detail interestDetail=new Detail();
                     JSONObject subObj=interestArray.getJSONObject(j);
                     interestDetail.setId(subObj.getString("id"));
                     interestDetail.setName(subObj.getString("name"));
                     interestModel.getInterests().add(interestDetail);
                 }
            }
            return interestModel;
        } catch (JSONException e) {
            e.printStackTrace();
            interestModel.error= Constants.Error.JSON_EXCEPTION;
            return interestModel;
        }
        /*CustomJsonObject responseObj=new CustomJsonObject(response);
        CustomJsonObject data=responseObj.getJsonObject("data");*/

    }
}
