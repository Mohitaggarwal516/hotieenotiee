package com.codeplaylabs.hotch.profileDetail;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by HP on 14-Aug-17.
 */

public class ReportParser extends BaseParser {
    @Override
    public BaseModel parseJson(JSONObject response) throws JSONException {
        return parseReportJson(response);
    }
    private BaseModel parseReportJson(JSONObject object){
        CustomJsonObject customJsonObject=new CustomJsonObject(object);
        boolean status=customJsonObject.getBoolean("status",false);
        if(status){
            return new BaseModel();
        }else {
            return null;
        }
    }
}
