package com.codeplaylabs.hotch.chats.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private final ArrayList<Fragment> fragments;
    //private final ChatFragmentActivity.FragmentPagerListener fragmentPagerListener;

    public SectionsPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments/*, ChatFragmentActivity.FragmentPagerListener fragmentPagerListener*/) {
            super(fm);
            //this.fragmentPagerListener = fragmentPagerListener;
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a OldChatListFragment (defined as a static inner class below).
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return fragments.size();
        }
    }
