package com.codeplaylabs.hotch.profileDetail.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.profileDetail.ui.ProfileDetailActivity;
import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.database.tables.UserImageTable;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by HP on 04-Jul-17.
 */

public class HeaderAdapter extends PagerAdapter {

    private List<UserImage> profileImageList = null;
    int[] img = {R.drawable.profileback, R.drawable.back_hover};
    private final Context context;
    LayoutInflater inflater;
    List<UserImageTable> imagesList;
    ProfileDetailActivity.From type = ProfileDetailActivity.From.MyProfile;
    private int placeHolderImg ;

    public HeaderAdapter(Context context,List<UserImageTable> imagesList, String gender) {
        this.context = context;
        this.imagesList = imagesList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        initialisePlaceHolder(gender);
    }
    public HeaderAdapter(Context context,List<UserImage> imagesList, ProfileDetailActivity.From type, String gender) {
        this.context = context;
        this.profileImageList = imagesList;
        this.type = type;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        initialisePlaceHolder(gender);
    }
    @Override
    public int getCount() {
        if(type == ProfileDetailActivity.From.MyProfile) {
            return imagesList.size();
        }else{
            return profileImageList.size();
        }
        //return img.length;
    }
    private void initialisePlaceHolder(String mGender){
        if(mGender.equalsIgnoreCase("male")){
            placeHolderImg=R.drawable.male_placeholder;
        }
        else
            placeHolderImg=R.drawable.female_placeholder;
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (View) object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        /*super.destroyItem(container, position, object);*/
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //return super.instantiateItem(container, position);
        View view = inflater.inflate(R.layout.eachpage_pllx_recyclerview_header_viewpager, container, false);
        ImageView imgView = (ImageView) view.findViewById(R.id.img_each_page);
        //imgView.setImageResource(img[position]);
        if(type == ProfileDetailActivity.From.MyProfile) {
            if (imagesList.get(position).imageUrl == null || imagesList.get(position).imageUrl.length() == 0) {
                Picasso.with(context)
                        .load(imagesList.get(position).imageUri).placeholder(placeHolderImg)
                        .into(imgView);
            } else //if (imagesList.get(position).imageSource.equals("Facebook_Album")) {
                Picasso.with(context)
                        .load(imagesList.get(position).getImageUrl()).placeholder(placeHolderImg)
                        .into(imgView);
            //} else {
//                Picasso.with(context)
//                        .load(imagesList.get(position).imageBaseUrl + "/" + imagesList.get(position).imageUrl)
//                        .into(imgView);
//            }
        }else{
           /* if (profileImageList.get(position).getImageUrl().contains("http")) {
                *//*Picasso.with(context)
                        .load(profileImageList.get(position).getImageUrl())
                        .into(imgView);*//*
                new SuperPicasso().passContext(context).into(imgView).load(profileImageList.get(position).getImageUrl());

            } else {*/
                Picasso.with(context)
                        .load(profileImageList.get(position).getImageUrl()).placeholder(placeHolderImg)
                        .into(imgView);/*
                new SuperPicasso().passContext(context).into(imgView).load(profileImageList.get(position).getImageUrl());*/
            //}
        }
        container.addView(view);
        return view;
    }
}
