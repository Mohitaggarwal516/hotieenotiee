package com.codeplaylabs.hotch.settings.models.albums.discovery;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.login.models.Detail;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by HP on 15-Jul-17.
 */

public class DiscoverySettingModel extends BaseModel {
    private String minAge="16";
    private String maxAge="50";
    private ArrayList<String> gender;
    private Detail currentLocation=null;
    private Detail hometown=null;
    private Detail country=null;
    private Detail currentlyAt=null;
    private HashMap<String,Boolean> notificationSetting;

    public String getMinAge() {
        return minAge;
    }

    public String getMaxAge() {
        return maxAge;
    }

    public ArrayList<String> getGender() {
        if(gender==null){
            gender=new ArrayList<>();
        }
        return gender;
    }

    public Detail getCurrentLocation() {

        return currentLocation;
    }

    public Detail getHometown() {

        return hometown;
    }

    public Detail getCountry() {

        return country;
    }

    public Detail getCurrentlyAt() {

        return currentlyAt;
    }

    public HashMap<String, Boolean> getNotificationSetting() {
        if(notificationSetting == null){
            notificationSetting = new HashMap<>();
        }
        return notificationSetting;
    }

    public void setMinAge(String minAge) {
        this.minAge = minAge;
    }

    public void setMaxAge(String maxAge) {
        this.maxAge = maxAge;
    }


    public void setCurrentLocation(Detail currentLocation) {
        this.currentLocation = currentLocation;
    }

    public void setHometown(Detail hometown) {
        this.hometown = hometown;
    }

    public void setCountry(Detail country) {
        this.country = country;
    }

    public void setCurrentlyAt(Detail currentlyAt) {
        this.currentlyAt = currentlyAt;
    }

}
