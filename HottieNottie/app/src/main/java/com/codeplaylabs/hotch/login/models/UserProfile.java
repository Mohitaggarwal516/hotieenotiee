package com.codeplaylabs.hotch.login.models;


import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.database.tables.UserImageTable;
import com.codeplaylabs.hotch.settings.handler.SettingsDbHandler;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by mohit on 1/12/2017.
 */

public class UserProfile extends BaseModel {

    private String id;
    private String userId;
    private String name;
    private String profileImageUrl;
    private String email;
    private String gender;
    private String birthday;
    private String about;
    private ArrayList<Education> educationList;
    private ArrayList<Work> workList;
    private Detail location = null;
    private Detail hometown = null;
    private ArrayList<UserImage> images;
    private ArrayList<Detail> interests ;


    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImageUrl() {
        if(profileImageUrl == null){
            List<UserImageTable> var = SettingsDbHandler.getAllDataFromImageTable();
            UserImageTable userImageTable = var.get(0);
            if(userImageTable.imageUrl.contains("http")){
                profileImageUrl = userImageTable.imageUrl;
            }else{
                profileImageUrl = userImageTable.imageBaseUrl +"/"+ userImageTable.imageUrl;
            }
        }
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public ArrayList<Education> getEducationList() {
        if(educationList == null){
            educationList = new ArrayList<>();
        }
        return educationList;
    }

    public ArrayList<Work> getWorkList() {

        if(workList == null){
         workList = new ArrayList<>();
        }
        return workList;
    }

    public ArrayList<UserImage> getImagesList() {

        if(images == null){
            images = new ArrayList<>();
        }
        return images;
    }

    public ArrayList<Detail> getInterestList() {

        if(interests == null){
            interests = new ArrayList<>();
        }
        return interests;
    }

    public Detail getLocation() {
        return location;
    }

    public void setLocation(Detail location) {
        this.location = location;
    }

    public Detail getHometown() {
        return hometown;
    }

    public void setHometown(Detail hometown) {
        this.hometown = hometown;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
