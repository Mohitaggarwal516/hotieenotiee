package com.codeplaylabs.hotch.ratings.interfaces;

import android.support.v7.widget.RecyclerView;

import com.codeplaylabs.hotch.ratings.models.RateDiscoveryWrapper;

/**
 * Created by HP on 28-Aug-17.
 */

public interface CallbackInterface {
    public void passRecyclerView(RecyclerView recyclerView, RateDiscoveryWrapper.filterEnum filterEnum, String id, /*ArrayList<ProfileModel> pgrProfileModelList,*/ PassProfileWrapper passProfileWrapper);

}
