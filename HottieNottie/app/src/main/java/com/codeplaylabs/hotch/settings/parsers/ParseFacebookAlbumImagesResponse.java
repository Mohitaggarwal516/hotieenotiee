package com.codeplaylabs.hotch.settings.parsers;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.settings.models.albums.AlbumModel;
import com.codeplaylabs.hotch.settings.models.albums.AlbumWrapper;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mohit on 30/06/17.
 */

public class ParseFacebookAlbumImagesResponse extends BaseParser {
    BaseModel baseModel = new BaseModel();

    @Override
    public BaseModel parseJson(JSONObject response) {
        return moduleJson(response);
    }

    /*
      * This method is called to collect the data received from the servers in a class for the session
      * @param response
     */
    public BaseModel moduleJson(JSONObject response) {

        try {
            if (response != null) {

                AlbumWrapper albumWrapper = new AlbumWrapper();

                JSONArray albumsObj = response.getJSONObject("photos").getJSONArray("data");

                for (int index = 0; index < albumsObj.length(); index++) {
                    AlbumModel albumModel = new AlbumModel();
                    CustomJsonObject obj = new CustomJsonObject(albumsObj.getJSONObject(index));
                    albumModel.setId(obj.getString("id"));
                    albumModel.setThumbnailImage(obj.getString("picture"));
                    String s = obj.getJsonArray("images").getJSONObject(0).getString("source");
                    albumModel.setUrl(s);
                    albumWrapper.getFacebookAlbums().add(albumModel);
                }

                return albumWrapper;

            } else {

                baseModel.error = Constants.Error.NULL_RESPONSE_ERROR;
                return baseModel;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            baseModel.error = Constants.Error.JSON_EXCEPTION;
            return baseModel;
        }


    }
}
