package com.codeplaylabs.hotch.login.parsers;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.login.models.FBUserProfileResponseModel;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONObject;

/**
 * Created by mohit on 10/07/17.
 */

public class ParseUserFbInterest extends BaseParser {
    BaseModel baseModel  = new BaseModel();
    @Override
    public BaseModel parseJson(JSONObject response) {
        if (validate(response) != null) {
            return moduleJson(response);
        } else {
            baseModel.error = Constants.Error.NULL_RESPONSE_ERROR;
            return baseModel;
        }
    }

    /*
      * This method is called to collect the data received from the servers in a class for the session
      * @param response
     */
    public FBUserProfileResponseModel moduleJson(JSONObject response) {


        CustomJsonObject cjObject = new CustomJsonObject(response);
        FBUserProfileResponseModel profile = new FBUserProfileResponseModel();
        cjObject = cjObject.getCustomJsonObject("data");

        return profile;

    }
}
