package com.codeplaylabs.hotch.ratings.models;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.login.models.Detail;

import java.util.ArrayList;

/**
 * Created by HP on 30-Aug-17.
 */

public class RateDiscoveryWrapper extends BaseModel {

    public enum filterEnum {Random, CurrentCountry, CurrentLocation, CurrentlyAt}

    ArrayList<Detail> discoveryWrapper;
    ArrayList<filterEnum> mEnumList;

    public ArrayList<filterEnum> getmEnumList() {
        if(mEnumList==null){
            mEnumList=new ArrayList<>();
        }
        return mEnumList;
    }

    public ArrayList<Detail> getDiscoveryWrapper() {
        if (discoveryWrapper==null){
            discoveryWrapper=new ArrayList<>();
        }
        return discoveryWrapper;
    }
}
