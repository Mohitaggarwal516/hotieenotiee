package com.codeplaylabs.hotch.profileDetail.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseActivity;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.interfaces.OnTaskCompleteInterface;
import com.codeplaylabs.hotch.chats.handler.ChatDBHandlerClass;
import com.codeplaylabs.hotch.chats.models.firbaseModels.UserModel;
import com.codeplaylabs.hotch.chats.ui.ChatFragmentActivity;
import com.codeplaylabs.hotch.database.tables.ChatUsersList;
import com.codeplaylabs.hotch.database.tables.UserImageTable;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.home.services.HomeServices;
import com.codeplaylabs.hotch.home.ui.HomeActivity;
import com.codeplaylabs.hotch.login.interfaces.FacebookUserInfoResponse;
import com.codeplaylabs.hotch.login.interfaces.SharedProfileResponse;
import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.login.models.UserProfile;
import com.codeplaylabs.hotch.login.services.FacebookGraphService;
import com.codeplaylabs.hotch.others.models.ProfileWrapper;
import com.codeplaylabs.hotch.profileDetail.MyParallaxScollListView;
import com.codeplaylabs.hotch.profileDetail.adapters.HeaderAdapter;
import com.codeplaylabs.hotch.profileDetail.adapters.PublicListViewAdapter;
import com.codeplaylabs.hotch.profileDetail.adapters.myListViewAdapter;
import com.codeplaylabs.hotch.profileDetail.interfaces.ProfileDetailOnClickListener;
import com.codeplaylabs.hotch.profileDetail.interfaces.ReportResponse;
import com.codeplaylabs.hotch.settings.handler.SettingsDbHandler;
import com.codeplaylabs.hotch.utils.CommonMethods;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.utils.LogAnalytics;
import com.codeplaylabs.hotch.utils.TextViewPlus;
import com.google.firebase.database.FirebaseDatabase;
import com.rd.PageIndicatorView;
import com.rd.animation.type.AnimationType;
import com.squareup.picasso.Picasso;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by kapish on 03-Jul-17.
 */

public class ProfileDetailActivity extends BaseActivity {

    private static final int RESULT_REPORT = 9876;
    MyParallaxScollListView parallaxListView;
    PagerAdapter pagerAdapter;
    ViewPager viewPager;
    UserProfile userProfile;


    private myListViewAdapter adapter;
    private PublicListViewAdapter publicAdapter;
    private List<UserImageTable> images;
    private LinearLayout interestLayout;
    private LinearLayout layout_pgController;
    private RadioButton[] radioBtn;
    private RadioGroup rg_pgIndicator;
    private PageIndicatorView pageIndicatorView;
    private From type = From.PublicProfile;
    private ProfileModel profileModel;
    private RelativeLayout btmStrip;
    private ImageView btmGradient, hot, superHot, not;
    private EditText reasonEd;
    private LinearLayout dgLay;
    //private int displayHeight;
    private RelativeLayout reportDgLay;
    private RelativeLayout layOutsideAlert;
    private int count = 0;
    private final int VISIBILITY_GONE = View.GONE;
    private final int VISIBILITY_INVISIBLE = View.INVISIBLE;
    private boolean isMatch;
    private boolean isFromHeyHottie;
    private RelativeLayout superHotNotAvailableLay;

    public enum From {PublicProfile, MyProfile, OTHER, RATING, CHAT}

    public static final int HOT_PRESSED = 1;
    public static final int SUPER_HOT_PRESSED = 2;
    public static final int NOT_PRESSED = 3;
    public static final int REPORTED = 4;
    private ImageView hotBanner, notBanner, superHotBanner;
    private TextViewPlus chatNowBtn;
    private RelativeLayout chatBtnLay, loader;
    private boolean callDesciprionService = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user_profile_detail);
        hotBanner = (ImageView) findViewById(R.id.hot_profile_banner);
        notBanner = (ImageView) findViewById(R.id.not_profile_banner);
        superHotBanner = (ImageView) findViewById(R.id.superhot_profile_banner);

        loader = (RelativeLayout) findViewById(R.id.loader_layout);
        //displayHeight=getWindowManager().getDefaultDisplay().getHeight();
        parallaxListView = (MyParallaxScollListView) findViewById(R.id.parallaxRecyclerView);
        hot = (ImageView) findViewById(R.id.hot);
        not = (ImageView) findViewById(R.id.not);
        superHot = (ImageView) findViewById(R.id.superHot);


        reportDgLay = (RelativeLayout) findViewById(R.id.root_alert);
        superHotNotAvailableLay = (RelativeLayout) findViewById(R.id.superhot_not_available_now);

        hot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent();
                //likeTextView.animate().alpha(1).setDuration(50);
                hotBanner.setVisibility(View.VISIBLE);

                intent.putExtra("profileId", profileModel.getProfileTable().id);
                setResult(HOT_PRESSED, intent);
                finish();
                overridePendingTransition(R.anim.zoom_in,
                        R.anim.slide_out_right);

                //startActivity(intent);

            }
        });

        not.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notBanner.setVisibility(View.VISIBLE);
                Intent intent = new Intent(ProfileDetailActivity.this, HomeActivity.class);
                intent.putExtra("profileId", profileModel.getProfileTable().id);
                setResult(NOT_PRESSED, intent);

                finish();
                overridePendingTransition(R.anim.zoom_in, R.anim.slide_out_left);
            }
        });

        superHot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().initialise(ProfileDetailActivity.this);
                final long value = com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().getLongPreference("superhotEnabled", 0);
                if (value < System.currentTimeMillis()) {
                    superHotBanner.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(ProfileDetailActivity.this, HomeActivity.class);
                    intent.putExtra("profileId", profileModel.getProfileTable().id);
                    setResult(SUPER_HOT_PRESSED, intent);
                    finish();


                    overridePendingTransition(R.anim.zoom_in,
                            R.anim.slide_out_top);
                } else {
                    showTimer(profileModel, value);
                }

            }
        });

        chatBtnLay = (RelativeLayout)

                findViewById(R.id.chatBtnLay);

        type = From.valueOf(

                getIntent().

                        getStringExtra("from"));
        if (type == From.MyProfile)

        {
            userProfile = (UserProfile) getIntent().getSerializableExtra("profile");
            images = SettingsDbHandler.query("order", null, null);
            hideBottomStrip(VISIBILITY_GONE);
            setImagesAdapter(images);
            setAdapter(userProfile);
        } else

        {
            profileModel = (ProfileModel) getIntent().getSerializableExtra("profile");
            isFromHeyHottie = getIntent().getBooleanExtra("isFromHeyHottie", false);
            callDesciprionService = getIntent().getBooleanExtra("CallDescription", false);
            //call interest service
            if (type == From.OTHER || callDesciprionService/*type==From.RATING*/) {
                // boolean isFromHeyHottie = getIntent().getBooleanExtra("isFromHeyHottie", false);
               /* if (isFromHeyHottie) {*/

                loader.setVisibility(View.VISIBLE);
                new FacebookGraphService().profileDescriptionService(ProfileDetailActivity.this, profileModel.getId(), "", new SharedProfileResponse() {
                    @Override
                    public void onResponse(final BaseModel baseModel, String jsonString, final String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loader.setVisibility(View.GONE);
                                if (reasonOfError == null) {
                                    ProfileWrapper wrapper = (ProfileWrapper) baseModel;
                                    if (wrapper.getProfileWrapper().size() > 0) {
                                        profileModel = wrapper.getProfileWrapper().get(0);
                                        setPublicProfile(profileModel);
                                    }
                                } else {
                                    showNoNetDialog();
                                }
                            }
                        });

                    }
                });
                if (isFromHeyHottie) {
                    chatBtnLay.setVisibility(View.VISIBLE);
                    isMatch = getIntent().getBooleanExtra("isMatch", false);
                    onChatNowClicked(isMatch);
                    hideBottomStrip(VISIBILITY_INVISIBLE);
                } else {
                    hideBottomStrip(VISIBILITY_GONE);
                }
            }
           /* if (type == From.RATING) {
                hideBottomStrip();
            }*/
            if (!isFromHeyHottie && !callDesciprionService/*type!=From.RATING*/) {
                setPublicProfile(profileModel);
            }
        }

    }//end_onCreate()

    private void showTimer(ProfileModel profileModel, final long time) {
        superHotNotAvailableLay.setVisibility(View.VISIBLE);
        final TextView reasonEdt = (TextView) superHotNotAvailableLay.findViewById(R.id.reasonTxt);
        ImageView image = (ImageView) findViewById(R.id.image_super_hot);
        int placeHolder = R.drawable.female_placeholder;
//        if (profileModel.getGender().equalsIgnoreCase("male")) {
//            placeHolder = R.drawable.male_placeholder;
//        }
        if (profileModel.getImages() == null || profileModel.getImages().get(0) == null || profileModel.getImages().get(0).getImageUrl(UserImage.ImgType.SIZE_100x100).length() == 0) {
            Picasso.with(ProfileDetailActivity.this).load(placeHolder).into(image);
        } else {
            Picasso.with(ProfileDetailActivity.this).load(profileModel.getImages().get(0).getImageUrl()).placeholder(placeHolder).centerCrop().resize(getWindowManager().getDefaultDisplay().getWidth(), getWindowManager().getDefaultDisplay().getHeight()).into(image);
        }
        RelativeLayout layOutsideAlert = (RelativeLayout) superHotNotAvailableLay.findViewById(R.id.lay_outside);
        reasonEdt.setText(CommonMethods.timer(time));
        final Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        reasonEdt.setText(CommonMethods.timer(time));
                    }
                });
            }
        }, 1000, 1000);
        TextViewPlus positiveBtn = (TextViewPlus) superHotNotAvailableLay.findViewById(R.id.positiveBtn);

        positiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                superHotNotAvailableLay.setVisibility(View.GONE);
            }
        });


        layOutsideAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                superHotNotAvailableLay.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onTryAgainClicked() {
        //Toast.makeText(this, "tryAgain", Toast.LENGTH_SHORT).show();
        dialog.dismiss();
        dialog = null;
        Intent intent = new Intent(ProfileDetailActivity.this, ProfileDetailActivity.class);
        intent.putExtra("from", "" + type);
        if (type == From.MyProfile)
            intent.putExtra("profile", userProfile);
        else
            intent.putExtra("profile", profileModel);
        if (isFromHeyHottie) {
            intent.putExtra("isFromHeyHottie", isFromHeyHottie);
            intent.putExtra("isMatch", isMatch);
        }
        intent.putExtra("CallDescription", callDesciprionService);
        startActivity(intent);
        finish();
        super.onTryAgainClicked();
    }

    private void onChatNowClicked(final boolean isMatch) {
        chatNowBtn = (TextViewPlus) findViewById(R.id.chat_now_btn);
        chatNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMatch) {

                    LogAnalytics.logButtonClickEvent(mTracker, Constants.Buttons.CHAT_NOW_PROFILE_DETAIL);
                    //intent to chat screen
                    ArrayList<String> arrayList = new ArrayList<>();
                    arrayList.add(LocalPreferenceManager.getInstance().getUserId());
                    arrayList.add(profileModel.getId());
                    final String key = CommonMethods.getUniqueKey(arrayList);
                    profileModel.setMatch(true);
                    new HomeServices().sendDataToServers(ProfileDetailActivity.this, profileModel, null);
                    Intent intent = new Intent(ProfileDetailActivity.this, ChatFragmentActivity.class);
                    ChatUsersList val = ChatDBHandlerClass.getChatFromTable(key);
                    if (val == null || val.getLastMessage() == null || val.getLastMessage().length() == 0) {
                        intent.putExtra("calledFrom", ChatFragmentActivity.NEW_CHAT);
                    } else {
                        intent.putExtra("calledFrom", ChatFragmentActivity.OLD_CHAT);
                    }
                    startActivity(intent);
                } else {
                    Snackbar snackbar = Snackbar.make((RelativeLayout) findViewById(R.id.rootLayout), "You can no longer chat with this person", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });
    }

//    private UserModel setProfileToFirebaseChat(ProfileModel profileModel, String key) {
//        UserModel userModel = new UserModel();
//        userModel.setName(profileModel.getName());
//        userModel.setChatId(key);
//        userModel.setTimestamp(System.currentTimeMillis());
//        userModel.setImage(profileModel.getImages().get(0).getImageUrl());
//        FirebaseDatabase.getInstance().getReference("User").child(LocalPreferenceManager.getInstance().getUserId())
//                .child("Matches").child(profileModel.getId())
//                .setValue(new UserModel().setName(profileModel.getName())
//                        .setTimestamp(System.currentTimeMillis())
//                        .setChatId(key));
//        FirebaseDatabase.getInstance().getReference("User").child(profileModel.getId())
//                .child("Matches").child(LocalPreferenceManager.getInstance().getUserId())
//                .setValue(new UserModel().setName(LocalPreferenceManager.getInstance().getUserName())
//                        .setTimestamp(System.currentTimeMillis())
//                        .setChatId(key));
//        FirebaseDatabase.getInstance().getReference("User").child(profileModel.getId())
//                .child("Detail").setValue(new UserModel().setName(profileModel.getName())
//                .setImage(profileModel.getImages().get(0).getImageUrl()));
//
//        FirebaseDatabase.getInstance().getReference("User").child(LocalPreferenceManager.getInstance().getUserId())
//                .child("Detail").setValue(new UserModel().setName(LocalPreferenceManager.getInstance().getUserName())
//                .setImage(LocalPreferenceManager.getInstance().getUserImage()));
//        return userModel;
//    }

    private void hideBottomStrip(int visibility) {
        btmStrip = (RelativeLayout) findViewById(R.id.btm_strip_lay);
        btmGradient = (ImageView) findViewById(R.id.btm_gradient);
//        btmGradient.setVisibility(View.GONE);
        btmStrip.setVisibility(visibility);
    }

    private void setPublicProfile(ProfileModel profileModel) {
        if (profileModel == null) {
            return;
        }
        setImagesAdapter(profileModel.getImages());
        setPublicProfileAdapter(profileModel);
        //setPublicProfileAdapter(profileModel);
    }


    private void setImagesAdapter(List list) {
        View header = LayoutInflater.from(this).inflate(R.layout.pllx_recyclerview_header_viewpager, null);
        viewPager = (ViewPager) header.findViewById(R.id.img_viewPager);

        if (pagerAdapter == null) {
            if (type == From.MyProfile) {
                pagerAdapter = new HeaderAdapter(ProfileDetailActivity.this, (List<UserImageTable>) list, userProfile.getGender());
            } else {
                pagerAdapter = new HeaderAdapter(ProfileDetailActivity.this, (List<UserImage>) list, type, profileModel.getGender());
            }

            // viewPager.setMinimumHeight(getWindowManager().getDefaultDisplay().getHeight()/2);
            //int minheight=(getWindowManager().getDefaultDisplay().getHeight())/2;
            ViewGroup.LayoutParams viewPagerParams = viewPager.getLayoutParams();
            viewPagerParams.height = ((getWindowManager().getDefaultDisplay().getHeight()) / 2) + 250;
            // viewPagerParams.height = (displayHeight / 2) + 250;

//setting margins around imageimageview


//adding attributes to the imageview
            viewPager.setLayoutParams(viewPagerParams);
            viewPager.setAdapter(pagerAdapter);

            //library
            pageIndicatorView = (PageIndicatorView) header.findViewById(R.id.pageIndicatorView);
            pageIndicatorView.setViewPager(viewPager);
            pageIndicatorView.setAnimationDuration(10000);
            pageIndicatorView.setAnimationType(AnimationType.DROP);
            pageIndicatorView.setInteractiveAnimation(true);
            pageIndicatorView.setRadius(3);
            RelativeLayout.LayoutParams pgIndicatorParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
            );


            pgIndicatorParams.setMargins(0, (viewPagerParams.height - 100), 0, 0);
            pgIndicatorParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

            pageIndicatorView.setLayoutParams(pgIndicatorParams);
            parallaxListView.setZoomRatio(MyParallaxScollListView.NO_ZOOM);
            //association between viewPager and listView
            parallaxListView.setParallaxImageView(viewPager);
            parallaxListView.addHeaderView(header);
        } else {
            pagerAdapter.notifyDataSetChanged();
        }
        //addPageIndiCator();
    }

    private void setPublicProfileAdapter(final ProfileModel profileModel) {
        if (publicAdapter == null) {

            ArrayList<ProfileModel> userProfiles = new ArrayList<>();
            userProfiles.add(profileModel);
            publicAdapter = new PublicListViewAdapter(ProfileDetailActivity.this, userProfiles, new ProfileDetailOnClickListener() {
                @Override
                public void onMenuClick(View v) {
                    PopupMenu menu = new PopupMenu(ProfileDetailActivity.this, v);
                    menu.inflate(R.menu.profile_detail_menu/*, (Menu) menu*/);
                    menu.show();

                    menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            if (item.getItemId() == R.id.profile_detail_report) {
                                /*open Alert
                                and call report service on click of yes
                                 */

                                customAlertDialog(ProfileDetailActivity.this, "Report " + profileModel.getName() + " ?", "Do you really want to report this profile.", "yes", "no");
                            }
                            return false;
                        }
                    });
                }

                @Override
                public void onShareClicked(Context context, ProfileModel profileModel, int position, final ProgressBar view, final ImageView shareBtn) {

                    LogAnalytics.logButtonClickEvent(mTracker, Constants.Buttons.SHARE_PUBLIC_PROFILE);
                    //Toast.makeText(ProfileDetailActivity.this, "shareClicked", Toast.LENGTH_SHORT).show();
                    createBranchLink(ProfileDetailActivity.this, profileModel.getImages().get(position).getImageUrl(), profileModel.getId(), Constants.SHARE_PUBLIC_PROFILE, new OnTaskCompleteInterface() {
                        @Override
                        public void onTaskComplete() {
                            view.setVisibility(View.GONE);
                            shareBtn.setVisibility(View.VISIBLE);
                        }
                    });

                }

                @Override
                public void shareMyProfile(Context context, UserProfile userProfile, int position, ProgressBar loader, ImageView shareBtn) {
                }

                @Override
                public void callFewInterestSerVice(final Context context, String id, final FlowLayout flowLayout, final CardView interestCard) {
                    new FacebookGraphService().getFewInterest((Activity) context, id, Constants.TYPE, new FacebookUserInfoResponse() {
                        @Override
                        public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                            if (baseModel != null) {
                                final ProfileModel interestModel = (ProfileModel) baseModel;
                                if (interestModel.getInterests().size() > 0)
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            interestCard.setVisibility(View.VISIBLE);
                                        }
                                    });

                                for (int i = 0; i < interestModel.getInterests().size(); i++) {
                                    final int finalI = i;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            TextView textView = new TextView(context);
                                            textView.setText(interestModel.getInterests().get(finalI).getName());
                                            textView.setBackground(getResources().getDrawable(R.drawable.bg_few_interests));
                                            textView.setTextColor(getResources().getColor(R.color.black));
                                            textView.setPadding(8, 8, 8, 8);
                                            textView.setGravity(Gravity.CENTER);
                                            FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                            params.setMargins(10, 10, 10, 10);
                                            textView.setLayoutParams(params);
                                            textView.setTextSize(14);
                                            textView.setMaxLines(1);
                                            textView.setMaxEms(15);
                                            textView.setEllipsize(TextUtils.TruncateAt.END);

                                            flowLayout.setPadding(5, 5, 5, 5);
                                            flowLayout.addView(textView);
                                        }
                                    });

                                }
                            }
                        }
                    });
                }
            });
            parallaxListView.setAdapter(publicAdapter);

        } else {
            //adapter.notifyDataSetChanged();
            publicAdapter.notifyDataSetChanged();
        }
    }
    /*private void createBranchLink(String imgUrl, String targetUserId,String description){
        BranchUniversalObject branchUniversalObject = new BranchUniversalObject()
                .setCanonicalIdentifier("item/12345")
                .setTitle("HottieNottie"*//*"My Content Title"*//*)
                .setContentDescription(description*//*"My Content Description"*//*)
                .setContentImageUrl(imgUrl*//*"https://example.com/mycontent-12345.png"*//*)
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .addContentMetadata("targetUserId",targetUserId);*//*
                            .addContentMetadata("userName", Singleton.getInstance().getUserProfile().getName())
                            .addContentMetadata("userId",Singleton.getInstance().getUserProfile().getId());*//*


        LinkProperties linkProperties = new LinkProperties()
                .setChannel("facebook")
                .setFeature("sharing");
                           *//* .addControlParameter("$desktop_url", "http://example.com/home")
                            .addControlParameter("$ios_url", "http://example.com/ios");*//*

        branchUniversalObject.generateShortUrl(ProfileDetailActivity.this, linkProperties, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error == null) {
                    Log.i("ProfileDetailActivity :", "got my Branch link to share: " + url);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_TEXT, url);
                    intent.setType("text/plain");
                    startActivity(intent);
                }
            }

        });
    }*/

    private void setAdapter(UserProfile userProfile) {
        if (adapter == null) {
            ArrayList<UserProfile> userProfiles = new ArrayList<>();
            userProfiles.add(userProfile);
            adapter = new myListViewAdapter(ProfileDetailActivity.this, userProfiles, new ProfileDetailOnClickListener() {
                @Override
                public void onMenuClick(View v) {
                    PopupMenu menu = new PopupMenu(ProfileDetailActivity.this, v);
                    menu.inflate(R.menu.profile_detail_menu);
                    menu.show();
                }

                @Override
                public void onShareClicked(Context context, ProfileModel userProfile, int position, ProgressBar loader, ImageView shareBtn) {
                }

                @Override
                public void shareMyProfile(Context context, UserProfile userProfile, int position, final ProgressBar loader, final ImageView shareBtn) {
                    LogAnalytics.logButtonClickEvent(mTracker, Constants.Buttons.SHARE_PERSONAL_PROFILE);
                    String myDpUrl = SettingsDbHandler.fetchMyDpFromUserImageTable();
                    createBranchLink(ProfileDetailActivity.this, myDpUrl/*userProfile.getProfileImageUrl()*/, userProfile.getUserId(), Constants.SHARE_My_PROFILE, new OnTaskCompleteInterface() {
                        @Override
                        public void onTaskComplete() {
                            loader.setVisibility(View.GONE);
                            shareBtn.setVisibility(View.VISIBLE);
                        }
                    });
                }

                @Override
                public void callFewInterestSerVice(Context context, String id, FlowLayout flowLayout, CardView interestCard) {

                }
            });
            parallaxListView.setAdapter(adapter);

        } else {
            adapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onBackPressed() {
        if (reportDgLay.getVisibility() == View.VISIBLE) {
            reportDgLay.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void customAlertDialog(Context context, String title, String msg, String posBtn, String negBtn) {
        /*final AlertDialog dialog = new AlertDialog.Builder(context).create();
        View view = getLayoutInflater().inflate(R.layout.alert_report, null, false);


        if (msg.length() > 0 && posBtn.length() > 0 && negBtn.length() > 0) {

            final int POSITIVE = 0, NEGATIVE = 1;

            TextViewPlus titleTxt = (TextViewPlus) view.findViewById(R.id.titleTxt);
            titleTxt.setText(title);

            *//*final LinearLayout rootLay= (LinearLayout) findViewById(R.id.dg_rootLay);
            rootLay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(rootLay.getWindowToken(), 0);
                }
            });*//*
            TextViewPlus msgTxt = (TextViewPlus) view.findViewById(R.id.msgTxt);
            msgTxt.setText(msg);
            reasonEd = (EditText) view.findViewById(R.id.reasonTxt);
            //hideKeyBoardOnPressingOutsideView(reasonEd);
            TextViewPlus positiveBtn = (TextViewPlus) view.findViewById(R.id.positiveBtn);
            TextViewPlus ntvBtn = (TextViewPlus) view.findViewById(R.id.ntvBtn);
            positiveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    actionOnClick(POSITIVE, reasonEd.getText().toString());

                }
            });

            ntvBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    //actionOnClick(NEGATIVE, "");
                }
            });
            dialog.setView(view);
            dialog.setCancelable(false);
            //dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            dialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
            }*/
        final int POSITIVE = 0, NEGATIVE = 1;
        //reportDg= (RelativeLayout) findViewById(R.id.dg_rootLay);
        reportDgLay.setVisibility(View.VISIBLE);

        reasonEd = (EditText) findViewById(R.id.reasonTxt);
        layOutsideAlert = (RelativeLayout) findViewById(R.id.lay_outside);
        //hideKeyBoardOnPressingOutsideView(reasonEd);
        TextViewPlus positiveBtn = (TextViewPlus) findViewById(R.id.positiveBtn);
        TextViewPlus ntvBtn = (TextViewPlus) findViewById(R.id.ntvBtn);

        positiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportDgLay.setVisibility(View.GONE);
                actionOnClick(POSITIVE, reasonEd.getText().toString());
                hideKeyboard(reasonEd);
            }
        });

        ntvBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportDgLay.setVisibility(View.GONE);
                //actionOnClick(NEGATIVE, "");
            }
        });

        layOutsideAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reasonEd.getText().length() > 0 && count == 0) {
                    hideKeyboard(reasonEd);
                    count = 1;
                } else {
                    count = 0;
                    hideKeyboard(reasonEd);
                    reportDgLay.setVisibility(View.GONE);
                }
            }
        });

    }

    private void actionOnClick(int clickedBtn, String reason) {
        if (clickedBtn == 0) {
            new FacebookGraphService().reportService(ProfileDetailActivity.this, profileModel.getId(), LocalPreferenceManager.getInstance().getUserId(), reason, new ReportResponse() {
                @Override
                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                    //Finish activity with animation
                    if (baseModel != null) {

                        if (callDesciprionService) {
                            if (type == From.OTHER) {
                                Intent intent = new Intent();
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                setResult(RESULT_REPORT, intent);
                                finish();
                            } else if (type == From.RATING) {
                                Intent intent = new Intent();
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                setResult(RESULT_REPORT, intent);
                                finish();
                            } else if (type == From.CHAT) {
                                finish();
                            }
                        } else {
                            Intent intent = new Intent();
                            intent.putExtra("profileId", profileModel.getProfileTable().id);
                            setResult(NOT_PRESSED, intent);
                            finish();
                        }
                   /*overridePendingTransition(R.anim.zoom_in,
                            R.anim.slide_out_right);*/
                    }
                }
            });
        } else if (clickedBtn == 1) {

        }
    }

    public void hideKeyboard(View view) {
        view.clearFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        superHotBanner.setVisibility(View.GONE);
        hotBanner.setVisibility(View.GONE);
        notBanner.setVisibility(View.GONE);
    }
}
