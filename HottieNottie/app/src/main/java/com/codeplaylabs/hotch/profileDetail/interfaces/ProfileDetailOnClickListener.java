package com.codeplaylabs.hotch.profileDetail.interfaces;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.models.UserProfile;

import org.apmem.tools.layouts.FlowLayout;

/**
 * Created by HP on 13-Jul-17.
 */

public interface ProfileDetailOnClickListener {
    public void onMenuClick(View v);
    //Share Public Profile
    public void onShareClicked(Context context, ProfileModel userProfile, int position, ProgressBar view, ImageView shareBtn);
    //Share My Profile
    public void shareMyProfile(Context context, UserProfile userProfile,int position,ProgressBar view,ImageView shareBtn);
    public void callFewInterestSerVice(Context context, String id, FlowLayout flowLayout, CardView interestCard);
}
