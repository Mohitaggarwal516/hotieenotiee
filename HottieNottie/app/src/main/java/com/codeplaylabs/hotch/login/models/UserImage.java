package com.codeplaylabs.hotch.login.models;

import com.codeplaylabs.hotch.base.BaseModel;

/**
 * Created by mohit on 27/06/17.
 */

public class UserImage extends BaseModel {

    public enum ImageSource {Facebook_Album, User_Library}

    //giving values to the enum variables
    public enum ImgType {
        NORMAL("main"), SIZE_100x100("100x100"), SIZE_200x300("200x300");
        private final String text;

        /**
         * @param text
         */
        private ImgType(final String text) {
            this.text = text;
        }

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return text;
        }
    }

    private String imageUrl = null;
    private String base64Image = null;
    private String contentType = null;
    private String imageBaseUrl = null;
    private String imageUri = null;
    private String imageSource = ImageSource.User_Library.toString();
    private String order;
    private String timeStamp = null;

    public String getImageUrl() {
        /*if(imageUrl != null && imageUrl.contains("http")) {
            return imageUrl;
        }else if(imageUrl != null){
            return imageBaseUrl + "/" + imageUrl;
        }else{
            return imageUrl;
        }*/
        return this.getImageUrl(ImgType.NORMAL);
    }

    public String getGenuineUrl() {
        return imageUrl;
    }

    public UserImage setImageUrl(String imageUrl) {
        if (imageUrl != null && imageUrl.equals("")) {
            imageUrl = null;
        }
        this.imageUrl = imageUrl;
        return this;
    }

    public String getBase64Image() {

        return base64Image;
    }

    public UserImage setBase64Image(String base64Image) {
        this.base64Image = base64Image;
        return this;
    }

    public String getContentType() {
        return contentType;
    }

    public UserImage setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public String getImageBaseUrl() {
        return imageBaseUrl;
    }

    public UserImage setImageBaseUrl(String imageBaseUrl) {
        this.imageBaseUrl = imageBaseUrl;
        return this;
    }

    public String getImageSource() {
        return imageSource;
    }

    public UserImage setImageSource(String imageSource) {
        this.imageSource = imageSource;
        return this;
    }

    public String getOrder() {
        return order;
    }

    public UserImage setOrder(String order) {
        this.order = order;
        return this;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public UserImage setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
        return this;
    }

    public String getImageUri() {
        return imageUri;
    }

    public UserImage setImageUri(String imageUri) {
        this.imageUri = imageUri;
        return this;
    }

    public String getImageUrl(ImgType imgType) {

        if (imageUrl != null && imageUrl.contains("http")) {
            return imageUrl;
        } else if (imageUrl != null) {
            return imageBaseUrl + "/" + imageUrl + "/" + imgType.toString();
        } else {
            return imageUrl;
        }


    }
}
