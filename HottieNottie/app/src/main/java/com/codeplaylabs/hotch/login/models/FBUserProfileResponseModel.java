package com.codeplaylabs.hotch.login.models;

import com.codeplaylabs.hotch.base.BaseModel;

/**
 * Created by mohit on 22/06/17.
 */

public class FBUserProfileResponseModel extends BaseModel {

    private String userId = "";
    private boolean newUser = false;
    private String xmppUsername = "";
    private String xmppPassword = "";
    private boolean blocked = false;
    private boolean isCurrentlyDetailsChanged = false;
    private UserProfile userOldDetails = null;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }



    public String getXmppUsername() {
        return xmppUsername;
    }

    public void setXmppUsername(String xmppUsername) {
        this.xmppUsername = xmppUsername;
    }

    public String getXmppPassword() {
        return xmppPassword;
    }

    public void setXmppPassword(String xmppPassword) {
        this.xmppPassword = xmppPassword;
    }

    public boolean isNewUser() {
        return newUser;
    }

    public void setNewUser(boolean newUser) {
        this.newUser = newUser;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isCurrentlyDetailsChanged() {
        return isCurrentlyDetailsChanged;
    }

    public void setCurrentlyDetailsChanged(boolean currentlyDetailsChanged) {
        isCurrentlyDetailsChanged = currentlyDetailsChanged;
    }

    public UserProfile getUserOldDetails() {
        return userOldDetails;
    }

    public void setUserOldDetails(UserProfile userOldDetails) {
        this.userOldDetails = userOldDetails;
    }
}
