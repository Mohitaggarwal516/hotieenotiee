package com.codeplaylabs.hotch.database.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by mohit on 29/08/17.
 */

@DatabaseTable(tableName = "devicetable")
public class DeviceTable extends BaseTable {

    /**
     * Model class for ImageTemplate database table
     */
    private static final long serialVersionUID = -222864131214757024L;

    // Primary key defined as an auto generated integer
    // If the database table column name differs than the Model class variable name, the way to map to use columnName

    @DatabaseField(generatedId =true)
    public int id;

    @DatabaseField(columnName = "device_type" )
    private String deviceType;

    @DatabaseField(columnName = "device_tokens" )
    private String deviceTokens;

    @DatabaseField(columnName = "user_id" )
    private String userId;

    
    public DeviceTable() {

    }

    public int getId() {
        return id;
    }

    public DeviceTable setId(int id) {
        this.id = id;
        return this;
    }
    public String getDeviceTokens() {
        return deviceTokens;
    }

    public DeviceTable setDeviceTokens(String deviceTokens) {
        this.deviceTokens = deviceTokens;
        return this;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public DeviceTable setDeviceType(String deviceType) {
        this.deviceType = deviceType;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
