package com.codeplaylabs.hotch.login.models;


import com.codeplaylabs.hotch.base.BaseModel;

/**
 * Created by mohit on 1/13/2017.
 */
public class Detail extends BaseModel {
    String id;
    String name = "";
    String Country = "";
    String countryCode = "";
    String type;
    boolean shouldUpdateOnDuplicate;

    public boolean isShouldUpdateOnDuplicate() {
        return shouldUpdateOnDuplicate;
    }

    public void setShouldUpdateOnDuplicate(boolean shouldUpdateOnDuplicate) {
        this.shouldUpdateOnDuplicate = shouldUpdateOnDuplicate;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }
}
