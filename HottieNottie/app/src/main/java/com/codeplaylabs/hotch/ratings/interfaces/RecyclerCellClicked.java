package com.codeplaylabs.hotch.ratings.interfaces;

import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.ratings.adapters.RecyclerAdapter;

/**
 * Created by HP on 30-Aug-17.
 */

public interface RecyclerCellClicked {
    public void onCellClicked(ProfileModel profileModel/*String id*/);
    public void callNextPg(int page, RecyclerAdapter recyclerAdapter, RateRecyclerCallBack rateRecyclerCallBack/*RateDiscoveryWrapper.filterEnum filterEnum, String id*/);
}
