package com.codeplaylabs.hotch.utils;

/**
 * Created by mohit on 22/06/17.
 */

public class URLs {
    public static final String BASE_URL = "http://api.gohotch.com:8082/";//"http://ec2-35-154-202-3.ap-south-1.compute.amazonaws.com:8082/";//"http://ec2-35-154-202-3.ap-south-1.compute.amazonaws.com:8082/";
    public static final String LOGIN_REGISTER_URL = BASE_URL + "loginRegister";
    public static final String GET_PROFILE_BY_ID = BASE_URL + "profileById?ids=";
    public static final String UPDATE_IMAGES = BASE_URL + "updateImages";
    public static final String UPDATE_INTERESTS = BASE_URL + "updateInterests";
    public static final String GET_DISCOVERY_SETTING = BASE_URL + "discoverySettingWithNotification?id=";
    public static final String REGISTER_DEVICE = BASE_URL + "registerDevice";
    public static final String FETCH_PROFILES = BASE_URL + "filteredProfiles";
    public static final String UPDATE_NOTIFICATIONS = BASE_URL + "updateNotificationSetting";

    public static final String SEND_HOT_NOT_DATA_TO_SERVERS = BASE_URL + "hotOrNotStatusUpdate";
    public static final String FETCH_MATCHES_CHAT_LIST = BASE_URL + "hotiee/getMatches?id=";
    public static final String GET_CUSTOM_TOKEN = BASE_URL + "firebaseAuthToken?id=";
    public static final String GET_PROFILE_DESCRIPTION = BASE_URL + "profileDescriptionById?ids=";
    public static final String UPDATE_PROFILE = BASE_URL + "updateProfileFromSetting";
    public static final String UPDATE_CURRENT_LOCATION = BASE_URL + "updateCurrentlyAt";
    public static final String REPORT = BASE_URL + "reportAbuse";
    public static final String FEW_INTEREST = BASE_URL + "interests?ids=";
    public static final String UNMATCH_URL = BASE_URL + "unMatch?id=";
    public static final String FETCH_SUPERHOT_LIST=BASE_URL+"superhotListForUser?id=";
    public static final String CHAT_MESSAGES_NOTIFICATION_URL = "https://fcm.googleapis.com/fcm/send";
    public static final String DEL_SUPERHOT=BASE_URL+"deleteSuperhotProfiles?id=";
    public static final String USER_CONFIGURATION = BASE_URL + "userConfiguration?id=";
    public static final String RATINGS=BASE_URL+"fetchRantingForFilter";
    public static final String APP_CONFIGURATION = BASE_URL + "appConfiguration?device=ANDROID";
    public static final String LOGOUT = BASE_URL+"logout?id=";
    public static final String CHAT_LAST_MSG_UPDATE = BASE_URL + "updateLastMessage";
    public static final String CHAT_WITH_MSG = BASE_URL + "getMatchesWithChat?id=";
    public static final String CHAT_WITHOUT_MSG = BASE_URL + "getMatchesWithoutChat?id=";
    public static final String MUTE_UNMUTE_CHAT = BASE_URL + "muteUnmuteChat?id=";
    public static final String DELETE_USER=BASE_URL+"deleteProfileForId/?id=";
//http://api.gohotch.com:8082/deleteProfileForId/?id=59c398ecd289aa13515982a7
}
