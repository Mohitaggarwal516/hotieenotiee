package com.codeplaylabs.hotch.home.parsers;

import com.codeplaylabs.hotch.home.models.FilterModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mohit on 25/07/17.
 */

public class ParseFilter {

    public FilterModel getParsedFilters(String jsonObj){
        FilterModel filterModel = new FilterModel();
        try {
            JSONObject jsonObject = new JSONObject(jsonObj);

            filterModel.setMinAge(jsonObject.getInt("minAge"));
            filterModel.setMaxAge(jsonObject.getInt("maxAge"));
            filterModel.setGender(jsonObject.getString("gender"));
            filterModel.setRandom(jsonObject.getBoolean("isRandom"));
            filterModel.setCurrentlyAtFbId(jsonObject.getString("currentlyAtFbId"));
            filterModel.setCurrentlyAtCountry(jsonObject.getString("currentlyAtCountry"));
            if(jsonObject.getJSONArray("currentlyAtLocations") != null) {
                JSONArray locationsArray = jsonObject.getJSONArray("currentlyAtLocations");
                for (int index = 0; index < locationsArray.length(); index++) {

                    filterModel.getCurrentlyAtLocations().add(locationsArray.getString(index));
                }
            }
            return filterModel;
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;

    }

}
