package com.codeplaylabs.hotch.login.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codeplaylabs.hotch.R;

/**
 * Created by HP on 26-Jul-17.
 */

public class ViewPagerAdapter extends PagerAdapter {

    Context context;
    LayoutInflater inflater;
    String[] text={"Discover exciting People around the world","Find the hottest people in Town","Enjoy matching and chatting with people across."
    ,"Archive the profiles and Be entertained"};
    int[] imgs={R.drawable.a_1,R.drawable.a_2,R.drawable.a_3,R.drawable.a_4};
    public ViewPagerAdapter(Context context){
        this.context=context;
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view=inflater.inflate(R.layout.each_pg_login_viewpager,container,false);
        TextView headerText= (TextView) view.findViewById(R.id.header_text);
        ImageView img= (ImageView) view.findViewById(R.id.imgView);
        headerText.setText(text[position]);
        img.setImageResource(imgs[position]);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return imgs.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (View) object;
    }
}
