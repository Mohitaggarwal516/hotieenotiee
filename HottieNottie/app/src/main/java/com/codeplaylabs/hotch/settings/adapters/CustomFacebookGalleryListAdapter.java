package com.codeplaylabs.hotch.settings.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codeplaylabs.hotch.settings.interfaces.GalleryListListener;
import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.settings.models.albums.AlbumModel;
import com.codeplaylabs.hotch.utils.ServiceSingleton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by mohit on 30/06/17.
 */

public class CustomFacebookGalleryListAdapter extends RecyclerView.Adapter<CustomFacebookGalleryListAdapter.MyViewHolder> {
    private final GalleryListListener galleryListListener;
    private final ArrayList<AlbumModel> albumList;
    private final Activity activity;

    public CustomFacebookGalleryListAdapter(Activity galleryActivity, ArrayList<AlbumModel> albumList, GalleryListListener galleryListListener) {
        this.activity = galleryActivity;
        this.albumList = albumList;
        this.galleryListListener = galleryListListener;
    }

    @Override
    public CustomFacebookGalleryListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.album_list_row, parent, false);

        return new CustomFacebookGalleryListAdapter.MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(CustomFacebookGalleryListAdapter.MyViewHolder holder, final int position) {

        final AlbumModel albumModel = albumList.get(position);
        holder.albumName.setText(albumModel.getName());
        Picasso.with(activity).load(albumModel.getUrl()).placeholder(ServiceSingleton.getInstance().getSelfPlaceholder()).into(holder.albumImage);
        holder.albumImagesCount.setText(albumModel.getPhotoCount());
        holder.albumCardLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!albumModel.getPhotoCount().equals("0")) {
                    galleryListListener.onItemClick(position, albumModel, null);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView albumName;
        private final ImageView albumImage;
        private final TextView albumImagesCount;
        private final LinearLayout albumCardLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            albumName = (TextView) itemView.findViewById(R.id.album_name);
            albumImage = (ImageView) itemView.findViewById(R.id.album_image);
            albumImagesCount = (TextView) itemView.findViewById(R.id.album_images_count);
            albumCardLayout = (LinearLayout) itemView.findViewById(R.id.album_card_lay);

        }


    }
}
