package com.codeplaylabs.hotch.chats.models.firbaseModels;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.utils.CommonMethods;

/**
 * Created by mohit on 18/08/17.
 */

public class UserModel extends BaseModel {

    private String chatId = null;
    private String name = null;
    private String image = null;
    private long timestamp ;
    private String userId = null;
    private FriendlyMessage  LastMessage = null;
    private String isNew = null;
    private String deviceType = null;
    private boolean muteStatus = false;
    private int gender = R.drawable.female_placeholder;;

    public UserModel() {

    }

    public String getChatId() {
        return chatId;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public long getTimestamp() {
        return CommonMethods.GMTToLocal(timestamp);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UserModel setChatId(String chatId) {
        this.chatId = chatId;
        return this;
    }

    public UserModel setName(String name) {
        this.name = name;
        return this;
    }

    public UserModel setImage(String image) {
        this.image = image;
        return this;
    }

    public UserModel setTimestamp(long timestamp) {
        this.timestamp = CommonMethods.localToGMT(timestamp);
        return this;
    }

    public FriendlyMessage getLastMessage() {
        return LastMessage;
    }

    public void setLastMessage(FriendlyMessage lastMessage) {
        LastMessage = lastMessage;
    }

    public String getIsNew() {
        return isNew;
    }

    public void setIsNew(String isNew) {
        this.isNew = isNew;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public void selfMuteStatus(boolean isNotificationDisabledBySelf) {
        this.muteStatus = isNotificationDisabledBySelf;

    }
    public boolean selfMuteStatus(){
        return muteStatus;
    }

    public UserModel setGender(String gender) {
        if(gender.equals("male")) {
            this.gender = R.drawable.male_placeholder;
        }else{
            this.gender = R.drawable.female_placeholder;
        }
        return this;
    }

    public int getGender() {
        return gender;
    }
}
