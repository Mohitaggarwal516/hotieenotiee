package com.codeplaylabs.hotch.others.parser;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.models.Detail;
import com.codeplaylabs.hotch.login.models.Education;
import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.login.models.Work;
import com.codeplaylabs.hotch.others.models.ProfileWrapper;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by HP on 21-Aug-17.
 */

public class DescriptionParser extends BaseParser {
    @Override
    public BaseModel parseJson(JSONObject response) throws JSONException {
        return parseDescriptionJson(response);
    }

    private BaseModel parseDescriptionJson(JSONObject response) {
        ProfileWrapper wrapper = new ProfileWrapper();

        try {
            CustomJsonObject dataObj = new CustomJsonObject(response.getJSONObject("data"));
            wrapper.setSelfRating(dataObj.getString("selfRating"));
            /*wrapper.setSelfGender(dataObj.getString("selfGender"));*/
            wrapper.setPageNo(dataObj.getInt("pageNumber")/*Integer.parseInt(dataObj.getString("pageNumber"))*/);
            JSONArray filterListArray = dataObj.getJsonArray("filteredList");
            int filterListLenght = filterListArray.length();
            for (int i = 0; i < filterListArray.length(); i++) {
                ProfileModel model = new ProfileModel();
                CustomJsonObject obj = new CustomJsonObject(filterListArray.getJSONObject(i));
                model.setId(obj.getString("id"));
                model.setName(obj.getString("name"));
                model.setAbout(obj.getString("about"));
                model.setDateOfBirth(obj.getString("birthday"));
                model.setYearOfBirthday(obj.getString("yearOfBirthday"));
                model.setGender(obj.getString("gender"));

                JSONObject loc = obj.getJsonObject("location");
                CustomJsonObject cLoc = new CustomJsonObject(loc);
                Detail mLoc = new Detail();
                mLoc.setName(cLoc.getString("city"));
                mLoc.setCountry(cLoc.getString("country"));
                mLoc.setCountryCode(cLoc.getString("countryCode"));
                if (mLoc.getName().length() > 0 || mLoc.getCountry().length() > 0 || mLoc.getCountryCode().length() > 0)
                    model.setLocation(mLoc);

                //Education to be parsed
                CustomJsonObject cEduDetailJObj=new CustomJsonObject(obj.getJsonObject("educationDetail"));
                Education mEduDetail=new Education();
                mEduDetail.setCurrentlyStudingHere(cEduDetailJObj.getBoolean("isCurrentlyStudingHere",false));
                mEduDetail.setType(cEduDetailJObj.getString("type"));

                CustomJsonObject instituteJsonObj=new CustomJsonObject(cEduDetailJObj.getJsonObject("institute"));
                Detail institute=new Detail();
                institute.setName(instituteJsonObj.getString("name"));

                mEduDetail.setInstitute(institute);
                model.setEducation(mEduDetail);
                // model.setEducation();
                CustomJsonObject workDetail = new CustomJsonObject(obj.getJsonObject("workDetail"));
                Work mWork = new Work();
                mWork.setFacebookId(workDetail.getString("facebookId"));
                mWork.setStartDate(workDetail.getString("startDate"));
                mWork.setEndDate(workDetail.getString("endDate"));

                CustomJsonObject workLocObj = new CustomJsonObject(workDetail.getJsonObject("location"));
                Detail mWorkLoc = new Detail();
                mWorkLoc.setName(workLocObj.getString("city"));
                mWorkLoc.setCountry(workLocObj.getString("country"));
                mWorkLoc.setCountryCode(workLocObj.getString("countryCode"));
                //mworkLoc.set(workLocObj.getString("facebookId"));
                mWork.setLocation(mWorkLoc);
                mWork.setPosition(workDetail.getString("position"));
                mWork.setPositionFacebookId(workDetail.getString("positionFacebookId"));

                Detail mEmployer = new Detail();
                CustomJsonObject jEmployerObj = new CustomJsonObject(workDetail.getJsonObject("employer"));
                mEmployer.setId(jEmployerObj.getString("id"));
                mEmployer.setName(jEmployerObj.getString("name"));
                mEmployer.setShouldUpdateOnDuplicate(jEmployerObj.getBoolean("shouldUpdateOnDuplicate", false));
                mWork.setEmployer(mEmployer);

                mWork.setIsCurrentlyWorkingHere(workDetail.getBoolean("isCurrentlyWorkingHere", false));
                model.setWork(mWork);

                JSONArray jImgArray = obj.getJsonArray("images");

                for (int j = 0; j < jImgArray.length(); j++) {
                    CustomJsonObject jUserImg = new CustomJsonObject(jImgArray.getJSONObject(j));
                    UserImage img = new UserImage();
                    img.setImageUrl(jUserImg.getString("imageUrl"));
                    img.setBase64Image(jUserImg.getString("base64Image"));
                    img.setContentType(jUserImg.getString("contentType"));
                    img.setImageBaseUrl(jUserImg.getString("imageBaseUrl"));
                    img.setImageSource(jUserImg.getString("imageSource"));
                    img.setOrder(jUserImg.getString("order"));
                    model.getImages().add(img);
                }
                JSONArray jInterestArray = obj.getJsonArray("interests");
                for (int k = 0; k < jInterestArray.length(); k++) {
                    /*Detail interests = (Detail) new InterestParser().parseInterestJson(jInterestArray.getJSONObject(i));
                    model.getInterests().add(interests);*/
                    Detail mInterest = new Detail();
                    JSONObject jInterestObj = jInterestArray.getJSONObject(k);
                    mInterest.setName(jInterestObj.getString("name"));
                    mInterest.setId(jInterestObj.getString("id"));
                    model.getInterests().add(mInterest);
                    // ProfileModel interestModel = (ProfileModel) new InterestParser().parseInterestJson(jInterestArray.getJSONObject(k));
                    //model.setInterests(interestModel.getInterests());
                }
                model.setMatch(obj.getBoolean("match", false));
                wrapper.getProfileWrapper().add(model);
            }
            return wrapper;
            //return model;
        } catch (JSONException e) {
            e.printStackTrace();
            wrapper.error = Constants.Error.JSON_EXCEPTION;

            return wrapper;
        }
    }
}
