package com.codeplaylabs.hotch.settings.interfaces;

/**
 * Created by HP on 16-Aug-17.
 */

public interface InformClickToActivity {
    void onShareMyProfileClicked();
    void shareAppClicked();
}
