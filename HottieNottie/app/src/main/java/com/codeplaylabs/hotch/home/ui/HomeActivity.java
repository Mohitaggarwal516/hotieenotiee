package com.codeplaylabs.hotch.home.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.FaceDetector;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.codeplaylabs.hotch.base.BaseActivity;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.chats.models.firbaseModels.UserModel;
import com.codeplaylabs.hotch.chats.ui.ChatFragmentActivity;
import com.codeplaylabs.hotch.database.tables.ProfileTable;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.interfaces.SharedProfileResponse;
import com.codeplaylabs.hotch.login.interfaces.TryAgainForAppConfig;
import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.login.services.FacebookGraphService;
import com.codeplaylabs.hotch.login.ui.LoginActivity;
import com.codeplaylabs.hotch.others.models.ProfileWrapper;
import com.codeplaylabs.hotch.others.parser.DescriptionParser;
import com.codeplaylabs.hotch.profileDetail.ui.ProfileDetailActivity;
import com.codeplaylabs.hotch.settings.handler.SettingsDbHandler;
import com.codeplaylabs.hotch.settings.ui.SettingsActivity;
import com.codeplaylabs.hotch.utils.CommonMethods;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.database.tables.UserImageTable;
import com.codeplaylabs.hotch.home.handler.HomeDBHandlerClass;
import com.codeplaylabs.hotch.home.services.HomeServices;
import com.codeplaylabs.hotch.utils.LogAnalytics;
import com.codeplaylabs.hotch.utils.ServiceSingleton;
import com.codeplaylabs.hotch.utils.Singleton;
import com.codeplaylabs.hotch.utils.TextViewPlus;
import com.etiennelawlor.tinderstack.interfaces.SetOnCardClickListener;
import com.etiennelawlor.tinderstack.ui.TinderCardView;
import com.etiennelawlor.tinderstack.ui.TinderStackLayout;
import com.etiennelawlor.tinderstack.utilities.DisplayUtility;
import com.google.firebase.database.FirebaseDatabase;
import com.j256.ormlite.stmt.query.Not;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HomeActivity extends BaseActivity implements SetOnCardClickListener, TryAgainForAppConfig {

    // region Constants
    private static final int STACK_SIZE = 3;
    private static final int CARD_CLICKED_REQ_CODE = 1234;
    // endregion

    // region Views
    private TinderStackLayout tinderStackLayout;
    // endregion

    // region Member Variables
    private int index = 0;
    private ImageView chatBtn;//menuBtn;
    //private LinearLayout menuScreenLinLayout;
    private ImageView menuSettingsButton;
    HashMap<String, ProfileModel> shownProfilesMap = new HashMap<String, ProfileModel>();
    private int placeholder;
    private int screenWidth;
    private LinearLayout matchScreenLinLayout;
    private ImageView matchLeftImage;
    private ImageView matchRightImage;
    private TextView sayHiBtn;
    private TextView chatNowBtn;
    private ImageView closeMatchScreen;
    private int height, width;
    private TextViewPlus resetProfilesText;
    // endregion

    private LocalPreferenceManager localPreferenceManager = LocalPreferenceManager.getInstance();
    private TextView reasonEd;
    private LinearLayout dgLay;
    //private int displayHeight;
    private RelativeLayout reportDgLay;
    private RelativeLayout layOutsideAlert;
    private int count = 0;
    private ImageView homeNewChatIcon;
    private RelativeLayout appUpdateDialogLayout;
    private RelativeLayout oneTimeView;
    private boolean cardReceived = true;
    //private TextViewPlus oneTimeViewBtn;

    // Firebase instance variables
    // region Listeners
    // endregion
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_home);
        super.onCreate(savedInstanceState);

        LocalPreferenceManager.getInstance().setPreference(Constants.FIRST_LOGIN, false);
        appUpdateDialogLayout = (RelativeLayout) findViewById(R.id.appUpdateDialogLayout);

        height = getWindowManager().getDefaultDisplay().getHeight();
        width = getWindowManager().getDefaultDisplay().getWidth();

        screenWidth = DisplayUtility.getScreenWidth(HomeActivity.this);

        //======
        reportDgLay = (RelativeLayout) findViewById(R.id.root_alert);
        matchScreenLinLayout = (LinearLayout) findViewById(R.id.match_screen_lin_layout);
        matchLeftImage = (ImageView) findViewById(R.id.match_left);
        matchRightImage = (ImageView) findViewById(R.id.match_right);
        sayHiBtn = (TextView) findViewById(R.id.say_hi_btn);
        chatNowBtn = (TextView) findViewById(R.id.chat_now_btn);
        closeMatchScreen = (ImageView) findViewById(R.id.close_view);
      //  oneTimeViewBtn = (TextViewPlus) findViewById(R.id.oneTimeViewBtn);

        oneTimeView = (RelativeLayout) findViewById(R.id.one_time_view);
        //to ensure first time view does'nt pop up if it's not first time.(Storing in pref manager of tinder stack since on logout the entire prefs of apps are removed)
        com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().initialise(this);
        if (com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().getBooleanPreference(Constants.ONE_TIME_VIEW, true)) {
            com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().setPreference(Constants.ONE_TIME_VIEW, false);
            oneTimeView.setVisibility(View.VISIBLE);
        } else {
            oneTimeView.setVisibility(View.GONE);
        }

        oneTimeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oneTimeView.setVisibility(View.GONE);
            }
        });
        // setMatchListeners(0);
        //========

        chatBtn = (ImageView) findViewById(R.id.home_title_chat_btn);
        homeNewChatIcon = (ImageView) findViewById(R.id.home_chat_icon);
        if (LocalPreferenceManager.getInstance().getBooleanPreference(Constants.Chat.PREF_NEW_MESSAGE, false)) {
            homeNewChatIcon.setVisibility(View.VISIBLE);
        } else {
            homeNewChatIcon.setVisibility(View.GONE);
        }
       /* menuBtn = (ImageView)findViewById(R.id.home_title_menu_btn);
        menuScreenLinLayout = (LinearLayout)findViewById(R.id.menu_screen_lin_layout);*/
        menuSettingsButton = (ImageView) findViewById(R.id.menu_settings_btn);

        tinderStackLayout = (TinderStackLayout) findViewById(R.id.tsl);


        resetProfilesText = (TextViewPlus) findViewById(R.id.reset_profiles_text);
        resetProfilesText.setMovementMethod(LinkMovementMethod.getInstance());
        ArrayList<String> array = new ArrayList<>();
        array.add("Reset profiles");
        array.add("change filters");
        resetProfilesText.setText(addClickablePart(getString(R.string.reset_profiles), array), TextView.BufferType.SPANNABLE);
           /* tc = new TinderCardView(this);
            tc.bind(getUser(index));*/
        findViewById(R.id.loader_layout).setVisibility(View.VISIBLE);

        //   tinderStackLayout.setVisibility(View.VISIBLE);
        new HomeServices().getNextCard(HomeActivity.this, HomeServices.DO_NOT_PERSIST_FILTERS, HomeServices.SHOULD_NOT_RESET_VALUES, new HomeServices.ProfileListener() {
            @Override
            public void profile(ProfileModel profile) {
                findViewById(R.id.loader_layout).setVisibility(View.GONE);
                if (profile != null) {
                    //findViewById(R.id.no_profile_left_text).setVisibility(View.GONE
                    //);
                    tinderStackLayout.addCard(setCard(profile), 0);

                    for (int i = index; index < i + STACK_SIZE; index++) {
                        if (tinderStackLayout.getChildCount() < STACK_SIZE) {
                            new HomeServices().getNextCard(HomeActivity.this, HomeServices.DO_NOT_PERSIST_FILTERS, HomeServices.SHOULD_NOT_RESET_VALUES, new HomeServices.ProfileListener() {
                                @Override
                                public void profile(ProfileModel profile) {
                                    if (profile != null) {
                                        tinderStackLayout.addCard(setCard(profile), 0);
                                    } else {
                                        //findViewById(R.id.no_profile_left_text).setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                        }
                    }
                } else {
                    //findViewById(R.id.no_profile_left_text).setVisibility(View.VISIBLE);
                }
            }
        });


        chatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeNewChatIcon.setVisibility(View.GONE);
                startActivity(new Intent(HomeActivity.this, ChatFragmentActivity.class));
            }
        });

        /*menuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuScreenLinLayout.setVisibility(View.VISIBLE);
            }
        });*/
        menuScreenLinLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuScreenLinLayout.setVisibility(View.GONE);
            }
        });
        menuSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                menuScreenLinLayout.setVisibility(View.GONE);
                Intent intent = new Intent(HomeActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });

        getSelfProfile();
    }

    private void setMatchListeners(final ProfileModel profileModel, String key) {
        int placeholder = 0;
        if (profileModel.getGender().equalsIgnoreCase("female")) {
            placeholder = R.drawable.male_placeholder;
        } else {
            placeholder = R.drawable.female_placeholder;
        }
        List<UserImageTable> myImageTable = SettingsDbHandler.query("order", null, null);
        UserImageTable myImage = null;
        if (myImageTable != null && myImageTable.size() > 0) {
            myImage = myImageTable.get(0);
        }


        if (myImage != null) {
            if (myImage.imageUrl.length() == 0) {
                Picasso.with(this)
                        .load(myImage.imageUri).placeholder(placeholder)
                        .into(matchLeftImage);
            } else if (myImage.imageSource.equals("Facebook_Album")) {
                Picasso.with(this)
                        .load(myImage.imageUrl).placeholder(placeholder)
                        .into(matchLeftImage);
            } else {
                Picasso.with(this)
                        .load(myImage.getImageUrl()).placeholder(placeholder)
                        .into(matchLeftImage);
            }
        } else {

            Picasso.with(this)
                    .load(R.drawable.male_placeholder)
                    .into(matchLeftImage);

        }
        UserImage userImage = profileModel.getImages().get(0);
        if (profileModel.getGender().equalsIgnoreCase("female")) {
            placeholder = R.drawable.female_placeholder;
        } else {
            placeholder = R.drawable.male_placeholder;
        }

        if (userImage.getImageUrl() != null && userImage.getImageUrl().trim().length() > 0) {
            Picasso.with(HomeActivity.this).load(userImage.getImageUrl()).placeholder(placeholder).into(matchRightImage);
        } else {
            Picasso.with(HomeActivity.this).load(placeholder).into(matchRightImage);

        }
        Animation animation1 =
                AnimationUtils.loadAnimation(HomeActivity.this,
                        R.anim.slide_in_left);
        matchLeftImage.startAnimation(animation1);
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                Animation animation2 = AnimationUtils.loadAnimation(HomeActivity.this,
                        R.anim.slide_in_right);
                matchRightImage.startAnimation(animation2);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animation1 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_right);
        matchRightImage.startAnimation(animation1);
        closeMatchScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                matchScreenLinLayout.setVisibility(View.GONE);
            }
        });

        final UserModel userModel = setProfileToFirebaseChat(profileModel, key);

        chatNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(HomeActivity.this, ChatFragmentActivity.class);
                intent.putExtra("calledFrom", ChatFragmentActivity.NEW_CHAT);
                matchScreenLinLayout.setVisibility(View.GONE);
                startActivity(intent);
            }
        });

        sayHiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                matchScreenLinLayout.setVisibility(View.GONE);
            }
        });


    }

    private UserModel setProfileToFirebaseChat(ProfileModel profileModel, String key) {
        UserModel userModel = new UserModel();
        userModel.setName(profileModel.getName());
        userModel.setChatId(key);
        userModel.setTimestamp(System.currentTimeMillis());
        userModel.setImage(profileModel.getImages().get(0).getImageUrl());
//        FirebaseDatabase.getInstance().getReference("User").child(LocalPreferenceManager.getInstance().getUserId())
//                .child("Matches").child(profileModel.getId())
//                .setValue(new UserModel().setName(profileModel.getName())
//                        .setTimestamp(System.currentTimeMillis())
//                        .setChatId(key));
//        FirebaseDatabase.getInstance().getReference("User").child(profileModel.getId())
//                .child("Matches").child(LocalPreferenceManager.getInstance().getUserId())
//                .setValue(new UserModel().setName(LocalPreferenceManager.getInstance().getUserName())
//                        .setTimestamp(System.currentTimeMillis())
//                        .setChatId(key));
//        FirebaseDatabase.getInstance().getReference("User").child(profileModel.getId())
//                .child("Detail").setValue(new UserModel().setName(profileModel.getName())
//                .setImage(profileModel.getImages().get(0).getImageUrl()));
//
//        FirebaseDatabase.getInstance().getReference("User").child(LocalPreferenceManager.getInstance().getUserId())
//                .child("Detail").setValue(new UserModel().setName(LocalPreferenceManager.getInstance().getUserName())
//                .setImage(LocalPreferenceManager.getInstance().getUserImage()));
//        ChatDBHandlerClass.saveDataInTable(ChatDBHandlerClass.dataBaseTables.chatListTable, new ChatUsersList().setUserId(profileModel.getId()).setName(profileModel.getName()).setId(key));
        return userModel;
    }

    private TinderCardView setCard(ProfileModel profile) {
        Log.e("CARDSET", profile.getName());
        shownProfilesMap.put(profile.getProfileTable().id, profile);


        TinderCardView tinderCardView = tinderStackLayout.getCard(HomeActivity.this);

        tinderCardView.setdbId(profile.getProfileTable().id);
        tinderCardView.clearAnimation();
        tinderCardView.setX(0);
        tinderCardView.setY(0);
        ImageView image = (ImageView) tinderCardView.findViewById(R.id.card_full_image);
        TextView age = (TextView) tinderCardView.findViewById(R.id.home_age);
        TextView name = (TextView) tinderCardView.findViewById(R.id.home_name);
        TextView location = (TextView) tinderCardView.findViewById(R.id.home_location);
        TextView designation = (TextView) tinderCardView.findViewById(R.id.home_designation);
        LinearLayout designationLayout = (LinearLayout) tinderCardView.findViewById(R.id.designation_layout);
        LinearLayout locationLayout = (LinearLayout) tinderCardView.findViewById(R.id.location_layout);
        LinearLayout gradientBackground = (LinearLayout) tinderCardView.findViewById(R.id.home_bottom_detail_lin_layout);
        LinearLayout commonLikesLay = (LinearLayout) tinderCardView.findViewById(R.id.common_likes_lay);
        TextView commonLikes = (TextView) tinderCardView.findViewById(R.id.home_common_likes);


        ImageView likeTextView = (ImageView) tinderCardView.findViewById(R.id.hot_banner);
        ImageView nopeTextView = (ImageView) tinderCardView.findViewById(R.id.not_banner);
        ImageView superHotView = (ImageView) tinderCardView.findViewById(R.id.superhot_banner);

        likeTextView.setAlpha(0f);
        nopeTextView.setAlpha(0f);
        superHotView.setAlpha(0f);
        tinderCardView.btnClicked = false;
        int year = 0;
        if (profile.getYearOfBirthday() == null || profile.getYearOfBirthday().length() == 0) {
            year = (new Date().getYear() + 1900);
        } else {
            year = (new Date().getYear() + 1900) - Integer.parseInt(profile.getYearOfBirthday());
        }
        age.setText(year + "");

        name.setText(profile.getName());
        if (profile.getLocation() != null && profile.getLocation().getCountry() != null && profile.getLocation().getCountry().length() > 0) {
            locationLayout.setVisibility(View.VISIBLE);
            location.setText(profile.getLocation().getCountry());
        } else {
            locationLayout.setVisibility(View.GONE);
        }
        if (profile.getWork() != null && profile.getWork().getPosition() != null && profile.getWork().getPosition().length() > 0) {
            designationLayout.setVisibility(View.VISIBLE);
            designation.setText(profile.getWork().getPosition());
        } else if (profile.getEducation() != null && profile.getEducation().getInstitute() != null && profile.getEducation().getInstitute().getName() != null && profile.getEducation().getInstitute().getName().length() > 0) {
            designationLayout.setVisibility(View.VISIBLE);
            designation.setText(profile.getEducation().getInstitute().getName());
        } else {
            designationLayout.setVisibility(View.GONE);
        }
        // UserImage image1 = profile.getImages().get(0);
        String imgOneUrl = "";
        if (profile.getImages().size() > 0 && profile.getImages() != null) {
            imgOneUrl = profile.getImages().get(0).getImageUrl();
/*
            if (profile.getImages().get(0).getImageUrl() != null && profile.getImages().get(0).getImageUrl().contains("http")) {
                imgOneUrl = profile.getImages().get(0).getImageUrl();
            } else {
                imgOneUrl = profile.getImages().get(0).getImageBaseUrl() + profile.getImages().get(0).getImageUrl();
            }*/
        }
        if (profile.getGender() != null && profile.getGender().length() > 0 && profile.getGender().equalsIgnoreCase("male")) {
            placeholder = R.drawable.male_placeholder;
            gradientBackground.setBackgroundResource(R.drawable.gradient_male_profile);
        } else {
            placeholder = R.drawable.female_placeholder;
            gradientBackground.setBackgroundResource(R.drawable.gradient_female_profile);
        }
        if (imgOneUrl == null || imgOneUrl.trim().length() == 0) {
            Picasso.with(HomeActivity.this).load(placeholder).centerCrop().resize(getWindowManager().getDefaultDisplay().getWidth(), getWindowManager().getDefaultDisplay().getHeight()).into(image);
        } else {
            Picasso.with(HomeActivity.this).load(imgOneUrl).placeholder(placeholder).centerCrop().resize(getWindowManager().getDefaultDisplay().getWidth(), getWindowManager().getDefaultDisplay().getHeight()).into(image);
        }

        if (profile.getInterests() != null && profile.getInterests().size() > 0) {
            commonLikes.setText(profile.getInterests().size() + "");
            commonLikesLay.setVisibility(View.VISIBLE);
        } else {
            commonLikesLay.setVisibility(View.GONE);
        }


        return tinderCardView;
    }

    // region Helper Methods

    @Override
    public void onBackClick(String id) {

    }


    @Override
    public void onSuperHotClick(final String id) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                final ProfileModel profileModel = shownProfilesMap.get(id);
                if (profileModel != null) {
                    new SpLikeSaveAsync().execute(profileModel);
                    LogAnalytics.logButtonClickEvent(mTracker, Constants.Buttons.SUPERHOT_BTN);
                }
                com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().initialise(HomeActivity.this);
                com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().setPreference("superhotEnabled", System.currentTimeMillis() + (Constants.ServiceConfiguration.SUPERHOT_AVAILABLE * 1000));
                updateProfile(id, String.valueOf(ProfileTable.Type.SuperHot));
                showMatchScreen(profileModel);
                syncProfilesWithServers();
            }
        }, 10);


    }

    private void showMatchScreen(final ProfileModel profileModel) {
        if (profileModel == null)
            return;
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(LocalPreferenceManager.getInstance().getUserId());
        arrayList.add(profileModel.getId());
        final String key = CommonMethods.getUniqueKey(arrayList);
//        FirebaseMessaging.getInstance().subscribeToTopic(key);
//
//        NotificationDbHandlerClass.saveDataInTable(new NotificationSubscriptionTable()
//                .setId(key).setSubscribed(true).setTimestamp(System.currentTimeMillis()));


        if (profileModel.isMatch()) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                           // new HomeServices().sendDataToServers(HomeActivity.this, profileModel, null);
                            matchScreenLinLayout.setVisibility(View.VISIBLE);
                            setMatchListeners(profileModel, key);
                        }
                    });
                }
            }, Constants.ServiceConfiguration.TIME_DELAY_IN_MATCH_VIEW);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    matchScreenLinLayout.setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    public void onHotClick(final String id) {
        updateProfile(id, String.valueOf(ProfileTable.Type.Hot));
        final ProfileModel profileModel = shownProfilesMap.get(id);

        showMatchScreen(profileModel);
        if (profileModel !=null && profileModel.isMatch()) {
            syncProfilesWithServers();
        }
    }

    @Override
    public void onNotClick(String id) {
        updateProfile(id, String.valueOf(ProfileTable.Type.Not));

      /*  new FacebookGraphService().deleleteUseLessProfiles(HomeActivity.this, id, new SharedProfileResponse() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

            }
        });*/
    }

    @Override
    public void getNextCard(final TinderStackLayout.GetCard card) {
        //card.card( new TinderCardView(this));
        new HomeServices().getNextCard(HomeActivity.this, HomeServices.DO_NOT_PERSIST_FILTERS, HomeServices.SHOULD_NOT_RESET_VALUES, new HomeServices.ProfileListener() {
            @Override
            public void profile(ProfileModel profile) {
                if (profile != null) {
                    LocalPreferenceManager.getInstance().removeKey("timestmp");// should be null to reset values
                    card.card(setCard(profile));
                } else {
                    //findViewById(R.id.no_profile_left_text).setVisibility(View.VISIBLE);
                    if (tinderStackLayout.getChildCount() == 0) {
                        //             tinderStackLayout.setVisibility(View.GONE);
                        syncProfilesWithServers();
                    }
                }
            }
        });
    }

    private void syncProfilesWithServers() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                List<ProfileTable> profiles = HomeDBHandlerClass.syncProfilesWithServers(true);
                if (profiles != null && !Singleton.getInstance().hotOrNotStatusUpdated) {
                    Singleton.getInstance().hotOrNotStatusUpdated = true;
                    new HomeServices().sendDataToServers(HomeActivity.this, profiles, null);

                }

            }
        }, 0);
    }

    @Override
    public void getPreviousCard(final TinderStackLayout.GetCard card) {
        //card.card(new TinderCardView(this));
        new HomeServices().getPreviousCard(HomeActivity.this, new HomeServices.ProfileListener() {
            @Override
            public void profile(ProfileModel profile) {
                if (profile != null) {
                    card.card(setCard(profile));
                }
            }
        });
    }

    @Override
    public void onCardClicked(String id) {
        Intent intent = new Intent(HomeActivity.this, ProfileDetailActivity.class);
        intent.putExtra("from", String.valueOf(ProfileDetailActivity.From.PublicProfile));
        intent.putExtra("profile", shownProfilesMap.get(id));
        startActivityForResult(intent, CARD_CLICKED_REQ_CODE);
    }

    @Override
    public void showSuperHotDialog(String dbId) {
        reportDgLay.setVisibility(View.VISIBLE);
        final long time = com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().getLongPreference("superhotEnabled", 0);
        reasonEd = (TextView) findViewById(R.id.reasonTxt);
        ImageView image = (ImageView) findViewById(R.id.image_super_hot);
        final ProfileModel profileModel = shownProfilesMap.get(dbId);
//        int placeHolder = R.drawable.female_placeholder;
//        if (profileModel.getGender().equalsIgnoreCase("male")) {
//            placeHolder = R.drawable.male_placeholder;
//        }
        if (profileModel.getImages() == null || profileModel.getImages().get(0) == null || profileModel.getImages().get(0).getImageUrl(UserImage.ImgType.SIZE_100x100).length() == 0) {
            Picasso.with(HomeActivity.this).load(placeholder).into(image);
        } else {
            Picasso.with(HomeActivity.this).load(profileModel.getImages().get(0).getImageUrl()).placeholder(placeholder).centerCrop().resize(getWindowManager().getDefaultDisplay().getWidth(), getWindowManager().getDefaultDisplay().getHeight()).into(image);
        }
        layOutsideAlert = (RelativeLayout) findViewById(R.id.lay_outside);
        reasonEd.setText(CommonMethods.timer(time));
        final Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        reasonEd.setText(CommonMethods.timer(time));
                    }
                });
            }
        }, 1000, 1000);
        TextViewPlus positiveBtn = (TextViewPlus) findViewById(R.id.positiveBtn);

        positiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                reportDgLay.setVisibility(View.GONE);
            }
        });


        layOutsideAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                reportDgLay.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean shouldRemoveView(String id) {
        if (shownProfilesMap.get(id) != null) {
            return !shownProfilesMap.get(id).getProfileTable().getType().equalsIgnoreCase(String.valueOf(ProfileTable.Type.NOT_SEEN));
        } else {
            return false;
        }
    }

    @Override
    public void onViewRemoved(String id) {
        if (shownProfilesMap.size() >= 50) {
            shownProfilesMap.remove(id);
        }
    }

    private void updateProfile(final String id, final String type) {

        ProfileModel profileModel = shownProfilesMap.get(id);
        if (profileModel != null) {
            ProfileTable profileTable = profileModel.getProfileTable();
            profileTable.setType(type);
            profileTable.setSynced(false);
            profileTable.setProfileTaken("true");
            if (type.equalsIgnoreCase(String.valueOf(ProfileTable.Type.SuperHot))) {
                profileTable.setTimestamp(-1);
            } else {
                profileTable.setTimestamp(System.currentTimeMillis());
            }
            HomeDBHandlerClass.updateProfile(profileTable);
        }

           /* }
        }, 0);*/
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                List<ProfileTable> profiles = HomeDBHandlerClass.syncProfilesWithServers(false);
                if (profiles != null && !Singleton.getInstance().hotOrNotStatusUpdated) {
                    Singleton.getInstance().hotOrNotStatusUpdated = true;
                    new HomeServices().sendDataToServers(HomeActivity.this, profiles, null);

                }

            }
        }, 0);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        shownProfilesMap.clear();
        if (tinderStackLayout != null) {
            tinderStackLayout.removeAllViews();
        } else {
            tinderStackLayout = new TinderStackLayout(this);
        }
        findViewById(R.id.loader_layout).setVisibility(View.VISIBLE);

        //   tinderStackLayout.setVisibility(View.VISIBLE);
        new HomeServices().getNextCard(HomeActivity.this, HomeServices.DO_NOT_PERSIST_FILTERS, HomeServices.SHOULD_NOT_RESET_VALUES, new HomeServices.ProfileListener() {
            @Override
            public void profile(ProfileModel profile) {
                findViewById(R.id.loader_layout).setVisibility(View.GONE);
                if (profile != null) {
                    //  findViewById(R.id.no_profile_left_text).setVisibility(View.GONE);

                    if (tinderStackLayout.getChildCount() < STACK_SIZE) {
                        tinderStackLayout.addCard(setCard(profile), 0);

                        for (int i = index; index < i + STACK_SIZE; index++) {
                            if (tinderStackLayout.getChildCount() < STACK_SIZE) {
                                new HomeServices().getNextCard(HomeActivity.this, HomeServices.DO_NOT_PERSIST_FILTERS, HomeServices.SHOULD_NOT_RESET_VALUES, new HomeServices.ProfileListener() {
                                    @Override
                                    public void profile(ProfileModel profile) {
                                        if (profile != null) {

                                            tinderStackLayout.addCard(setCard(profile), 0);
                                        } else {
                                            //             findViewById(R.id.no_profile_left_text).setVisibility(View.VISIBLE);
                                        }
                                    }
                                });
                            }
                        }
                    }
                } else {
                    // findViewById(R.id.no_profile_left_text).setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();

        try {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    try {

                        ArrayList<String> ids = new ArrayList<>();
                        int count = tinderStackLayout.getChildCount();
                        for (int index = 0; index < count; index++) {
                            ids.add(((TinderCardView) tinderStackLayout.getChildAt(index)).getdbId());
                        }
                        for (ProfileModel profileModel :
                                shownProfilesMap.values()) {
                            for (int i = 0; i < ids.size(); i++) {


                                if (profileModel.getId().equals(ids.get(i))) {
                                    HomeDBHandlerClass.updateProfile(profileModel.getProfileTable().setProfileTaken(String.valueOf(false)).setType(ProfileTable.Type.NOT_SEEN.toString()).setSynced(false));
                                    ids.remove(i);
                                    i--;
                                }
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    // endregion


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CARD_CLICKED_REQ_CODE) {
            switch (resultCode) {
                case (ProfileDetailActivity.HOT_PRESSED): {
                    String id = data.getStringExtra("profileId");
                    //int id = Integer.parseInt(data.getStringExtra("profileId"));
                    for (int i = tinderStackLayout.getChildCount() - 1; i >= 0; i--) {
                        final TinderCardView cardView = ((TinderCardView) tinderStackLayout.getChildAt(i));
                        if (cardView.getdbId().equalsIgnoreCase(id)) {
                            HomeActivity.this.onHotClick(id);
                            HomeActivity.this.onViewRemoved(id);
                            HomeActivity.this.getNextCard(new TinderStackLayout.GetCard() {
                                @Override
                                public void card(TinderCardView tinderCardView) {
                                    tinderStackLayout.addCard(tinderCardView, 0);
                                }
                            });
                            //cardView.hotBtn.callOnClick();
                            break;
                        }
                    }
                    break;
                    //updateProfile(Integer.parseInt(id), String.valueOf(ProfileTable.Type.HOT));
                }
                case (ProfileDetailActivity.NOT_PRESSED): {
                    String id = data.getStringExtra("profileId");
                    //int id = Integer.parseInt(data.getStringExtra("profileId"));
                    for (int i = tinderStackLayout.getChildCount() - 1; i >= 0; i--) {
                        final TinderCardView cardView = ((TinderCardView) tinderStackLayout.getChildAt(i));
                        if (cardView.getdbId().equalsIgnoreCase(id)) {
                            HomeActivity.this.onNotClick(id);
                            HomeActivity.this.onViewRemoved(id);
                            HomeActivity.this.getNextCard(new TinderStackLayout.GetCard() {
                                @Override
                                public void card(TinderCardView tinderCardView) {
                                    tinderStackLayout.addCard(tinderCardView, 0);
                                }
                            });
                            //  cardView.notBtn.callOnClick();
                        }
                    }
                    break;
                }

                case (ProfileDetailActivity.SUPER_HOT_PRESSED): {
                    String id = data.getStringExtra("profileId");
                    //int id = Integer.parseInt(data.getStringExtra("profileId"));
                    for (int i = tinderStackLayout.getChildCount() - 1; i >= 0; i--) {
                        final TinderCardView cardView = ((TinderCardView) tinderStackLayout.getChildAt(i));
                        if (cardView.getdbId().equalsIgnoreCase(id)) {
//                            cardView.superHotBtn.callOnClick();
                            HomeActivity.this.onSuperHotClick(id);
                            HomeActivity.this.onViewRemoved(id);
                            HomeActivity.this.getNextCard(new TinderStackLayout.GetCard() {
                                @Override
                                public void card(TinderCardView tinderCardView) {
                                    tinderStackLayout.addCard(tinderCardView, 0);
                                }
                            });
                        }
                    }
                    break;

                }

                case (ProfileDetailActivity.REPORTED): {
                    String id = data.getStringExtra("profileId");
                    //int id = Integer.parseInt(data.getStringExtra("profileId"));
                    for (int i = tinderStackLayout.getChildCount() - 1; i >= 0; i--) {
                        final TinderCardView cardView = ((TinderCardView) tinderStackLayout.getChildAt(i));
                        if (cardView.getdbId().equalsIgnoreCase(id)) {
                            cardView.notBtn.callOnClick();
                        }
                    }
                    break;
                }

            }

        }

    }

    private SpannableStringBuilder addClickablePart(String str, final ArrayList<String> strings) {
        SpannableStringBuilder ssb = new SpannableStringBuilder(str);
        int index = 0;
        int idx1 = str.indexOf(strings.get(index));
        int idx2 = 0;
        while (idx1 != -1 && index < strings.size()) {
            idx2 = idx1 + strings.get(index).length();
            final String clickString = str.substring(idx1, idx2);
            final int finalIndex = index;
            final int finalIndex1 = index;
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {
                    Log.e("tappedVal", strings.get(finalIndex));
                    if (cardReceived && strings.get(finalIndex).equals("Reset profiles") && tinderStackLayout.getChildCount() == 0) {
                        cardReceived = false;
                        //                     tinderStackLayout.setVisibility(View.VISIBLE);
                        Singleton.getInstance().filterRecieved = true;
                        HomeActivity.this.index = 0;
                        tinderStackLayout.cardArray = null;// to use new card for profiles
                        new HomeServices().getNextCard(HomeActivity.this, HomeServices.DO_NOT_PERSIST_FILTERS, HomeServices.SHOULD_RESET_VALUES, new HomeServices.ProfileListener() {
                            @Override
                            public void profile(ProfileModel profile) {
                                findViewById(R.id.loader_layout).setVisibility(View.GONE);
                                if (profile != null) {
                                    //        findViewById(R.id.no_profile_left_text).setVisibility(View.GONE
                                    //);
                                    cardReceived = true;
                                    if (tinderStackLayout.getChildCount() < STACK_SIZE) {
                                        tinderStackLayout.addCard(setCard(profile), 0);

                                        for (int i = HomeActivity.this.index; HomeActivity.this.index < i + STACK_SIZE; HomeActivity.this.index++) {
                                            if (tinderStackLayout.getChildCount() < STACK_SIZE) {
                                                new HomeServices().getNextCard(HomeActivity.this, HomeServices.DO_NOT_PERSIST_FILTERS, HomeServices.SHOULD_NOT_RESET_VALUES, new HomeServices.ProfileListener() {
                                                    @Override
                                                    public void profile(ProfileModel profile) {
                                                        if (profile != null) {
                                                            tinderStackLayout.addCard(setCard(profile), 0);
                                                        } else {

                                                            Log.e("CARD", finalIndex + " mot null ");
                                                            //                     findViewById(R.id.no_profile_left_text).setVisibility(View.VISIBLE);
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                } else {
                                    //       findViewById(R.id.no_profile_left_text).setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    } else if (strings.get(finalIndex).equals("change filters") && tinderStackLayout.getChildCount() == 0) {
                        Intent intent = new Intent(HomeActivity.this, SettingsActivity.class);
                        startActivity(intent);
                    }

                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(getResources().getColor(R.color.grey_700));
                    ds.bgColor = getResources().getColor(R.color.grey_200);
                }
            }, idx1, idx2, 0);
            index++;
            if (index < strings.size())
                idx1 = str.indexOf(strings.get(index));

        }

        return ssb;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        syncProfilesWithServers();
    }

    @Override
    protected void onHomeBtnPressed() {
        super.onHomeBtnPressed();
        syncProfilesWithServers();
    }


    class SpLikeSaveAsync extends AsyncTask<ProfileModel, Void, Void> {
        @Override
        protected Void doInBackground(ProfileModel... profileModels) {
            ProfileModel model = profileModels[0];
            String dpUrl = "";
            if (model.getImages().size() > 0 && model.getImages() != null) {
                if (model.getImages().get(0).getImageUrl() != null && model.getImages().get(0).getImageUrl().contains("http")) {
                    dpUrl = model.getImages().get(0).getImageUrl();
                } else {
                    dpUrl = model.getImages().get(0).getImageBaseUrl() + model.getImages().get(0).getImageUrl();
                }
            }
            HomeDBHandlerClass.saveSpLikes(model, dpUrl);
            Log.e("HomeActivity", "SpLikeSaveAsync executed");
            return null;
        }
    }

    public void getSelfProfile() {
        if (LocalPreferenceManager.getInstance().getSelfProfileJson() != null) {
            Log.e("jsonString", LocalPreferenceManager.getInstance().getSelfProfileJson());
            try {
                JSONObject selfProfileJson = new JSONObject(LocalPreferenceManager.getInstance().getSelfProfileJson());
                BaseModel baseModel = new DescriptionParser().parseJson(selfProfileJson);
                ServiceSingleton.getInstance().setSelfProfile(((ProfileWrapper) baseModel).getProfileWrapper().get(0));
                if (((ProfileWrapper) baseModel).getProfileWrapper().get(0).getGender().equals("male")) {
                    ServiceSingleton.getInstance().setSelfPlaceholder(R.drawable.male_placeholder);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        new FacebookGraphService().profileDescriptionService(HomeActivity.this, LocalPreferenceManager.getInstance().getUserId(), "", new SharedProfileResponse() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                if (reasonOfError == null) {
                    ProfileWrapper profileWrapper = (ProfileWrapper) baseModel;
                    if (profileWrapper.getProfileWrapper().size() > 0) {
                        LocalPreferenceManager.getInstance().setSelfProfile(jsonString);
                        LocalPreferenceManager.getInstance().setUserName(profileWrapper.getProfileWrapper().get(0).getName());
                        LocalPreferenceManager.getInstance().setUserImage(profileWrapper.getProfileWrapper().get(0).getImages().get(0).getImageUrl());
                        LocalPreferenceManager.getInstance().setUserId(profileWrapper.getProfileWrapper().get(0).getId());
                        ServiceSingleton.getInstance().setSelfProfile(profileWrapper.getProfileWrapper().get(0));
                        if ((profileWrapper.getProfileWrapper().get(0).getGender().equals("male"))) {
                            ServiceSingleton.getInstance().setSelfPlaceholder(R.drawable.male_placeholder);
                        }
                    }
                }
            }
        });

    }

    @Override
    protected void onResume() {
        LogAnalytics.logScreenView(mTracker, Constants.ActivityName.HOME_SCREEN);
        LocalBroadcastManager.getInstance(HomeActivity.this).registerReceiver(appUpdateReceiver, new IntentFilter(LoginActivity.UPDATE_APP_INTENT_ACTION));
        super.onResume();
    }

    BroadcastReceiver appUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            Log.w("LoginBroadCast", "Received");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    appUpdateDialogLayout.setVisibility(View.VISIBLE);
                    appUpdateDialogLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });

                    final int tag = intent.getIntExtra(LoginActivity.UPDATE_DG_TYPE_KEY, 0);
                    String msgTxt = intent.getStringExtra(LoginActivity.UPDATE_MSG_KEY);

                    TextViewPlus msg = (TextViewPlus) appUpdateDialogLayout.findViewById(R.id.msgTxt);
                    msg.setText(Constants.ServiceConfiguration.APP_UPDATE_MESSAGE);
                    TextViewPlus positiveBtn = (TextViewPlus) appUpdateDialogLayout.findViewById(R.id.updateBtn);
                    TextViewPlus negBtn = (TextViewPlus) appUpdateDialogLayout.findViewById(R.id.notNowBtn);

                    positiveBtn.setText("Update");
                    negBtn.setText("Not Now");


                    if (tag == LoginActivity.MANDATORY_UPDATE_TAG) {
                        negBtn.setVisibility(View.GONE);
                    } //else {
                    negBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (tag == LoginActivity.UPDATE_TAG) {
                                LocalPreferenceManager.getInstance().setPreference("myVersionCode", Constants.ServiceConfiguration.APP_CURRENT_VERSION);
                                // mDialog.dismiss();
                                appUpdateDialogLayout.setVisibility(View.GONE);
                            } /*else if (tag == UPDATE_INFO_DIALOG) {
                                //negBtn=> update new(callservice)
                                callLoginService(FacebookDTO.MANDATORY_UPDATE, accessToken);
                            }*/
                        }
                    });
                    //}

                    positiveBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (tag == LoginActivity.MANDATORY_UPDATE_TAG || tag == LoginActivity.UPDATE_TAG) {
                                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                } finally {
                                    if (tag == LoginActivity.MANDATORY_UPDATE_TAG) {
                                        //mDialog.show();
                                        appUpdateDialogLayout.setVisibility(View.VISIBLE);
                                    }
                                }
                            } /*else if (tag == UPDATE_INFO_DIALOG) {
                                //posBtn=>continue with old
                                if (!fbUserProfileInfo.getFbUserProfileResponseModel().isBlocked()) {
                                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    showOkAlert(CommonMethods.getErrorMessage(Constants.Error.LoginError.USER_BLOCKED), "OOPS..!!", BLOCKED_OKH);
                                }
                            }*/
                        }
                    });

                }
            });

        }
    };

    @Override
    protected void onTryAgainClicked() {
        callAppConfigService();
        dialog.dismiss();
        super.onTryAgainClicked();
    }

    @Override
    public void appConfigTryAgainClicked() {
        showNoNetDialog();
    }
}
