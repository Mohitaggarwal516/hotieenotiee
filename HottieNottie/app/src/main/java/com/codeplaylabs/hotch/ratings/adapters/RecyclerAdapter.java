package com.codeplaylabs.hotch.ratings.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.customUi.CircleImageView;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.ratings.RatingsActivity;
import com.codeplaylabs.hotch.ratings.interfaces.RecyclerCellClicked;
import com.codeplaylabs.hotch.ratings.interfaces.RateRecyclerCallBack;
import com.codeplaylabs.hotch.ratings.models.RateDiscoveryWrapper;
import com.codeplaylabs.hotch.utils.TextViewPlus;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by HP on 28-Aug-17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    private ArrayList<ProfileModel> profilemodelList;
    // ProfileWrapper profileWrapper;
    Context context;
    RecyclerCellClicked cellClicked;
    String gender;
    RateDiscoveryWrapper.filterEnum filterEnum;
    public static boolean hideBottomLoader = false;
    int page = 1;

    public RecyclerAdapter(Context context, ArrayList<ProfileModel> profilemodelList, String gender, RateDiscoveryWrapper.filterEnum filterEnum, RecyclerCellClicked cellClicked) {
        this.context = context;
        this.profilemodelList = profilemodelList;
        this.gender = gender;
        this.filterEnum = filterEnum;
        this.cellClicked = cellClicked;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_rating_recyclerview, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.descriptionTxt.setVisibility(View.VISIBLE);
        holder.locPin.setVisibility(View.VISIBLE);
        holder.bottomLoader.setVisibility(View.GONE);

        if (hideBottomLoader == true) {
            holder.bottomLoader.setVisibility(View.GONE);
        }

        if (position == (getItemCount() - 1) && position > 4) {
            holder.bottomLoader.setVisibility(View.VISIBLE);
            cellClicked.callNextPg(page, this, new RateRecyclerCallBack() {
                @Override
                public void setBottomLoaderVisibility(boolean show) {
                    if (!show)
                        holder.bottomLoader.setVisibility(View.GONE);
                }
            });
        }
        //currentlyAt
        ProfileModel model = profilemodelList/*profileWrapper.getProfileWrapper()*/.get(position);

        // String name="<b>"+" #" + (position + 1) + "   " +model.getName()+"<b>";
        holder.name.setText(" #" + (position + 1) + "   " + model.getName());
        // holder.name.setText(Html.fromHtml(name));
        if (filterEnum == RateDiscoveryWrapper.filterEnum.CurrentlyAt) {
            String pos = model.getWork().getPosition();
            if (pos != null && pos.length() > 0) {
                holder.descriptionTxt.setText(pos);
                holder.locPin.setImageResource(0);//to remove the loc pin
                holder.locPin.setImageResource(R.drawable.ic_currently_at);
            } else {
                holder.descriptionTxt.setVisibility(View.GONE);
                holder.locPin.setVisibility(View.GONE);
            }
        }//currentLoc
        else if (filterEnum == RateDiscoveryWrapper.filterEnum.CurrentLocation) {
            if (model.getWork() != null && model.getWork().getEmployer() != null && model.getWork().getEmployer().getName() != null && model.getWork().getEmployer()
                .getName().length() > 0) {
                holder.descriptionTxt.setText(model.getWork().getEmployer().getName());

            } else if (model.getEducation() != null && model.getEducation().getInstitute() != null && model.getEducation().getInstitute().getName() != null && model.getEducation().getInstitute().getName().length() > 0) {

                holder.descriptionTxt.setText(model.getEducation().getInstitute().getName());

            } else {
 

                holder.descriptionTxt.setVisibility(View.GONE);
                holder.locPin.setVisibility(View.GONE);
            }
        }
        //country
        else if (filterEnum == RateDiscoveryWrapper.filterEnum.CurrentCountry) {

            if (model.getLocation() != null && model.getLocation().getName().length() > 0) {
                holder.descriptionTxt.setText(model.getLocation().getName());
            } else {
                holder.descriptionTxt.setVisibility(View.GONE);
                holder.locPin.setVisibility(View.GONE);
            }
        }
        //world
        else {
            if (model.getLocation() != null && model.getLocation().getCountry().length() > 0) {
                holder.descriptionTxt.setText(model.getLocation().getCountry());
            } else {
                holder.descriptionTxt.setVisibility(View.GONE);
                holder.locPin.setVisibility(View.GONE);
            }
        }

        //for placeholders
        Picasso.with(context).cancelRequest(holder.displayPic);//to cancel img req for previous view(to avoid mem issues)
        if (model.getImages() != null && model.getImages().size() > 0) {
            if (gender.equalsIgnoreCase(RatingsActivity.FEMALE))
                Picasso.with(context).load(model.getImages().get(0).getImageUrl(UserImage.ImgType.SIZE_100x100)).placeholder(R.drawable.female_placeholder).into(holder.displayPic);
            else
                Picasso.with(context).load(model.getImages().get(0).getImageUrl(UserImage.ImgType.SIZE_100x100)).placeholder(R.drawable.male_placeholder).into(holder.displayPic);
        } else {
            if (gender.equalsIgnoreCase(RatingsActivity.FEMALE)) {
                //test(R.drawable.female_placeholder,holder.displayPic,model,false);
                Picasso.with(context).load(R.drawable.female_placeholder).placeholder(R.drawable.male_placeholder).into(holder.displayPic);
            } else {
                Picasso.with(context).load(R.drawable.male_placeholder).placeholder(R.drawable.male_placeholder).into(holder.displayPic);
            }
        }
        //onClick
        holder.cellLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cellClicked.onCellClicked(profilemodelList/*profileWrapper.getProfileWrapper()*/.get(position)/*.getId()*/);
            }
        });
    }

   /* private void test(int placeHolder,ImageView view,ProfileModel model,boolean isImagesLenGrtrThan0){
        if(isImagesLenGrtrThan0){
            Picasso.with(context).load(model.getImages().get(0).getImageUrl(UserImage.ImgType.SIZE_100x100)).placeholder(placeHolder).into(view);
        }else{
            Picasso.with(context).load(placeHolder).placeholder(placeHolder).into(view);
        }
    }*/

    @Override
    public int getItemCount() {
        return profilemodelList/*profileWrapper.getProfileWrapper()*/.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextViewPlus name, descriptionTxt;
        CircleImageView displayPic;
        ImageView locPin;
        LinearLayout cellLay;
        ProgressBar bottomLoader;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = (TextViewPlus) itemView.findViewById(R.id.name);
            descriptionTxt = (TextViewPlus) itemView.findViewById(R.id.loc);
            displayPic = (CircleImageView) itemView.findViewById(R.id.dp_rate);
            locPin = (ImageView) itemView.findViewById(R.id.loc_pin);
            cellLay = (LinearLayout) itemView.findViewById(R.id.cell_lay);
            bottomLoader = (ProgressBar) itemView.findViewById(R.id.bottom_loader);
        }
    }
}
