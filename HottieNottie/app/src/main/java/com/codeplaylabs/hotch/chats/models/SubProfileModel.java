package com.codeplaylabs.hotch.chats.models;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.utils.CommonMethods;

import java.util.ArrayList;

/**
 * Created by mohit on 01/09/17.
 */

public class SubProfileModel extends BaseModel {

    private ProfileModel profileModel = null;
    private String chatId ;
    private String lastMessage;
    private String messageSender;
    private long lastMessageTimeStamp;
    private long matchDate;
   // private String deviceTokens;
    private boolean isNotificationDisabledByPartner;
    private boolean isNotificationDisabledBySelf;
  //  private String deviceType;
    private ArrayList<Devices> devices;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getMessageSender() {
        return messageSender;
    }

    public void setMessageSender(String messageSender) {
        this.messageSender = messageSender;
    }

    public long getLastMessageTimeStamp() {
        return CommonMethods.GMTToLocal(lastMessageTimeStamp);
    }

    public void setLastMessageTimeStamp(long lastMessageTimeStamp) {
        this.lastMessageTimeStamp = CommonMethods.localToGMT(lastMessageTimeStamp);
    }

    public long getMatchDate() {
        return CommonMethods.GMTToLocal(matchDate);
    }

    public void setMatchDate(long matchDate) {
        this.matchDate = CommonMethods.localToGMT(matchDate);
    }

    public ProfileModel getProfileModel() {
        return profileModel;
    }

    public void setProfileModel(ProfileModel profileModel) {
        this.profileModel = profileModel;
    }

    public boolean getIsNotificationDisabledByPartner() {
        return isNotificationDisabledByPartner;
    }

    public void setIsNotificationDisabledByPartner(boolean isNotificationDisabledByPartner) {
        this.isNotificationDisabledByPartner = isNotificationDisabledByPartner;
    }

    public boolean getIsNotificationDisabledBySelf() {
        return isNotificationDisabledBySelf;
    }

    public void setIsNotificationDisabledBySelf(boolean isNotificationDisabledBySelf) {
        this.isNotificationDisabledBySelf = isNotificationDisabledBySelf;
    }

    public ArrayList<Devices> getDevices() {
        if(devices == null){
            devices = new ArrayList<>();
        }
        return devices;
    }

    public class Devices{
        private String deviceType;
        private String deviceTokens;

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceTokens() {
            return deviceTokens;
        }

        public void setDeviceTokens(String deviceTokens) {
            this.deviceTokens = deviceTokens;
        }
    }
}
