package com.codeplaylabs.hotch.login.models.dtoModels;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.login.models.FBUserProfileResponseModel;
import com.codeplaylabs.hotch.login.models.UserProfile;

/**
 * Created by mohit on 22/06/17.
 */

public class FBUserProfileInfo extends BaseModel {

    private UserProfile userProfile  = null;
    private FBUserProfileResponseModel fbUserProfileResponseModel = null;

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public FBUserProfileResponseModel getFbUserProfileResponseModel() {
        return fbUserProfileResponseModel;
    }

    public void setFbUserProfileResponseModel(FBUserProfileResponseModel fbUserProfileResponseModel) {
        this.fbUserProfileResponseModel = fbUserProfileResponseModel;
    }
}
