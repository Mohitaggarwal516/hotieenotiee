package com.codeplaylabs.hotch.chats.models;

import com.codeplaylabs.hotch.base.BaseModel;

import java.util.ArrayList;

/**
 * Created by mohit on 31/07/17.
 */

public class ChatListWrapper extends BaseModel {

    private ArrayList<SubProfileModel> profileModels;

    public ArrayList<SubProfileModel> getProfileModels() {
        if(profileModels == null){
            profileModels = new ArrayList<>();
        }
        return profileModels;
    }
}
