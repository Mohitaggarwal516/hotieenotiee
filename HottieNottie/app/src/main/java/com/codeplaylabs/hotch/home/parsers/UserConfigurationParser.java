package com.codeplaylabs.hotch.home.parsers;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONObject;

/**
 * Created by mohit on 28/08/17.
 */

public class UserConfigurationParser extends BaseParser {
    BaseModel baseModel = new BaseModel();

    @Override
    public BaseModel parseJson(JSONObject response) {
        if (validate(response) != null) {
            return moduleJson(response);
        } else {
            baseModel.error = Constants.Error.SettingsError.PROFILE_SETTING_ERROR;
            return baseModel;
        }
    }

    /*
      * This method is called to collect the data received from the servers in a class for the session
      * @param response
     */
    public BaseModel moduleJson(JSONObject response) {

        //UserProfile userProfile = new UserProfile();
        try {
            if (response != null) {
                CustomJsonObject cjObject = new CustomJsonObject(response);
                cjObject = cjObject.getCustomJsonObject("data");

                com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().setPreference("superhotEnabled", System.currentTimeMillis() + (1000 * (Long.parseLong(cjObject.getString("nextSuperHotInSec")))));

            }
            baseModel.error = Constants.Error.NULL_RESPONSE_ERROR;
            return baseModel;
        }catch (Exception e){
            e.printStackTrace();
            baseModel.error = Constants.Error.NULL_RESPONSE_ERROR;
            return baseModel;
        }
    }

}
