package com.codeplaylabs.hotch.settings.parsers;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.login.models.Detail;
import com.codeplaylabs.hotch.settings.models.albums.discovery.DiscoverySettingModel;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by HP on 15-Jul-17.
 */

public class ParseDiscoveryResponse extends BaseParser {
    @Override
    public BaseModel parseJson(JSONObject response) throws JSONException {
        return parseDiscovery(response);
    }

    private BaseModel parseDiscovery(JSONObject response) {
        DiscoverySettingModel discovery = new DiscoverySettingModel();
        CustomJsonObject customReponse = new CustomJsonObject(response);
        CustomJsonObject data = new CustomJsonObject(customReponse.getJsonObject("data"));
        discovery.setMinAge(data.getString("minAge"));
        discovery.setMaxAge(data.getString("maxAge"));
        JSONArray genders = data.getJsonArray("genders");
        for (int i = 0; i < genders.length(); i++) {
            try {
                discovery.getGender().add(genders.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        CustomJsonObject cCurrentLoc = new CustomJsonObject(data.getJsonObject("currentLocation"));
        if(cCurrentLoc != null){
            Detail currLoc = new Detail();
            currLoc.setName(cCurrentLoc.getString("city"));
            currLoc.setId(cCurrentLoc.getString("facebookId"));
            if(currLoc.getName().length() > 0 && currLoc.getId().length() > 0) {
                discovery.setCurrentLocation(currLoc);
            }
        }
        CustomJsonObject cHomeTown=new CustomJsonObject(data.getJsonObject("hometown"));
        if(cHomeTown!=null){
            Detail homeTown = new Detail();
            homeTown.setName(cHomeTown.getString("city"));
            homeTown.setId(cHomeTown.getString("facebookId"));
            if(homeTown.getName().length() > 0 && homeTown.getId().length() > 0) {
                discovery.setHometown(homeTown);
            }
        }
        CustomJsonObject cCountry=new CustomJsonObject(data.getJsonObject("country"));
        if(cCountry!=null){
            Detail country = new Detail();
            country.setName(cCountry.getString("name"));
            country.setId(cCountry.getString("facebookId"));
            if(country.getName().length() > 0 && country.getId().length() > 0) {
                discovery.setCountry(country);
            }
        }
        CustomJsonObject cCurrentlyAt=new CustomJsonObject(data.getJsonObject("currentlyAt"));
        if(cCurrentlyAt!=null){
            Detail currAt = new Detail();
            currAt.setName(cCurrentlyAt.getString("name"));
            currAt.setId(cCurrentlyAt.getString("facebookId"));
            if(currAt.getName().length() > 0 && currAt.getId().length() > 0) {
                discovery.setCurrentlyAt(currAt);
            }
        }

        CustomJsonObject cNotificationSettingObj=new CustomJsonObject(data.getJsonObject("notificationSetting"));

        discovery.getNotificationSetting().put("superHotNotification",cNotificationSettingObj.getBoolean("superHotNotification",false));
        discovery.getNotificationSetting().put("newMatchNotification",cNotificationSettingObj.getBoolean("newMatchNotification",false));
        discovery.getNotificationSetting().put("promotionalNotification",cNotificationSettingObj.getBoolean("promotionalNotification",false));
        discovery.getNotificationSetting().put("messageNotification",cNotificationSettingObj.getBoolean("messageNotification",false));

        return discovery;
    }
}
