package com.codeplaylabs.hotch.chats.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseFragment;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.chats.adapters.ChatListAdapter;
import com.codeplaylabs.hotch.chats.handler.ChatDBHandlerClass;
import com.codeplaylabs.hotch.chats.interfaces.ChatListResponse;
import com.codeplaylabs.hotch.chats.models.ChatListWrapper;
import com.codeplaylabs.hotch.chats.models.SubProfileModel;
import com.codeplaylabs.hotch.chats.models.firbaseModels.FriendlyMessage;
import com.codeplaylabs.hotch.chats.models.firbaseModels.UserModel;
import com.codeplaylabs.hotch.chats.services.ChatServices;
import com.codeplaylabs.hotch.chats.ui.ChatActivity;
import com.codeplaylabs.hotch.chats.ui.ChatFragmentActivity;
import com.codeplaylabs.hotch.database.tables.ChatUsersList;
import com.codeplaylabs.hotch.database.tables.DeviceTable;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.utils.ServiceSingleton;
import com.codeplaylabs.hotch.utils.Singleton;
import com.codeplaylabs.hotch.utils.TextViewPlus;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class OldChatListFragment extends BaseFragment{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private String userId;
    private DatabaseReference mFirebaseDatabaseReference;
    private ChatListAdapter chatListAdapter;
    private TextViewPlus noListTextView;
    private Context context;
    private boolean serviceRecieved = false;
    private LinearLayoutManager layoutMananger;
    private RelativeLayout loader;


    public OldChatListFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static OldChatListFragment newInstance() {
        OldChatListFragment fragment = new OldChatListFragment();
//        Bundle args = new Bundle();
//        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_chat_old, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.chat_swipe_refresh);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.chat_recycler_view);

        noListTextView = (TextViewPlus) rootView.findViewById(R.id.no_chat_list_text);
        loader = (RelativeLayout)rootView.findViewById(R.id.loader_layout);
        userId = LocalPreferenceManager.getInstance().getUserId();

        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference("User").child(userId);
        layoutMananger = new LinearLayoutManager(getActivity());


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                callListListener(0);
            }
        });

        callListListener(0);
        return rootView;
    }


    public void callListListener(int pageNo) {
        if (!ServiceSingleton.getInstance().isChatWithMsgServiceRunning() && ServiceSingleton.getInstance().getModel() == null) {
            loader.setVisibility(View.VISIBLE);
            new ChatServices().getChatWithMessages((Activity) context, pageNo, new ChatListResponse() {
                @Override
                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                    loader.setVisibility(View.GONE);
                    serviceRecieved = true;
                    if (baseModel != null && reasonOfError == null) {
                        setUpChatList((ChatListWrapper) baseModel);
                        OldChatListFragment.this.onStart();
                    } else {
                        ((Activity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showNoNetDialogFrag(context);
                            }
                        });

                    }

                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        } else {
            serviceRecieved = true;
            loader.setVisibility(View.GONE);
            setUpChatList((ChatListWrapper) ServiceSingleton.getInstance().getModel());
            OldChatListFragment.this.onStart();
        }
    }
    @Override
    protected void onTryAgainClicked() {
        //Toast.makeText(this, "tryAgain", Toast.LENGTH_SHORT).show();
        dialog.dismiss();
        dialog=null;
        startActivity(new Intent(context, ChatFragmentActivity.class));
        ((Activity)context).finish();
        super.onTryAgainClicked();
    }

    public void setUpChatList(ChatListWrapper chatListWrapper) {
        if(chatListWrapper == null){
            return;
        }
        ArrayList<SubProfileModel> list = chatListWrapper.getProfileModels();

        for (SubProfileModel subProfileModel : list) {
            String[] arrayList = subProfileModel.getChatId().split("_");
            ArrayList<String> array = new ArrayList<>();
            array.add(arrayList[0]);
            array.add(arrayList[1]);
            ChatUsersList chatUsersList = new ChatUsersList();
            // UserModel userModel = new UserModel();
            ProfileModel profile = subProfileModel.getProfileModel();

            chatUsersList.setUserId(profile.getId());
            chatUsersList.setId(subProfileModel.getChatId());
            chatUsersList.setMatchDate(subProfileModel.getMatchDate());
            chatUsersList.setName(profile.getName());
            chatUsersList.setGender(profile.getGender());
            chatUsersList.setImageUrl(profile.getImages().get(0).getImageUrl(UserImage.ImgType.SIZE_100x100));
            chatUsersList.setLastMessage(subProfileModel.getLastMessage());
            chatUsersList.setLastMessageTimestamp(subProfileModel.getLastMessageTimeStamp());
            chatUsersList.setSelf(array.indexOf(subProfileModel.getMessageSender()));
            chatUsersList.setIsNotificationDisabledBySelf(subProfileModel.getIsNotificationDisabledBySelf());
            chatUsersList.setIsNotificationDisabledByPartner(subProfileModel.getIsNotificationDisabledByPartner());

            ChatDBHandlerClass.deleteDevicesForUserId(subProfileModel.getProfileModel().getId());
            for (SubProfileModel.Devices device : subProfileModel.getDevices()) {
                DeviceTable deviceTable = new DeviceTable();
                deviceTable.setDeviceType(device.getDeviceType());
                deviceTable.setDeviceTokens(device.getDeviceTokens());
                deviceTable.setUserId(subProfileModel.getProfileModel().getId());
                ChatDBHandlerClass.saveDataInTable(ChatDBHandlerClass.dataBaseTables.deviceTable,
                        deviceTable);

            }
            ChatUsersList chatUser = ChatDBHandlerClass.getChatFromTable(chatUsersList.getId());
            if (chatUser != null) {
                if (chatUser.getLastMessageTimestamp() >= subProfileModel.getLastMessageTimeStamp()) {
                    chatUsersList.setLastMessage(chatUser.getLastMessage());
                    chatUsersList.setLastMessageTimestamp(chatUser.getLastMessageTimestamp());
                    chatUsersList.setSelf(chatUser.isSelf());

                }
                try {
                    if (chatUsersList.getLastMessageTimestamp() > chatUser.getLastSeenTimestamp() && !array.get(chatUsersList.isSelf()).equals(LocalPreferenceManager.getInstance().getUserId())) {
                        chatUsersList.setIsNew("true");
                    } else {
                        chatUsersList.setIsNew("false");
                    }
                }catch (Exception e){
                    chatUsersList.setIsNew("true");
                }
                chatUsersList.setLastSeenTimestamp(chatUser.getLastSeenTimestamp());
            }

            ChatDBHandlerClass.saveDataInTable(ChatDBHandlerClass.dataBaseTables.chatListTable,
                    chatUsersList);

        }


    }

    private void setAdapter(List<UserModel> users) {
        boolean type = false;
        if (/*chatListAdapter == null && */users != null && users.size() > 0) {
            noListTextView.setVisibility(View.GONE);
            chatListAdapter = new ChatListAdapter(getActivity(), users, ChatListAdapter.chatListEnum.OLD, new OldChatListFragment.ChatListListener() {
                @Override
                public void onItemClicked(int position, UserModel user) {
                    UserModel userModel = new UserModel();
                    userModel.setTimestamp(user.getTimestamp()).setChatId(user.getChatId())
                            .setName(user.getName());
                    Intent intent = new Intent(getActivity(), ChatActivity.class);
                    intent.putExtra("otherUser", user);
                    if(Singleton.getInstance().isNetworkAvailable()) {
                        startActivity(intent);
                    }else{
                        Toast.makeText(getActivity(),"No internet connection, please try again.",Toast.LENGTH_SHORT).show();
                    }
                }

            });

            recyclerView.setLayoutManager(layoutMananger);
            recyclerView.scrollToPosition(layoutMananger.findFirstVisibleItemPosition());
            recyclerView.setAdapter(chatListAdapter);
        } else if (users != null && users.size() > 0) {
            noListTextView.setVisibility(View.GONE);
            chatListAdapter.notifyDataSetChanged();
        } else {
            noListTextView.setVisibility(View.VISIBLE);
        }
    }


    public interface ChatListListener {
        void onItemClicked(int position, UserModel userModel);
    }


    @Override
    public void onStart() {
        try {
            super.onStart();
            Log.e("CHAT", "START");
            if (serviceRecieved) {
                setValueForAdapter();
            }
            //callListListener(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setValueForAdapter() {

        List<ChatUsersList> chats = ChatDBHandlerClass.getAllChatsFromTableOrderByTimeDesc();

        ArrayList<UserModel> userModels = new ArrayList<>();
        if (chats != null) {

            for (ChatUsersList chat : chats) {

                UserModel userModel = new UserModel();
                if(chat.getGender() == null || chat.getId() == null ){

                }else {
                    userModel.setUserId(chat.getUserId());
                    userModel.setGender(chat.getGender());

                    userModel.setName(chat.getName());
                    userModel.setChatId(chat.getId());
                    userModel.setTimestamp(chat.getMatchDate());
                    userModel.selfMuteStatus(chat.getIsNotificationDisabledBySelf());

                    FriendlyMessage friendlyMessage = new FriendlyMessage();
                    friendlyMessage.setText(chat.getLastMessage())
                            .setTimeStamp(chat.getLastMessageTimestamp())
                            .setSelf(chat.isSelf());
                    userModel.setIsNew(chat.getIsNew());
                    userModel.setLastMessage(friendlyMessage);
                    userModel.setImage(chat.getImageUrl());
                    if (userModel.getChatId() != null) {
                        userModels.add(userModel);
                    }
                }
            }

            setAdapter(userModels);
        } else {
            // text to display

        }
    }


}