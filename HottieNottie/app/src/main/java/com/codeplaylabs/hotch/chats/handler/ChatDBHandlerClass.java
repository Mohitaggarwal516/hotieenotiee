package com.codeplaylabs.hotch.chats.handler;

import android.util.Log;

import com.codeplaylabs.hotch.database.databaseInitialization.DatabaseHelper;
import com.codeplaylabs.hotch.database.tables.BaseTable;
import com.codeplaylabs.hotch.database.tables.ChatUsersList;
import com.codeplaylabs.hotch.database.tables.DeviceTable;
import com.codeplaylabs.hotch.database.tables.FilterTable;
import com.codeplaylabs.hotch.database.tables.ProfileTable;
import com.codeplaylabs.hotch.utils.Singleton;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mohit on 06/07/17.
 */

public class ChatDBHandlerClass {


    private static String tag="ChatDbHandler";

    public enum dataBaseTables {chatListTable,deviceTable}

    public static boolean saveDataInTable(dataBaseTables tableName, BaseTable baseTable) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        switch (tableName) {
            case chatListTable: {

                try {
                    Dao<ChatUsersList, Integer> chatListDao = databaseHelper.getChatListDao();
                    chatListDao.createOrUpdate((ChatUsersList) baseTable);
                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            case deviceTable: {
                try {
                   // deleteDevicesForUserId(((DeviceTable)baseTable).getUserId());

                    Dao<DeviceTable, Integer> profileDao = databaseHelper.getDeviceDao();
                    profileDao.createOrUpdate((DeviceTable) baseTable);


                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            default: {
                return false;
            }
        }

    }

    public static boolean deleteDevicesForUserId(String userId) {
        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<DeviceTable, Integer> profileDao = databaseHelper.getDeviceDao();
            DeleteBuilder<DeviceTable, Integer> builder = profileDao.deleteBuilder();
            builder.where().eq("user_id", userId);
            int build = builder.delete();
            if (build > 0) {
                return true;
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    public static List<DeviceTable> getAllDevicesByUserId(String userId){
        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<DeviceTable, Integer> profileDao = databaseHelper.getDeviceDao();
            QueryBuilder<DeviceTable, Integer> builder = profileDao.queryBuilder();
            builder.where().eq("user_id",userId);
            List<DeviceTable> deviceTables = profileDao.query(builder.prepare());
            return deviceTables;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean clearTable(dataBaseTables tableName) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        switch (tableName) {
//            case filterTable: {
//                try {
//                    Dao<FilterTable, Integer> filterDao = databaseHelper.getFilterDao();
//                    filterDao.delete(filterDao.queryForAll());
//                    return true;
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
//                break;
//            }
            case chatListTable: {
                try {
                    Dao<ChatUsersList, Integer> profileDao = databaseHelper.getChatListDao();
                    profileDao.delete(profileDao.queryForAll());
                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            }

        }
        return false;
    }

    public static boolean deleteChatFromDb(String id) {
        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<ChatUsersList, Integer> profileDao = databaseHelper.getChatListDao();

            DeleteBuilder<ChatUsersList, Integer> builder = profileDao.deleteBuilder();
            builder.where().eq("id", id);
            int build = builder.delete();
            if (build > 0) {
                return true;
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    public static List<ChatUsersList> getAllChatsFromTableOrderByTimeDesc() {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<ChatUsersList, Integer> profileDao = databaseHelper.getChatListDao();
            QueryBuilder<ChatUsersList, Integer> builder = profileDao.queryBuilder();
            builder.where().not().eq("last_message","");
            builder.orderBy("last_message_timestamp",false);
            List<ChatUsersList> chatUsersLists = profileDao.query(builder.prepare());
            return chatUsersLists;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<ChatUsersList> getAllMatchesFromTableOrderByMatchDesc() {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<ChatUsersList, Integer> profileDao = databaseHelper.getChatListDao();
            QueryBuilder<ChatUsersList, Integer> builder = profileDao.queryBuilder();
            builder.where().eq("last_message","");
            builder.orderBy("match_date",false);
            List<ChatUsersList> chatUsersLists = profileDao.query(builder.prepare());
            return chatUsersLists;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static FilterTable getFilterFromTable() {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<FilterTable, Integer> filterDao = databaseHelper.getFilterDao();
            List<FilterTable> val = filterDao.queryForAll();
            if (val != null && val.size() > 0) {
                return val.get(0);
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ChatUsersList getChatFromTable(String id) {
        HashMap<String, String> params = new HashMap<>();
        params.put("id",id);
        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<ChatUsersList, Integer> profileDao = databaseHelper.getChatListDao();
            QueryBuilder<ChatUsersList, Integer> builder = profileDao.queryBuilder();
            builder.where().eq("id", params.get("id"));
            ChatUsersList profileTable = profileDao.queryForFirst(builder.prepare());



            return profileTable;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static long getCountOfProfilesFromTable(HashMap<String, String> params) {

        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<ProfileTable, Integer> profileDao = databaseHelper.getProfileDao();
            QueryBuilder<ProfileTable, Integer> builder = profileDao.queryBuilder();
            builder.where().eq("filter", params.get("filter")).and().eq("type", params.get("type")).and().eq("profile_taken", Boolean.valueOf(params.get("profile_taken")));
            builder.setCountOf(true);
            long count = profileDao.countOf(builder.prepare());
            Log.e("profile", count + "");
            return count;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void updateProfile(ChatUsersList chatUsersList) {
        DatabaseHelper databaseHelper = Singleton.getInstance().getHelper();
        try {
            Dao<ChatUsersList, Integer> userProfileDao = databaseHelper.getChatListDao();
            userProfileDao.update(chatUsersList);
            // testMethod(getAllProfilesFromTable(null));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}