package com.codeplaylabs.hotch.login.dto;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.database.tables.FbProfileTable;
import com.codeplaylabs.hotch.firebase.services.FirebaseServices;
import com.codeplaylabs.hotch.login.LoginDbHandler;
import com.codeplaylabs.hotch.login.interfaces.FacebookUserInfoResponse;
import com.codeplaylabs.hotch.login.interfaces.TokenResponse;
import com.codeplaylabs.hotch.login.models.CustomToken;
import com.codeplaylabs.hotch.login.models.Education;
import com.codeplaylabs.hotch.login.models.UserProfile;
import com.codeplaylabs.hotch.login.models.dtoModels.FBUserProfileInfo;
import com.codeplaylabs.hotch.login.parsers.ParseFacebookDataResponse;
import com.codeplaylabs.hotch.login.services.FacebookGraphService;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.utils.Singleton;
import com.codeplaylabs.hotch.login.interfaces.FacebookLikesResponse;
import com.codeplaylabs.hotch.login.models.LikesDetail;
import com.codeplaylabs.hotch.login.interfaces.FacebookDTOResponse;
import com.codeplaylabs.hotch.login.models.FBUserProfileResponseModel;
import com.codeplaylabs.hotch.login.models.Work;
import com.facebook.AccessToken;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;

/**
 * Created by mohit on 21/06/17.
 */

public class FacebookDTO extends BaseModel {
    public static final int NON_MANDATORY_UPDATE = 0;
    public static final int MANDATORY_UPDATE = 1;
    private AccessToken accessToken;
    private String change = "";

    class Async extends AsyncTask<Activity, Void, Void> {

        @Override
        protected Void doInBackground(final Activity... activity) {

            new FacebookGraphService().getUserLikes(activity[0], accessToken, new FacebookLikesResponse() {

                @Override
                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {


                    if (baseModel != null) {
                        LikesDetail likesDetail = (LikesDetail) baseModel;

                        new FacebookGraphService().setUserLikesToServer(activity[0], likesDetail, new FacebookLikesResponse() {
                            @Override
                            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                            }
                        });
                    }

                }
            });

            checkForSignIn(activity[0]);
            return null;
        }


    }

    public void getFacebookDetails(final Activity activity, final AccessToken accessToken, final int updateType, final FacebookDTOResponse facebookDTOResponse) {
        this.accessToken = accessToken;

        //fetching details from facebook
        new FacebookGraphService().getUserInfo(activity, accessToken, new FacebookUserInfoResponse() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {


                if (baseModel != null) {

                    boolean chkForUpdates = checkForDataUpdates(jsonString, (UserProfile) baseModel);
                    final UserProfile superBaseModel = (UserProfile) baseModel;
                    Singleton.getInstance().setUserProfile(superBaseModel);
                    if (chkForUpdates) {
                        facebookDTOResponse.showUpdateDialog(superBaseModel, new FacebookDTO.DialogButtonPressed() {
                            @Override
                            public void okClick(String id) {
                                sendDataToServers(activity, superBaseModel, MANDATORY_UPDATE, facebookDTOResponse);
                            }
                        });
                    } else {
                        sendDataToServers(activity, superBaseModel, updateType, facebookDTOResponse);
                    }


                } else {
                    facebookDTOResponse.onResponse(null, jsonString, Constants.Error.LoginError.LOGIN_FACEBOOK_RESPONSE_ERROR, null);
                }

            }
        });

    }

    private void sendDataToServers(final Activity activity, final UserProfile superBaseModel, int updateType, final FacebookDTOResponse facebookDTOResponse) {
        new FacebookGraphService().sendFacebookUserProfileDataToServers(activity, superBaseModel, updateType, new FacebookUserInfoResponse() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                if (baseModel != null) {
                    FBUserProfileInfo profileInfo = new FBUserProfileInfo();
                    LocalPreferenceManager.getInstance().initialise(activity);
                    LocalPreferenceManager.getInstance().setUserName(superBaseModel.getName());
                    profileInfo.setFbUserProfileResponseModel((FBUserProfileResponseModel) baseModel);
                    LocalPreferenceManager.getInstance().setUserId(profileInfo.getFbUserProfileResponseModel().getUserId());
                    new Async().execute(activity);
                    String userId = LocalPreferenceManager.getInstance().getUserId();
                    String fireBaseToken = com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().getStringPreference("firebaseToken", null);
                    if (userId != null && fireBaseToken != null) {
                        new FirebaseServices().sendTokenToServer(activity, userId, fireBaseToken, null);

                    }
                    if (profileInfo.getFbUserProfileResponseModel().isCurrentlyDetailsChanged()) {
                        change = "Some of your details have been changed";
                        facebookDTOResponse.onResponse(profileInfo, change, Constants.Error.LoginError.DATA_UPDATED, Constants.ReceivedFrom.SERVICE);
                    } else {
                        facebookDTOResponse.onResponse(profileInfo, jsonString, null, null);
                    }


                } else {
                    facebookDTOResponse.onResponse(null, jsonString, Constants.Error.LoginError.LOGIN_SERVER_RESPONSE_ERROR, null);
                }

            }
        });
    }

    private boolean checkForDataUpdates(String jsonString, UserProfile newUserProfile) {

        FbProfileTable fbProfileTable = LoginDbHandler.getFbProfileFromTable();
        if (fbProfileTable == null) {
            FbProfileTable fbProfileTable1 = new FbProfileTable();
            fbProfileTable1.setProfile(jsonString)
                    .setTimestamp(System.currentTimeMillis());
            LoginDbHandler.saveDataInTable(LoginDbHandler.dataBaseTables.fbProfileTable, fbProfileTable1);
            return false;
        } else {
            try {
                UserProfile oldUserModel = (UserProfile) new ParseFacebookDataResponse().parseJson(new JSONObject(fbProfileTable.getProfile()));

                int sizeEdOld = 0;
                int sizeEdNew = 0;
                int sizeWrOld = 0;
                int sizeWrNew = 0;
                if(oldUserModel.getEducationList() != null){
                    sizeEdOld = oldUserModel.getEducationList().size();
                }
                if(oldUserModel.getWorkList() != null){
                    sizeWrOld = oldUserModel.getWorkList().size();
                }
                if(newUserProfile.getEducationList() != null){
                    sizeEdNew = newUserProfile.getEducationList().size();
                }
                if(newUserProfile.getWorkList() != null){
                    sizeWrNew = newUserProfile.getWorkList().size();
                }


                if (sizeWrOld != sizeWrNew ||
                        sizeEdOld != sizeEdNew) {
                    FbProfileTable fbProfileTable1 = new FbProfileTable();
                    fbProfileTable1.setProfile(jsonString)
                            .setTimestamp(System.currentTimeMillis());
                    LoginDbHandler.saveDataInTable(LoginDbHandler.dataBaseTables.fbProfileTable, fbProfileTable1);
                    if(sizeWrNew > 0 || sizeEdNew > 0) {
                        return true;
                    }else{
                        return false;
                    }
                } else {
                    Collections.sort(oldUserModel.getWorkList(), new Comparator<Work>() {
                        @Override
                        public int compare(Work o1, Work o2) {
                            return o1.getFacebookId().compareToIgnoreCase(o2.getFacebookId());
                        }
                    });
                    Collections.sort(newUserProfile.getWorkList(), new Comparator<Work>() {
                        @Override
                        public int compare(Work o1, Work o2) {
                            return o1.getFacebookId().compareToIgnoreCase(o2.getFacebookId());
                        }
                    });

                    for (int index = 0; index < sizeWrOld; index++) {
                        if (!oldUserModel.getWorkList().get(index).getFacebookId().equalsIgnoreCase(newUserProfile.getWorkList().get(index).getFacebookId())) {
                            FbProfileTable fbProfileTable1 = new FbProfileTable();
                            fbProfileTable1.setProfile(jsonString)
                                    .setTimestamp(System.currentTimeMillis());
                            LoginDbHandler.saveDataInTable(LoginDbHandler.dataBaseTables.fbProfileTable, fbProfileTable1);
                            return true;
                        }
                    }

                    Collections.sort(oldUserModel.getEducationList(), new Comparator<Education>() {
                        @Override
                        public int compare(Education o1, Education o2) {
                            return o1.getFacebookId().compareToIgnoreCase(o2.getFacebookId());
                        }
                    });
                    Collections.sort(newUserProfile.getEducationList(), new Comparator<Education>() {
                        @Override
                        public int compare(Education o1, Education o2) {
                            return o1.getFacebookId().compareToIgnoreCase(o2.getFacebookId());
                        }
                    });

                    for (int index = 0; index < sizeEdOld; index++) {
                        if (!oldUserModel.getEducationList().get(index).getFacebookId().equalsIgnoreCase(newUserProfile.getEducationList().get(index).getFacebookId())) {
                            FbProfileTable fbProfileTable1 = new FbProfileTable();
                            fbProfileTable1.setProfile(jsonString)
                                    .setTimestamp(System.currentTimeMillis());
                            LoginDbHandler.saveDataInTable(LoginDbHandler.dataBaseTables.fbProfileTable, fbProfileTable1);
                            return true;
                        }
                    }
                    return false;
                }

            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }
        //return false;
    }

    public void checkForSignIn(final Activity activity) {
        final int[] i = {3};
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //if (currentUser == null) {

            callTokenFromServers(activity, i, mAuth);

        //}
    }

    private void callTokenFromServers(final Activity activity, final int[] i, final FirebaseAuth mAuth) {
        new FacebookGraphService().getCustomToken(activity, new TokenResponse() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                if (baseModel != null && reasonOfError == null) {
                    mAuth.signInWithCustomToken(((CustomToken) baseModel).getToken())
                            .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d("TAG", "signInWithCustomToken:success");
                                        LocalPreferenceManager.getInstance().initialise(activity);
                                        //LocalPreferenceManager.getInstance().setUserName(mAuth.getCurrentUser().getDisplayName());
                                     //   LocalPreferenceManager.getInstance().setUserImage(String.valueOf(mAuth.getCurrentUser().getPhotoUrl()));

                                        //FirebaseUser user = mAuth.getCurrentUser();

                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w("TAG", "signInWithCustomToken:failure", task.getException());

                                    }
                                }
                            });

                } else {
                    if (i[0] > 0) {
                        i[0]--;
                        callTokenFromServers(activity, i, mAuth);
                    }
                }
            }
        });
    }

    private String getChangedValues(UserProfile superBaseModel, UserProfile userOldDetails) {
        String s = "";
        if (!superBaseModel.getHometown().getId().equals(userOldDetails.getHometown().getId())) {
            s += userOldDetails.getHometown().getName() + " -> " + superBaseModel.getHometown().getName() + "\n";
        }
        if (!superBaseModel.getLocation().getId().equals(userOldDetails.getLocation().getId())) {
            s += userOldDetails.getLocation().getName() + " -> " + superBaseModel.getLocation().getName() + "\n";
        }
        boolean flag = false;
        for (int index = 0; index < superBaseModel.getWorkList().size(); index++) {
            Work newWork = superBaseModel.getWorkList().get(index);

            for (int indexJ = 0; indexJ < userOldDetails.getWorkList().size(); indexJ++) {
                Work oldWork = userOldDetails.getWorkList().get(indexJ);

                if (newWork.getIsCurrentlyWorkingHere() && oldWork.getIsCurrentlyWorkingHere()) {
                    if (!newWork.getFacebookId().equals(oldWork.getFacebookId())) {
                        s += newWork.getEmployer().getName() + " -> " + oldWork.getEmployer().getName() + "\n";
                        flag = true;
                        break;
                    }
                }
            }
            if (flag) {
                break;
            }
        }
        for (int index = 0; index < superBaseModel.getEducationList().size(); index++) {
            Education newWork = superBaseModel.getEducationList().get(index);

            for (int indexJ = 0; indexJ < userOldDetails.getEducationList().size(); indexJ++) {
                Education oldWork = userOldDetails.getEducationList().get(indexJ);

                if (newWork.isCurrentlyStudingHere() && oldWork.isCurrentlyStudingHere()) {
                    if (!newWork.getFacebookId().equals(oldWork.getFacebookId())) {
                        s += newWork.getInstitute().getName() + " -> " + oldWork.getInstitute().getName() + "\n";
                        flag = true;
                        break;
                    }
                }
            }
            if (flag) {
                break;
            }
        }


        return null;
    }

    public interface DialogButtonPressed {

        void okClick(String id);


    }
}
