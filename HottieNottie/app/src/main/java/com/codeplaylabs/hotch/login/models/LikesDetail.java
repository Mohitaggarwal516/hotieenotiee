package com.codeplaylabs.hotch.login.models;



import com.codeplaylabs.hotch.base.BaseModel;

import java.util.ArrayList;


/**
 * Created by mohit on 1/13/2017.
 */

public class LikesDetail extends BaseModel {

    private ArrayList<Detail> likesDetails;
    private String cursorBefore;
    private String cursorAfter;

    public void setCursorAfter(String cursorAfter) {
        this.cursorAfter = cursorAfter;
    }

    public void setCursorBefore(String cursorBefore) {
        this.cursorBefore = cursorBefore;
    }

    public String getCursorAfter() {
        return cursorAfter;
    }

    public String getCursorBefore() {
        return cursorBefore;
    }

    public ArrayList<Detail> getLikesDetails() {

        if(likesDetails == null){
            likesDetails = new ArrayList<>();
        }
        return likesDetails;
    }


}
