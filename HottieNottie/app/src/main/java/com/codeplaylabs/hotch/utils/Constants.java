package com.codeplaylabs.hotch.utils;

import com.codeplaylabs.hotch.BuildConfig;

/**
 * Created by mohit on 22/06/17.
 */

public class Constants {


    public static final String FB_ALBUMS_LIST = "FB_ALBUMS_LIST";
    public static final String FB_USER_INFO = "FB_USER_INFO";
    public static final String FB_USER_LIKES = "FB_USER_LIKES";
    public static final String SHARE_PUBLIC_PROFILE = "Hey, i Found a really hot profile on hotch app.Check this out!";
    public static final String SHARE_My_PROFILE = "Checkout my Hotch Profile.Vote and help me to be the next Hot Star.";//"Why don't you vote my profile and help me raising the temperatures.";
    public static final String FIRST_LOGIN = "isFirstLogin";
    public static final String PROFILE_SET = "profileSet";
    public static final String LAST_ACCESS_TOKEN_GENERATED = "lastAccessTokenGenerated";
    public static final String TYPE = "Few";
    public static final String SHARE_PROFILE = "shareProfile";
    public static final String DEVICE = "ANDROID";
    public static final java.lang.String ONE_TIME_VIEW = "onetimeview";
    public static final String ONE_TIME_VIEW_RATING = "onetimeviewrating";
    public static String MAIN_IMG_NAME = "main";
    public static final String JSON_STRING_NULL = "JsonStringNull";

    public interface Chat {

        String CHAT_NOTIFICATION = "chatNotification";
        String SENDER_USERID = "senderUserId";
        String CHAT_ID = "chatId";
        String OTHER_USER_NAME = "otherUserName";
        String OTHER_USER_IMAGE = "otherUserImage";
        String OTHER_USER_TIMESTAMP = "timeStamp";
        String OTHER_USER_IS_NEW = "isNewUser";
        String BODY = "body";
        String TITLE = "title";
        String PREFERENCE_CHATSET_KEY = "chats";
        String PREFERENCE_CHATHEADS_KEY = "chatHeads";
        String PREFERENCE_CHAT_COUNT = "chatCount";
        String LOCAL_BROADCAST = "localBroadcast";
        String LAST_NEW_CHAT_TIME = "lastSeenTIme";
        String CHAT_NOTIF_DISABLED_BY_ME = "selfChatNotificationDisabled";
        String CHAT_NOTIF_DISABLED_BY_PARTNER = "partnerChatNotificationDisabled";
        String LOCAL_BROADCAST_NEW_MESSAGE = "localBroadcastNewMessage";
        String TEXT = "text";
        String PREF_NEW_MESSAGE = "isNewChatMessages";
        String SOUND = "sound";
    }

    public enum ReceivedFrom {
        CACHE,
        SERVICE,
        CONNECTION_FAILURE_CACHE
    }

    public interface ActivityName {
        String LOGIN_SCREEN = "LoginScreen";
        String HOME_SCREEN = "HomeScreen";
        String OTHERS_SCREEN = "OthersScreen";
        String RATINGS_SCREEN = "RatingsScreen";
        String CHAT_SCREEN = "ChatScreen";
        String CHAT_MSGS_SCREEN = "OnGoingChatScreen";
        String SETTINGS_SCREEN = "SettingsScreen";
    }

    public interface Buttons {
        String SUPERHOT_BTN = "SuperHotButton";
        String SYNC_TO_FB_BTN = "SyncToFbButton";
        String CHAT_NOW_PROFILE_DETAIL = "ProfileDetail_ChatNow";
        String RATINGS_MALE_CLICKED = "Ratings_MaleClicked";
        String RATINGS_FEMALE_CLICKED = "Ratings_FemaleClicked";
        String SHARE_PUBLIC_PROFILE = "SharePublicProfile";
        String SHARE_PERSONAL_PROFILE = "SharePersonalProfile";
        String SHARE_APP = "ShareApp";
    }

    public enum Modules {
        PERSONAL_PROFILE,
        IMAGE_MODULE,
        LOGIN_MODULE,
        LIKES_MODULE, INTEREST_MODULE,
        CHAT_MODULE, RATINGS, DESCRIPTION, SUPERHOT_LIST_MOD, FEW_INTEREST
    }

    public interface Error {
        String NETWORK_ERROR = "NETWORK_ERROR";
        String JSON_EXCEPTION = "JSON_EXCEPTION";
        String NULL_RESPONSE_ERROR = "NULL_RESPONSE_ERROR";

        interface LoginError {
            String LOGIN_SERVER_RESPONSE_ERROR = "LOGIN_SERVER_RESPONSE_ERROR";
            String LOGIN_FACEBOOK_RESPONSE_ERROR = "LOGIN_FACEBOOK_RESPONSE_ERROR";
            String USER_BLOCKED = "USER_BLOCKED";
            String DATA_UPDATED = "Data Updated";
        }

        interface SettingsError {
            String PROFILE_SETTING_ERROR = "PROFILE_SETTING_ERROR";
            String PROFILE_NOT_AVAILABLE_ERROR = "PROFILE_NOT_AVAILABLE_ERROR";
        }

        interface GalleryError {
            String FACEBOOK_GALLERY_NULL = "FACEBOOK_GALLERY_NULL";
        }
    }

    public static class ServiceConfiguration {
        public static final int CHAT_UPDATE = 15 * 60;
        public static final int PUBLIC_PROFILE = 2 * 60 * 60;
        public static final int CHAT_MESSAGES_COUNT_AT_A_TIME = 25;
        public static String DEFAULT_ACTION_ON_SUPERHOT = "SuperHot";
        public static String APP_SERVICE_OUTAGE_MESSAGE = "";
        public static boolean APP_IS_SERVICE_OUTAGE = false;
        public static String APP_DOWNLOAD_URL = "";
        public static String APP_UPDATE_MESSAGE = "New Update Available!";
        public static int APP_CURRENT_VERSION;//= BuildConfig.VERSION_NAME;
        // All settings are done on seconds bases
        public static int GET_PROFILE_BY_ID = 6 * 60 * 60;
        public static int FB_GRAPH_IMAGES = 24 * 60 * 60;
        public static int LOGIN_DATA = 24 * 60 * 60;
        public static int FB_USER_INFO = 24 * 60 * 60;
        public static int UPDATE_INTERESTS = 96 * 60 * 60;
        public static int UPDATE_DISCOVERY = 30 * 60;
        public static int SYNC_NUMBER_OF_PROFILES = 15;
        public static long CLEAR_DATABASE_AFTER_HOURS = 5 * 24 * 60 * 60;
        public static long TIME_DELAY_IN_MATCH_VIEW = 650;// milli sec
        public static long SUPERHOT_AVAILABLE = 3 * 60 * 60;
        public static int RATINGS_TIME_FACTOR = 6 * 60;
        public static int DESCRIPTION_TIME_FACTOR = 6 * 60;
        public static int SUPERHOT_TIME_FACTOR = 1 * 60;

        public static boolean MANDATORY_UPDATE = false;
    }


}
