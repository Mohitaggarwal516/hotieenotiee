package com.codeplaylabs.hotch.chats.ui;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.Fragment;

import android.support.v4.view.ViewPager;
import android.os.Bundle;

import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseActivity;
import com.codeplaylabs.hotch.chats.adapters.SectionsPagerAdapter;
import com.codeplaylabs.hotch.chats.fragments.NewChatListFragment;
import com.codeplaylabs.hotch.chats.fragments.OldChatListFragment;
import com.codeplaylabs.hotch.home.ui.HomeActivity;
import com.codeplaylabs.hotch.settings.tools.interfaces.SettingsToggleClickListener;
import com.codeplaylabs.hotch.settings.tools.ui.SettingsToggleFunction;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.utils.LogAnalytics;
import com.codeplaylabs.hotch.utils.ServiceSingleton;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;

public class ChatFragmentActivity extends BaseActivity implements ServiceSingleton.ServiceCallBacks {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerListener} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static final int FIRST_PAGE = 0;
    public static final int OLD_CHAT = 12;
    public static final int NEW_CHAT = 13;
    private int calledFrom;

    private static final String RIGHT_TAB_NAME = "MATCHES";
    private static final String LEFT_TAB_NAME = "CHATS";

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private ArrayList<Fragment> fragments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //setting drawable to status bar
            Window window = getWindow();
            Drawable background = getResources().getDrawable(R.drawable.status_bar_gradient);
            /*specifying flags programmtically since this Activity is'nt takin
            the ones specified as a part of theme in style*/
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(getResources().getColor(R.color.transparent));
            }
            window.setBackgroundDrawable(background);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setNavigationBarColor(getResources().getColor(R.color.black));
            }
        }
        setContentView(R.layout.activity_chat_fragment);
        super.onCreate(savedInstanceState);
        ServiceSingleton.getInstance().setActivity(this);

//=========================
        calledFrom = getIntent().getIntExtra("calledFrom", -1);

        LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHATHEADS_KEY, new LinkedHashSet<String>());
        LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHATSET_KEY, new HashSet<String>());
        LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHAT_COUNT, 0);

//=============================

        callToggleSwitch(SettingsToggleFunction.PAGE_ZERO);

        fragments.add(OldChatListFragment.newInstance());
        fragments.add(NewChatListFragment.newInstance());

        setAdapter(fragments);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREF_NEW_MESSAGE,false);

    }

    private void setAdapter(ArrayList<Fragment> fragments) {
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), fragments);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        if (calledFrom == NEW_CHAT) {
            ServiceSingleton.getInstance().setSecModel(null);

        }
        mViewPager.setAdapter(mSectionsPagerAdapter);
        if (calledFrom == NEW_CHAT) {

            mViewPager.setCurrentItem(1);
            callToggleSwitch(SettingsToggleFunction.PAGE_ONE);

        } else {
            mViewPager.setCurrentItem(0);
            callToggleSwitch(SettingsToggleFunction.PAGE_ZERO);
        }

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                callToggleSwitch(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    private void callToggleSwitch(final int position) {
        SettingsToggleFunction.toggleSwitch(this, position, LEFT_TAB_NAME, RIGHT_TAB_NAME, new SettingsToggleClickListener() {
            @Override
            public void toggleClickedLeft(TextView left, TextView right/*int position*/) {
                mViewPager.setCurrentItem(SettingsToggleFunction.PAGE_ZERO, true);

            }

            @Override
            public void toggleClickedRight(TextView left, TextView right) {
                mViewPager.setCurrentItem(SettingsToggleFunction.PAGE_ONE, true);

            }
        });
    }


    @Override
    public void onServiceComplete(Object object) {
        try {
            if (object instanceof Integer) {

                ((OldChatListFragment) fragments.get(0)).callListListener(0);

            } else if (object instanceof String) {

                ((NewChatListFragment) fragments.get(1)).callListListener(0);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface FragmentPagerListener {
        void onItemClick(int position);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (calledFrom == OLD_CHAT || calledFrom == NEW_CHAT) {
            Intent intent = new Intent(ChatFragmentActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

    }

    @Override
    protected void updateChatList() {
        super.updateChatList();

        ((OldChatListFragment) fragments.get(0)).setValueForAdapter();
        ((NewChatListFragment) fragments.get(1)).setValueForAdapter();
    }

    @Override
    protected void onResume() {
        LogAnalytics.logScreenView(mTracker,Constants.ActivityName.CHAT_SCREEN);
        super.onResume();
    }
}
