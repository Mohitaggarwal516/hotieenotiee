package com.codeplaylabs.hotch.login.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.codeplaylabs.hotch.base.BaseActivity;
import com.codeplaylabs.hotch.R;

/**
 * Created by sachingupta on 18/06/16.
 */

public class WebViewActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_webview);


        WebView lWebView = (WebView)findViewById(R.id.webview);

//        lWebView.loadUrl("file:///android_asset/about.html");
        String url = getIntent().getStringExtra("url");
        Intent intent = getIntent();
        final Boolean fromWhatsWeb = intent.getBooleanExtra("FromWhatsWeb",false);
        if(isOnline()) {
                lWebView.loadUrl(url);
            //findViewById(R.id.story_error_text).setVisibility(View.GONE);
        }else{
            //findViewById(R.id.story_error_text).setVisibility(View.VISIBLE);
        }

        lWebView.setKeepScreenOn(true);
        lWebView.getSettings().setJavaScriptEnabled(true);
        lWebView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                findViewById(R.id.loader_layout).setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                findViewById(R.id.loader_layout).setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                findViewById(R.id.loader_layout).setVisibility(View.GONE);

            }
        });
    }


}
