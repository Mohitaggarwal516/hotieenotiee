package com.codeplaylabs.hotch.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import com.codeplaylabs.hotch.BuildConfig;
import com.codeplaylabs.hotch.MyApplication;
import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseActivity;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.home.services.HomeServices;
import com.codeplaylabs.hotch.home.ui.HomeActivity;
import com.codeplaylabs.hotch.login.dto.FacebookDTO;
import com.codeplaylabs.hotch.login.interfaces.TryAgainForAppConfig;
import com.codeplaylabs.hotch.login.services.FacebookGraphService;
import com.codeplaylabs.hotch.login.ui.LoginActivity;
import com.codeplaylabs.hotch.settings.interfaces.ProfileResponseListener;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by mohit on 24/08/17.
 */

public class LifeCycleCallbackClass implements Application.ActivityLifecycleCallbacks {
    int activityCount = 0;
    private boolean configCalled = false;

    @Override
    public void onActivityCreated(final Activity activity, Bundle savedInstanceState) {

        if (activityCount == 0) {
            try {

                MyApplication myAppInstance= (MyApplication) activity.getApplication();
                Tracker mTracker = myAppInstance.getTracker();
                try {


                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("View")
                            .setAction("App View")
                            //.setLabel(Singleton.getInstance(activity).getEmail())
                            .build());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                LocalPreferenceManager.getInstance().initialise(activity);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        activityCount++;

        if((activity instanceof HomeActivity || activity instanceof LoginActivity) && !configCalled ){
            configCalled = true;
            ((BaseActivity)activity).callAppConfigService();

//            if (!ServiceSingleton.getInstance().isChatWithMsgServiceRunning()) {
//                ServiceSingleton.getInstance().setChatWithMsgServiceRunning(true);
//                new ChatServices().getChatWithMessages(activity, 0, new ChatListResponse() {
//                    @Override
//                    public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
//                        if (baseModel != null && jsonString == null) {
//                            ServiceSingleton.getInstance().setModel(baseModel);
//                        }
//                        ServiceSingleton.getInstance().setChatWithMsgServiceRunning(false);
//                        if (ServiceSingleton.getInstance().getActivity() != null) {
//                            ((ServiceSingleton.ServiceCallBacks) ServiceSingleton.getInstance().getActivity()).onServiceComplete(new Integer(1));
//                        }
//                    }
//                });
//
//                ServiceSingleton.getInstance().setChatWithoutMsgServiceRunning(true);
//                new ChatServices().getChatWithoutMessages(activity, 0, new ChatListResponse() {
//                    @Override
//                    public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
//                        if (baseModel != null && jsonString == null) {
//                            ServiceSingleton.getInstance().setSecModel(baseModel);
//                        }
//                        ServiceSingleton.getInstance().setChatWithoutMsgServiceRunning(false);
//                        if (ServiceSingleton.getInstance().getActivity() != null) {
//                            ((ServiceSingleton.ServiceCallBacks) ServiceSingleton.getInstance().getActivity()).onServiceComplete(new String());
//                        }
//                    }
//                });
//            }
        }
    }


    @Override
    public void onActivityStarted(Activity activity) {
        if (activityCount == 1 || activityCount == 2) {
            Log.e("ACTIVITY_COUNTIN", activityCount + "");
            com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager.getInstance().initialise(activity);
            if (LocalPreferenceManager.getInstance().getUserId() != null) {
                new HomeServices().getPreviousSuperhotTime(activity, null);
            }


        } else {
            Log.e("ACTIVITY_COUNTIN!", activityCount + "");
        }
        activityCount++;

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
        activityCount--;
        if (activityCount == 0) {
            Log.e("ACTIVITY_COUNTOUT", activityCount + "");
        } else {
            Log.e("ACTIVITY_COUNTOUT1", activityCount + "");
        }

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        activityCount--;
        if(activityCount == 1 || activityCount == 0){
            configCalled = false;
        }
    }


}
