package com.codeplaylabs.hotch.settings.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codeplaylabs.hotch.settings.interfaces.GalleryListListener;
import com.codeplaylabs.hotch.R;

import java.util.ArrayList;

/**
 * Created by mohit on 30/06/17.
 */

public class CustomGalleryListAdapter extends RecyclerView.Adapter<CustomGalleryListAdapter.MyViewHolder>{
    private final GalleryListListener galleryListListener;
    private final ArrayList<String> arrayList;
    private final Activity activity;

    public CustomGalleryListAdapter(Activity activity, ArrayList<String> arrayList, GalleryListListener galleryListListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.galleryListListener = galleryListListener;
    }

    @Override
    public CustomGalleryListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_list_row, parent, false);

        return new CustomGalleryListAdapter.MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(CustomGalleryListAdapter.MyViewHolder holder, final int position) {

        final String stringItem = arrayList.get(position);
        holder.listItem.setText(stringItem);
        holder.listItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryListListener.onItemClick(position,stringItem, null);
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private final TextView listItem;

        public MyViewHolder(View itemView) {
            super(itemView);

            listItem = (TextView)itemView.findViewById(R.id.list_item_row);


        }


    }
}
