package com.codeplaylabs.hotch.settings.interfaces;

/**
 * Created by mohit on 30/06/17.
 */

public interface SelectedImageListener {
    void selectedImage(String base64,String imageUri, String timeStamp, String from);

}
