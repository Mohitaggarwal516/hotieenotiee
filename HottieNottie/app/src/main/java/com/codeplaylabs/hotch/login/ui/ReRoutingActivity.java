package com.codeplaylabs.hotch.login.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseActivity;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.home.handler.HomeDBHandlerClass;
import com.codeplaylabs.hotch.home.ui.HomeActivity;
import com.codeplaylabs.hotch.login.interfaces.SharedProfileResponse;
import com.codeplaylabs.hotch.login.services.FacebookGraphService;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.facebook.AccessToken;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

public class ReRoutingActivity extends BaseActivity {

    private RelativeLayout loaderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_re_routing);

        LocalPreferenceManager.getInstance().initialise(this);
        loaderLayout = (RelativeLayout) findViewById(R.id.loader_layout);
        addForBranchIo();
    }

    void addForBranchIo() {
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        updateUI(currentUser);

        //for branch io
        Branch branch = Branch.getInstance();

        loaderLayout.setVisibility(View.VISIBLE);
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {

                loaderLayout.setVisibility(View.GONE);
                if (error == null) {
                    // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
                    // params will be empty if no data found
                    // ... insert custom logic here ...

                    try {
                        String targetUserId = referringParams.getString("targetUserId");

                        //String userName=referringParams.getString("userName");
                        //String userId=referringParams.getString("userId");
                        //fire intent for profile detail

                        AccessToken accessToken = AccessToken.getCurrentAccessToken();
                        if ((accessToken != null && accessToken.getDeclinedPermissions().size() == 0) && ((Constants.ServiceConfiguration.LOGIN_DATA * 1000) + LocalPreferenceManager.getInstance().getLongPreference(Constants.LAST_ACCESS_TOKEN_GENERATED, 0)) > System.currentTimeMillis()) {

                            if (targetUserId.equals(LocalPreferenceManager.getInstance().getUserId())) {
                                Toast.makeText(ReRoutingActivity.this, "Your shared profile can be viewed by others only.", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(ReRoutingActivity.this, HomeActivity.class));
                                finish();
                                return;
                            } /*else if (targetUserId == null || targetUserId.equals("")) {
                                Toast.makeText(ReRoutingActivity.this, "Could not open the shared profile.", Toast.LENGTH_SHORT).show();
                                finish();
                                return;
                            }*/
                            Log.e("ReRouting", "Control outside if ");
                            HomeDBHandlerClass.clearTable(HomeDBHandlerClass.dataBaseTables.profileTable);
                            loaderLayout.setVisibility(View.VISIBLE);
                            new FacebookGraphService().profileDescriptionService(ReRoutingActivity.this, targetUserId, Constants.SHARE_PROFILE, new SharedProfileResponse() {
                                @Override
                                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                                    loaderLayout.setVisibility(View.GONE);
                                    Intent intent = new Intent(ReRoutingActivity.this, HomeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                        } else {
                            Toast.makeText(ReRoutingActivity.this, "Could not fetch your shared profile, please try again.", Toast.LENGTH_LONG);
                            Intent intent = new Intent(ReRoutingActivity.this, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.i("ProfileDetail: ", error.getMessage());
                    Toast.makeText(ReRoutingActivity.this, "Could not fetch your shared profile, please try again.", Toast.LENGTH_LONG);
                    startActivity(new Intent(ReRoutingActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }, this.getIntent().getData(), this);

    }

    private void updateUI(FirebaseUser user) {
        //hideProgressDialog();

    }

}
