package com.codeplaylabs.hotch.home.parsers;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.database.tables.ProfileTable;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.database.tables.FilterTable;
import com.codeplaylabs.hotch.home.handler.HomeDBHandlerClass;
import com.codeplaylabs.hotch.utils.CustomJsonObject;
import com.codeplaylabs.hotch.utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mohit on 27/06/17.
 */

public class ParseProfiles extends BaseParser {
    BaseModel baseModel = new BaseModel();

    @Override
    public BaseModel parseJson(JSONObject response) {
        if (validate(response) != null) {
            return moduleJson(response);
        } else {
            baseModel.error = Constants.Error.SettingsError.PROFILE_SETTING_ERROR;
            return baseModel;
        }
    }

    /*
      * This method is called to collect the data received from the servers in a class for the session
      * @param response
     */
    public BaseModel moduleJson(JSONObject response) {

        //UserProfile userProfile = new UserProfile();

        if (response != null) {
            CustomJsonObject cjObject = new CustomJsonObject(response);
            cjObject = cjObject.getCustomJsonObject("data");
            JSONArray users = cjObject.getJsonArray("filteredList");
            if (users.length() > 0) {
                Singleton.getInstance().filterRecieved = true;
                for (int index = 0 ; index < users.length() ; index++ ){
                    ProfileTable profileTable = new ProfileTable();
                    try {
                        FilterTable filter = HomeDBHandlerClass.getFilterFromTable();
                        profileTable.setProfile(users.getJSONObject(index).toString())
                                .setType(ProfileTable.Type.NOT_SEEN.toString())
                                .setSynced(false)
                                .setTimestamp(System.currentTimeMillis())
                                .setProfileTaken(String.valueOf(false))
                                .setId(users.getJSONObject(index).getString("id"))
                        .setFilter(filter.getFilterKey());
                        HomeDBHandlerClass.saveDataInTable(HomeDBHandlerClass.dataBaseTables.profileTable,profileTable);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                return new ProfileModel();
            } else {
                Singleton.getInstance().filterRecieved = false;
                baseModel.error = Constants.Error.SettingsError.PROFILE_NOT_AVAILABLE_ERROR;
                return baseModel;
            }

        }
        baseModel.error = Constants.Error.NULL_RESPONSE_ERROR;
        return baseModel;

    }
}
