package com.codeplaylabs.hotch.home.models;

import com.codeplaylabs.hotch.base.BaseModel;

import java.util.ArrayList;

/**
 * Created by mohit on 25/07/17.
 */

public class FilterModel extends BaseModel {

    int minAge = 18;
    int maxAge = 55;
    String gender = "female";
    String currentlyAtFbId;
    String currentlyAtCountry;
    ArrayList<String>  currentlyAtLocations = new ArrayList<>();
    boolean isRandom = true;

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCurrentlyAtFbId() {
        if(currentlyAtFbId == null){
            currentlyAtFbId = "";
        }
        return currentlyAtFbId;
    }

    public void setCurrentlyAtFbId(String currentlyAtFbId) {
        this.currentlyAtFbId = currentlyAtFbId;
    }

    public String getCurrentlyAtCountry() {
        if(currentlyAtCountry == null){
            currentlyAtCountry  = "";
        }
        return currentlyAtCountry;
    }

    public void setCurrentlyAtCountry(String currentlyAtCountry) {
        this.currentlyAtCountry = currentlyAtCountry;
    }

    public ArrayList<String> getCurrentlyAtLocations() {
        return currentlyAtLocations;
    }

    public boolean isRandom() {
        return isRandom;
    }

    public void setRandom(boolean random) {
        isRandom = random;
    }
}
