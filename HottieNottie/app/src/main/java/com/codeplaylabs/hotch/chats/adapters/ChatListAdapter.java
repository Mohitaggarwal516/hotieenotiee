package com.codeplaylabs.hotch.chats.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.chats.fragments.NewChatListFragment;
import com.codeplaylabs.hotch.chats.fragments.OldChatListFragment;
import com.codeplaylabs.hotch.chats.models.firbaseModels.UserModel;
import com.codeplaylabs.hotch.utils.CommonMethods;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;

/**
 * Created by mohit on 19/08/17.
 */

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> {
    private final Activity activity;
    private final List<UserModel> users;
    private OldChatListFragment.ChatListListener chatListListener;
    private NewChatListFragment.ChatListListener newChatListListener;

    int HEADER_FLAG = 123;

    public enum chatListEnum {OLD, NEW}

    chatListEnum mEnum;

    public ChatListAdapter(Activity activity, List<UserModel> users, chatListEnum mEnum, OldChatListFragment.ChatListListener chatListListener) {
        this.activity = activity;
        this.users = users;
        this.chatListListener = chatListListener;
        this.mEnum = mEnum;
    }

    public ChatListAdapter(Activity activity, List<UserModel> users, chatListEnum mEnum, NewChatListFragment.ChatListListener chatListListener) {
        this.activity = activity;
        this.users = users;
        this.newChatListListener = chatListListener;
        this.mEnum = mEnum;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_card, parent, false);
        //}
        return new ChatListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final UserModel userModel = users.get(position);

        if(userModel.selfMuteStatus()){
            holder.muteIcon.setVisibility(View.VISIBLE);
        }else{
            holder.muteIcon.setVisibility(View.GONE);
        }

        if (userModel.getImage() != null && userModel.getImage().length() > 0) {
            Picasso.with(activity).load(userModel.getImage()).placeholder(userModel.getGender()).into(holder.imageView);
        }else{
            Picasso.with(activity).load(userModel.getGender()).into(holder.imageView);
        }
        holder.name.setText(userModel.getName());
      /* holder.message.setText(userModel.getLastMessage().getText());
       holder.time.setText(""+userModel.getTimestamp());*/
        if (mEnum == chatListEnum.OLD && userModel.getLastMessage() != null) {
            holder.message.setVisibility(View.VISIBLE);
            holder.time.setVisibility(View.VISIBLE);

            holder.message.setText(userModel.getLastMessage().getText());
            Date date = new Date(userModel.getLastMessage().getTimeStamp());

//            String hours = "";
//            if(date.getHours() < 10){
//                hours = "0" + date.getHours();
//            }else{
//                hours =""+ date.getHours();
//            }
//            String mins = "";
//            if(date.getMinutes() < 10){
//                mins = "0" + date.getMinutes();
//            }else{
//                mins =""+ date.getMinutes();
//            }
            holder.time.setText(CommonMethods.getChatDateAndTime(userModel.getLastMessage().getTimeStamp())/*hours +" : "+ mins*/);

        } else {
            holder.message.setVisibility(View.GONE);
            holder.time.setVisibility(View.GONE);

        }
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEnum == chatListEnum.OLD) {
                    chatListListener.onItemClicked(position, userModel);
                } else {
                    newChatListListener.onItemClicked(position, userModel);
                }
            }
        });

        if (userModel.getIsNew() != null && !userModel.getIsNew().equals("false")) {
            holder.notificationDot.setVisibility(View.GONE);
        }else{
            holder.notificationDot.setVisibility(View.GONE);
        }

        //==================================================


    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imageView;
        private final TextView name;
        private final TextView message;
        private final TextView time;
        private final LinearLayout card;
        private final LinearLayout horizontalLay;
        private final RecyclerView horizontalRecyclerView;
        private final ImageView muteIcon;
        ImageView notificationDot;

        public MyViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.chat_user_image);
            name = (TextView) itemView.findViewById(R.id.name);
            message = (TextView) itemView.findViewById(R.id.last_message);
            time = (TextView) itemView.findViewById(R.id.last_message_time);
            card = (LinearLayout) itemView.findViewById(R.id.view_card);
            notificationDot = (ImageView) itemView.findViewById(R.id.notifiction_ic);

            horizontalLay = (LinearLayout) itemView.findViewById(R.id.horizontal_list_lay);
            horizontalRecyclerView = (RecyclerView) itemView.findViewById(R.id.horizontal_recycler_chat);
            muteIcon = (ImageView) itemView.findViewById(R.id.mute_ic);


        }
    }


}
