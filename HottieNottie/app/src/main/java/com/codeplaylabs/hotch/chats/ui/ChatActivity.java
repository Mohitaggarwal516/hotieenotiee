/**
 * Copyright Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.codeplaylabs.hotch.chats.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.AlignmentSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseActivity;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.chats.adapters.ChatAdapter;
import com.codeplaylabs.hotch.chats.handler.ChatDBHandlerClass;
import com.codeplaylabs.hotch.chats.interfaces.UnMatchResponse;
import com.codeplaylabs.hotch.chats.models.firbaseModels.FriendlyMessage;
import com.codeplaylabs.hotch.chats.models.firbaseModels.UserModel;
import com.codeplaylabs.hotch.chats.services.ChatServices;
import com.codeplaylabs.hotch.customUi.CircleImageView;
import com.codeplaylabs.hotch.database.tables.ChatUsersList;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.services.FacebookGraphService;
import com.codeplaylabs.hotch.profileDetail.interfaces.ReportResponse;
import com.codeplaylabs.hotch.profileDetail.ui.ProfileDetailActivity;
import com.codeplaylabs.hotch.utils.CommonMethods;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.utils.LogAnalytics;
import com.codeplaylabs.hotch.utils.ServiceSingleton;
import com.codeplaylabs.hotch.utils.Singleton;
import com.codeplaylabs.hotch.utils.TextViewPlus;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Timer;
import java.util.TimerTask;

import github.ankushsachdeva.emojicon.EmojiconEditText;
import github.ankushsachdeva.emojicon.EmojiconGridView;
import github.ankushsachdeva.emojicon.EmojiconsPopup;

public class ChatActivity extends BaseActivity
        implements GoogleApiClient.OnConnectionFailedListener {

    private static final int UNMATCH = 1432;
    private static final String LAST_MESSAGE = "LastMessageDetail";
    private static final int OTHER_UNMATCH = 3456;
    private String TEST_NAME = "Test Name";
    private int MY_LOCATION = 0;


    public static final String FRIENDLY_MSG_LENGTH = "friendly_msg_length";

    private static final String TAG = "MainActivity";
    //public static final String MESSAGES_CHILD = "messages";
    private static final int REQUEST_INVITE = 1;
    private static final int REQUEST_IMAGE = 2;
    private static final String LOADING_IMAGE_URL = "https://www.google.com/images/spin-32.gif";
    public static final int DEFAULT_MSG_LENGTH_LIMIT = 100;
    public static final String ANONYMOUS = "anonymous";
    private static final String MESSAGE_SENT_EVENT = "message_sent";
    private String mUsername;
    private String mPhotoUrl;
    private SharedPreferences mSharedPreferences;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    private TextView mSendButton;
    private RecyclerView mMessageRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ProgressBar mProgressBar;
    private EmojiconEditText mMessageEditText;
    private ImageView mAddMessageImageView;

    private DatabaseReference mFirebaseDatabaseReference;
    private ChatAdapter mFirebaseAdapter;
    private Button mBlockBtn;
    boolean blocked = false;
    private RelativeLayout rootView;
    private CircleImageView chatUserImage;
    private TextView activityTitle;
    private ImageView menuBtn;
    private String userId;
    final ArrayList<String> array = new ArrayList<>();
    private UserModel otherUser;
    private FriendlyMessage friendlyMessage = new FriendlyMessage();
    private ChildEventListener childEventListener = null;
    private boolean unMatched = false;


    private EditText reasonEd;
    private LinearLayout dgLay;
    //private int displayHeight;
    private RelativeLayout reportDgLay;
    private RelativeLayout layOutsideAlert;
    private int count = 0;
    private ChatUsersList chatUsersList;
    private String lastMessage = "";
    private boolean notificationSent = true;
    private Query var;
    private ArrayList<FriendlyMessage> messages = new ArrayList<>();
    private String[] notificationSentFor = {""};

    // Firebase instance variables

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        // Set default username is anonymous.
        mUsername = ANONYMOUS;

        reportDgLay = (RelativeLayout) findViewById(R.id.root_alert);
        otherUser = (UserModel) getIntent().getSerializableExtra("otherUser");
        if (!Singleton.getInstance().isNetworkAvailable()) {

            Toast.makeText(ChatActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        chatUsersList = ChatDBHandlerClass.getChatFromTable(otherUser.getChatId());
        if (chatUsersList == null) {
            finish();
            return;

        }
        chatUsersList.setIsNew("false");
        LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHATHEADS_KEY, new LinkedHashSet<String>());
        LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHATSET_KEY, new HashSet<String>());
        LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHAT_COUNT, 0);
        // Log.e("TOKEN123",FirebaseInstanceId.getInstance().getToken());
        chatUserImage = (CircleImageView) findViewById(R.id.view_profile);
        activityTitle = (TextView) findViewById(R.id.chat_user_title);
        menuBtn = (ImageView) findViewById(R.id.home_title_menu_btn);
        menuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu menu = new PopupMenu(ChatActivity.this, v);
                menu.inflate(R.menu.chat_menu);
                menu.show();
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getItemId() == R.id.chat_report_menu) {
                                /*open Alert
                                and call report service on click of yes
                                 */
                            customAlertDialog(ChatActivity.this, "Report " + otherUser.getName()/*+ profileModel.getName()*/ + " ?", "Do you really want to report this profile.", "Yes", "No");
                        } else if (item.getItemId() == R.id.chat_unmatch_menu) {

                            showCustomDialog("Un-Match " + otherUser.getName() /*+ profileModel.getName()*/ + " ?", "Do you really want to Un-Match this profile.", UNMATCH, "Yes", "No");


                        } else if (item.getItemId() == R.id.chat_mute_menu) {

                            if (chatUsersList.getIsNotificationDisabledBySelf()) {
                                chatUsersList.setIsNotificationDisabledBySelf(false);
                                menu.getMenu().getItem(2).setTitle(R.string.mute_notification);
                            } else {
                                chatUsersList.setIsNotificationDisabledBySelf(true);
                                menu.getMenu().getItem(2).setTitle(R.string.unmute_notification);
                            }
                            ChatDBHandlerClass.saveDataInTable(ChatDBHandlerClass.dataBaseTables.chatListTable, chatUsersList);
                            new ChatServices().muteUnmute(ChatActivity.this, otherUser.getChatId(), chatUsersList.getIsNotificationDisabledBySelf(), null);
                            chatUsersList = ChatDBHandlerClass.getChatFromTable(otherUser.getChatId());
                        }

                        return false;
                    }
                });

                if (chatUsersList.getIsNotificationDisabledBySelf()) {
                    menu.getMenu().getItem(2).setTitle(R.string.unmute_notification);
                } else {
                    menu.getMenu().getItem(2).setTitle(R.string.mute_notification);
                }

            }
        });


        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            //startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        } else {
            mUsername = mFirebaseUser.getDisplayName();
            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
            }
            if (mUsername == null) {
                mUsername = getResources().getString(R.string.app_name) + " User";
            }
            mPhotoUrl = otherUser.getImage();
            activityTitle.setText(otherUser.getName());
            int placeHolder = R.drawable.female_placeholder;
            //if(otherUser.getName())
            if (otherUser.getImage() == null || otherUser.getImage().length() == 0) {
                Picasso.with(this).load(otherUser.getGender()).into(chatUserImage);
            } else {
                Picasso.with(this).load(otherUser.getImage()).placeholder(otherUser.getGender()).into(chatUserImage);
            }
            chatUserImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    otherUser.getUserId();
                    ProfileModel model = new ProfileModel();
                    model.setId(array.get(1 - MY_LOCATION));
                    Intent intent = new Intent(ChatActivity.this, ProfileDetailActivity.class);
                    intent.putExtra("profile", model);
                    intent.putExtra("from", "" + ProfileDetailActivity.From.CHAT);
                    intent.putExtra("CallDescription", true);
                    startActivity(intent);
                }
            });
        }



        rootView = (RelativeLayout) findViewById(R.id.rootLayout);
        final EmojiconsPopup popup = new EmojiconsPopup(rootView, this);

        //Will automatically set size according to the soft keyboard size
        popup.setSizeForSoftKeyboard();

        //If the emoji popup is dismissed, change emojiButton to smiley icon
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                changeEmojiKeyboardIcon(mAddMessageImageView, R.drawable.smiley);
            }
        });


        //If the text keyboard closes, also dismiss the emoji popup
        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });


        //On emoji clicked, add it to edittext
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(github.ankushsachdeva.emojicon.emoji.Emojicon emojicon) {
                if (mMessageEditText == null || emojicon == null) {
                    return;
                }

                int start = mMessageEditText.getSelectionStart();
                int end = mMessageEditText.getSelectionEnd();
                if (start < 0) {
                    mMessageEditText.append(emojicon.getEmoji());
                } else {
                    mMessageEditText.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());
                }
            }
        });

        //On backspace clicked, emulate the KEYCODE_DEL key event
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                mMessageEditText.dispatchKeyEvent(event);
            }
        });

        mAddMessageImageView = (ImageView) findViewById(R.id.addMessageImageView);

        // To toggle between text keyboard and emoji keyboard keyboard(Popup)
        mAddMessageImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //If popup is not showing => emoji keyboard is not visible, we need to show it
                if (!popup.isShowing()) {

                    //If keyboard is visible, simply show the emoji popup
                    if (popup.isKeyBoardOpen()) {
                        popup.showAtBottom();
                        changeEmojiKeyboardIcon(mAddMessageImageView, R.drawable.ic_action_keyboard);
                    }

                    //else, open the text keyboard first and immediately after that show the emoji popup
                    else {
                        mMessageEditText.setFocusableInTouchMode(true);
                        mMessageEditText.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(mMessageEditText, InputMethodManager.SHOW_IMPLICIT);
                        changeEmojiKeyboardIcon(mAddMessageImageView, R.drawable.ic_action_keyboard);
                    }
                }

                //If popup is showing, simply dismiss it to show the undelying text keyboard
                else {
                    popup.dismiss();
                }
            }
        });

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mMessageRecyclerView = (RecyclerView) findViewById(R.id.messageRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true);
        mLinearLayoutManager.setStackFromEnd(true);
        mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);
        mSendButton = (TextView) findViewById(R.id.sendButton);

        userId = LocalPreferenceManager.getInstance().getUserId();
        //array.add(LocalPreferenceManager.getInstance().getUserId());
        String[] val = otherUser.getChatId().split("_");

        array.add(val[0]);
        array.add(val[1]);
        //mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference(CommonMethods.getUniqueKey(array));

        MY_LOCATION = array.indexOf(userId);

        if (MY_LOCATION == -1) {
            if (ServiceSingleton.getInstance().getSelfProfile() != null && ServiceSingleton.getInstance().getSelfProfile().getId() != null) {
                MY_LOCATION = array.indexOf(ServiceSingleton.getInstance().getSelfProfile().getId());

                if (MY_LOCATION == -1) {
                    Spannable centeredText = new SpannableString(getResources().getString(R.string.no_internet));
                    centeredText.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
                            0, getResources().getString(R.string.no_internet).length() - 1,
                            Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                    Toast.makeText(this, centeredText, Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        }
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference("Chat").child(otherUser.getChatId());
        mFirebaseDatabaseReference.getDatabase().goOnline();
        callData(new Date().getTime());

        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMessageEditText.getText().toString().trim().length() > 0) {
                    notificationSent = false;
                    friendlyMessage.setSelf(MY_LOCATION).setText(mMessageEditText.getText().toString().trim())
                            .setTimeStamp(new Date().getTime());
                    mFirebaseDatabaseReference.push().setValue(friendlyMessage);
                    lastMessage = mMessageEditText.getText().toString().trim();
                    FriendlyMessage newMessage = new FriendlyMessage();
                    newMessage.setSelf(friendlyMessage.isSelf()).setText(friendlyMessage.getText()).setTimeStamp(new Date().getTime()/*friendlyMessage.getTimeStamp()*/);
                    messages.add(0, newMessage);
                    mMessageEditText.setText("");

                    setAdapter();
                    if (mMessageRecyclerView != null && mFirebaseAdapter != null) {
                        mMessageRecyclerView.scrollToPosition(0);
                    }
                    chatUsersList = ChatDBHandlerClass.getChatFromTable(otherUser.getChatId());
                    if (chatUsersList == null) {
                        chatUsersList = new ChatUsersList();
                    }
                    chatUsersList.setId(otherUser.getChatId()).setLastMessage(friendlyMessage.getText()).setName(otherUser.getName())
                            .setLastMessageTimestamp(System.currentTimeMillis()).setUserId(array.get(1 - MY_LOCATION));
                    if (friendlyMessage.isSelf() == MY_LOCATION) {
                        chatUsersList.setSelf(MY_LOCATION);
                        chatUsersList.setLastSeenTimestamp(System.currentTimeMillis());
                        ChatDBHandlerClass.saveDataInTable(ChatDBHandlerClass.dataBaseTables.chatListTable, chatUsersList);
                    } else {
                        chatUsersList.setSelf(1 - MY_LOCATION);
                    }
                }

            }
        });


        // setAdapter(otherUser.getChatId(),new ArrayList<FriendlyMessage>());


        mMessageEditText = (EmojiconEditText) findViewById(R.id.messageEditText);
        mMessageRecyclerView.setOnClickListener(null);
        mMessageRecyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(v instanceof EditText)) {
                    CommonMethods.hideSoftKeyboard(ChatActivity.this);
                }
            }
        });


        mMessageEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(mSharedPreferences
                .getInt(FRIENDLY_MSG_LENGTH, DEFAULT_MSG_LENGTH_LIMIT))});
        mMessageEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMessageRecyclerView != null)
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mMessageRecyclerView.scrollToPosition(0);
                                }
                            });
                        }
                    }, 230);

            }
        });
        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    mSendButton.setEnabled(true);
                } else {
                    mSendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (mProgressBar.getVisibility() == View.VISIBLE && messages.size() == 0) {
                    LogAnalytics.logSerViceFailEvent(mTracker, userId, otherUser.getChatId());
                }
            }
        }, 5000);

    }//onCreate

    private void getSingleRecordOnAdd() {

        mFirebaseDatabaseReference.limitToLast(1).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                FriendlyMessage friendlyMessage1 = dataSnapshot.getValue(FriendlyMessage.class);
                if (friendlyMessage1.isSelf() == MY_LOCATION && lastMessage.length() > 0 && !notificationSentFor[0].equals(friendlyMessage.getText())) {
                    notificationSentFor[0] = friendlyMessage.getText();
                    sendNotification(chatUsersList, friendlyMessage.getText());

                } else if (friendlyMessage1.isSelf() != MY_LOCATION) {
                    FriendlyMessage friendlyMessage = null;
                    if (messages.isEmpty() == false) {
                        friendlyMessage = messages.get(0);
                    }
                    if (friendlyMessage == null || (friendlyMessage.getTimeStamp() != friendlyMessage1.getTimeStamp()) || friendlyMessage.getText().equals(friendlyMessage1.getText()) == false) {
                        messages.add(0, friendlyMessage1);

                        setAdapter();
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //  Log.e("CHILD", "CHANGED");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.e("CHILD", "REMOVED " + dataSnapshot.getKey());
                //if(dataSnapshot.getKey().equals(otherUser.getChatId())){
//                    mFirebaseDatabaseReference.removeEventListener(this);
//                    if (!unMatched) {
//                        onOtherPersonUnMatchedClicked();
//                    }

                // }

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                //   Log.e("CHILD", "MOVED");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("CHILD", "CANCELED");
            }
        });


    }

    private void callData(long timeStamp) {
        //mFirebaseDatabaseReference.getDatabase().goOnline();
        DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    Log.e("1234567890", "connected");
                } else {
                    Log.e("1234567890", "not connected");
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e("1234567890", "Listener was cancelled");
                //System.err.println("Listener was cancelled");
            }
        });
        if (messages == null || messages.size() == 0) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
        var = mFirebaseDatabaseReference.orderByChild("timeStamp").endAt(timeStamp - 1).limitToLast(Constants.ServiceConfiguration.CHAT_MESSAGES_COUNT_AT_A_TIME);
        var.keepSynced(true);
        var.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mProgressBar.setVisibility(View.GONE);
                Log.e("1234567890", dataSnapshot.getChildrenCount() + "");
                Iterator<DataSnapshot> children = dataSnapshot.getChildren().iterator();
                int size = messages.size();
                ArrayList<FriendlyMessage> localMessages = new ArrayList<>();
                while (children.hasNext()) {
                    DataSnapshot child = children.next();
                    FriendlyMessage frndMessage = child.getValue(FriendlyMessage.class);
                    frndMessage.setTimeStamp(frndMessage.getTimeStamp());
                    localMessages.add(frndMessage);

                }
                if (localMessages.size() > 0) {
                    Collections.sort(localMessages, new Comparator<FriendlyMessage>() {
                        @Override
                        public int compare(FriendlyMessage o1, FriendlyMessage o2) {
                            if (o1.getTimeStamp() < o2.getTimeStamp())
                                return 1;
                            else if (o1.getTimeStamp() == o2.getTimeStamp())
                                return 0;
                            else
                                return -1;
                        }
                    });
                    messages.addAll(localMessages);

                }

                if (size < messages.size())
                    setAdapter();
                if (messages.size() == localMessages.size()) {
                    getSingleRecordOnAdd();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mProgressBar.setVisibility(View.GONE);
                Log.e("0987654321", databaseError.getMessage());
            }
        });
    }

    private void setAdapter() {


        if (mFirebaseAdapter == null) {
            mFirebaseAdapter = new ChatAdapter(this, messages, MY_LOCATION, otherUser.getImage(), new ChatAdapter.ChatListener() {
                @Override
                public void callNextPage(long timeStamp) {
                    callData(timeStamp);
                }
            });


//            mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//                @Override
//                public void onItemRangeInserted(int positionStart, int itemCount) {
//                    super.onItemRangeInserted(positionStart, itemCount);
//                    int friendlyMessageCount = mFirebaseAdapter.getItemCount();
//                    int lastVisiblePosition =
//                            mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
//                    int firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition();
//                    // If the recycler view is initially being loaded or the
//                    // user is at the bottom of the list, scroll to the bottom
//                    // of the list to show the newly added message.
//                    if (lastVisiblePosition == -1 ||
//                            (positionStart >= (friendlyMessageCount - 1) &&
//                                    lastVisiblePosition == (positionStart - 1))) {
//                        mMessageRecyclerView.scrollToPosition(positionStart);
//                    }
//                    //mMessageRecyclerView.scrollToPosition(friendlyMessageCount + itemCount);
//                }
//            });
//

            mLinearLayoutManager.scrollToPosition(0);
            mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);
            mMessageRecyclerView.setAdapter(mFirebaseAdapter);

        } else {
            mFirebaseAdapter.notifyDataSetChanged();
            //mMessageRecyclerView.scrollToPosition(0);
        }


    }

    private void sendNotification(ChatUsersList chatUsersList, String text) {

        new ChatServices().sendChatMessageNotification(ChatActivity.this, otherUser, text, LocalPreferenceManager.getInstance().getUserName(), null);
    }

    private void onOtherPersonUnMatchedClicked() {

        showOkAlert("Your partner has Un-Matched you.\nYou can no longer send or receive message on this chat.", "Un-Match", OTHER_UNMATCH);

    }

    private void setEmojiconFragment(boolean useSystemDefault) {
        getSupportFragmentManager()
                .beginTransaction();
                /*.add(R.id.emojicons_view, EmojiconsFragment.newInstance(useSystemDefault))
                .commit();*/
    }

    @Override
    public void onStart() {
        super.onStart();
        mFirebaseDatabaseReference.getDatabase().goOnline();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ChatUsersList chatUsersList1 = ChatDBHandlerClass.getChatFromTable(otherUser.getChatId());
        if (chatUsersList1 != null) {
            chatUsersList1.setLastSeenTimestamp(new Date().getTime());
            chatUsersList1.setIsNew("false");
            ChatDBHandlerClass.saveDataInTable(ChatDBHandlerClass.dataBaseTables.chatListTable, chatUsersList1);
        }
        //LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREF_NEW_MESSAGE, false);

    }

    @Override
    public void onStop() {
        super.onStop();

        if (chatUsersList != null) {
            // chatUsersList.setLastSeenTimestamp(System.currentTimeMillis());
            // ChatDBHandlerClass.saveDataInTable(ChatDBHandlerClass.dataBaseTables.chatListTable, chatUsersList);
            if (chatUsersList.isSelf() == MY_LOCATION && lastMessage.trim().length() > 0) {
                new ChatServices().sendLastChatToServer(this, chatUsersList, null);
            }
        }

        LocalPreferenceManager.getInstance().setPreference("chat" + otherUser.getChatId(), new Date().getTime());
        mFirebaseDatabaseReference.getDatabase().goOffline();
    }


    @Override
    public void onResume() {
        LogAnalytics.logScreenView(mTracker, Constants.ActivityName.CHAT_MSGS_SCREEN);
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        lastMessage = "";

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        // Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }


    private void customAlertDialog(Context context, String title, String msg, String posBtn, String negBtn) {

        final int POSITIVE = 0, NEGATIVE = 1;
        //reportDg= (RelativeLayout) findViewById(R.id.dg_rootLay);
        reportDgLay.setVisibility(View.VISIBLE);

        reasonEd = (EditText) findViewById(R.id.reasonTxt);
        layOutsideAlert = (RelativeLayout) findViewById(R.id.lay_outside);
        //hideKeyBoardOnPressingOutsideView(reasonEd);
        TextViewPlus positiveBtn = (TextViewPlus) findViewById(R.id.positiveBtn);
        TextViewPlus ntvBtn = (TextViewPlus) findViewById(R.id.ntvBtn);

        positiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportDgLay.setVisibility(View.GONE);
                actionOnClick(POSITIVE, reasonEd.getText().toString());

            }
        });

        ntvBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportDgLay.setVisibility(View.GONE);
                //actionOnClick(NEGATIVE, "");
            }
        });

        layOutsideAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reasonEd.getText().length() > 0 && count == 0) {
                    CommonMethods.hideSoftKeyboard(ChatActivity.this);
                    count = 1;
                } else {
                    count = 0;
                    CommonMethods.hideSoftKeyboard(ChatActivity.this);
                    reportDgLay.setVisibility(View.GONE);
                }
            }
        });

    }

    private void actionOnClick(int clickedBtn, String reason) {
        if (clickedBtn == 0) {
            //Toast.makeText(ChatActivity.this, "Successfully reported.", Toast.LENGTH_SHORT).show();
            CommonMethods.hideSoftKeyboard(ChatActivity.this);
            new FacebookGraphService().reportService(ChatActivity.this, otherUser.getUserId()/*"Some Profile"*//*profileModel.getId()*/, LocalPreferenceManager.getInstance().getUserId(), reason, new ReportResponse() {
                @Override
                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                    //Finish activity with animation
                    if (baseModel != null) {
                        onShowAlertOkPressed(UNMATCH);
                   /*overridePendingTransition(R.anim.zoom_in,
                            R.anim.slide_out_right);*/
                    }
                }
            });
        } else if (clickedBtn == 1) {

        }
    }


    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }


    @Override
    protected void onShowAlertOkPressed(int tag) {
        super.onShowAlertOkPressed(tag);
        if (tag == UNMATCH) {
            unMatched = true;
            new ChatServices().callUnMatchService(ChatActivity.this, userId, array.get(1 - MY_LOCATION), new UnMatchResponse() {
                @Override
                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                    if (jsonString.contains("true")) {
                        //  Toast.makeText(ChatActivity.this, "Request completed.", Toast.LENGTH_SHORT).show();
                        mFirebaseDatabaseReference.removeValue();

                        finish();
                    } else {
                        Toast.makeText(ChatActivity.this, "Something went wrong, Try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else if (tag == OTHER_UNMATCH) {
            finish();
        }
        if (tag == UNMATCH || tag == OTHER_UNMATCH) {
//            FirebaseMessaging.getInstance().unsubscribeFromTopic(otherUser.getChatId());
//            NotificationDbHandlerClass.delete(otherUser.getChatId());

            ChatDBHandlerClass.deleteChatFromDb(otherUser.getChatId());
            chatUsersList = null;

        }
    }

    protected void showCustomDialog(String title, String message, final int tag, String positiveBtnText, String negativeBtnText) {
        final AlertDialog dialog = new AlertDialog.Builder(this).create();

        View view = getLayoutInflater().inflate(R.layout.dg_unmatch, null, false);
        dialog.setView(view);
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        TextViewPlus msg = (TextViewPlus) view.findViewById(R.id.msgTxt);
        msg.setText("Do you really want to Un-Match " + otherUser.getName());
        EditText reportReasonEd = (EditText) view.findViewById(R.id.reasonTxt);
        reportReasonEd.setVisibility(View.GONE);
        dialog.show();

        TextViewPlus yes = (TextViewPlus) view.findViewById(R.id.positiveBtn);
        TextViewPlus cancel = (TextViewPlus) view.findViewById(R.id.ntvBtn);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onShowAlertOkPressed(UNMATCH);
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


    @Override
    protected void chatUnMatched(String id) {
        super.chatUnMatched(id);

        if (id.equals(otherUser.getChatId())) {

            if (!unMatched) {
                onOtherPersonUnMatchedClicked();
            }

        }

    }
}
