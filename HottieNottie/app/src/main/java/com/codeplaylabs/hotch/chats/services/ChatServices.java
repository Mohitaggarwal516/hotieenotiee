package com.codeplaylabs.hotch.chats.services;

import android.app.Activity;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.chats.handler.ChatDBHandlerClass;
import com.codeplaylabs.hotch.chats.interfaces.UnMatchResponse;
import com.codeplaylabs.hotch.chats.models.firbaseModels.UserModel;
import com.codeplaylabs.hotch.chats.parsers.ParseChatList;
import com.codeplaylabs.hotch.database.tables.ChatUsersList;
import com.codeplaylabs.hotch.database.tables.DeviceTable;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.base.BaseService;
import com.codeplaylabs.hotch.chats.interfaces.ChatListResponse;
import com.codeplaylabs.hotch.utils.URLs;
import com.codeplaylabs.hotch.webConnection.WebConnectionModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by mohit on 31/07/17.
 */

public class  ChatServices extends BaseService {
    public void getChatList(Activity activity, int pageNo, ChatListResponse chatListResponse) {
        String id = LocalPreferenceManager.getInstance().getUserId();
        ;
        String url = URLs.FETCH_MATCHES_CHAT_LIST + id + "&pageNo=" + pageNo;
        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setForceRefresh(true)
                .setRequestType(WebConnectionModel.JsonRequestType.GET)
                .setJsonRequest(null)
                .setUrl(url);
        callService(activity, webConnectionModel, new ParseChatList(), chatListResponse);

    }

    public void callUnMatchService(Activity activity, String userId, String otherUserId, UnMatchResponse unMatchResponse) {
        String url = URLs.UNMATCH_URL + userId + "&partnerId=" + otherUserId;
        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setForceRefresh(true)
                .setRequestType(WebConnectionModel.JsonRequestType.GET)
                .setJsonRequest(null)
                .setUrl(url);
        callService(activity, webConnectionModel, null, unMatchResponse);

    }

    public void sendChatMessageNotification(Activity activity, UserModel userModel, String text, String userName, ChatListResponse chatListResponse) {

        ChatUsersList chatUsersList = ChatDBHandlerClass.getChatFromTable(userModel.getChatId());
        if (chatUsersList == null) {
            return;
        }
        List<DeviceTable> devices = ChatDBHandlerClass.getAllDevicesByUserId(chatUsersList.getUserId());
        if (devices != null && devices.size() > 0) {

            for (DeviceTable dev : devices) {

                if (chatUsersList.getIsNotificationDisabledByPartner() && dev.getDeviceType().equalsIgnoreCase("iPhone")) {

                } else {
                    JSONObject params = new JSONObject();
                    try {
                        params.put("to", dev.getDeviceTokens());
                        params.put("priority","high");

                        JSONObject data = new JSONObject();
                        data.put(Constants.Chat.BODY, text);

                        data.put("notificationType", Constants.Chat.CHAT_NOTIFICATION);
                        data.put(Constants.Chat.SENDER_USERID, LocalPreferenceManager.getInstance().getUserId());
                        data.put(Constants.Chat.CHAT_ID, userModel.getChatId());
                        data.put(Constants.Chat.OTHER_USER_NAME, LocalPreferenceManager.getInstance().getUserName());
                        data.put(Constants.Chat.OTHER_USER_IMAGE, LocalPreferenceManager.getInstance().getUserImage());
                        data.put(Constants.Chat.OTHER_USER_TIMESTAMP, System.currentTimeMillis());
                        data.put(Constants.Chat.CHAT_NOTIF_DISABLED_BY_ME, chatUsersList.getIsNotificationDisabledByPartner());
                        data.put(Constants.Chat.CHAT_NOTIF_DISABLED_BY_PARTNER, chatUsersList.getIsNotificationDisabledBySelf());


                        params.put("data", data);

                        if (!chatUsersList.getIsNotificationDisabledByPartner() && dev.getDeviceType().equalsIgnoreCase("iPhone")) {

                            JSONObject notif = new JSONObject();
                            notif.put(Constants.Chat.TEXT, text);
                            notif.put(Constants.Chat.TITLE, LocalPreferenceManager.getInstance().getUserName());
                            notif.put(Constants.Chat.SOUND,"default");
                            params.put("notification", notif);
                        }

                        WebConnectionModel webConnectionModel = new WebConnectionModel();
                        webConnectionModel.setForceRefresh(true)
                                .setRequestType(WebConnectionModel.JsonRequestType.CHAT_MESSAGES_NOTIFICATION)
                                .setUrl(URLs.CHAT_MESSAGES_NOTIFICATION_URL)
                                .setJsonRequest(params.toString());

                        callService(activity, webConnectionModel, null, new ChatListResponse() {
                            @Override
                            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void sendLastChatToServer(Activity activity, ChatUsersList chatUsersList, ChatListResponse response) {

        JSONObject params = new JSONObject();
        try {

            JSONObject data = new JSONObject();
            data.put("chatId", chatUsersList.getId());
            data.put("messageSender", LocalPreferenceManager.getInstance().getUserId());
            data.put("lastMessage", chatUsersList.getLastMessage());
            data.put("lastMessageTimeStamp", chatUsersList.getLastMessageTimestamp());
            JSONArray array = new JSONArray();
            array.put(data);
            params.put("messages", array);

            WebConnectionModel webConnectionModel = new WebConnectionModel();
            webConnectionModel.setForceRefresh(true)
                    .setRequestType(WebConnectionModel.JsonRequestType.POST)
                    .setUrl(URLs.CHAT_LAST_MSG_UPDATE)
                    .setJsonRequest(params.toString());

            callService(activity, webConnectionModel, null, new ChatListResponse() {
                @Override
                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getChatWithMessages(Activity activity, int pageNo, ChatListResponse response) {

        String id = LocalPreferenceManager.getInstance().getUserId();
        ;
        String url = URLs.CHAT_WITH_MSG + id + "&pageNo=" + pageNo;
        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setForceRefresh(true)
                .setShouldClearModule(true)
                .setRequestType(WebConnectionModel.JsonRequestType.GET)
                .setUrl(url);
        callService(activity, webConnectionModel, new ParseChatList(), response);


    }

    public void getChatWithoutMessages(Activity activity, int pageNo, ChatListResponse response) {

        String id = LocalPreferenceManager.getInstance().getUserId();
        ;
        String url = URLs.CHAT_WITHOUT_MSG + id + "&pageNo=" + pageNo;
        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setForceRefresh(true)
                .setShouldClearModule(true)
                .setRequestType(WebConnectionModel.JsonRequestType.GET)
                .setUrl(url);
        callService(activity, webConnectionModel, new ParseChatList(), response);


    }

    public void muteUnmute(Activity chatActivity, String chatId, boolean isNotificationDisabledBySelf, ChatListResponse chatListListener) {
        String id = LocalPreferenceManager.getInstance().getUserId();
        String url = URLs.MUTE_UNMUTE_CHAT + id + "&chatId=" + chatId + "&isMute=" + isNotificationDisabledBySelf;

        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setForceRefresh(true)
                .setRequestType(WebConnectionModel.JsonRequestType.GET)
                .setUrl(url);

        callService(chatActivity, webConnectionModel, null, new ChatListResponse() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

            }
        });


    }

}
