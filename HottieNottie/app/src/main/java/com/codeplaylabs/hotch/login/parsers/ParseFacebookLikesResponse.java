package com.codeplaylabs.hotch.login.parsers;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.login.models.Detail;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.login.models.LikesDetail;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mohit on 21/06/17.
 */

public class ParseFacebookLikesResponse extends BaseParser {
    BaseModel baseModel = new BaseModel();
    @Override
    public BaseModel parseJson(JSONObject response) {
        return moduleJson(response);
    }

    /*
      * This method is called to collect the data received from the servers in a class for the session
      * @param response
     */
    public BaseModel moduleJson(JSONObject response) {
        LikesDetail userProfile= new LikesDetail();
        try {
            if(response != null) {
                JSONArray data = response.getJSONArray("data");
                for(int index = 0 ; index < data.length() ; index++ ){
                    JSONObject friends = data.getJSONObject(index);
                    Detail likesDetail = new Detail();
                    likesDetail.setId(friends.getString("id"));
                    likesDetail.setName(friends.getString("name"));
                    userProfile.getLikesDetails().add(likesDetail);
                }
                JSONObject paging = response.getJSONObject("paging");
                JSONObject cursors = paging.getJSONObject("cursors");
                userProfile.setCursorBefore(cursors.getString("before"));
                userProfile.setCursorAfter(cursors.getString("after"));
                return userProfile;
            }
            baseModel.error = Constants.Error.NULL_RESPONSE_ERROR;
            return baseModel;


        } catch (JSONException e) {
            baseModel.error = Constants.Error.JSON_EXCEPTION;
            return baseModel;
        }
    }
}
