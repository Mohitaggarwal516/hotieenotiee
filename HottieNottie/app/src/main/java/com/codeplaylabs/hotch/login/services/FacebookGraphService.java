package com.codeplaylabs.hotch.login.services;

import android.app.Activity;

import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.home.parsers.ParseProfiles;
import com.codeplaylabs.hotch.login.interfaces.SharedProfileResponse;
import com.codeplaylabs.hotch.login.models.Detail;
import com.codeplaylabs.hotch.login.parsers.ParseAppConfiguration;
import com.codeplaylabs.hotch.others.parser.DescriptionParser;
import com.codeplaylabs.hotch.profileDetail.ReportParser;
import com.codeplaylabs.hotch.profileDetail.interfaces.ReportResponse;
import com.codeplaylabs.hotch.profileDetail.parsers.InterestParser;
import com.codeplaylabs.hotch.settings.interfaces.ProfileResponseListener;
import com.codeplaylabs.hotch.ratings.models.RateDiscoveryWrapper;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.base.BaseService;
import com.codeplaylabs.hotch.login.interfaces.FacebookLikesResponse;
import com.codeplaylabs.hotch.login.interfaces.FacebookUserInfoResponse;
import com.codeplaylabs.hotch.login.interfaces.TokenResponse;
import com.codeplaylabs.hotch.login.models.Education;
import com.codeplaylabs.hotch.login.models.LikesDetail;
import com.codeplaylabs.hotch.login.models.UserProfile;
import com.codeplaylabs.hotch.login.models.Work;
import com.codeplaylabs.hotch.login.parsers.ParseFacebookDataResponse;
import com.codeplaylabs.hotch.login.parsers.ParseFacebookLikesResponse;
import com.codeplaylabs.hotch.login.parsers.ParseLoginSubmissionResponse;
import com.codeplaylabs.hotch.login.parsers.ParseUserFbInterest;
import com.codeplaylabs.hotch.login.parsers.ParseCustomToken;
import com.codeplaylabs.hotch.utils.URLs;
import com.codeplaylabs.hotch.webConnection.WebConnectionModel;
import com.facebook.AccessToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.codeplaylabs.hotch.webConnection.WebConnectionModel.JsonRequestType.GET;

/**
 * Created by mohit on 21/06/17.
 */

public class FacebookGraphService extends BaseService {
    public void getUserLikes(Activity activity, AccessToken accessToken, FacebookLikesResponse facebookLikesResponse) {
        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setModule(String.valueOf(Constants.Modules.LIKES_MODULE))
                .setRequestType(WebConnectionModel.JsonRequestType.GRAPH_LIKES)
                .setShouldCache(false)
                .setUrl(Constants.FB_USER_LIKES)
                .setFbAccessToken(accessToken)
                .setJsonRequest(Constants.FB_USER_LIKES);


        callService(activity, webConnectionModel, new ParseFacebookLikesResponse(), facebookLikesResponse);
    }

    public void deleleteUseLessProfiles(Activity activity,String id,SharedProfileResponse response){

        WebConnectionModel webConnectionModel=new WebConnectionModel();
        webConnectionModel.setForceRefresh(true).setUrl(URLs.DELETE_USER+id)
        .setRequestType(WebConnectionModel.JsonRequestType.GET);

        callService(activity,webConnectionModel,new BaseParser(),response);
    }

    public void ratingService(Activity activity, RateDiscoveryWrapper.filterEnum filterType, int pageNo, String gender, boolean isSelfRatingRequired, String filterFbID, SharedProfileResponse response) {
//     webConnectionModel.setForceRefresh(true).setShouldCache(false).setUrl(URLs.REPORT).setJsonRequest(params.toString()).setRequestType(WebConnectionModel.JsonRequestType.POST);
        JSONObject params=new JSONObject();
        try {
            params.put("filterType",String.valueOf(filterType));
            params.put("id",LocalPreferenceManager.getInstance().getUserId());
            params.put("pageNumber",pageNo);
            params.put("gender",gender);
            params.put("isSelfRatingRequired",isSelfRatingRequired);
            params.put("filterFacebookId",filterFbID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        WebConnectionModel webConnectionModel = new WebConnectionModel();

        webConnectionModel.setForceRefresh(false).setShouldCache(true).setUrl(URLs.RATINGS).setModule(""+Constants.Modules.RATINGS)
                .setTimeFactor(Constants.ServiceConfiguration.RATINGS_TIME_FACTOR).setJsonRequest(params.toString())
                .setRequestType(WebConnectionModel.JsonRequestType.POST).setShouldClearModule(false);

        callService(activity,webConnectionModel,new DescriptionParser(),response);
    }

    public void profileDescriptionService(Activity activity, String targetId, String type, SharedProfileResponse profileResponse) {

        String url = URLs.GET_PROFILE_DESCRIPTION + targetId + "&askedBy=" + LocalPreferenceManager.getInstance().getUserId();

        WebConnectionModel webConnectionModel = new WebConnectionModel();

      /*  webConnectionModel.setForceRefresh(false).setUrl(url)
                .setTimeFactor(Constants.ServiceConfiguration.PUBLIC_PROFILE)
                .setShouldCache(true)
                .setRequestType(WebConnectionModel.JsonRequestType.GET);*/

        if (type.equals(Constants.SHARE_PROFILE)) {
            webConnectionModel.setForceRefresh(true).setUrl(url).setRequestType(WebConnectionModel.JsonRequestType.GET);
            callService(activity, webConnectionModel, new ParseProfiles(), profileResponse);
        }else {

            webConnectionModel.setForceRefresh(false).setUrl(url).
                    setRequestType(WebConnectionModel.JsonRequestType.GET)
                    .setTimeFactor(Constants.ServiceConfiguration.DESCRIPTION_TIME_FACTOR)
                    .setModule(""+Constants.Modules.DESCRIPTION).setShouldCache(true);
            callService(activity, webConnectionModel, new DescriptionParser(), profileResponse);
        }
    }

    public void fetchSuperHotList(Activity activity, String page, SharedProfileResponse response) {
        String url = URLs.FETCH_SUPERHOT_LIST + LocalPreferenceManager.getInstance().getUserId() + "&pageNumber=" + page;

        WebConnectionModel webConnectionModel = new WebConnectionModel();

        webConnectionModel.setForceRefresh(false).setUrl(url).setRequestType(WebConnectionModel.JsonRequestType.GET)
        .setShouldCache(true).setModule(""+Constants.Modules.SUPERHOT_LIST_MOD).setTimeFactor(Constants.ServiceConfiguration.SUPERHOT_TIME_FACTOR);

        callService(activity, webConnectionModel, new DescriptionParser(), response);
    }

    public void getFewInterest(Activity activity, String targetId, String type, FacebookUserInfoResponse response) {
        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setForceRefresh(false).setShouldCache(true)
                .setModule(""+Constants.Modules.FEW_INTEREST)
                .setTimeFactor(Constants.ServiceConfiguration.SUPERHOT_TIME_FACTOR)
                .setUrl(URLs.FEW_INTEREST + targetId + "&type=" + type)
                .setRequestType(WebConnectionModel.JsonRequestType.GET);


        callService(activity, webConnectionModel, new InterestParser(), response);
    }

    public void delSuperHot(Activity activity, String targetId, FacebookUserInfoResponse response) {
        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setForceRefresh(true).setShouldCache(false).setUrl(URLs.DEL_SUPERHOT + LocalPreferenceManager.getInstance().getUserId() + "&partnerIds=" + targetId).setRequestType(WebConnectionModel.JsonRequestType.GET);

        callService(activity, webConnectionModel, new BaseParser(), response);
    }

    public void reportService(Activity activity, String targetUserId, String userId, String reason, ReportResponse reportResponse) {
        JSONObject params = new JSONObject();
        /*{
          "reportedForUserId":"593f773202338a47584b351e",
          "reportedByUserId":"abcd",
          "reason":"hello"
}*/
        try {
            params.put("reportedForUserId", targetUserId);
            params.put("reportedByUserId", userId);
            params.put("reason", reason);
            WebConnectionModel webConnectionModel = new WebConnectionModel();
            webConnectionModel.setForceRefresh(true).setShouldCache(false).setUrl(URLs.REPORT).setJsonRequest(params.toString()).setRequestType(WebConnectionModel.JsonRequestType.POST);

            callService(activity, webConnectionModel, new ReportParser(), reportResponse);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        /*ApiClient apiClient=new ApiClient();
        apiClient.jsonRequest(activity);*/

    }

    public void getUserInfo(Activity activity, AccessToken accessToken, FacebookUserInfoResponse facebookUserInfoResponse) {
        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setModule(String.valueOf(Constants.Modules.LOGIN_MODULE))
                .setRequestType(WebConnectionModel.JsonRequestType.GRAPH_DATA)
                .setShouldCache(true)
                .setShouldClearModule(true)
                .setTimeFactor(Constants.ServiceConfiguration.FB_USER_INFO)
                .setUrl(Constants.FB_USER_INFO).setForceRefresh(true)
                .setFbAccessToken(accessToken)
                .setJsonRequest(Constants.FB_USER_INFO);


        callService(activity, webConnectionModel, new ParseFacebookDataResponse(), facebookUserInfoResponse);

    }


    public void sendFacebookUserProfileDataToServers(Activity activity, UserProfile userProfile, int forceUpdate, FacebookUserInfoResponse facebookUserInfoResponse) {
        try {
            JSONObject params = new JSONObject();
            params.put("shouldForceSync", forceUpdate);
            params.put("email", userProfile.getEmail());
            params.put("facebookId", userProfile.getId());
            params.put("profileImageUrl", userProfile.getProfileImageUrl());
            params.put("birthday", userProfile.getBirthday());
            params.put("gender", userProfile.getGender());
            params.put("name", userProfile.getName());
            params.put("about", userProfile.getAbout());
            JSONObject hometown = new JSONObject();
            hometown.put("city", userProfile.getHometown().getName());
            hometown.put("country", userProfile.getHometown().getCountry());
            hometown.put("countryCode", userProfile.getHometown().getCountryCode());
            hometown.put("facebookId", userProfile.getHometown().getId());
            params.put("hometown", hometown);

            JSONObject location = new JSONObject();
            location.put("city", userProfile.getLocation().getName());
            location.put("country", userProfile.getLocation().getCountry());
            location.put("countryCode", userProfile.getLocation().getCountryCode());
            location.put("facebookId", userProfile.getLocation().getId());
            params.put("currentLocation", location);


            JSONArray education = new JSONArray();
            for (int index = 0; index < userProfile.getEducationList().size(); index++) {
                JSONObject eduObj = new JSONObject();
                Education educationInner = userProfile.getEducationList().get(index);
                eduObj.put("type", educationInner.getType());
                eduObj.put("typeFacebookId", educationInner.getTypeFacebookId());
                eduObj.put("year", educationInner.getYear());
                eduObj.put("facebookId", educationInner.getFacebookId());
                eduObj.put("isCurrentlyStudingHere", educationInner.isCurrentlyStudingHere());

                JSONObject institute = new JSONObject();
                institute.put("name", educationInner.getInstitute().getName());
                institute.put("instituteBranchFacebookId", educationInner.getInstitute().getId());
                eduObj.put("institute", institute);

                education.put(eduObj);
            }
            params.put("education", education);

            JSONArray works = new JSONArray();
            for (int index = 0; index < userProfile.getWorkList().size(); index++) {
                JSONObject workObj = new JSONObject();
                Work workInner = userProfile.getWorkList().get(index);
                workObj.put("facebookId", workInner.getFacebookId());
                workObj.put("startDate", workInner.getStartDate());
                workObj.put("endDate", workInner.getEndDate());
                workObj.put("position", workInner.getPosition());
                workObj.put("positionFacebookId", workInner.getPositionFacebookId());
                workObj.put("isCurrentlyWorkingHere", workInner.getIsCurrentlyWorkingHere());

                JSONObject locationWorkObj = new JSONObject();
                locationWorkObj.put("city", workInner.getLocation().getName());
                locationWorkObj.put("facebookId", workInner.getLocation().getId());
                workObj.put("location", locationWorkObj);

                JSONObject employerWorkObj = new JSONObject();
                employerWorkObj.put("name", workInner.getEmployer().getName());
                employerWorkObj.put("facebookId", workInner.getEmployer().getId());
                workObj.put("employer", employerWorkObj);

                works.put(workObj);

            }

            params.put("works", works);

            WebConnectionModel webConnectionModel = new WebConnectionModel();
            webConnectionModel.setForceRefresh(true)
                    .setModule(Constants.Modules.LOGIN_MODULE.toString())
                    .setRequestType(WebConnectionModel.JsonRequestType.POST)
                    .setShouldCache(false)
                    /*.setShouldClearModule(true)*/
                    .setTimeFactor(Constants.ServiceConfiguration.LOGIN_DATA)
                    .setUrl(URLs.LOGIN_REGISTER_URL)
                    .setJsonRequest(params.toString());

            callService(activity, webConnectionModel, new ParseLoginSubmissionResponse(), facebookUserInfoResponse);

        } catch (JSONException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setUserLikesToServer(Activity activity, LikesDetail likesDetail, FacebookLikesResponse facebookLikesResponse) {

        JSONObject params = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            params.put("id", LocalPreferenceManager.getInstance().getUserId());
            for (Detail detail : likesDetail.getLikesDetails()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", detail.getName());
                jsonObject.put("facebookId", detail.getId());
                jsonArray.put(jsonObject);
            }
            params.put("interests", jsonArray);
            WebConnectionModel webConnectionModel = new WebConnectionModel();
            webConnectionModel.setForceRefresh(true)
                    .setModule(Constants.Modules.INTEREST_MODULE.toString())
                    .setRequestType(WebConnectionModel.JsonRequestType.POST)
                    .setShouldCache(true)
                    .setShouldClearModule(true)
                    .setTimeFactor(Constants.ServiceConfiguration.UPDATE_INTERESTS)
                    .setUrl(URLs.UPDATE_INTERESTS)
                    .setJsonRequest(params.toString());

            callService(activity, webConnectionModel, new ParseUserFbInterest(), facebookLikesResponse);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getCustomToken(Activity activity, TokenResponse tokenResponse) {

        String url = URLs.GET_CUSTOM_TOKEN + LocalPreferenceManager.getInstance().getUserId();

        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setForceRefresh(true).setRequestType(GET)
                .setShouldCache(false)
                .setUrl(url)
                .setJsonRequest(null);

        callService(activity, webConnectionModel, new ParseCustomToken(), tokenResponse);

    }

    public void getAppConfigurations(Activity activity, ProfileResponseListener profileResponseListener) {
        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setForceRefresh(true).setRequestType(GET)
                .setShouldCache(false)
                .setUrl(URLs.APP_CONFIGURATION)
                .setJsonRequest(null);

        callService(activity, webConnectionModel, new ParseAppConfiguration(), profileResponseListener);

    }
}
