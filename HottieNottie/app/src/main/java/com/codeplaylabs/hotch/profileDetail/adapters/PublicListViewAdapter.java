package com.codeplaylabs.hotch.profileDetail.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.models.Detail;
import com.codeplaylabs.hotch.profileDetail.interfaces.ProfileDetailOnClickListener;
import com.codeplaylabs.hotch.R;

import org.apmem.tools.layouts.FlowLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by mohit on 27/07/17.
 */

public class PublicListViewAdapter extends ArrayAdapter<ProfileModel> {

    private ArrayList<ProfileModel> userProfile;
    private Context context;
    private ProfileDetailOnClickListener profileDetailOnClickListener1;

    public PublicListViewAdapter(Activity activity, ArrayList<ProfileModel> userProfiles, ProfileDetailOnClickListener profileDetailOnClickListener) {
        super(activity, R.layout.row_parallax_listview, userProfiles);
        this.userProfile = userProfiles;
        this.context = activity;
        this.profileDetailOnClickListener1 = profileDetailOnClickListener;
        //private String TAG="mListViewAdapter";
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        View rowView;
        rowView = convertView;
        if (convertView == null) {
            //view = context.getLayoutInflater().inflate(R.layout.dashboard_grid_row,null);
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_parallax_listview, null);
        }
        TextView nameAge = (TextView) rowView.findViewById(R.id.name);
        TextView age = (TextView) rowView.findViewById(R.id.age);
        TextView about = (TextView) rowView.findViewById(R.id.about);
        TextView currAt = (TextView) rowView.findViewById(R.id.currently_at_text);
        CardView aboutCard = (CardView) rowView.findViewById(R.id.about_me_card);
        CardView currAtCard = (CardView) rowView.findViewById(R.id.curr_at_card);
        CardView interestCard = (CardView) rowView.findViewById(R.id.interest_card);
        final ProgressBar shareLoader = (ProgressBar) rowView.findViewById(R.id.shareLoader);

        TextView currentLocation = (TextView) rowView.findViewById(R.id.currentLocation);
        ImageView locPin = (ImageView) rowView.findViewById(R.id.locPin);
        final ImageView shareBtn = (ImageView) rowView.findViewById(R.id.share);
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareBtn.setVisibility(View.GONE);
                shareLoader.setVisibility(View.VISIBLE);
                profileDetailOnClickListener1.onShareClicked(context, userProfile.get(position), position, shareLoader, shareBtn);
            }
        });
        //RecyclerView recyclerView= (RecyclerView) rowView.findViewById(R.id.recyclerViewInListView);
        if (userProfile.get(position).getAbout() != null && userProfile.get(position).getAbout().length() > 0) {
            aboutCard.setVisibility(View.VISIBLE);
            about.setText(userProfile.get(position).getAbout());
        } else {
            aboutCard.setVisibility(View.GONE);
        }
        //if (userProfile.get(position).getWork() != null) {

        if (userProfile.get(position).getWork() != null && userProfile.get(position).getWork().getPosition() != null && userProfile.get(position).getWork().getPosition().length() > 0) {
            currAtCard.setVisibility(View.VISIBLE);
            currAt.setText(userProfile.get(position).getWork().getPosition() + "(" + userProfile.get(position).getWork().getEmployer().getName() + ")");

        } else if (userProfile.get(position).getWork().getEmployer() != null && userProfile.get(position).getWork().getEmployer().getName() != null && userProfile.get(position).getWork().getEmployer().getName().length() > 0) {
            currAtCard.setVisibility(View.VISIBLE);
            currAt.setText(userProfile.get(position).getWork().getEmployer().getName());

        } else if (userProfile.get(position).getEducation() != null && userProfile.get(position).getEducation().getInstitute() != null && userProfile.get(position).getEducation().getInstitute().getName() != null && userProfile.get(position).getEducation().getInstitute().getName().length() > 0) {
            currAtCard.setVisibility(View.VISIBLE);
            currAt.setText(userProfile.get(position).getEducation().getInstitute().getName());

        } else {

            currAtCard.setVisibility(View.GONE);

        }


        ImageView profileDetail_Menu = (ImageView) rowView.findViewById(R.id.menu_profileDetail);
        profileDetail_Menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //callback menu clicked
                profileDetailOnClickListener1.onMenuClick(v);
            }
        });
        FlowLayout flowLayout = (FlowLayout) rowView.findViewById(R.id.linLay_test);
        Calendar dob = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

        int year = 0;
        if (userProfile.get(position).getYearOfBirthday() == null || userProfile.get(position).getYearOfBirthday().length() == 0) {
            year = (new Date().getYear() + 1900);
        } else {
            year = (new Date().getYear() + 1900) - Integer.parseInt(userProfile.get(position).getYearOfBirthday());
        }
        age.setText(year + "");
        try {
            nameAge.setText(userProfile.get(position).getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (userProfile.get(position).getLocation() != null && (userProfile.get(position).getLocation().getName().length() > 0 || userProfile.get(position).getLocation().getCountry().length() > 0)) {
            Detail loc = userProfile.get(position).getLocation();
            if (loc.getName().length() > 0 && loc.getCountry().length() > 0) {
                //both city n country available
                if (loc.getName().contains(loc.getCountry())) {
                    currentLocation.setText(loc.getName()/*+", "+loc.getCountry()*/);
                } else {
                    currentLocation.setText(loc.getName() + ", " + loc.getCountry());
                }

            } else if (loc.getName().length() == 0 && loc.getCountry().length() > 0) {
                //no city but country
                currentLocation.setText(loc.getCountry());
            } else if (loc.getName().length() > 0 && loc.getCountry().length() == 0) {
                //no country but city
                currentLocation.setText(loc.getName());
            }
            //currentLocation.setText(userProfile.get(position).getLocation().getName());
        } else {
            locPin.setVisibility(View.GONE);
            currentLocation.setVisibility(View.GONE);
        }

        if (userProfile.get(position).getInterests() == null || userProfile.get(position).getInterests().size() == 0) {//userProfile.get(position).getInterests()=>common Interests

            interestCard.setVisibility(View.GONE);

        } else {

            interestCard.setVisibility(View.VISIBLE);

        }
        profileDetailOnClickListener1.callFewInterestSerVice(context, userProfile.get(position).getId(), flowLayout,interestCard);
        for (Detail interList : userProfile.get(position).getInterests()
                ) {
            TextView textView = new TextView(context);
            textView.setBackground(context.getResources().getDrawable(R.drawable.bg_interest_profile_detail/*bg_interest_recyclerview_txtvw*/));
            //textView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            textView.setPadding(8, 8, 8, 8);
            textView.setGravity(Gravity.CENTER);

            FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(10, 10, 10, 10);
            textView.setLayoutParams(params);

            textView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            textView.setTextSize(14);
            textView.setMaxLines(1);
            textView.setMaxEms(15);
            textView.setEllipsize(TextUtils.TruncateAt.END);
            textView.setText(interList.getName());
            flowLayout.setPadding(5, 5, 5, 5);
            flowLayout.addView(textView);
        }
        return rowView;
    }
}

