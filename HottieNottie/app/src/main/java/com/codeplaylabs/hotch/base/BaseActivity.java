package com.codeplaylabs.hotch.base;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.codeplaylabs.hotch.BuildConfig;
import com.codeplaylabs.hotch.MyApplication;
import com.codeplaylabs.hotch.chats.ui.ChatFragmentActivity;
import com.codeplaylabs.hotch.login.interfaces.TryAgainForAppConfig;
import com.codeplaylabs.hotch.login.services.FacebookGraphService;
import com.codeplaylabs.hotch.login.ui.ReRoutingActivity;
import com.codeplaylabs.hotch.base.interfaces.OnTaskCompleteInterface;
import com.codeplaylabs.hotch.others.ui.OthersTabActivity;
import com.codeplaylabs.hotch.profileDetail.ui.ProfileDetailActivity;
import com.codeplaylabs.hotch.ratings.models.RateDiscoveryWrapper;
import com.codeplaylabs.hotch.ratings.RatingsActivity;
import com.codeplaylabs.hotch.settings.interfaces.DiscoveryResponse;
import com.codeplaylabs.hotch.settings.interfaces.ProfileResponseListener;
import com.codeplaylabs.hotch.settings.models.albums.discovery.DiscoverySettingModel;
import com.codeplaylabs.hotch.settings.services.SettingsService;
import com.codeplaylabs.hotch.settings.ui.FullScreenActivity;
import com.codeplaylabs.hotch.utils.ConnectivityReceiver;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.utils.Singleton;
import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.chats.ui.ChatActivity;
import com.codeplaylabs.hotch.home.ui.HomeActivity;
import com.codeplaylabs.hotch.login.ui.LoginActivity;
import com.codeplaylabs.hotch.login.ui.WebViewActivity;
import com.codeplaylabs.hotch.settings.ui.GalleryActivity;
import com.codeplaylabs.hotch.settings.ui.SettingsActivity;
import com.codeplaylabs.hotch.utils.HomeWatcher;
import com.codeplaylabs.hotch.utils.TextViewPlus;
import com.google.android.gms.analytics.Tracker;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.LinkedHashSet;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;

public class BaseActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {


    private ProgressDialog progressDialog;
    protected static PowerManager powerManager;
    protected static PowerManager.WakeLock wakeLock;
    protected static KeyguardManager keyguardManager;
    protected static KeyguardManager.KeyguardLock lock;
    protected static final int FINISH = 0;
    private HomeWatcher mHomeWatcher;
    private ImageView menuBtn, menuRatingBtn;
    protected LinearLayout menuScreenLinLayout;
    private ImageView menuHomeBtn, menuSettingBtn;
    private ImageView menuChatBtn, menuOthersBtn;
    private LayoutInflater inflater;
    protected android.app.AlertDialog dialog;
    protected MyApplication myAppInstance;
    protected Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalPreferenceManager.getInstance().initialise(this);
        Singleton.getInstance().initializeHelper(this);
        Singleton.getInstance().activity = new WeakReference<Activity>(this);

        myAppInstance = (MyApplication) getApplication();
        mTracker = myAppInstance.getTracker();


        LocalBroadcastManager.getInstance(this).registerReceiver(chatReciever,
                new IntentFilter(Constants.Chat.LOCAL_BROADCAST));
        LocalBroadcastManager.getInstance(this).registerReceiver(chatReciever,
                new IntentFilter(Constants.Chat.LOCAL_BROADCAST_NEW_MESSAGE));

        try {
            NotificationManager mNotificationManager = (NotificationManager)
                    getSystemService(NOTIFICATION_SERVICE);
            mNotificationManager.cancelAll();

        } catch (Exception e) {
        }
        if (!hasOptionMenu(this)) {

            //do nothing
        } else {
            menuBtn = (ImageView) findViewById(R.id.home_title_menu_btn);
            menuRatingBtn = (ImageView) findViewById(R.id.menu_rating_btn);
            menuScreenLinLayout = (LinearLayout) findViewById(R.id.menu_screen_lin_layout);
            menuHomeBtn = (ImageView) findViewById(R.id.menu_home_btn);

            menuChatBtn = (ImageView) findViewById(R.id.menu_chat_btn);
            menuChatBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menuScreenLinLayout.setVisibility(View.GONE);
                    if (BaseActivity.this instanceof ChatFragmentActivity) {
                        menuScreenLinLayout.setVisibility(View.GONE);
                    } else {
                        checkForSettingsUpdate();
                        Intent intent = new Intent(BaseActivity.this, ChatFragmentActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        if (!(BaseActivity.this instanceof HomeActivity)) {
                            finish();
                        }
                    }
                }
            });

            menuRatingBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (BaseActivity.this instanceof RatingsActivity) {
                        menuScreenLinLayout.setVisibility(View.GONE);
                    } else {
                        checkForSettingsUpdate();
                        new SettingsService().getDiscoverySetting(BaseActivity.this, false, new DiscoveryResponse() {
                            @Override
                            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                                if (reasonOfError == null) {
                                    final DiscoverySettingModel model = (DiscoverySettingModel) baseModel;
                                    if (model != null) {
                                    /*RatingsActivity.filterEnum mEnum=null;
                                    String filterId="";*/
                                        RateDiscoveryWrapper dWrapper = new RateDiscoveryWrapper();
                                        //ArrayList<Detail> discoveryList=new ArrayList<>();

                                        if (model.getCurrentlyAt() != null) {
                                      /*  mEnum = RatingsActivity.filterEnum.CurrentlyAt;
                                        filterId = model.getCurrentlyAt().getId();
                                        discoveryList.add(model.getCurrentlyAt());*/
                                            dWrapper.getmEnumList().add(RateDiscoveryWrapper.filterEnum.CurrentlyAt);
                                            dWrapper.getDiscoveryWrapper().add(model.getCurrentlyAt());
                                        }
                                        if (model.getCurrentLocation() != null) {
                                        /*mEnum = RatingsActivity.filterEnum.CurrentLocation;
                                        filterId = model.getCurrentLocation().getId();
                                        discoveryList.add(model.getCurrentLocation());*/
                                            dWrapper.getmEnumList().add(RateDiscoveryWrapper.filterEnum.CurrentLocation);
                                            dWrapper.getDiscoveryWrapper().add(model.getCurrentLocation());
                                        }
                                        if (model.getCountry() != null) {
                                        /*mEnum = RatingsActivity.filterEnum.CurrentCountry;
                                        filterId = model.getCountry().getId();
                                        discoveryList.add(model.getCountry());*/
                                            dWrapper.getmEnumList().add(RateDiscoveryWrapper.filterEnum.CurrentCountry);
                                            dWrapper.getDiscoveryWrapper().add(model.getCountry());
                                        }

                                        Intent intent = new Intent(BaseActivity.this, RatingsActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra("rateDisWrapper", dWrapper);
                                        startActivity(intent);
                                        if (!(BaseActivity.this instanceof HomeActivity)) {
                                            finish();
                                        }
                                    }
                                } else {
                                    showSnack(false, false);
                                }
                            }
                        });
                        menuScreenLinLayout.setVisibility(View.GONE);
                    }
                }
            });
            menuSettingBtn = (ImageView) findViewById(R.id.menu_settings_btn);
            menuSettingBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (BaseActivity.this instanceof SettingsActivity) {
                        menuScreenLinLayout.setVisibility(View.GONE);
                    } else {
                        menuScreenLinLayout.setVisibility(View.GONE);
                        Intent intent = new Intent(new Intent(BaseActivity.this, SettingsActivity.class));
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        if (!(BaseActivity.this instanceof HomeActivity)) {
                            finish();
                        }
                    }
                }
            });

            menuOthersBtn = (ImageView) findViewById(R.id.menu_others_btn);
            menuOthersBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (BaseActivity.this instanceof OthersTabActivity) {
                        menuScreenLinLayout.setVisibility(View.GONE);
                    } else {
                        checkForSettingsUpdate();
                        menuScreenLinLayout.setVisibility(View.GONE);
                        Intent intent = new Intent(BaseActivity.this, OthersTabActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        if (!(BaseActivity.this instanceof HomeActivity)) {
                            finish();
                        }
                    }
                }
            });
            menuHomeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (BaseActivity.this instanceof HomeActivity) {
                        menuScreenLinLayout.setVisibility(View.GONE);
                    } else {
                        checkForSettingsUpdate();
                        menuScreenLinLayout.setVisibility(View.GONE);
                        Intent intent = new Intent(BaseActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        menuScreenLinLayout.setVisibility(View.GONE);
                        startActivity(intent);
                    }
                }
            });

            menuBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menuScreenLinLayout.setVisibility(View.VISIBLE);
                }
            });

            menuScreenLinLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menuScreenLinLayout.setVisibility(View.GONE);
                }
            });
/*
            menuScreenLinLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menuScreenLinLayout.setVisibility(View.GONE);
                }
            });*/
        }//else


        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");

        //===========================screen lock test=======================


        keyguardManager = (KeyguardManager) getSystemService(Activity.KEYGUARD_SERVICE);
        lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);


        //===========================screen lock test=======================

        mHomeWatcher = new HomeWatcher(this);
        mHomeWatcher.setOnHomePressedListener(new HomeWatcher.OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                // do something here...
                onHomeBtnPressed();

            }

            @Override
            public void onHomeLongPressed() {
            }
        });
        mHomeWatcher.startWatch();


    }

    protected void checkForSettingsUpdate() {
    }

    public boolean hasOptionMenu(BaseActivity obj) {
        if (obj instanceof WebViewActivity || obj instanceof LoginActivity || obj instanceof ProfileDetailActivity || obj instanceof GalleryActivity || obj instanceof ChatActivity || obj instanceof FullScreenActivity || obj instanceof ReRoutingActivity)
            return false;
        return true;
    }

    //==============================soundTest===================================


    protected void showOkAlert(String msg, String title, final int tag) {
        try {
            if (msg != null) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                if (title.length() != 0) {
                    dialog.setTitle(title);
                }
                dialog.setMessage(msg);
                dialog.setCancelable(false);
                dialog.setNeutralButton(R.string.Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        onShowAlertOkPressed(tag);
                    }
                });
                dialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void showCustomDialog(String title, String message, final int tag) {
        if (message != null) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            if (title.length() != 0) {
                dialog.setTitle(title);
            }
            dialog.setMessage(message);
            dialog.setCancelable(false);
            dialog.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    onShowAlertOkPressed(tag);


                }
            });
            dialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    onShowAlertNotOkPressed(tag);


                }
            });

            dialog.show();
        }
    }

    protected void onShowAlertNotOkPressed(int tag) {
        if (tag == FINISH) {
            finish();
        }
    }


    protected void onShowAlertOkPressed(int tag) {
        if (tag == FINISH) {
            finish();
        }
    }

    protected ProgressDialog getProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
        }
        return progressDialog;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHomeWatcher != null) {
            mHomeWatcher.stopWatch();
        }

        LocalBroadcastManager.getInstance(this).unregisterReceiver(chatReciever);

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    protected void showCustomDialog(String title, String message, final int tag, String positiveBtnText, String negativeBtnText) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setPositiveButton(positiveBtnText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onShowAlertOkPressed(tag);
            }
        });
        dialog.setNegativeButton(negativeBtnText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onShowAlertNotOkPressed(tag);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    protected void onHomeBtnPressed() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(chatReciever);
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    protected void createBranchLink(Context context, String imgUrl, String targetUserId, String description, final OnTaskCompleteInterface taskCompleteInterface) {
        BranchUniversalObject branchUniversalObject = new BranchUniversalObject()
                .setCanonicalIdentifier("item/12345")
                .setTitle("Hotch"/*"My Content Title"*/)
                .setContentDescription(description/*"My Content Description"*/)
                .setContentImageUrl(imgUrl/*"https://example.com/mycontent-12345.png"*/)
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .addContentMetadata("targetUserId", targetUserId);/*
                            .addContentMetadata("userName", Singleton.getInstance().getUserProfile().getName())
                            .addContentMetadata("userId",Singleton.getInstance().getUserProfile().getId());*/


        LinkProperties linkProperties = new LinkProperties()
                .setChannel("facebook")
                .setFeature("sharing");
                           /* .addControlParameter("$desktop_url", "http://example.com/home")
                            .addControlParameter("$ios_url", "http://example.com/ios");*/

        branchUniversalObject.generateShortUrl(context, linkProperties, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (taskCompleteInterface != null) {
                    taskCompleteInterface.onTaskComplete();
                }
                if (error == null) {
                    Log.i("SharedLink  :", "got my Branch link to share: " + url);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_TEXT, url);
                    intent.setType("text/plain");
                    startActivity(intent);
                }
            }

        });
    }

    BroadcastReceiver chatReciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, final Intent intent) {
            Log.e("MUTEUNMUTE", "data=======" + intent.getAction());
           // Toast.makeText(context,remoteMessage.getData().get("notificationType"),Toast.LENGTH_SHORT).show();
            if (intent.getAction().equals(Constants.Chat.LOCAL_BROADCAST)) {

                if (BaseActivity.this instanceof ChatFragmentActivity || BaseActivity.this instanceof ChatActivity) {
                    try {
                        NotificationManager mNotificationManager = (NotificationManager)
                                getSystemService(NOTIFICATION_SERVICE);
                        mNotificationManager.cancelAll();
                        LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHATHEADS_KEY, new LinkedHashSet<String>());
                        LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHATSET_KEY, new HashSet<String>());
                        LocalPreferenceManager.getInstance().setPreference(Constants.Chat.PREFERENCE_CHAT_COUNT, 0);
                    } catch (Exception e) {

                    }
                }

            } else if (intent.getAction().equals(Constants.Chat.LOCAL_BROADCAST_NEW_MESSAGE)) {
                if(intent.getStringExtra("val").equals("unmatch")){
                    chatUnMatched(intent.getStringExtra("id"));
                }
                updateChatList();

            }
        }
    };

    protected void chatUnMatched(String id) {

    }

    protected void updateChatList() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        // register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);
    }

    /*// Method to manually check connection status
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        //showSnack(isConnected);
    }*/

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected, true);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected, boolean isFromBroadCast) {
        String message;
        int color;
        if (isFromBroadCast) {
            message = "Sorry! Not connected to internet";
            /*if (isConnected) {
                message = "Good! Connected to Internet";
                //   color = Color.WHITE;
            } else {
                message = "Sorry! Not connected to internet";
            }*/
        } else {
            message = getResources().getString(R.string.no_internet);
        }
        if (!isConnected) {
            color = Color.WHITE;

            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.rootLayout), message, Snackbar.LENGTH_LONG);

            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }
    }

    protected void showNoNetDialog() {
        if (inflater == null)
            inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dg_network_not_available, null, false);
        //CustomDialog dialog = new CustomDialog.CustomBuilder(this).create();
        if (dialog == null)
            dialog = new android.app.AlertDialog.Builder(this).create();
        //to listen back pressed while dg's at focus instead of an activity.
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK &&
                        event.getAction() == KeyEvent.ACTION_UP &&
                        !event.isCanceled()) {
                    //dialog.cancel();
                    finish();
                    //showDialog(DIALOG_MENU);
                    //Toast.makeText(BaseActivity.this, "back listened", Toast.LENGTH_LONG).show();
                    return true;
                }
                return false;
            }
        });
        dialog.setView(view);

        dialog.setCancelable(false);
        if (!((Activity) this).isFinishing()) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
        }

        TextViewPlus tryAgain = (TextViewPlus) view.findViewById(R.id.tryAgain);
        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTryAgainClicked();
            }
        });
    }

    //override as per requirement
    protected void onTryAgainClicked() {

    }

    public void callAppConfigService(){
        new FacebookGraphService().getAppConfigurations(this, new ProfileResponseListener() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                if (reasonOfError == null) {

                    if (Constants.ServiceConfiguration.APP_CURRENT_VERSION > BuildConfig.VERSION_CODE && Constants.ServiceConfiguration.MANDATORY_UPDATE) {
                            /*String UPDATE_APP_INTENT_ACTION="Show app update dialog";
                            String UPDATE_DG_TYPE_KEY*/

                        Intent intent = new Intent(LoginActivity.UPDATE_APP_INTENT_ACTION);
                        intent.putExtra(LoginActivity.UPDATE_DG_TYPE_KEY, LoginActivity.MANDATORY_UPDATE_TAG);
                        intent.putExtra(LoginActivity.UPDATE_MSG_KEY, Constants.ServiceConfiguration.APP_UPDATE_MESSAGE);
                        Log.w("Mandatory", "dg broadCast send");
                        LocalBroadcastManager.getInstance(BaseActivity.this).sendBroadcast(intent);
                            /*showCustomDialog("Update Available", Constants.ServiceConfiguration.APP_UPDATE_MESSAGE, "Update", "", MANDATORY_UPDATE_TAG);MANDATORY_UPDATE_TAG*/
                    } else if (Constants.ServiceConfiguration.APP_CURRENT_VERSION > BuildConfig.VERSION_CODE && !Constants.ServiceConfiguration.MANDATORY_UPDATE) {
                        if (LocalPreferenceManager.getInstance().getIntPreference("myVersionCode", 0) < Constants.ServiceConfiguration.APP_CURRENT_VERSION) {
                            Intent intent = new Intent(LoginActivity.UPDATE_APP_INTENT_ACTION);
                            intent.putExtra(LoginActivity.UPDATE_DG_TYPE_KEY, LoginActivity.UPDATE_TAG);
                            intent.putExtra(LoginActivity.UPDATE_MSG_KEY, Constants.ServiceConfiguration.APP_UPDATE_MESSAGE);
                            Log.w("UnMandatory", "dg broadCast send");
                            LocalBroadcastManager.getInstance(BaseActivity.this).sendBroadcast(intent);
                        }
 /*                               showCustomDialog("", Constants.ServiceConfiguration.APP_UPDATE_MESSAGE, "Update", "NotNow", UPDATE_TAG);*/
                    }
                }else{
                    ((TryAgainForAppConfig)BaseActivity.this).appConfigTryAgainClicked();
                    /*if (activity instanceof LoginActivity){
                        ((TryAgainForAppConfig)activity).onTryAgainClicked();
                    }*/
                }
            }
        });
    }
}
