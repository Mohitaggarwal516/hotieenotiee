package com.codeplaylabs.hotch.firebase;

import android.util.Log;

import com.codeplaylabs.hotch.firebase.services.FirebaseServices;
import com.codeplaylabs.hotch.utils.Singleton;
import com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager;
import com.google.firebase.iid.FirebaseInstanceId;

import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by sachingupta on 30/11/16.
 */

public class MyFirebaseIdService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";
    private static final String FRIENDLY_ENGAGE_TOPIC = "friendly_engage";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        LocalPreferenceManager.getInstance().initialise(this);
        com.codeplaylabs.hotch.utils.LocalPreferenceManager.getInstance().initialise(this);

        sendRegistrationToServer(refreshedToken);
//        FirebaseMessaging.getInstance()
//                .subscribeToTopic(FRIENDLY_ENGAGE_TOPIC);
    }

    private void sendRegistrationToServer(String refreshedToken) {
        LocalPreferenceManager.getInstance().setPreference("firebaseToken", refreshedToken);
        String userId = com.codeplaylabs.hotch.utils.LocalPreferenceManager.getInstance().getUserId();
        String fireBaseToken = LocalPreferenceManager.getInstance().getStringPreference("firebaseToken", null);
        if (userId != null && fireBaseToken != null && Singleton.getInstance().activity != null) {
            new FirebaseServices().sendTokenToServer(Singleton.getInstance().activity.get(), userId, fireBaseToken, null);

        }
    }
}
