package com.codeplaylabs.hotch.chats.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.chats.models.firbaseModels.FriendlyMessage;
import com.codeplaylabs.hotch.customUi.CircleImageView;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import github.ankushsachdeva.emojicon.EmojiconTextView;

/**
 * Created by mohit on 09/09/17.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {
    private final String myImage;
    private final String otherImage;
    private int MY_LOCATION ;
    private final Activity activity;
    private final ArrayList<FriendlyMessage> messages;
    private final ChatListener chatLister;

    public ChatAdapter(Activity activity, ArrayList<FriendlyMessage> friendlyMessages,
            int myLocation, String otherImage, ChatAdapter.ChatListener chatListener) {
        this.activity = activity;
        this.messages = friendlyMessages;
        this.chatLister = chatListener;
        this.MY_LOCATION = myLocation;
        this.otherImage = otherImage;
        this.myImage = LocalPreferenceManager.getInstance().getUserImage();
    }

    @Override
    public ChatAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_message, parent, false);
        return new ChatAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ChatAdapter.MyViewHolder viewHolder, int position) {
        FriendlyMessage friendlyMessage = messages.get(position);

        if(position == messages.size() - 1 && position > 6){
            chatLister.callNextPage(friendlyMessage.getTimeStamp());
        }
        if(friendlyMessage.isSelf() == MY_LOCATION){

            viewHolder.messengerImageView.setVisibility(View.GONE);
            viewHolder.selfMessengerImageView.setVisibility(View.GONE);
            viewHolder.otherFrame.setVisibility(View.GONE);
            viewHolder.myFrame.setVisibility(View.VISIBLE);

  //          long timestamp = friendlyMessage.getTimeStamp();
//            Date date = new Date(timestamp);

            viewHolder.timeRightSide.setText(friendlyMessage.getDateStr());

        }else{

            viewHolder.messengerImageView.setVisibility(View.VISIBLE);
            viewHolder.selfMessengerImageView.setVisibility(View.GONE);
            if(otherImage == null || otherImage.trim().length() == 0) {
                Picasso.with(activity)
                        .load(R.drawable.ic_account_circle_black_36dp)
                        .into(viewHolder.messengerImageView);
            }else{
                Picasso.with(activity)
                        .load(otherImage)
                        .placeholder(R.drawable.ic_account_circle_black_36dp)
                        .into(viewHolder.messengerImageView);
            }
            viewHolder.otherFrame.setVisibility(View.VISIBLE);
            viewHolder.myFrame.setVisibility(View.GONE);
            viewHolder.timeLeftSide.setText(friendlyMessage.getDateStr());
        }
        if (friendlyMessage.getText() != null) {

            if(friendlyMessage.isSelf() == MY_LOCATION){
                viewHolder.selfMessageTextView.setText(friendlyMessage.getText());
                viewHolder.selfMessageTextView.setVisibility(View.VISIBLE);
                viewHolder.messageTextView.setVisibility(View.GONE);
            }else {
                viewHolder.messageTextView.setText(friendlyMessage.getText());
                viewHolder.messageTextView.setVisibility(TextView.VISIBLE);
                viewHolder.selfMessageTextView.setVisibility(View.GONE);
            }
            viewHolder.messageImageView.setVisibility(ImageView.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView timeLeftSide, timeRightSide;
        EmojiconTextView selfMessageTextView;
        CircleImageView selfMessengerImageView;
        LinearLayout linLay,myFrame,otherFrame;
        EmojiconTextView messageTextView;
        ImageView messageImageView;
        TextView messengerTextView;
        CircleImageView messengerImageView;

        public MyViewHolder(View v) {
            super(v);
            messageTextView = (EmojiconTextView) itemView.findViewById(R.id.messageTextView);
            selfMessageTextView = (EmojiconTextView) itemView.findViewById(R.id.sendermessageTextView);
            messageImageView = (ImageView) itemView.findViewById(R.id.messageImageView);
            messengerTextView = (TextView) itemView.findViewById(R.id.messengerTextView);
            messengerImageView = (CircleImageView) itemView.findViewById(R.id.messengerImageView);
            linLay = (LinearLayout) itemView.findViewById(R.id.zxc);
            selfMessengerImageView = (CircleImageView) itemView.findViewById(R.id.selfMessengerImageView);
            myFrame = (LinearLayout) itemView.findViewById(R.id.frame_me);
            otherFrame = (LinearLayout) itemView.findViewById(R.id.frame);
            timeLeftSide = (TextView) itemView.findViewById(R.id.time_left_side);
            timeRightSide = (TextView) itemView.findViewById(R.id.time_right_side);


        }
    }

    public interface ChatListener {
        void callNextPage(long timeStamp);
    }

}
