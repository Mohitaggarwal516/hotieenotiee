package com.codeplaylabs.hotch.settings.interfaces;

import android.graphics.Bitmap;

/**
 * Created by mohit on 30/06/17.
 */

public interface GalleryListListener {
    void onItemClick(int position, Object item, Bitmap bitmap);
}
