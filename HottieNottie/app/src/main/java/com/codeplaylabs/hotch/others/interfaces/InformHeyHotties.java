package com.codeplaylabs.hotch.others.interfaces;

import android.widget.ImageView;

import com.codeplaylabs.hotch.home.models.ProfileModel;

/**
 * Created by HP on 23-Aug-17.
 */

public interface InformHeyHotties {
    public void callNextPage(int position);
    public void onCellClicked(ProfileModel profileModel,int position);
    public void onCrossClicked(ProfileModel profileModel, ImageView view);
}
