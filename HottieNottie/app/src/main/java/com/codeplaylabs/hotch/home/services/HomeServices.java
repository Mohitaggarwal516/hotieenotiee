package com.codeplaylabs.hotch.home.services;

import android.app.Activity;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseService;
import com.codeplaylabs.hotch.database.tables.FilterTable;
import com.codeplaylabs.hotch.database.tables.ProfileTable;
import com.codeplaylabs.hotch.home.interfaces.ProfileSyncResponse;
import com.codeplaylabs.hotch.home.interfaces.UserConfiguration;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.home.parsers.ParseProfiles;
import com.codeplaylabs.hotch.home.parsers.ParseSingleProfile;
import com.codeplaylabs.hotch.home.parsers.UserConfigurationParser;
import com.codeplaylabs.hotch.settings.interfaces.ProfileResponseListener;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.utils.Singleton;
import com.codeplaylabs.hotch.utils.URLs;
import com.codeplaylabs.hotch.webConnection.WebConnectionModel;
import com.codeplaylabs.hotch.home.handler.HomeDBHandlerClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by mohit on 21/07/17.
 */

public class HomeServices extends BaseService {
    public static final int PERSIST_FILTERS = 1;
    public static final int DO_NOT_PERSIST_FILTERS = 0;
    public static final int SHOULD_RESET_VALUES = 1;
    public static final int SHOULD_NOT_RESET_VALUES = 0;


    public void getNextCard(Activity activity, int persist, int reset, final ProfileListener profileListener) {

        FilterTable filter = HomeDBHandlerClass.getFilterFromTable();
        if (filter!=null) {
            final HashMap<String, String> params = new HashMap<>();
            params.put("filter", filter.getFilterKey());
            params.put("type", ProfileTable.Type.NOT_SEEN.toString());
            params.put("profile_taken", String.valueOf(false));
            boolean dataSent = false;

            final ProfileTable profile = HomeDBHandlerClass.getFirstProfileFromTable(params);
            if (profile != null) {
                try {
                    ProfileModel profileModel = new ParseSingleProfile().getParsedProfile(new JSONObject(profile.getProfile()));
                    profileModel.setProfileTable(profile);
                    profileListener.profile(profileModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dataSent = true;
            }
            if (HomeDBHandlerClass.getCountOfProfilesFromTable(params) <= 3 || profile == null) {
                final boolean finalDataSent = dataSent;
                if (reset == HomeServices.SHOULD_RESET_VALUES) {
                    HomeDBHandlerClass.clearTable(HomeDBHandlerClass.dataBaseTables.profileTable);
                }
                if (Singleton.getInstance().filterRecieved) {
                    callDataFromService(activity, filter, persist, reset, new ProfileTableListener() {
                        @Override
                        public void profile(ProfileTable profileTable) {
                            if (!finalDataSent) {
                                final ProfileTable profile = HomeDBHandlerClass.getFirstProfileFromTable(params);
                                try {
                                    if (profile == null) {
                                        profileListener.profile(null);
                                    }
                                    ProfileModel profileModel = new ParseSingleProfile().getParsedProfile(new JSONObject(profile.getProfile()));
                                    profileModel.setProfileTable(profile);
                                    profileListener.profile(profileModel);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    profileListener.profile(null);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    profileListener.profile(null);
                                }

                            }
                        }
                    });
                } else {
                    profileListener.profile(null);
                }

            }
        }

    }

    public void callDataFromService(Activity activity, FilterTable filter, int persist, int reset, final ProfileTableListener profileResponseListener) {

        try {
            final JSONObject params = new JSONObject(filter.getFilter());

            params.put("id", LocalPreferenceManager.getInstance().getUserId());
            params.put("shouldPersistFilters", persist);
            params.put("shouldResetSeenProfiles", reset);

            WebConnectionModel webConnectionModel = new WebConnectionModel();
            webConnectionModel.setForceRefresh(true)
                    .setRequestType(WebConnectionModel.JsonRequestType.POST)
                    .setJsonRequest(params.toString())
                    .setUrl(URLs.FETCH_PROFILES);
            callService(activity, webConnectionModel, new ParseProfiles(), new ProfileResponseListener() {
                @Override
                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                    if (profileResponseListener != null) {
                        profileResponseListener.profile(null);
                    }

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getPreviousCard(Activity activity, ProfileListener profileListener) {
        final HashMap<String, String> params = new HashMap<>();
        params.put("type", ProfileTable.Type.NOT_SEEN.toString());
        params.put("profile_taken", String.valueOf(true));
        long c = System.currentTimeMillis();
        params.put("timestamp", LocalPreferenceManager.getInstance().getStringPreference("timestmp", String.valueOf(c)));
        final ProfileTable profile = HomeDBHandlerClass.getLastSeenProfile(params);
        if (profile != null) {
            try {
                /*profile.setType(ProfileTable.Type.NOT_SEEN.toString());
                ChatDBHandlerClass.updateProfile(profile);*/
                ProfileModel profileModel = new ParseSingleProfile().getParsedProfile(new JSONObject(profile.getProfile()));
                profileModel.setProfileTable(profile);
                LocalPreferenceManager.getInstance().setPreference("timestmp", String.valueOf(profile.getTimestamp()));


                profileListener.profile(profileModel);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {
            //  LocalPreferenceManager.getInstance().setPreference("timestmp", null);
            profileListener.profile(null);
        }
    }

    public void sendDataToServers(Activity activity, final List<ProfileTable> profiles, ProfileSyncResponse profileSyncResponse) {

        try {
            final JSONObject params = new JSONObject();
            JSONArray array = new JSONArray();
            if (profiles == null || profiles.size() < 1) {
                return;
            }
            for (ProfileTable pr :
                    profiles) {
                ParseSingleProfile parseSingleProfile = new ParseSingleProfile();
                ProfileModel profileModel = parseSingleProfile.getDataToSendToServers(new JSONObject(pr.getProfile()));
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("targetUserId", profileModel.getId());
                jsonObject.put("isMatch", profileModel.isMatch());
                jsonObject.put("action", pr.getType());
                array.put(jsonObject);
            }
            params.put("usersHotNotStatusUpdate", array);

            params.put("id", LocalPreferenceManager.getInstance().getUserId());

            WebConnectionModel webConnectionModel = new WebConnectionModel();
            webConnectionModel.setForceRefresh(true)
                    .setRequestType(WebConnectionModel.JsonRequestType.POST)
                    .setJsonRequest(params.toString())
                    .setUrl(URLs.SEND_HOT_NOT_DATA_TO_SERVERS);
            callService(activity, webConnectionModel, null, new ProfileSyncResponse() {
                @Override
                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                    Singleton.getInstance().hotOrNotStatusUpdated = false;
                    if (jsonString != null && jsonString.contains("true")) {
                        for (ProfileTable profile :
                                profiles) {
                            profile.setSynced(true);
                            HomeDBHandlerClass.updateProfile(profile);
                        }
                    }

                }
            });

            if (LocalPreferenceManager.getInstance().getLongPreference("lastDeletionTime") < System.currentTimeMillis() - (Constants.ServiceConfiguration.CLEAR_DATABASE_AFTER_HOURS * 1000)) {
                if (HomeDBHandlerClass.deleteProfilesFromDb(System.currentTimeMillis() - (Constants.ServiceConfiguration.CLEAR_DATABASE_AFTER_HOURS * 1000))) {
                    LocalPreferenceManager.getInstance().setPreference("lastDeletionTime", System.currentTimeMillis() - (Constants.ServiceConfiguration.CLEAR_DATABASE_AFTER_HOURS * 1000));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void sendDataToServers(Activity activity, final ProfileModel profileModel, ProfileSyncResponse profileSyncResponse) {

        try {
            final JSONObject params = new JSONObject();
            JSONArray array = new JSONArray();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("targetUserId", profileModel.getId());
            jsonObject.put("isMatch", profileModel.isMatch());
            jsonObject.put("action", Constants.ServiceConfiguration.DEFAULT_ACTION_ON_SUPERHOT);
            array.put(jsonObject);
            params.put("usersHotNotStatusUpdate", array);

            params.put("id", LocalPreferenceManager.getInstance().getUserId());

            WebConnectionModel webConnectionModel = new WebConnectionModel();
            webConnectionModel.setForceRefresh(true)
                    .setRequestType(WebConnectionModel.JsonRequestType.POST)
                    .setJsonRequest(params.toString())
                    .setUrl(URLs.SEND_HOT_NOT_DATA_TO_SERVERS);
            callService(activity, webConnectionModel, null, new ProfileSyncResponse() {
                @Override
                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {


                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void getPreviousSuperhotTime(Activity activity, UserConfiguration userConfiguration) {

        String url = URLs.USER_CONFIGURATION + LocalPreferenceManager.getInstance().getUserId();
        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setUrl(url).setRequestType(WebConnectionModel.JsonRequestType.GET)
                .setForceRefresh(true).setShouldCache(false);

        callService(activity, webConnectionModel, new UserConfigurationParser(), new UserConfiguration() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

            }
        });
    }

    public interface ProfileListener {
        void profile(ProfileModel profile);
    }

    public interface ProfileTableListener {
        void profile(ProfileTable profileTable);
    }
}
