package com.codeplaylabs.hotch.chats.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.base.BaseFragment;
import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.chats.adapters.ChatListAdapter;
import com.codeplaylabs.hotch.chats.handler.ChatDBHandlerClass;
import com.codeplaylabs.hotch.chats.interfaces.ChatListResponse;
import com.codeplaylabs.hotch.chats.models.ChatListWrapper;
import com.codeplaylabs.hotch.chats.models.SubProfileModel;
import com.codeplaylabs.hotch.chats.models.firbaseModels.UserModel;
import com.codeplaylabs.hotch.chats.services.ChatServices;
import com.codeplaylabs.hotch.chats.ui.ChatActivity;
import com.codeplaylabs.hotch.chats.ui.ChatFragmentActivity;
import com.codeplaylabs.hotch.database.tables.ChatUsersList;
import com.codeplaylabs.hotch.database.tables.DeviceTable;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.login.models.UserImage;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.utils.ServiceSingleton;
import com.codeplaylabs.hotch.utils.Singleton;
import com.codeplaylabs.hotch.utils.TextViewPlus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 01-Sep-17.
 */

public class NewChatListFragment extends BaseFragment {


    View view;
  private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private String userId;
     private ChatListAdapter chatListAdapter;
    private TextViewPlus noListTextView;
    private Context context;
    private boolean serviceRecieved = false;
    private RelativeLayout loader;

    public NewChatListFragment() {

    }

    public static NewChatListFragment newInstance() {
        NewChatListFragment fragment = new NewChatListFragment();
//        Bundle args = new Bundle();
//        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        view = inflater.inflate(R.layout.fragment_chat_new, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_new_chat);
        noListTextView = (TextViewPlus) view.findViewById(R.id.no_chat_list_text);
        loader = (RelativeLayout)view.findViewById(R.id.loader_layout);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh_new_chat);
        userId = LocalPreferenceManager.getInstance().getUserId();


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                callListListener(0);
            }
        });

        callListListener(0);
        return view;
    }

    public void callListListener(int pageNo) {

        if (!ServiceSingleton.getInstance().isChatWithoutMsgServiceRunning() && ServiceSingleton.getInstance().getSecModel() == null) {
            loader.setVisibility(View.VISIBLE);
            new ChatServices().getChatWithoutMessages((Activity) context, pageNo, new ChatListResponse() {
                @Override
                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                    serviceRecieved = true;
                    loader.setVisibility(View.GONE);
                    if(baseModel != null && reasonOfError == null) {
                        setUpChatList((ChatListWrapper)baseModel);
                        NewChatListFragment.this.onStart();
                    }else{
                        showNoNetDialogFrag(context);

                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }else{
            loader.setVisibility(View.GONE);
            serviceRecieved = true;
            setUpChatList((ChatListWrapper) ServiceSingleton.getInstance().getSecModel());
            NewChatListFragment.this.onStart();
        }
    }

    @Override
    protected void onTryAgainClicked() {
        //Toast.makeText(this, "tryAgain", Toast.LENGTH_SHORT).show();
        dialog.dismiss();
        dialog=null;
        startActivity(new Intent(context, ChatFragmentActivity.class));
        ((Activity)context).finish();
        super.onTryAgainClicked();
    }

    public void setUpChatList(ChatListWrapper chatListWrapper) {
        if(chatListWrapper == null){
            return;
        }
        ArrayList<SubProfileModel> list = chatListWrapper.getProfileModels();
        ArrayList<UserModel> userModels = new ArrayList<>();

        for (SubProfileModel subProfileModel : list) {
            String[] arrayList = subProfileModel.getChatId().split("_");
            ArrayList<String> array = new ArrayList<>();
            array.add(arrayList[0]);
            array.add(arrayList[1]);
            ChatUsersList chatUsersList = new ChatUsersList();
            // UserModel userModel = new UserModel();
            ProfileModel profile = subProfileModel.getProfileModel();

            chatUsersList.setUserId(profile.getId());
            chatUsersList.setId(subProfileModel.getChatId());
            chatUsersList.setMatchDate(subProfileModel.getMatchDate());
            chatUsersList.setName(profile.getName());
            chatUsersList.setImageUrl(profile.getImages().get(0).getImageUrl(UserImage.ImgType.SIZE_100x100));
            chatUsersList.setLastMessage(subProfileModel.getLastMessage());
            chatUsersList.setGender(profile.getGender());
            chatUsersList.setLastMessageTimestamp(subProfileModel.getLastMessageTimeStamp());
            chatUsersList.setSelf(array.indexOf(subProfileModel.getMessageSender()));
            chatUsersList.setIsNotificationDisabledBySelf(subProfileModel.getIsNotificationDisabledBySelf());
            chatUsersList.setIsNotificationDisabledByPartner(subProfileModel.getIsNotificationDisabledByPartner());

            ChatDBHandlerClass.deleteDevicesForUserId(subProfileModel.getProfileModel().getId());
            for (SubProfileModel.Devices device : subProfileModel.getDevices()) {
                DeviceTable deviceTable = new DeviceTable();
                deviceTable.setDeviceType(device.getDeviceType());
                deviceTable.setDeviceTokens(device.getDeviceTokens());
                deviceTable.setUserId(subProfileModel.getProfileModel().getId());
                ChatDBHandlerClass.saveDataInTable(ChatDBHandlerClass.dataBaseTables.deviceTable,
                        deviceTable);

            }
            ChatUsersList chatUser = ChatDBHandlerClass.getChatFromTable(chatUsersList.getId());
            if (chatUser != null) {
                if (chatUser.getLastMessageTimestamp() > subProfileModel.getLastMessageTimeStamp()) {
                    chatUsersList.setLastMessage(chatUser.getLastMessage());
                    chatUsersList.setLastMessageTimestamp(chatUser.getLastMessageTimestamp());
                    chatUsersList.setSelf(chatUser.isSelf());

                }

                if (LocalPreferenceManager.getInstance().getLongPreference(Constants.Chat.LAST_NEW_CHAT_TIME,0) < subProfileModel.getMatchDate()) {
                    chatUsersList.setIsNew("true");
                }
            }

            ChatDBHandlerClass.saveDataInTable(ChatDBHandlerClass.dataBaseTables.chatListTable,
                    chatUsersList);

        }


    }

    private void setAdapter(List<UserModel> users) {
        boolean type = false;


        if (/*chatListAdapter == null && */users != null && users.size() > 0) {
            noListTextView.setVisibility(View.GONE);
            chatListAdapter = new ChatListAdapter(getActivity(), users, ChatListAdapter.chatListEnum.NEW, new NewChatListFragment.ChatListListener() {
                @Override
                public void onItemClicked(int position, UserModel user) {
                    UserModel userModel = new UserModel();
                    userModel.setIsNew("false");
                    userModel.setTimestamp(user.getTimestamp()).setChatId(user.getChatId())
                            .setName(user.getName());
                    Intent intent = new Intent(getActivity(), ChatActivity.class);
                    intent.putExtra("otherUser", user);
                    if(Singleton.getInstance().isNetworkAvailable()) {
                        startActivity(intent);
                    }else{
                        Toast.makeText(getActivity(),"No internet connection, please try again.",Toast.LENGTH_SHORT).show();
                    }
                }

            });

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(chatListAdapter);
        } else if (users != null && users.size() > 0) {
            noListTextView.setVisibility(View.GONE);
            chatListAdapter.notifyDataSetChanged();
        } else {
            noListTextView.setVisibility(View.VISIBLE);
        }
    }


    public interface ChatListListener {
        void onItemClicked(int position, UserModel userModel);
    }


    @Override
    public void onStart() {
        try {
            super.onStart();
            if (serviceRecieved) {
                setValueForAdapter();
            }
        } catch (Exception e) {

        }
    }

    public void setValueForAdapter() {

        List<ChatUsersList> chats = ChatDBHandlerClass.getAllMatchesFromTableOrderByMatchDesc();

        if(chats != null) {
            ArrayList<UserModel> userModels = new ArrayList<>();

            for (ChatUsersList chat : chats) {
                UserModel userModel = new UserModel();

                userModel.setUserId(chat.getUserId());
                userModel.setGender(chat.getGender());

                userModel.setName(chat.getName());
                userModel.setChatId(chat.getId());
                userModel.setTimestamp(chat.getMatchDate());
                userModel.setIsNew(chat.getIsNew());
                userModel.setImage(chat.getImageUrl());
                userModels.add(userModel);
            }

            setAdapter(userModels);
        }else{
            // text to display
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalPreferenceManager.getInstance().setPreference(Constants.Chat.LAST_NEW_CHAT_TIME,System.currentTimeMillis());
    }
}