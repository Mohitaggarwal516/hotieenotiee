package com.codeplaylabs.hotch.others.models;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.home.models.ProfileModel;

import java.util.ArrayList;

/**
 * Created by HP on 23-Aug-17.
 */

public class ProfileWrapper extends BaseModel{
    ArrayList<ProfileModel> profileWrapper;

    int pageNo=0;

    String selfRating="";
//    String selfGender="";



    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public String getSelfRating() {
        return selfRating;
    }

    public void setSelfRating(String selfRating) {
        this.selfRating = selfRating;
    }

    public ArrayList<ProfileModel> getProfileWrapper() {
        if (profileWrapper == null) {
            profileWrapper=new ArrayList<>();
        }
        return profileWrapper;
    }
}
