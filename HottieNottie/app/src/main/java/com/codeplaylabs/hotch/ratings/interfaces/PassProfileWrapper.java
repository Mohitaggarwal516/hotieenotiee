package com.codeplaylabs.hotch.ratings.interfaces;

import com.codeplaylabs.hotch.others.models.ProfileWrapper;

/**
 * Created by HP on 30-Aug-17.
 */

public interface PassProfileWrapper {
    public void onProfilWrapperPassed(ProfileWrapper profileWrapper);
    public void onWrapperSizeZero();
}
