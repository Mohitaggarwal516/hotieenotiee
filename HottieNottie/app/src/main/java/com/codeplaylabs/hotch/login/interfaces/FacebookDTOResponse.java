package com.codeplaylabs.hotch.login.interfaces;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.interfaces.BaseInterface;
import com.codeplaylabs.hotch.login.dto.FacebookDTO;
import com.codeplaylabs.hotch.login.models.UserProfile;

/**
 * Created by mohit on 21/06/17.
 */

public interface FacebookDTOResponse extends BaseInterface {

    BaseModel showUpdateDialog(UserProfile userProfile, FacebookDTO.DialogButtonPressed dialogButtonPressed);
}
