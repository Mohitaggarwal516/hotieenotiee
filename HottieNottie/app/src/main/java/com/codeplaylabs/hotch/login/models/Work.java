package com.codeplaylabs.hotch.login.models;

import com.codeplaylabs.hotch.base.BaseModel;

/**
 * Created by mohit on 22/06/17.
 */

public class Work extends BaseModel {

    private String facebookId = "";
    private String startDate = "";
    private String endDate = "";
    private String position = "";
    private String positionFacebookId = "";
    private boolean isCurrentlyWorkingHere = false;
    private Detail location = null;
    private Detail employer = null;

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        if(endDate == null)
            endDate = "";
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
        if(endDate.length() == 0 && endDate.equalsIgnoreCase("0000-00")){
            this.isCurrentlyWorkingHere = true;
        }
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPositionFacebookId() {
        return positionFacebookId;
    }

    public void setPositionFacebookId(String positionFacebookId) {
        this.positionFacebookId = positionFacebookId;
    }

    public boolean getIsCurrentlyWorkingHere() {
        return isCurrentlyWorkingHere;
    }

    public void setIsCurrentlyWorkingHere(boolean isCurrentlyWorkingHere) {
        this.isCurrentlyWorkingHere = isCurrentlyWorkingHere;
    }

    public Detail getLocation() {
        return location;
    }

    public void setLocation(Detail location) {
        this.location = location;
    }

    public Detail getEmployer() {
        return employer;
    }

    public void setEmployer(Detail employer) {
        this.employer = employer;
    }
}
