package com.codeplaylabs.hotch.login.parsers;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.CustomJsonObject;

import org.json.JSONObject;

/**
 * Created by mohit on 28/08/17.
 */

public class ParseAppConfiguration extends BaseParser {
    BaseModel baseModel  = new BaseModel();
    @Override
    public BaseModel parseJson(JSONObject response) {
        if (validate(response) != null) {
            return moduleJson(response);
        } else {
            baseModel.error = Constants.Error.NULL_RESPONSE_ERROR;
            return baseModel;
        }
    }

    /*
      * This method is called to collect the data received from the servers in a class for the session
      * @param response
     */
    public BaseModel moduleJson(JSONObject response) {


        CustomJsonObject cjObject = new CustomJsonObject(response);
        cjObject = cjObject.getCustomJsonObject("data");
        Constants.ServiceConfiguration.APP_CURRENT_VERSION = cjObject.getInt("appCurrentVersion");
        Constants.ServiceConfiguration.APP_UPDATE_MESSAGE = cjObject.getString("appUpdateMessage");
        Constants.ServiceConfiguration.APP_DOWNLOAD_URL = cjObject.getString("downloadUrl");
        Constants.ServiceConfiguration.APP_IS_SERVICE_OUTAGE = cjObject.getBoolean("isServiceOutage",false);
        Constants.ServiceConfiguration.APP_SERVICE_OUTAGE_MESSAGE = cjObject.getString("serviceOutageMessage");
        Constants.ServiceConfiguration.SYNC_NUMBER_OF_PROFILES = Integer.parseInt(cjObject.getString("maxProfileToWaitAfterActions"));
        Constants.ServiceConfiguration.DEFAULT_ACTION_ON_SUPERHOT = cjObject.getString("defaultActionOnSuperhotChat");
        Constants.ServiceConfiguration.MANDATORY_UPDATE = cjObject.getBoolean("mandatoryUpdate",false);
        Constants.ServiceConfiguration.SUPERHOT_AVAILABLE = cjObject.getInt("numberOfSecToWaitBetweenSuperhot");
        return baseModel;

    }
}
