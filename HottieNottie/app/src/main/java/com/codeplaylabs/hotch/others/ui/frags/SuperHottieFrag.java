package com.codeplaylabs.hotch.others.ui.frags;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.database.tables.SuperLikedTable;
import com.codeplaylabs.hotch.home.handler.HomeDBHandlerClass;
import com.codeplaylabs.hotch.home.models.ProfileModel;
import com.codeplaylabs.hotch.others.Adapters.HottiesAdapter;
import com.codeplaylabs.hotch.others.interfaces.CallBackInterface;
import com.codeplaylabs.hotch.profileDetail.ui.ProfileDetailActivity;
import com.codeplaylabs.hotch.utils.TextViewPlus;

import java.util.List;

/**
 * Created by HP on 17-Aug-17.
 */

public class SuperHottieFrag extends Fragment {
    Context context;
    HottiesAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView recyclerView;
    TextViewPlus emptyListTxt;
    View view;

    @Override
    public void onAttach(Context context) {
        this.context = context;
        super.onAttach(context);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        new SpHottiesAsync().execute();
        view = inflater.inflate(R.layout.frag_heyhottiees, container, false);
        emptyListTxt = (TextViewPlus) view.findViewById(R.id.if_empty_text);
        emptyListTxt.setText("All the superhot profiles you voted will appear here. Start swiping now");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.others_recyclerView);
        layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

    }

    class SpHottiesAsync extends AsyncTask<Void, Void, List<SuperLikedTable>> {
        @Override
        protected List<SuperLikedTable> doInBackground(Void... voids) {
            return HomeDBHandlerClass.fetchSpLikes();
        }

        @Override
        protected void onPostExecute(final List<SuperLikedTable> spLikedList) {
            if (spLikedList != null && spLikedList.size() > 0)
                setAdapter(context, spLikedList, HottiesAdapter.showCrossEnum.HIDE_CROSS);
            else {
                emptyListTxt.setVisibility(View.VISIBLE);
            }//super.onPostExecute(spLikedList);
        }
    }

    private void setAdapter(final Context context, final List<SuperLikedTable> spLikedList, HottiesAdapter.showCrossEnum mEnum) {
        emptyListTxt.setVisibility(View.GONE);
        if (adapter == null) {
            adapter = new HottiesAdapter(context, spLikedList, mEnum/*HottiesAdapter.showCrossEnum.HIDE_CROSS*/, new CallBackInterface() {
                @Override
                public void onCellClicked(String id,boolean isMatch) {
                    ProfileModel model=new ProfileModel();
                    model.setId(id);
                    //Log.e("Id from Adapter ","in super hotties"+id);
                    Intent intent = new Intent(context, ProfileDetailActivity.class);
                    intent.putExtra("from", "" + ProfileDetailActivity.From.OTHER);
                    intent.putExtra("CallDescription",true);
                    intent.putExtra("profile", model);
                    startActivityForResult(intent,1);


                   /* new FacebookGraphService().profileDescriptionService(getActivity(), id, "", new SharedProfileResponse() {
                        @Override
                        public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                            if (reasonOfError == null) {
                                // ProfileModel model = (ProfileModel) baseModel;
                                ProfileWrapper wrapper = (ProfileWrapper) baseModel;
                                Intent intent = new Intent(context, ProfileDetailActivity.class);
                                intent.putExtra("from", "" + ProfileDetailActivity.From.OTHER);
                                intent.putExtra("profile", wrapper.getProfileWrapper().get(0)*//*model*//*);
                                startActivity(intent);
                            }
                        }
                    });*/
                }

                @Override
                public void onCellLongPressed(List<SuperLikedTable> spLikedList) {

                    /*int[] firstVisible = ((StaggeredGridLayoutManager) layoutManager).findFirstVisibleItemPositions(null);
                    int[] lastVisible = ((StaggeredGridLayoutManager) layoutManager).findLastVisibleItemPositions(null);
                        for (int i = firstVisible[0]; i <= lastVisible[0]; ++i) {
                            HottiesAdapter.MyViewHolder holder = (HottiesAdapter.MyViewHolder) recyclerView.findViewHolderForAdapterPosition(i);
                            ImageView cross= (ImageView) holder.itemView.findViewById(R.id.ic_cross);
                            cross.setVisibility(View.VISIBLE);
                        }
                    for (int childCount = recyclerView.getChildCount(), i = 0; i < childCount; ++i) {
                        final HottiesAdapter.MyViewHolder holder = (HottiesAdapter.MyViewHolder) recyclerView.getChildViewHolder(recyclerView.getChildAt(i));
                        ImageView cross = (ImageView) holder.itemView.findViewById(R.id.ic_cross);
                        cross.setVisibility(View.VISIBLE);
                    }*/

                }

                @Override
                public void onCrossClicked(SuperLikedTable record, ImageView view) {
                    view.setVisibility(View.GONE);
                    HomeDBHandlerClass.delSpLikes(record.id);
                    spLikedList.remove(record);
                    adapter.notifyDataSetChanged();
                    if (spLikedList.size() == 0) {
                        emptyListTxt.setVisibility(View.VISIBLE);
                    }
                }
            });
            recyclerView.setLayoutManager(layoutManager);

            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==1){

        }
    }
}

