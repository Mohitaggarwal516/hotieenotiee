package com.codeplaylabs.hotch.settings.services;

import android.app.Activity;

import com.codeplaylabs.hotch.base.BaseModel;
import com.codeplaylabs.hotch.base.BaseParser;
import com.codeplaylabs.hotch.database.tables.ServiceDataCache;
import com.codeplaylabs.hotch.login.interfaces.SharedProfileResponse;
import com.codeplaylabs.hotch.login.models.Education;
import com.codeplaylabs.hotch.login.models.UserProfile;
import com.codeplaylabs.hotch.login.models.Work;
import com.codeplaylabs.hotch.settings.handler.SettingsDbHandler;
import com.codeplaylabs.hotch.settings.interfaces.AlbumResponseListener;
import com.codeplaylabs.hotch.settings.interfaces.DiscoveryResponse;
import com.codeplaylabs.hotch.settings.interfaces.ProfileResponseListener;
import com.codeplaylabs.hotch.settings.parsers.ParseFacebookAlbumImagesResponse;
import com.codeplaylabs.hotch.settings.parsers.ParseImageUpdateResponse;
import com.codeplaylabs.hotch.utils.CommonMethods;
import com.codeplaylabs.hotch.utils.Constants;
import com.codeplaylabs.hotch.utils.LocalPreferenceManager;
import com.codeplaylabs.hotch.webConnection.WebConnectionHandler;
import com.codeplaylabs.hotch.base.BaseService;
import com.codeplaylabs.hotch.database.tables.UserImageTable;
import com.codeplaylabs.hotch.settings.models.albums.AlbumModel;
import com.codeplaylabs.hotch.settings.parsers.ParseDiscoveryResponse;
import com.codeplaylabs.hotch.settings.parsers.ParseFacebookAlbumsResponse;
import com.codeplaylabs.hotch.settings.parsers.ParseProfileById;
import com.codeplaylabs.hotch.utils.URLs;
import com.codeplaylabs.hotch.webConnection.WebConnectionModel;
import com.facebook.AccessToken;
import com.j256.ormlite.dao.Dao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mohit on 03/07/17.
 */

public class SettingsService extends BaseService {


    private Dao<ServiceDataCache, Integer> dataDao;
    private ServiceDataCache serviceDataCache;

    public void logoutService(Activity activity,SharedProfileResponse response) {
        String url = URLs.LOGOUT+LocalPreferenceManager.getInstance().getUserId()+"&device="+Constants.DEVICE;
        WebConnectionModel webConnectionModel=new WebConnectionModel();
        webConnectionModel.setForceRefresh(true).setRequestType(WebConnectionModel.JsonRequestType.GET).setUrl(url);
        callService(activity,webConnectionModel,new BaseParser(),response);
    }

    public void getProfileById(Activity activity, String userId, boolean respondAfterImmediateResponse, boolean shouldForceRefresh, ProfileResponseListener profileResponseListener) {

        String url = URLs.GET_PROFILE_BY_ID + userId;

        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setForceRefresh(shouldForceRefresh)
                .setModule(Constants.Modules.PERSONAL_PROFILE.toString())
                .setRequestType(WebConnectionModel.JsonRequestType.GET)
                .setShouldCache(true)
                .setShouldClearModule(true)
                .setShouldSendServiceResponseAfterImmediateCacheResponse(respondAfterImmediateResponse)
                .setTimeFactor(Constants.ServiceConfiguration.GET_PROFILE_BY_ID)
                .setUrl(url)
                .setJsonRequest(null);


        callService(activity, webConnectionModel, new ParseProfileById(), profileResponseListener);
    }

    private List<ServiceDataCache> getProfileListFromDb(String userId) throws SQLException {
        HashMap<String, Object> map = new HashMap<>();
        map.put("params", userId);
        map.put("url", URLs.GET_PROFILE_BY_ID + userId);
        dataDao = dbHelper.getServiceDataCacheDao();
        return dataDao.queryForFieldValues(map);
    }


    public void sendAndSaveImages(final Activity activity, final String id, final List<UserImageTable> images, ProfileResponseListener profileResponseListener) {
        sendImagesToServer(activity, id, images, new ProfileResponseListener() {
            @Override
            public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                if (baseModel != null && reasonOfError == null) {

                    List<UserImageTable> imagesFromTable = SettingsDbHandler.getAllDataFromImageTable();
                    if (imagesFromTable == null) {
                        return;
                    }
                    WebConnectionHandler webConnectionHandler = new WebConnectionHandler();
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("url", URLs.GET_PROFILE_BY_ID + LocalPreferenceManager.getInstance().getUserId());
                    ServiceDataCache serviceDataCache = (ServiceDataCache) webConnectionHandler.fetchDataFromCacheTable(activity, params);
                    if (serviceDataCache != null) {
                        webConnectionHandler.deleteDataFromCacheTable(activity, serviceDataCache);
                    }

                    for (UserImageTable image : imagesFromTable) {
                        if (!image.isSynced) {
                            image.setSynced(true).setTimeStamp(String.valueOf(System.currentTimeMillis()));
                            SettingsDbHandler.saveDataInTable(SettingsDbHandler.dataBaseTables.UserImageTable, image);
                        }
                    }
                }
            }
        });

    }

    public void sendImagesToServer(Activity activity, String id, List<UserImageTable> images, ProfileResponseListener profileResponseListener) {
        JSONObject params = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        //SettingsDbHandler.clearUserImageTable();
        try {
            params.put("id", id);
            for (UserImageTable us : images
                    ) {
                //UserImageTable userImageTable = new UserImageTable();
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("imageUrl", us.imageUrl);
                //userImageTable.setImageUrl(us.getImageUrl());

                jsonObject1.put("contentType", "images/jpeg");
                // userImageTable.setContentType(us.getContentType());

                jsonObject1.put("imageBaseUrl", us.imageBaseUrl);
                //userImageTable.setImageBaseUrl(us.getImageBaseUrl());

                jsonObject1.put("imageSource", us.imageSource.toString());
                //  userImageTable.setImageSource(us.getImageSource().toString());

                jsonObject1.put("order", us.order);
                //  userImageTable.setOrder(us.getOrder());

                if (us.imageUri != null && !us.imageUri.contains("http")) {
                    jsonObject1.put("base64Image", CommonMethods.UriToBase64(activity, us.imageUri));
                    //      userImageTable.setImageUri(us.getImageUri());
                    jsonObject1.put("imageUrl", "");

                } else if (us.imageUri != null && us.imageUri.contains("http")) {
                    jsonObject1.put("imageUrl", us.imageUri);
                    jsonObject1.put("base64Image", "");
                    //     userImageTable.setImageUri(us.getImageUri());
                    //      userImageTable.setBase64Image(us.getBase64Image());
                } else {
                    jsonObject1.put("base64Image", "");
                    //     userImageTable.setBase64Image(us.getBase64Image());

                }
                jsonArray.put(jsonObject1);
                //  userImageTable.setTimeStamp(String.valueOf(System.currentTimeMillis()));
                //   userImageTable.setSynced(false);
                //   SettingsDbHandler.saveDataInTable(SettingsDbHandler.dataBaseTables.UserImageTable,userImageTable);
            }
            params.put("images", jsonArray);

            WebConnectionModel webConnectionModel = new WebConnectionModel();
            webConnectionModel.setJsonRequest(params.toString())
                    .setModule(String.valueOf(Constants.Modules.IMAGE_MODULE))
                    .setRequestType(WebConnectionModel.JsonRequestType.POST)
                    .setShouldCache(false)
                    .setShouldClearModule(false)
                    .setTimeFactor(NO_TIME_FACTOR_REQ)
                    .setUrl(URLs.UPDATE_IMAGES)
                    .setForceRefresh(true);

            callService(activity, webConnectionModel, new ParseImageUpdateResponse(), profileResponseListener);
            //jsonObject.put()
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getAlbumsImages(Activity activity, AccessToken accessToken, AlbumModel albumModel, final AlbumResponseListener albumResponseListener) {


        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setModule(String.valueOf(Constants.Modules.IMAGE_MODULE))
                .setRequestType(WebConnectionModel.JsonRequestType.GRAPH_IMAGES)
                .setShouldCache(true)
                .setShouldClearModule(true)
                .setShouldSendServiceResponseAfterImmediateCacheResponse(true)
                .setTimeFactor(Constants.ServiceConfiguration.FB_GRAPH_IMAGES)
                .setUrl(albumModel.getUrl())
                .setFbAccessToken(accessToken)
                .setFbAlbumModel(albumModel)
                .setJsonRequest(albumModel.getId());


        callService(activity, webConnectionModel, new ParseFacebookAlbumImagesResponse(), albumResponseListener);

    }

    public void getAlbumsList(Activity activity, AccessToken accessToken, final AlbumResponseListener albumResponseListener) {


        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setModule(String.valueOf(Constants.Modules.IMAGE_MODULE))
                .setRequestType(WebConnectionModel.JsonRequestType.GRAPH_ALBUM)
                .setShouldCache(true)
                .setShouldClearModule(true)
                .setTimeFactor(Constants.ServiceConfiguration.FB_GRAPH_IMAGES)
                .setUrl(Constants.FB_ALBUMS_LIST)
                .setFbAccessToken(accessToken)
                .setJsonRequest(Constants.FB_ALBUMS_LIST);


        callService(activity, webConnectionModel, new ParseFacebookAlbumsResponse(), albumResponseListener);

    }

    public void getDiscoverySetting(Activity activity, boolean shouldForceRefresh, DiscoveryResponse discoveryResponse) {
        String userId = LocalPreferenceManager.getInstance().getUserId();

        WebConnectionModel webConnectionModel = new WebConnectionModel();
        webConnectionModel.setRequestType(WebConnectionModel.JsonRequestType.GET).setShouldCache(true)
                .setShouldClearModule(true)
                .setTimeFactor(Constants.ServiceConfiguration.UPDATE_DISCOVERY)
                .setUrl(URLs.GET_DISCOVERY_SETTING + userId)
                .setJsonRequest(null)
                .setForceRefresh(shouldForceRefresh);
        //call callService()
        callService(activity, webConnectionModel, new ParseDiscoveryResponse(), discoveryResponse);
    }

    public void setNotificationSettings(Activity activity, String id, HashMap<String, Boolean> notificationsSettings, DiscoveryResponse discoveryResponse) {

        WebConnectionHandler webConnectionHandler = new WebConnectionHandler();
        HashMap<String, Object> params = new HashMap<>();
        params.put("url", URLs.GET_DISCOVERY_SETTING + id);
        ServiceDataCache serviceDataCache = (ServiceDataCache) webConnectionHandler.fetchDataFromCacheTable(activity, params);
        if (serviceDataCache != null) {
            webConnectionHandler.deleteDataFromCacheTable(activity, serviceDataCache);
        }


        try {
            JSONObject jsonObject = new JSONObject(notificationsSettings);
            jsonObject.put("userId", id);
            WebConnectionModel webConnectionModel = new WebConnectionModel();
            webConnectionModel.setRequestType(WebConnectionModel.JsonRequestType.POST)
                    .setShouldCache(false)
                    .setUrl(URLs.UPDATE_NOTIFICATIONS)
                    .setJsonRequest(jsonObject.toString())
                    .setForceRefresh(true);
            //call callService()
            callService(activity, webConnectionModel, null, new DiscoveryResponse() {
                @Override
                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void updateProfileService(final Activity activity, final String id, UserProfile userProfile, ProfileResponseListener profileResponseListener) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
            jsonObject.put("name", userProfile.getName());
            jsonObject.put("about", userProfile.getAbout());
            jsonObject.put("gender", userProfile.getGender());
            WebConnectionModel webConnectionModel = new WebConnectionModel();
            webConnectionModel.setRequestType(WebConnectionModel.JsonRequestType.POST)
                    .setShouldCache(false)
                    .setUrl(URLs.UPDATE_PROFILE)
                    .setJsonRequest(jsonObject.toString())
                    .setForceRefresh(true);

            callService(activity, webConnectionModel, null, new ProfileResponseListener() {
                @Override
                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {


                    WebConnectionHandler webConnectionHandler = new WebConnectionHandler();
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("url", URLs.GET_PROFILE_BY_ID + id);
                    ServiceDataCache serviceDataCache = (ServiceDataCache) webConnectionHandler.fetchDataFromCacheTable(activity, params);
                    if (serviceDataCache != null) {
                        webConnectionHandler.deleteDataFromCacheTable(activity, serviceDataCache);
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void updateCurrentLocationService(final Activity activity, UserProfile userProfile, final ProfileResponseListener responseListener) {

        try {
            JSONObject params = new JSONObject();

            final String id = LocalPreferenceManager.getInstance().getUserId();
            params.put("id", id);

            JSONArray education = new JSONArray();
            for (int index = 0; index < userProfile.getEducationList().size(); index++) {
                JSONObject eduObj = new JSONObject();
                Education educationInner = userProfile.getEducationList().get(index);
                eduObj.put("type", educationInner.getType());
                eduObj.put("typeFacebookId", educationInner.getTypeFacebookId());
                eduObj.put("year", educationInner.getYear());
                eduObj.put("facebookId", educationInner.getFacebookId());
                eduObj.put("isCurrentlyStudingHere", educationInner.isCurrentlyStudingHere());

                JSONObject institute = new JSONObject();
                institute.put("name", educationInner.getInstitute().getName());
                institute.put("instituteBranchFacebookId", educationInner.getInstitute().getId());
                eduObj.put("institute", institute);

                education.put(eduObj);
            }
            params.put("education", education);

            JSONArray works = new JSONArray();
            for (int index = 0; index < userProfile.getWorkList().size(); index++) {
                JSONObject workObj = new JSONObject();
                Work workInner = userProfile.getWorkList().get(index);
                workObj.put("facebookId", workInner.getFacebookId());
                workObj.put("startDate", workInner.getStartDate());
                workObj.put("endDate", workInner.getEndDate());
                workObj.put("position", workInner.getPosition());
                workObj.put("positionFacebookId", workInner.getPositionFacebookId());
                workObj.put("isCurrentlyWorkingHere", workInner.getIsCurrentlyWorkingHere());

                JSONObject locationWorkObj = new JSONObject();
                locationWorkObj.put("city", workInner.getLocation().getName());
                locationWorkObj.put("facebookId", workInner.getLocation().getId());
                workObj.put("location", locationWorkObj);

                JSONObject employerWorkObj = new JSONObject();
                employerWorkObj.put("name", workInner.getEmployer().getName());
                employerWorkObj.put("facebookId", workInner.getEmployer().getId());
                workObj.put("employer", employerWorkObj);

                works.put(workObj);

            }

            params.put("works", works);

            WebConnectionModel webConnectionModel = new WebConnectionModel();
            webConnectionModel.setForceRefresh(true)
                    .setRequestType(WebConnectionModel.JsonRequestType.POST)
                    .setUrl(URLs.UPDATE_CURRENT_LOCATION)
                    .setJsonRequest(params.toString());

            callService(activity, webConnectionModel, null, new ProfileResponseListener() {
                @Override
                public void onResponse(BaseModel baseModel, String jsonString, String reasonOfError, Constants.ReceivedFrom receivedFrom) {
                    WebConnectionHandler webConnectionHandler = new WebConnectionHandler();
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("url", URLs.GET_PROFILE_BY_ID + id);
                    ServiceDataCache serviceDataCache = (ServiceDataCache) webConnectionHandler.fetchDataFromCacheTable(activity, params);
                    if (serviceDataCache != null) {
                        webConnectionHandler.deleteDataFromCacheTable(activity, serviceDataCache);
                    }
                    responseListener.onResponse(baseModel, jsonString, reasonOfError, receivedFrom);
                }
            });
        } catch (Exception e) {

        }
    }
}
