package com.codeplaylabs.hotch.ratings.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.codeplaylabs.hotch.R;
import com.codeplaylabs.hotch.others.models.ProfileWrapper;
import com.codeplaylabs.hotch.ratings.RatingsActivity;
import com.codeplaylabs.hotch.ratings.interfaces.CallbackInterface;
import com.codeplaylabs.hotch.ratings.interfaces.PassProfileWrapper;
import com.codeplaylabs.hotch.ratings.models.RateDiscoveryWrapper;
import com.codeplaylabs.hotch.utils.ServiceSingleton;
import com.codeplaylabs.hotch.utils.TextViewPlus;

/**
 * Created by HP on 28-Aug-17.
 */

public class RatingPagerAdapter extends PagerAdapter {
    // ArrayList<Detail> detailList;
    Context context;
    LayoutInflater inflater;
    //ProfileWrapper profileWrapper;
    CallbackInterface callBack;
    String gender;
    RateDiscoveryWrapper disWrapper;
    //ArrayList<ProfileModel> pgrProfileModelList;
    private String HOTTEST_FEMALE_TXT = "The Hottest female in ";
    private String HOTTEST_MALE_TXT = "The Hottest male in ";
    private Typeface typeface;

    /* public RatingPagerAdapter(Context context, ArrayList<Detail> detailList, ProfileWrapper profileWrapper,CallbackInterface callBack) {
         this.context = context;
         this.detailList = detailList;
         this.profileWrapper=profileWrapper;
         this.callBack=callBack;
         inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
     }*/
    public RatingPagerAdapter(Context context, RateDiscoveryWrapper disWrapper, String gender, CallbackInterface callBack) {
        this.context = context;
        this.disWrapper = disWrapper;
        this.gender = gender;
        this.callBack = callBack;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {


        View view = inflater.inflate(R.layout.eachpg_rate_viewpager, container, false);
        TextView place = (TextView) view.findViewById(R.id.place);
        final TextView emptytext= (TextView) view.findViewById(R.id.empty_txt);
        final TextView hottestTxt = (TextView) view.findViewById(R.id.hottest_txt);
        final TextViewPlus displayRank = (TextViewPlus) view.findViewById(R.id.dp_rating);
        final LinearLayout rankLayout= (LinearLayout) view.findViewById(R.id.rankLayout);
        final ProgressBar progressBar= (ProgressBar) view.findViewById(R.id.prgBar);
        progressBar.setVisibility(View.VISIBLE);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_rate_test);


        if (position == (getCount() - 1)) {
            callBack.passRecyclerView(recyclerView, RateDiscoveryWrapper.filterEnum.Random, "", /*pgrProfileModelList,*/ new PassProfileWrapper() {
                @Override
                public void onProfilWrapperPassed(final ProfileWrapper profileWrapper) {
                    progressBar.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                    if (!gender.equalsIgnoreCase(ServiceSingleton.getInstance().getSelfProfile().getGender()/*RatingsActivity.FEMALE*/)) {
                       // displayRank.setVisibility(View.GONE);
                        rankLayout.setVisibility(View.GONE);
                    } else {


                        displayRank.post(new Runnable() {
                            @Override
                            public void run() {
                                displayRank.setText("#" + profileWrapper.getSelfRating());
                            }
                        });

                    }
                }

                @Override
                public void onWrapperSizeZero() {
                    emptytext.setVisibility(View.VISIBLE);
                    setTypeface(emptytext);
                }
            });
            place.setText("World");
        } else {
            callBack.passRecyclerView(recyclerView, disWrapper.getmEnumList().get(position), disWrapper.getDiscoveryWrapper().get(position).getId(), /*pgrProfileModelList,*/ new PassProfileWrapper() {
                @Override
                public void onProfilWrapperPassed(final ProfileWrapper profileWrapper) {
                    progressBar.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                    if (!gender.equalsIgnoreCase(ServiceSingleton.getInstance().getSelfProfile().getGender()/*gender.equalsIgnoreCase(RatingsActivity.FEMALE*/)) {
                        //displayRank.setVisibility(View.GONE);
                        rankLayout.setVisibility(View.GONE);
                    } else {
                        displayRank.post(new Runnable() {
                            @Override
                            public void run() {
                                displayRank.setText(/*My Rank : */"# " + profileWrapper.getSelfRating());
                            }
                        });
                       // displayRank.setText(/*My Rank : */"# " + profileWrapper.getSelfRating());
                    }
                }

                @Override
                public void onWrapperSizeZero() {
                    emptytext.setVisibility(View.VISIBLE);
                    setTypeface(emptytext);
                }
            });
            place.setText(disWrapper.getDiscoveryWrapper().get(position).getName()/*objAtPos.getName()*/);
        }


        if (gender.equalsIgnoreCase(RatingsActivity.MALE)) {
            hottestTxt.setText(HOTTEST_MALE_TXT);
        } else {
            hottestTxt.setText(HOTTEST_FEMALE_TXT);
        }
        setTypeface(hottestTxt);
        setTypeface(place);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (View) object);
    }

    @Override
    public int getCount() {
        return disWrapper.getDiscoveryWrapper().size() + 1;//detailList.size() /*+ 1*/;
    }

    private void setTypeface(TextView view) {

        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getAssets(), "noteworthy.otf");
        }
            view.setTypeface(typeface);
    }
}
