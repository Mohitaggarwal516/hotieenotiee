package com.codeplaylabs.hotch.profileDetail.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.codeplaylabs.hotch.login.models.Detail;
import com.codeplaylabs.hotch.login.models.Education;
import com.codeplaylabs.hotch.login.models.UserProfile;
import com.codeplaylabs.hotch.login.models.Work;
import com.codeplaylabs.hotch.profileDetail.interfaces.ProfileDetailOnClickListener;
import com.codeplaylabs.hotch.utils.CommonMethods;
import com.codeplaylabs.hotch.R;

import org.apmem.tools.layouts.FlowLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by HP on 05-Jul-17.
 */

public class myListViewAdapter extends ArrayAdapter<UserProfile> {

    private final ArrayList<UserProfile> userProfile;
    private final Context context;
    private ProfileDetailOnClickListener profileDetailOnClickListener;
    //private String TAG="mListViewAdapter";

    public myListViewAdapter(Context context, ArrayList<UserProfile> userProfile, ProfileDetailOnClickListener profileDetailOnClickListener) {
        super(context, R.layout.row_parallax_listview, userProfile);
        this.context = context;
        this.userProfile = userProfile;
        this.profileDetailOnClickListener = profileDetailOnClickListener;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable final View convertView, @NonNull ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        View rowView;
        rowView = convertView;
        if (convertView == null) {
            //view = context.getLayoutInflater().inflate(R.layout.dashboard_grid_row,null);
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_parallax_listview, null);
        }
        TextView nameAge = (TextView) rowView.findViewById(R.id.name);
        TextView age = (TextView) rowView.findViewById(R.id.age);
        TextView about = (TextView) rowView.findViewById(R.id.about);
        TextView currAt = (TextView) rowView.findViewById(R.id.currently_at_text);
        CardView aboutCard = (CardView) rowView.findViewById(R.id.about_me_card);
        CardView currAtCard = (CardView) rowView.findViewById(R.id.curr_at_card);
        CardView interestCard = (CardView) rowView.findViewById(R.id.interest_card);
        TextView currentLocation = (TextView) rowView.findViewById(R.id.currentLocation);
        ImageView locPin= (ImageView) rowView.findViewById(R.id.locPin);
        //RecyclerView recyclerView= (RecyclerView) rowView.findViewById(R.id.recyclerViewInListView);
        final ImageView share= (ImageView) rowView.findViewById(R.id.share);
        final ProgressBar shareLoader= (ProgressBar) rowView.findViewById(R.id.shareLoader);
        //share.setVisibility(View.v);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareLoader.setVisibility(View.VISIBLE);
                share.setVisibility(View.GONE);
                profileDetailOnClickListener.shareMyProfile(context,userProfile.get(position),position,shareLoader,share);
            }
        });
        if(userProfile.get(position).getInterestList().size()==0){

            interestCard.setVisibility(View.GONE);
        }

        if (userProfile.get(position).getAbout() != null && userProfile.get(position).getAbout().length() > 0) {
            aboutCard.setVisibility(View.VISIBLE);
            about.setText(userProfile.get(position).getAbout());
        } else {
            aboutCard.setVisibility(View.GONE);
        }
        if (userProfile.get(position).getWorkList() != null && userProfile.get(position).getWorkList().size() > 0) {

            Work work = null;
            for (Work work1 : userProfile.get(position).getWorkList()) {
                if (work1.getIsCurrentlyWorkingHere()) {
                    work = work1;
                    break;
                }
            }
            if (work != null) {
                currAtCard.setVisibility(View.VISIBLE);
               // currAt.setText(work.getPosition() + "(" + work.getEmployer().getName() + ")");
                currAt.setText(work.getPosition()!=null?work.getPosition() + "(" + work.getEmployer().getName() + ")":work.getEmployer().getName());
            } else {
                currAtCard.setVisibility(View.GONE);
            }
        }
        if (currAtCard.getVisibility() == View.GONE && userProfile.get(position).getEducationList() != null && userProfile.get(position).getEducationList().size() > 0) {
            Education education = null;
            for (Education education1 : userProfile.get(position).getEducationList()) {

                if (education1.isCurrentlyStudingHere()) {
                    education = education1;
                    break;
                }

            }
            if (education != null) {
                currAtCard.setVisibility(View.VISIBLE);
                currAt.setText(education.getInstitute().getName());
            } else {
                currAtCard.setVisibility(View.GONE);

            }
        }


        ImageView profileDetail_Menu = (ImageView) rowView.findViewById(R.id.menu_profileDetail);
        profileDetail_Menu.setVisibility(View.GONE);
       /* profileDetail_Menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //callback menu clicked
                profileDetailOnClickListener.onMenuClick(v);
            }
        });*/
        FlowLayout flowLayout = (FlowLayout) rowView.findViewById(R.id.linLay_test);
        Calendar dob = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

        try {
            dob.setTime(sdf.parse(userProfile.get(position).getBirthday()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            nameAge.setText(userProfile.get(position).getName());
            age.setText("" + CommonMethods.calculateAge(dob));
        } catch (Exception e) {
            e.printStackTrace();
        }
        about.setText(userProfile.get(position).getAbout());
        if(userProfile.get(position).getLocation().getName().length()>0) {
            currentLocation.setText(userProfile.get(position).getLocation().getName());
        }else
        {
            currentLocation.setVisibility(View.GONE);
            locPin.setVisibility(View.GONE);
        }
        for (Detail interList : userProfile.get(position).getInterestList()
                ) {
            TextView textView = new TextView(context);
            textView.setBackground(context.getResources().getDrawable(R.drawable.bg_interest_profile_detail/*bg_interest_recyclerview_txtvw*/));
            textView.setPadding(8, 8, 8, 8);
            textView.setGravity(Gravity.CENTER);

            FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(10, 10, 10, 10);
            textView.setLayoutParams(params);

            textView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            textView.setTextSize(14);
            textView.setMaxLines(1);
            textView.setMaxEms(15);
            textView.setEllipsize(TextUtils.TruncateAt.END);
            textView.setText(interList.getName());
            flowLayout.setPadding(5, 5, 5, 5);
            flowLayout.addView(textView);
        }
        return rowView;
    }
}
