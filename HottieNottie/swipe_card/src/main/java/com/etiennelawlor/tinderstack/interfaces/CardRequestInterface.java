package com.etiennelawlor.tinderstack.interfaces;

import android.content.Context;

import com.etiennelawlor.tinderstack.ui.TinderCardView;

/**
 * Created by mohit on 16/08/17.
 */

public interface CardRequestInterface {

    TinderCardView getCard(Context context);

}
