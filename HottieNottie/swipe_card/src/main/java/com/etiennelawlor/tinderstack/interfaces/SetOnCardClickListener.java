package com.etiennelawlor.tinderstack.interfaces;

import com.etiennelawlor.tinderstack.ui.TinderStackLayout;

/**
 * Created by mohit on 22/06/17.
 */

public interface SetOnCardClickListener {


    void onBackClick(String id);
    void onSuperHotClick(String id);
    void onHotClick(String id);
    void onNotClick(String id);

    void getNextCard(TinderStackLayout.GetCard tinderCardView);
    void getPreviousCard(TinderStackLayout.GetCard tinderCardView);

    boolean shouldRemoveView(String id);

    void onViewRemoved(String id);

    void onCardClicked(String id);

    void showSuperHotDialog(String dbId);
}
