package com.etiennelawlor.tinderstack.ui;

import android.animation.Animator;
import android.app.Activity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.etiennelawlor.tinderstack.R;
import com.etiennelawlor.tinderstack.bus.RxBus;
import com.etiennelawlor.tinderstack.bus.events.TopCardMovedEvent;
import com.etiennelawlor.tinderstack.interfaces.SetOnCardClickListener;
import com.etiennelawlor.tinderstack.models.User;
import com.etiennelawlor.tinderstack.utilities.DisplayUtility;
import com.etiennelawlor.tinderstack.utilities.LocalPreferenceManager;

import imagelib.codeplaylaps.com.customimglib.SuperPicasso;

import static android.os.Build.VERSION_CODES.M;

/**
 * Created by etiennelawlor on 11/17/16.
 */

public class TinderCardView extends FrameLayout implements View.OnTouchListener, Animator.AnimatorListener {

    private Activity context = null;
    public int pos;
    // region Constants
    private static final float CARD_ROTATION_DEGREES = 40.0f;
    private static final float BADGE_ROTATION_DEGREES = 15.0f;
    private static final int DURATION = 150;
    // endregion

    // region Views
    private ImageView imageView;
    private TextView displayNameTextView;
    private TextView usernameTextView;
    private ImageView likeTextView, superHotView;
    private ImageView nopeTextView;
    // endregion

    // region Member Variables
    private float oldX;
    private float oldY;
    private float newX;
    private float newY;
    private float dX;
    private float dY;
    private float rightBoundary;
    private float leftBoundary;
    private int screenWidth;
    private int padding;
    //////////////////////////////////////
    private int screenHeight;
    private float topBoundary;
    public LinearLayout backBtn, hotBtn, superHotBtn, notBtn;
    private SetOnCardClickListener cardClickListener;

    private String dbId;
    public boolean btnClicked = false;
    private RelativeLayout completeCard;
    private GestureDetector gestureDetector;
    private ViewPropertyAnimator var;


    //public SuperPicasso superPicasso = new SuperPicasso();
    ////////////////////////////////////
    // endregion

    public String getdbId() {
        return dbId;
    }

    public void setdbId(String dbId) {
        this.dbId = dbId;
    }

    // region Constructors
    public TinderCardView(Activity context) {
        super(context);
        init(context, null);
        this.context = context;
        LocalPreferenceManager.getInstance().initialise(context);
    }

    public TinderCardView(Activity context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TinderCardView(Activity context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }
    // endregion

    public void onReuse() {
        setOnTouchListener(this);
    }

    // region View.OnTouchListener Methods
    @Override
    public boolean onTouch(final View view, MotionEvent motionEvent) {
        Log.e("touch", "listen");

        TinderStackLayout tinderStackLayout = ((TinderStackLayout) view.getParent());
        TinderCardView topCard = (TinderCardView) tinderStackLayout.getChildAt(tinderStackLayout.getChildCount() - 1);
        Log.e("touchView", view + " -- " + topCard);
        if (topCard.equals(view)) {

            if (gestureDetector.onTouchEvent(motionEvent)) {
                cardClickListener.onCardClicked(topCard.dbId);
                return true;
            }
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    Log.e("touch", "down");
                    oldX = motionEvent.getX();
                    oldY = motionEvent.getY();

                    // cancel any current animations
                    view.clearAnimation();
                    return true;
                case MotionEvent.ACTION_UP:
                    Log.e("touch", "up");

                    if (isCardBeyondLeftBoundary(view)) {
                        RxBus.getInstance().send(new TopCardMovedEvent(-(screenWidth)));
                        cardClickListener.onNotClick(topCard.dbId);
                        dismissCard(view, -(screenWidth ), 0);

                    } else if (isCardBeyondRightBoundary(view)) {
                        view.setFocusable(false);
                        RxBus.getInstance().send(new TopCardMovedEvent(screenWidth));
                        cardClickListener.onHotClick(topCard.dbId);
                        dismissCard(view, (screenWidth ), 0);

                    } else if (isCardBeyondTopBoundary(view)) {
                        if (LocalPreferenceManager.getInstance().getLongPreference("superhotEnabled",                                   0) < System.currentTimeMillis()) {////////////////////////////////////////////////////////////////////////////
                            RxBus.getInstance().send(new TopCardMovedEvent(-screenHeight));
                            cardClickListener.onSuperHotClick(topCard.dbId);
                            dismissCard(view, 0, -(screenHeight ));//////////////////////////////////////////////////////////////////////
                        } else {
                            RxBus.getInstance().send(new TopCardMovedEvent(0));
                            resetCard(view);
                            cardClickListener.showSuperHotDialog(topCard.dbId);
                        }
                    } else {
                        RxBus.getInstance().send(new TopCardMovedEvent(0));
                        resetCard(view);
                    }
                    return true;
                case MotionEvent.ACTION_MOVE:
                    Log.e("touch", "move");

                    newX = motionEvent.getX();
                    newY = motionEvent.getY();

                    dX = newX - oldX;
                    dY = newY - oldY;

                    float posX = view.getX() + dX;
                    float posY = view.getY() + dY;

                    RxBus.getInstance().send(new TopCardMovedEvent(posX));

                    // Set new position
                    view.setX(view.getX() + dX);
                    view.setY(view.getY() + dY);

//                   setCardRotation(view, view.getX());

                    updateAlphaOfBadges(posX, posY);
                    return true;

                case M:
                default:


                    return super.onTouchEvent(motionEvent);
            }
        }
        return super.onTouchEvent(motionEvent);
    }
    // endregion

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setOnTouchListener(null);
    }

    // region Helper Methods
    private void init(final Activity context, AttributeSet attrs) {
        if (!isInEditMode()) {
            cardClickListener = (SetOnCardClickListener) context;

            final TinderCardView view = (TinderCardView) inflate(context, R.layout.tinder_card, this);
            gestureDetector = new GestureDetector(context, new SingleTapConfirm());
            backBtn = (LinearLayout) findViewById(R.id.home_bottom_back_layout);
            hotBtn = (LinearLayout) findViewById(R.id.home_bottom_hot_layout);
            superHotBtn = (LinearLayout) findViewById(R.id.home_bottom_super_hot_layout);
            notBtn = (LinearLayout) findViewById(R.id.home_bottom_not_layout);
            completeCard = (RelativeLayout) findViewById(R.id.complete_card);

            imageView = (ImageView) findViewById(R.id.iv);
            displayNameTextView = (TextView) findViewById(R.id.display_name_tv);
            usernameTextView = (TextView) findViewById(R.id.username_tv);
            likeTextView = (ImageView) findViewById(R.id.hot_banner);
            nopeTextView = (ImageView) findViewById(R.id.not_banner);
            superHotView = (ImageView) findViewById(R.id.superhot_banner);
            //==


            ///=======
            likeTextView.setRotation(-(BADGE_ROTATION_DEGREES));
            nopeTextView.setRotation(BADGE_ROTATION_DEGREES);
            superHotView.setRotation(0);

            screenWidth = DisplayUtility.getScreenWidth(context);
            leftBoundary = screenWidth * (0.1f / 1.0f); // Left 1.5/6 of screen
            rightBoundary = screenWidth * (0.1f / 1.0f); // Right 1.5/6 of screen(Right side)
            /////////////////////////////////////////////////////////

            screenHeight = DisplayUtility.getScreenHeight(context);
            topBoundary = screenHeight * (0.1f / 1.0f);//Top 1.5/6 of screen

            ////////////////////////////////////////////////////////

       /*     completeCard.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    cardClickListener.onCardClicked(TinderCardView.this.getdbId());
                }
            });*/
            padding = DisplayUtility.dp2px(context, 16);

            setOnTouchListener(this);
            backBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //View view = backBtn.getFocusedChild();

                    TinderStackLayout tinderStackLayout = ((TinderStackLayout) v.getParent().getParent().getParent().getParent().getParent().getParent().getParent());

                    if (!btnClicked && tinderStackLayout.getChildCount() >= 1) {
                        btnClicked = true;
                        //backBtn.getChildAt(0).callOnClick();

                        cardClickListener.onBackClick(view.dbId);

                        tinderStackLayout.removeViewAt(0);
                        tinderStackLayout.bringChildToFront(tinderStackLayout.getChildAt(tinderStackLayout.getChildCount() - 1));
                        btnClicked = false;
                    }
                }
            });
            hotBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!btnClicked) {
                        btnClicked = true;
                        hotBtn.getChildAt(0).callOnClick();
                        cardClickListener.onHotClick(view.dbId);
                        dismissCard((TinderCardView) v.getParent().getParent().getParent().getParent().getParent().getParent(), (screenWidth * 2), 0, 150);
                        likeTextView.animate().alpha(1).setDuration(50);
                    }
                }
            });
            superHotBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!btnClicked && LocalPreferenceManager.getInstance().getLongPreference("superhotEnabled", 0) < System.currentTimeMillis()) {
                        btnClicked = true;
                        superHotBtn.getChildAt(0).callOnClick();
                        cardClickListener.onSuperHotClick(view.dbId);
                        dismissCard((TinderCardView) v.getParent().getParent().getParent().getParent().getParent().getParent(), 0, -(screenHeight * 2), 150);
                        superHotView.animate().alpha(1).setDuration(50);
                    }else {
                        cardClickListener.showSuperHotDialog(view.dbId);
                    }
                }
            });
            notBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!btnClicked) {
                        btnClicked = true;
                        notBtn.getChildAt(0).callOnClick();
                        cardClickListener.onNotClick(view.dbId);
                        dismissCard((TinderCardView) v.getParent().getParent().getParent().getParent().getParent().getParent(), -(screenWidth * 2), 0, 150);
                        nopeTextView.animate().alpha(1).setDuration(50);
                    }
                }
            });
        }
    }

    // Check if card's middle is beyond the left boundary
    private boolean isCardBeyondLeftBoundary(View view) {
        int width = (view.getWidth() / 2);
        float vgx = view.getX();
        Log.e("BOUNDRY",width + " " + vgx + " " + leftBoundary);
        return ((vgx + leftBoundary) <= 0);
        //return ((vgx + width) < leftBoundary);
    }

    // Check if card's middle is beyond the right boundary
    private boolean isCardBeyondRightBoundary(View view) {
        int width = (view.getWidth() / 2);
        float vgx = view.getX();
        Log.e("BOUNDRY",width + " " + vgx + " " + rightBoundary);

        return ((vgx - rightBoundary) >= 0);
        //return (view.getX() + (view.getWidth() / 2) > rightBoundary);
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Check if card's middle is beyond the top boundary
    private boolean isCardBeyondTopBoundary(View view) {
        float vgx = view.getY();
        return  ((vgx + topBoundary) <= 0);
        //return (view.getY() + (view.getHeight() / 2) < topBoundary);
    }
////////////////////////////////////////////////////////////////////////////////////////

    public void dismissCard(final View view, int xPos, int yPos) {
        dismissCard(view, xPos, yPos, 0);
    }

    public void dismissCard(final View view, int xPos, int yPos, long delay) {
        Log.e("sachin", "dismiss: " + this.toString() );

     /*   Animation animation1 =
                AnimationUtils.loadAnimation(context,
                        R.anim.slide_out_left);
        view.startAnimation(animation1);
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                ViewGroup viewGroup = (ViewGroup) view.getParent();
                if(viewGroup != null) {
                    viewGroup.removeView(TinderCardView.this);

                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });*/

        var  = this.animate()
                .x(xPos)
                .y(yPos)
                .setInterpolator(new DecelerateInterpolator())
                .setDuration(DURATION).setStartDelay(delay)
                .setListener(this);

    }

    private void resetCard(View view) {
        view.animate()
                .x(0)
                .y(0)
                .rotation(0)
                .setInterpolator(new OvershootInterpolator())
                .setDuration(DURATION);

        likeTextView.setAlpha(0f);
        nopeTextView.setAlpha(0f);
        superHotView.setAlpha(0f);
    }

    private synchronized void setCardRotation(View view, float posX) {
        float rotation = (CARD_ROTATION_DEGREES * (posX)) / screenWidth;
        int halfCardHeight = (view.getHeight() / 2);
        if (oldY < halfCardHeight - (2 * padding)) {
            view.setRotation(rotation);
        } else {
            view.setRotation(-rotation);
        }
    }

    // set alpha of like and nope badges
    private void updateAlphaOfBadges(float posX, float posY) {
        float alpha = (posX - padding) / (screenWidth * 0.50f);
        float alphaTop = (posY - padding) / (screenHeight * 0.5f);
        likeTextView.setAlpha(alpha);

        nopeTextView.setAlpha(-alpha);

        if (Math.abs(alpha) > Math.abs(alphaTop)) {
            likeTextView.setAlpha(alpha);
            if (alpha < (-0.15)) {
                nopeTextView.setAlpha(-alpha);
            } else {
                nopeTextView.setAlpha(0f);
            }
            superHotView.setAlpha(0f);
        } else {
            superHotView.setAlpha(-alphaTop);
            likeTextView.setAlpha(0f);
            nopeTextView.setAlpha(0f);

        }

    }

    public void bind(User user) {
        if (user == null)
            return;

       /* setUpImage(imageView, user);
        setUpDisplayName(displayNameTextView, user);
        setUpUsername(usernameTextView, user);*/
    }

    @Override
    public void onAnimationStart(Animator animation) {
        Log.e("sachin", "onAnimationStart: " + this.toString());
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        Log.e("sachin", "onAnimationEnd: " + this.toString());
        TinderCardView.this.clearAnimation();
        // var.cancel();
//  Animation animation = TinderCardView.this.getAnimation();
//                        Log.e("sachin",animation.toString());
//                        animation.cancel();
//

        ViewGroup viewGroup = (ViewGroup) this.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(TinderCardView.this);
            var.setListener(null);

        }
//                        if (viewGroup != null) {
//                            int count = viewGroup.getChildCount();
//                            if (viewGroup.getChildAt(count - 1) == view) {
//                                viewGroup.removeView(viewGroup.getChildAt(count - 1));
//                            }
//                        }
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }
    }

   /* private void setUpImage(ImageView iv, User user){
        String avatarUrl = user.getAvatarUrl();
        if(!TextUtils.isEmpty(avatarUrl)){
            Picasso.with(iv.getContext())
                    .load(avatarUrl)
                    .into(iv);
        }
    }

    private void setUpDisplayName(TextView tv, User user){
        String displayName = user.getDisplayName();
        if(!TextUtils.isEmpty(displayName)){
            tv.setText(displayName);
        }
    }

    private void setUpUsername(TextView tv, User user){
        String username = user.getUsername();
        if(!TextUtils.isEmpty(username)){
            tv.setText(username);
        }
    }
*/

    // endregion
}
