package com.etiennelawlor.tinderstack.ui;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.FrameLayout;

import com.etiennelawlor.tinderstack.bus.RxBus;
import com.etiennelawlor.tinderstack.bus.events.TopCardMovedEvent;
import com.etiennelawlor.tinderstack.interfaces.CardRequestInterface;
import com.etiennelawlor.tinderstack.interfaces.SetOnCardClickListener;
import com.etiennelawlor.tinderstack.utilities.DisplayUtility;

import java.util.ArrayList;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by etiennelawlor on 11/18/16.
 */

public class TinderStackLayout extends FrameLayout implements CardRequestInterface {

    // region Constants
    private static final int DURATION = 300;
    private final SetOnCardClickListener cardClickListener;
    // endregion

    // region Member Variables
    private PublishSubject<Integer> publishSubject = PublishSubject.create();
    private CompositeSubscription compositeSubscription;
    private int screenWidth;
    private int yMultiplier;
    public ArrayList<TinderCardView> cardArray;
    // endregion

    // region Constructors
    public TinderStackLayout(Context context) {
        super(context);
        cardClickListener = (SetOnCardClickListener) context;
        init();
    }

    public TinderStackLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        cardClickListener = (SetOnCardClickListener) context;
        init();
    }

    public TinderStackLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        cardClickListener = (SetOnCardClickListener) context;
        init();
    }
    // endregion

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        if (publishSubject != null) {
            // publishSubject.onNext(getChildCount());
        }
    }

    @Override
    public void removeView(View view) {
        boolean test = false;
        /*if(this.getChildAt(0) == view){
            test = true;
        }*/
        getCardArray().add((TinderCardView) view);
        super.removeView(view);
        cardClickListener.onViewRemoved(((TinderCardView) view).getdbId());
        if (test) {
           /* if(cardClickListener.shouldRemoveView()){
                if(TinderStackLayout.this.getChildCount() > 1) {
                    removeViewAt(TinderStackLayout.this.getChildCount() - 1);
                }
            }*/
            cardClickListener.getPreviousCard(new TinderStackLayout.GetCard() {
                @Override
                public void card(TinderCardView tinderCardView) {
                    addCard(tinderCardView, TinderStackLayout.this.getChildCount());
                }
            });
        } else {
            cardClickListener.getNextCard(new TinderStackLayout.GetCard() {
                @Override
                public void card(TinderCardView tinderCardView) {
                    addCard(tinderCardView, 0);
                }
            });
        }
    }

    @Override
    public void removeViewAt(int index) {

        cardClickListener.getPreviousCard(new TinderStackLayout.GetCard() {
            @Override
            public void card(TinderCardView tinderCardView) {
                if (cardClickListener.shouldRemoveView(((TinderCardView) TinderStackLayout.this.getChildAt(TinderStackLayout.this.getChildCount() - 1)).getdbId())) {
                    if (TinderStackLayout.this.getChildCount() > 1) {
                        getCardArray().add((TinderCardView) TinderStackLayout.this.getChildAt(TinderStackLayout.this.getChildCount() - 1));
                        removeViewInLayout(TinderStackLayout.this.getChildAt(TinderStackLayout.this.getChildCount() - 1));
                    }
                }
                addCard(tinderCardView, TinderStackLayout.this.getChildCount());
            }
        });
    }

    @Override
    public void removeViewInLayout(View view) {
        super.removeViewInLayout(view);
        cardClickListener.onViewRemoved(((TinderCardView) view).getdbId());
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        compositeSubscription.unsubscribe();
    }

    // region Helper Methods
    private void init() {
        setClipChildren(false);

        screenWidth = DisplayUtility.getScreenWidth(getContext());
        yMultiplier = DisplayUtility.dp2px(getContext(), 0); //original value was 8, prev val ??
//
        compositeSubscription = new CompositeSubscription();

        setUpRxBusSubscription();
    }

    private void setUpRxBusSubscription() {
        Subscription rxBusSubscription = RxBus.getInstance().toObserverable()
                .observeOn(AndroidSchedulers.mainThread()) // UI Thread
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object event) {
                        if (event == null) {
                            return;
                        }

                        if (event instanceof TopCardMovedEvent) {
                            float posX = ((TopCardMovedEvent) event).getPosX();

                            int childCount = getChildCount();
                            for (int i = childCount - 2; i >= 0; i--) {
                                TinderCardView tinderCardView = (TinderCardView) getChildAt(i);

                                if (tinderCardView != null) {
                                    if (Math.abs(posX) == (float) screenWidth) {
                                        float scaleValue = 1 - ((childCount - 2 - i) / 50.0f);////scale value not removed/////////////////////////////////////////

                                        tinderCardView.animate()
                                                .x(0)
                                                .y((childCount - 2 - i) * yMultiplier)
                                                .scaleX(1)//scalevalue
                                                .scaleY(1)//scalevalue
                                                .rotation(0)
                                                .setInterpolator(new AnticipateOvershootInterpolator())
                                                .setDuration(DURATION);
                                    } else {
//                                        float multiplier =  (DisplayUtility.dp2px(getContext(), 8)) / (float)screenWidth;
//                                        float dy = -(Math.abs(posX * multiplier));
//                                        tinderCard.setTranslationY(dy);
                                    }
                                }
                            }
                        }
                    }
                });

        compositeSubscription.add(rxBusSubscription);
    }

    public PublishSubject<Integer> getPublishSubject() {
        return publishSubject;
    }

    public void addCard(TinderCardView tc, int pos) {
        ViewGroup.LayoutParams layoutParams;
        layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tc.pos = pos;
        int childCount = getChildCount();
        addView(tc, pos, layoutParams);
        tc.onReuse();

        float scaleValue = 1 - (childCount / 50.0f); //scale value notremoved/////////////////////////////////////////

        tc.animate()
                .x(0)
                .y(childCount * yMultiplier)
                .scaleX(1)
                .scaleY(1)//additionaly added for all side zoom
                .setInterpolator(new AnticipateOvershootInterpolator())
                .setDuration(DURATION);
    }

    @Override
    public TinderCardView getCard(Context context) {
        if(getCardArray() != null && getCardArray().size() > 0){
            TinderCardView tinderCardView = getCardArray().get(0);
            getCardArray().remove(0);
            return tinderCardView;
        }
        return new TinderCardView((Activity) context);
    }

    public interface GetCard {

        void card(TinderCardView tinderCardView);

    }


    ArrayList<TinderCardView> getCardArray(){
        if(cardArray == null){
            cardArray = new ArrayList<>();
        }
        return cardArray;
    }
    // endregion

}
