package imagelib.codeplaylaps.com.customimglib.utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by HP on 28-Jul-17.
 */

public class UtilityClass {

    // convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        bitmap=null;
       /* byte[] b= stream.toByteArray();
        String encodedImageString = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImageString;*/
        return stream.toByteArray();
    }

    // convert from byte array to bitmap
    public static Bitmap getImage(byte[] image/*, int width, int height*/) {

        //return BitmapFactory.decodeByteArray(image, 0, image.length);
        Bitmap bmp=BitmapFactory.decodeByteArray(image, 0, image.length);
        image=null;
        return bmp;
        /*Bitmap b = BitmapFactory.decodeByteArray(image, 0, image.length);
        return Bitmap.createScaledBitmap(b, width, height, false);*/

    }

    public static long getFileSize(final File file)
    {
        if(file==null||!file.exists())
            return 0;
        if(!file.isDirectory())
            return file.length();
        final List<File> dirs=new LinkedList<File>();
        dirs.add(file);
        long result=0;
        while(!dirs.isEmpty())
        {
            final File dir=dirs.remove(0);
            if(!dir.exists())
                continue;
            final File[] listFiles=dir.listFiles();
            if(listFiles==null||listFiles.length==0)
                continue;
            for(final File child : listFiles)
            {
                result+=child.length();
                if(child.isDirectory())
                    dirs.add(child);
            }
        }
        return (result/1000000);//almost in mbs
    }
}

