package imagelib.codeplaylaps.com.customimglib;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

import imagelib.codeplaylaps.com.customimglib.handler.ServiceHandler;

/**
 * Created by HP on 31-Jul-17.
 */

public class SuperPicasso {
    private WeakReference<Context>  context;
    private ImageView view;
    private ViewProvider mViewProvider;
    //int width, height;

    public ViewProvider passContext(Context context) {
        this.context = new WeakReference<Context>(context);
        /*this.width=width;
        this.height=height;*/
        mViewProvider = new ViewProvider();
        return mViewProvider;
    }

    /*public SuperPicasso into(ImageView view) {
        this.view = view;
        return this;
    }

    public void load(String url) {
        new ServiceHandler().method2(context, url, "a",getView());
    }*/

    /*public ImageView getView() {
        if (view != null) {
            return view;
        }
        return null;
    }

    @Override
    public void load(String url) {
        new ServiceHandler().method2(context, url, "a",getView());
    }

    @Override
    public LoadImage into(ImageView view) {
        this.view=view;
        return this;
    }


    public interface ViewProvider{
        LoadImageInterface into(ImageView imgView);
    }

    public interface LoadImageInterface{
        void load(String url);
    }*/
    public class ViewProvider {
        public ImageLoader into(ImageView mView) {
            view = mView;
            return new ImageLoader();
        }


        public ImageView getView() {
            if (view != null) {
                return view;
            }
            return null;
        }
    }

    public class ImageLoader {
        public void load(final String url/*,int width,int height*/) {

            new ServiceHandler().method2(context.get(), url, "a", mViewProvider.getView()/*, width,height*/, new LastAccesInterface() {
                @Override
                public void callUpdateLastAccessTime(String url, DatabaseHelper mHelper) {
                    mHelper.updateLastAccessTime(url);
                }

                @Override
                public void setImageView(ImageView view, Bitmap bmp) {
                    view.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    view.setImageBitmap(bmp);
                    //bmp.recycle();
                    context = null; // to test
                    bmp = null;
                    view = null;//
                }

                @Override
                public void setImageViaUri(ImageView view, String path) {
                    view.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    view.setImageURI(Uri.parse(path));
                    context = null; // to test
                    view=null;//
                }
            });


        }

        public ImageLoader addPlaceHolder(int id) {
            mViewProvider.getView().setScaleType(ImageView.ScaleType.CENTER_CROP);
            mViewProvider.getView().setImageResource(id);
            return this;
        }

    }

}
