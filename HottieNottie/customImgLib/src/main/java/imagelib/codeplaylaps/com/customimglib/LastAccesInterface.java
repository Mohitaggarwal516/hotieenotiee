package imagelib.codeplaylaps.com.customimglib;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by HP on 01-Aug-17.
 */

public interface LastAccesInterface {
    public void callUpdateLastAccessTime(String url,DatabaseHelper mHelper);
    public void setImageView(ImageView view, Bitmap bmp);
    public void setImageViaUri(ImageView view,String path);
}
