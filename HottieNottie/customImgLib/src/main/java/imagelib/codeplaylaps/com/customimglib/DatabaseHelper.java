package imagelib.codeplaylaps.com.customimglib;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.util.Date;

/**
 * Created by HP on 31-Jul-17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    Context context;
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "database_name";

    // Table Names
    private static final String DB_TABLE = "table_image";

    // column names
    /*private static final String KEY_NAME = "image_name";
    private static final String KEY_IMAGE = "image_data";*/
    private static final String KEY_ID = "_id";
    private static final String KEY_IMAGE_URL = "_url";
    private static final String KEY_LAST_ACCESS_DATE = "last_access_date";
    private static final String KEY_IMAGE_DATA = "image_data";
    private static final String KEY_IMAGE_SIZE = "image_size";
    private static final String KEY_IMAGE_CATEGORY = "image_category";
    //private static final String KEY_IMAGE = "image_data";

    private static final String KEY_IMAGE_PATH = "image_path";

    private static final String SELECT = "select * from " + DB_TABLE;

    // Table create statement
    /*private static final String CREATE_TABLE_IMAGE = "CREATE TABLE " + DB_TABLE + "(" +
            KEY_ID + " INTEGER AUTO INCREMENT," + KEY_IMAGE_URL + " TEXT," +
            KEY_LAST_ACCESS_DATE + " TEXT," + KEY_IMAGE_DATA + " BLOB," + KEY_IMAGE_SIZE + " TEXT," + KEY_IMAGE_CATEGORY + " TEXT);";
   */
    //new Table
    private static final String CREATE_TABLE_IMAGE = "CREATE TABLE " + DB_TABLE + "(" +
            KEY_ID + " INTEGER AUTO INCREMENT," + KEY_IMAGE_URL + " TEXT," +KEY_IMAGE_PATH + " TEXT," +
            KEY_LAST_ACCESS_DATE + " TEXT);";


    private int NUM_OF_REC_TO_DEL = 9;
    private SQLiteDatabase dbWrite, dbRead;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating table
        db.execSQL(CREATE_TABLE_IMAGE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE);
        // create new table
        onCreate(db);
    }

    public void addEntry(String url/*, byte[] image*/, String lastAccessDate,String path/*, String imageSize, String category*/) throws SQLiteException {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_IMAGE_URL, url);
        //cv.put(KEY_IMAGE_DATA, image);
        cv.put(KEY_LAST_ACCESS_DATE, lastAccessDate);
        cv.put(KEY_IMAGE_PATH,path);
        //cv.put(KEY_IMAGE_SIZE, imageSize);
        //cv.put(KEY_IMAGE_CATEGORY, category);
        database.insert(DB_TABLE, null, cv);
    }

    public Cursor queryDb(String url) {
        SQLiteDatabase database = this.getReadableDatabase();
        //return database.rawQuery("select " + KEY_IMAGE_DATA + " from " + DB_TABLE + " where _url=" + "'" + url + "'", null);
         return database.rawQuery("select "+KEY_IMAGE_PATH+" from " + DB_TABLE+ " where _url=" + "'" + url + "'", null);
    }

    public long chechDbSize() {
        File f = context.getDatabasePath(DATABASE_NAME);
        long dbSize = f.length();
        return dbSize;
    }

    //deletes records with oldest record accessDate.(KEY_LAST_ACCESS_DATE)
    public void delRecords() {
        SQLiteDatabase database = this.getWritableDatabase();
//        SELECT datefield FROM yourtable ORDER By datefield ASC LIMIT n

        database.rawQuery("DELETE from " + DB_TABLE + " ORDER By " + KEY_LAST_ACCESS_DATE + " ASC LIMIT " + NUM_OF_REC_TO_DEL, null);

    }

    public Cursor getOldestImgPath(int limit){
        SQLiteDatabase database = this.getReadableDatabase();
        return database.rawQuery("SELECT "+KEY_IMAGE_PATH+" from "+DB_TABLE+" ORDER By "+KEY_LAST_ACCESS_DATE+" ASC LIMIT "+limit,null);
    }

    public void delByPathFromDb(String path) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DELETE from " + DB_TABLE + " where " + KEY_IMAGE_PATH+ " = '" + path+"'");
    }

    public void updateLastAccessTime(String url) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("UPDATE " + DB_TABLE + " SET " + KEY_LAST_ACCESS_DATE + " = '" + String.valueOf(new Date().getTime()) + "' WHERE " + KEY_IMAGE_URL + " = '" + url + "'");
        //Cursor c= getReadableDatabase().rawQuery("SELECT "+KEY_LAST_ACCESS_DATE+" from "+DB_TABLE,null);
        /*Cursor c=database.rawQuery("select * from " + DB_TABLE+" where ? = ?", new String[]{KEY_IMAGE_URL,url});
        int lstDate = c.getColumnIndex(KEY_LAST_ACCESS_DATE);
        if (c.moveToFirst()) {
            Log.w("DatabaseHelper : ", "Url "+c.getString(c.getColumnIndex(KEY_IMAGE_URL))+"  LSTdATE : " + c.getString(lstDate));
            //c.getColumnNames();
        }*/
    }

   /* private SQLiteDatabase getWritableDbObj(){
         if(dbWrite==null)
         {
             dbWrite= this.getWritableDatabase();
         }
         return dbWrite;
    }

    private SQLiteDatabase getReadableDbObj(){
        if(dbRead==null)
        {
            dbRead= this.getWritableDatabase();
        }
        return dbRead;
    }*/
}