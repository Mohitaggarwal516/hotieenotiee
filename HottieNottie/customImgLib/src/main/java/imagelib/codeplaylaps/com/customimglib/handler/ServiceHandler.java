package imagelib.codeplaylaps.com.customimglib.handler;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;

import imagelib.codeplaylaps.com.customimglib.DatabaseHelper;
import imagelib.codeplaylaps.com.customimglib.LastAccesInterface;
import imagelib.codeplaylaps.com.customimglib.utility.UtilityClass;

/**
 * Created by HP on 28-Jul-17.
 */

public class ServiceHandler {

    public static final String TAG = "ServiceHandler";
    public static final int SIZE_LIMIT = 40;

    final File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/HottieNottie");
    final File tempDir = new File(dir.getAbsolutePath() + "/Temp");


/*    public void methodL(Context context, final String url, final String category,ImageView view){
        initialiseDao(context);
        if(isDaoInitialised) {
            QueryBuilder<ImageTable, Integer> qb = imgTableDao.queryBuilder();
            try {
                qb.where().eq("image_url", url);
                List<ImageTable> list = imgTableDao.query(qb.prepare());
                if (list != null && list.size() > 0) {
                    *//*String result;
                    GenericRawResults<String[]> rawResults = imgTableDao.queryRaw("select image_data from ImageTable where image_url =" + url);
                    List<String[]> results = rawResults.getResults();
                    // This will select the first result (the first and maybe only row returned)
                    String[] resultArray = results.get(0);
                    //This will select the first field in the result which should be the ID
                    result = resultArray[0];*//*

                    Bitmap bmp=DbBitmapUtility.getImage(list.get(0).imageData);
                    view.setImageBitmap(bmp);
                } else {
                    //make Image Request
                    imageRequest(context, url, category,view);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }*/

    public void method2(final Context context, final String url, final String category, final ImageView view, /*int width, int height,*/ final LastAccesInterface lastAccesInterface) {
        final DatabaseHelper mHelper = new DatabaseHelper(context);
        final Cursor cursor = mHelper.queryDb(url);//gives img path
        if (cursor != null && cursor.moveToFirst()) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    lastAccesInterface.callUpdateLastAccessTime(url, mHelper);

                }
            }).start();
            lastAccesInterface.setImageViaUri(view,cursor.getString(0/*cursor.getColumnIndex("image_path")*/));
           /*
             final String path = cursor.getString(0);
            File imgFile = new File(path);
            FileInputStream inputStream = null;
            try {
                inputStream = new FileInputStream(imgFile);
                lastAccesInterface.setImageView(view,BitmapFactory.decodeStream(inputStream)*//*DbBitmapUtility.getImage(img)*//*);
            } catch (FileNotFoundException e) {
                //e.printStackTrace();
                imageRequest(context, url, "a", mHelper, view, lastAccesInterface);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {

                if (cursor != null) {
                    cursor.close();
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }*/

     //========================retrievin n convertin bitmap From db========================
            //byte[] img = cursor.getBlob(3);


            // int imgNo=PreferenceManager.getPreferences(context).getInt("imgNo",0);


            /*byte[] img=cursor.getBlob(3);
            String lstdate=cursor.getString(2);
            Log.w("last_Access_time","DATE "+lstdate+" , "+"url : "+cursor.getString(1));

            view.setScaleType(ImageView.ScaleType.CENTER_CROP);
            view.setImageBitmap(DbBitmapUtility.getImage(img));*/
    //=====================================================================================

        } else {
            imageRequest(context, url, "a", mHelper, view, lastAccesInterface);
        }
    }

    protected void imageRequest(final Context context, final String url, final String category, final DatabaseHelper mHelper, final ImageView view, final LastAccesInterface lastAccesInterface) {

        ImageRequest imgReq = null;
        //final String urlEncoded = URLEncoder.encode(url);
        //final File root=Environment.getExternalStorageDirectory();
        // final File dir=new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/ZZ");


        //final File extStorageDirectory = Environment.getExternalStorageDirectory();
        imgReq = new ImageRequest(url, new Response.Listener<Bitmap>() {
            @Override
            protected void finalize() throws Throwable {
                super.finalize();
                Log.e("called","FINALIZE SERVICE HANDLER_IMage response");
            }

            @Override
            public void onResponse(final Bitmap response) {
                Log.e("called", "resp"+response.toString());

                RequestQueue requestQueue = Volley.newRequestQueue(context);
                requestQueue.getCache().clear() ;
                Log.e("called", "    resp  "+response.toString());
                //lastAccesInterface.setImageView(view, response);
                //final int imgNo=PreferenceManager.getPreferences(context).getInt("imgNo",0);

                System.gc();
             /*   new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final File file = new File(tempDir, URLEncoder.encode(url));
                        mHelper.addEntry(url, String.valueOf(new Date().getTime()), file.getAbsolutePath()*//*, String.valueOf(imgByteArray.length), category*//*);
                       *//* Log.w(TAG," : "+ UtilityClass.getFileSize(tempDir));*//*
                        FileOutputStream outStream = null;
                        try {
                            if (dir.exists() == false) {
                                dir.mkdir();
                            }
                            if (tempDir.exists() == false) {
                                tempDir.mkdir();
                            }
                            outStream = new FileOutputStream(file);
                            response.compress(Bitmap.CompressFormat.PNG, 45, outStream);
                            //PreferenceManager.getPreferencesForEdit(context).putInt("imgNo",(imgNo+1)).commit();
                            outStream.flush();
                            //outStream.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        finally {
                            if(outStream!=null){
                                try {
                                    outStream.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }



                    }
                }).start();
*/
                /*new Thread(new Runnable() {
                    @Override
                    public void run() {
                        long fileSize = UtilityClass.getFileSize(tempDir);
                        Log.w(TAG, "FileSize: " + fileSize);
                        if (fileSize >= 10*//*SIZE_LIMIT*//*) {
                            Cursor cursor = mHelper.getOldestImgPath(5);
                            if (cursor.moveToFirst()) {
                                File toBeDel = new File(cursor.getString(0));
                                //toBeDel.getAbsoluteFile().delete();
                                if (toBeDel.exists()){
                                    Log.w(TAG, "File Exists @" + cursor.getString(0));
                                }else{
                                    Log.w(TAG, "File doesnot exist @" + cursor.getString(0));
                                }
                                if (toBeDel.delete()) {
                                    Log.w(TAG, "File Deleted (bf while loop)" + cursor.getString(0));
                                }
                                toBeDel=null;
                                mHelper.delByPathFromDb(cursor.getString(0));
                                while (cursor.moveToNext()) {
                                    File toBeDeleted = new File(cursor.getString(0));
                                    //toBeDel.getAbsoluteFile().delete();
                                    if (toBeDeleted.delete()) {
                                        Log.w(TAG, "File Deleted" + cursor.getString(0));
                                    }
                                    mHelper.delByPathFromDb(cursor.getString(0));
                                    toBeDeleted=null;
                                    //Log.w(TAG, "Cursor value : " + cursor.getString(0));
                                }
                                cursor.close();
                            }
                        }
                    }
                }).start();
*/


               /* new Thread(new Runnable() {
                    @Override
                    public void run() {
                        byte[] imgByteArray = DbBitmapUtility.getBytes(response);
                        mHelper.addEntry(url, imgByteArray, String.valueOf(new Date().getTime()), String.valueOf(imgByteArray.length), category);

                        imgByteArray=null;
                    }
                }).start();*/


            }
        }, 0, 0, ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                Log.e("Volly Error", error.toString());
            }
        }){
            @Override
            protected void finalize() throws Throwable {
                super.finalize();
                Log.e("called","FINALIZE SERVICE HANDLER_IMage Req");
            }
        };
        imgReq.setShouldCache(false);
        int socketTimeout = 10000;//10 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        imgReq.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        requestQueue.add(imgReq);

    }

   /* public void initialiseDao(Context context) {
        databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        try {
            imgTableDao = databaseHelper.getImageTableDao();
            isDaoInitialised = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }*/

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        Log.e("called","FINALIZE SERVICE HANDLER");
    }
}
